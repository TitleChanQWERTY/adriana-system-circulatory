from random import randint

import pygame

from ITEM import create_item
from config_script import createImage, WINDOWS_SIZE, sc
from SCENE import scene_config
from group_config import bossGroup, anyBullet, pistol_bullet, simple_bullet
from BULLET import bullet_enemy


class Danna(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/BOSS/Danna/Stay/1.png")
        self.image = pygame.transform.scale(self.image, (50, 87))
        self.rect = self.image.get_rect(center=(WINDOWS_SIZE[0] // 2, 105))
        self.add(bossGroup)

        self.Health: int = 50400

        self.x = WINDOWS_SIZE[0] // 2
        self.y = 100

        self.moveStep: int = 0
        self.selectMove: int = 0

        self.waitTime: int = 0
        self.timeChangeMove: int = 0

        self.Frame: int = 10

        self.countBullet: int = 0
        self.iCountBullet: int = 0
        self.timeShoot: int = 0
        self.maxShoot = randint(50, 150)

        self.createBulletMax: int = 12 + scene_config.DIFFICULTY + 1

        self.isFill: bool = True
        self.isFill2: bool = True
        self.isFill3: bool = True

        self.font1 = pygame.font.Font("ASSETS/FONT/twoweekendssans-regular.otf", 15)
        self.txtHealth = self.font1.render(str(self.Health), scene_config.AA_TEXT, (255, 255, 255))

    def createBulletPistol(self):
        if self.timeShoot >= 7:
            if self.countBullet < 8:
                self.countBullet += 1
            else:
                self.countBullet = 0
            bullet_enemy.BulletHell(self.rect.x + 32, self.rect.y + 37, "ASSETS/SPRITE/BULLET/ENEMY/2.png",
                                    self.countBullet, 35)
            self.timeShoot = 0
        else:
            self.timeShoot += 1

    def createBulletHellOne(self):
        if self.timeShoot >= self.maxShoot:
            for i in range(self.createBulletMax):
                for b in range(7):
                    bullet_enemy.BulletHell(self.rect.x + 31, self.rect.y + 37, "ASSETS/SPRITE/BULLET/ENEMY/5.png",
                                            i, 40, randint(35, 295))
            self.timeShoot = 0
            self.maxShoot = randint(140, 465)
        else:
            self.timeShoot += 1

    def createBulletHellTwo(self):
        if self.timeShoot >= self.maxShoot:
            for i in range(self.createBulletMax + 2):
                if self.iCountBullet >= 8:
                    self.iCountBullet = 0
                bullet_enemy.BulletHell(self.rect.x + 31, self.rect.y + 37, "ASSETS/SPRITE/BULLET/ENEMY/14.bmp",
                                            self.iCountBullet, 75, randint(0, 95))
                self.iCountBullet += 1
            self.timeShoot = 0
            if scene_config.DIFFICULTY == 0:
                self.maxShoot = randint(155, 285)
            if scene_config.DIFFICULTY == 1:
                self.maxShoot = randint(135, 215)
            if scene_config.DIFFICULTY == 2:
                self.maxShoot = randint(125, 205)
        else:
            self.timeShoot += 1

    def createBulletHellThree(self):
        if self.timeShoot == 75:
            for i in range(self.createBulletMax):
                bullet_enemy.BulletHell(self.rect.x + 31, self.rect.y + 37, "ASSETS/SPRITE/BULLET/ENEMY/16.bmp",
                                            i, 50, 0, 55)
                bullet_enemy.BulletHell(self.rect.x + 75, self.rect.y + 37, "ASSETS/SPRITE/BULLET/ENEMY/16.bmp",
                                            i, 50, 0, 55)
                bullet_enemy.BulletHell(self.rect.x - 20, self.rect.y + 37, "ASSETS/SPRITE/BULLET/ENEMY/16.bmp",
                                            i, 50, 0, 55)
        if self.timeShoot <= 55:
            bullet_enemy.BulletHell(self.rect.x + 32, self.rect.y + 37, "ASSETS/SPRITE/BULLET/ENEMY/15.bmp",
                                    self.countBullet, 75, 0, 45)
            if self.countBullet < 8:
                self.countBullet += 1
            else:
                self.countBullet = 0
            if scene_config.DIFFICULTY == 0:
                self.maxShoot = randint(155, 285)
            if scene_config.DIFFICULTY == 1:
                self.maxShoot = randint(135, 215)
            if scene_config.DIFFICULTY == 2:
                self.maxShoot = randint(125, 205)
        else:
            if self.timeShoot > 165:
                self.timeShoot = 0
        self.timeShoot += 1

    def checkHealth(self):
        if self.Health < 40500:
            if self.isFill2:
                scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/white.png", 1)
                for e_b in anyBullet:
                    e_b.kill()
                for e_b in pistol_bullet:
                    e_b.kill()
                for e_b in simple_bullet:
                    e_b.kill()
                for i in range(4):
                    create_item.create_ammo(self.rect.centerx, self.rect.centery)
                self.isFill2 = False
            self.selectMove = 1
        if self.Health < 30550:
            if self.isFill:
                scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/white.png", 3)
                for e_b in anyBullet:
                    e_b.kill()
                for e_b in pistol_bullet:
                    e_b.kill()
                for e_b in simple_bullet:
                    e_b.kill()
                self.maxShoot = randint(125, 205)
                for i in range(4):
                    create_item.create_ammo(self.rect.centerx, self.rect.centery)
                self.isFill = False
            self.selectMove = 2
        if self.Health < 21500:
            if self.isFill3:
                scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/white.png", 3)
                for e_b in anyBullet:
                    e_b.kill()
                for e_b in pistol_bullet:
                    e_b.kill()
                for e_b in simple_bullet:
                    e_b.kill()
                self.maxShoot = randint(125, 205)
                for i in range(5):
                    create_item.create_ammo(self.rect.centerx, self.rect.centery)
                self.isFill3 = False
            self.selectMove = 3
        if self.Health < 9000:
            self.selectMove = 1

    def animation(self):
        if self.selectMove == 2 or 3:
            match self.Frame:
                case 45:
                    self.y -= 3
                    self.image = createImage("ASSETS/SPRITE/BOSS/Danna/Stay/1.png")
                    self.image = pygame.transform.scale(self.image, (50, 87))
                    self.Frame = 0
                case 25:
                    self.y += 3
                    self.image = createImage("ASSETS/SPRITE/BOSS/Danna/Stay/2.png")
                    self.image = pygame.transform.scale(self.image, (60, 87))
                    self.Frame += 1
                case _:
                    self.Frame += 1

    def update(self):
        self.animation()
        self.checkHealth()
        self.move()
        self.txtHealth = self.font1.render(str(self.Health), scene_config.AA_TEXT, (255, 255, 255))
        sc.blit(self.txtHealth, self.txtHealth.get_rect(center=(self.rect.x + 25, self.rect.y - 22)))

    def move(self):
        match self.selectMove:
            case 0:
                if self.rect.x > 55 and self.moveStep == 0:
                    self.rect.x -= 2
                    self.image = createImage("ASSETS/SPRITE/BOSS/Danna/Rotate.png")
                    self.image = pygame.transform.scale(self.image, (87, 87))
                else:
                    self.moveStep = 1
                if self.rect.x < 1105 and self.moveStep == 1:
                    self.rect.x += 2
                    self.image = createImage("ASSETS/SPRITE/BOSS/Danna/Rotate.png")
                    self.image = pygame.transform.scale(self.image, (87, 87))
                    self.image = pygame.transform.flip(self.image, True, False)
                else:
                    self.moveStep = 0
                self.createBulletHellOne()
            case 1:
                if self.waitTime >= 155:
                    self.createBulletPistol()
                    match self.moveStep:
                        case 0:
                            if self.rect.x > 65:
                                self.image = createImage("ASSETS/SPRITE/BOSS/Danna/Rotate.png")
                                self.image = pygame.transform.scale(self.image, (87, 87))
                                self.rect.x -= 5
                                self.rect.y += 2
                            else:
                                self.moveStep = 1
                        case 1:
                            if self.rect.x < 1125:
                                self.image = createImage("ASSETS/SPRITE/BOSS/Danna/Rotate.png")
                                self.image = pygame.transform.scale(self.image, (87, 87))
                                self.image = pygame.transform.flip(self.image, True, False)
                                self.rect.x += 6
                                self.rect.y += 1
                            else:
                                self.moveStep = 0
                        case 2:
                            if self.rect.y > 35:
                                self.image = createImage("ASSETS/SPRITE/BOSS/Danna/Stay/1.png")
                                self.image = pygame.transform.scale(self.image, (50, 87))
                                self.rect.y -= 3
                            else:
                                self.moveStep = randint(0, 1)
                    if self.rect.y > 600:
                        self.moveStep = 2
                else:
                    self.image = createImage("ASSETS/SPRITE/BOSS/Danna/Angry.png")
                    self.image = pygame.transform.scale(self.image, (50, 87))
                    self.waitTime += 1
            case 2:
                self.createBulletHellTwo()
                self.rect = self.image.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2))
            case 3:
                if self.rect.y > 125:
                    self.rect.y -= 4
                else:
                    self.createBulletHellThree()
