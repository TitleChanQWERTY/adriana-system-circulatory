from random import randint

import pygame

from BULLET import bullet_enemy
from config_script import sc, WINDOWS_SIZE, createImage
from SCENE import scene_config
from group_config import bossGroup


class NoName(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/BOSS/NONAME/1.png")
        self.image = pygame.transform.scale(self.image, (53, 76))
        self.rect = self.image.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 50))

        self.Health: int = 50000

        self.timeShoot: int = 0
        self.timeShootMax: int = randint(95, 255)
        self.posFlyBullet: int = 0

        self.selectShoot: int = 0
        self.timeSelect: int = 0

        self.moveStep: int = randint(0, 3)
        self.timeStep: int = 0

        self.speedMove: int = randint(2, 5)

        self.font1 = pygame.font.Font("ASSETS/FONT/twoweekendssans-regular.otf", 16)
        self.txtHealth = self.font1.render(str(self.Health), scene_config.AA_TEXT, (255, 255, 255))

        self.add(bossGroup)

    def bulletFirst(self):
        if self.timeShoot >= self.timeShootMax - 10:
            for i in range(9):
                bullet_enemy.BulletHell(self.rect.x + 31, self.rect.y + 37, "ASSETS/SPRITE/BULLET/ENEMY/8.png",
                                        i, 40, 0)
            self.timeShoot = 0
        self.timeShoot += 1

    def bulletSecond(self):
        if self.timeShoot <= 45:
            bullet_enemy.BulletHell(self.rect.x + 32, self.rect.y + 37, "ASSETS/SPRITE/BULLET/ENEMY/15.bmp",
                                    self.posFlyBullet, 65, 3, 45)
            if self.posFlyBullet < 9:
                self.posFlyBullet += 1
            else:
                self.posFlyBullet = 0
        else:
            if self.timeShoot > 155:
                self.timeShoot = 0
        self.timeShoot += 1

    def bulletThree(self):
        if self.timeShoot >= 155:
            for i in range(9):
                bullet_enemy.BulletHell(self.rect.x + 32, self.rect.y + 37, "ASSETS/SPRITE/BULLET/ENEMY/16.bmp",
                                        i, 120, 0, 9)
            self.timeShoot = 0
        if self.timeShoot >= 95:
            if self.posFlyBullet < 9:
                self.posFlyBullet += 1
            else:
                self.posFlyBullet = 0
            bullet_enemy.BulletHell(self.rect.x + 32, self.rect.y + 37, "ASSETS/SPRITE/BULLET/ENEMY/14.bmp",
                                    self.posFlyBullet, 39)
        self.timeShoot += 1

    def bulletFour(self):
        if self.timeShoot > 45:
            for i in range(9):
                bullet_enemy.BulletHell(self.rect.x + 32, self.rect.y + 37, "ASSETS/SPRITE/BULLET/ENEMY/10.bmp",
                                        i, 25, randint(20, 135))
            self.timeShoot = 0
        self.timeShoot += 1

    def bulletFive(self):
        if self.timeShoot >= 115:
            for i in range(10):
                bullet_enemy.RandomBullet(self.rect.centerx, self.rect.centery, "ASSETS/SPRITE/BULLET/ENEMY/12.png", 29)
            self.timeShoot = 0
        self.timeShoot += 1

    def update(self):
        self.txtHealth = self.font1.render(str(self.Health), scene_config.AA_TEXT, (255, 255, 255))
        sc.blit(self.txtHealth, self.txtHealth.get_rect(center=(self.rect.x + 25, self.rect.y - 22)))
        match self.selectShoot:
            case 0:
                self.bulletFirst()
            case 1:
                self.bulletSecond()
            case 2:
                self.bulletThree()
            case 3:
                self.bulletFour()
            case 4:
                self.bulletFive()
            case 6:
                self.bulletSecond()
                self.bulletFour()
            case 5:
                self.bulletFirst()
                self.bulletFive()
        self.move()
        if self.timeStep > 85:
            self.speedMove = 2
            self.moveStep = randint(0, 3)
            self.timeStep = 0
        self.timeStep += 1

        if self.timeSelect >= 275:
            self.timeShootMax: int = randint(95, 150)
            self.selectShoot = randint(0, 6)
            self.timeSelect = 0
        self.timeSelect += 1

    def move(self):
        match self.moveStep:
            case 0:
                if self.rect.y > 15 and self.rect.x > 15:
                    self.image = createImage("ASSETS/SPRITE/BOSS/NONAME/2.png")
                    self.image = pygame.transform.scale(self.image, (53, 76))
                    self.image = pygame.transform.flip(self.image, True, False)
                    self.rect.y -= self.speedMove+1
                    self.rect.x -= self.speedMove
                else:
                    self.moveStep = randint(1, 2)
            case 1:
                if self.rect.y < 630 and self.rect.x < 1120:
                    self.image = createImage("ASSETS/SPRITE/BOSS/NONAME/2.png")
                    self.image = pygame.transform.scale(self.image, (53, 76))
                    self.rect.y += self.speedMove
                    self.rect.x += self.speedMove + 5
                else:
                    self.moveStep = 0
            case 2:
                if self.rect.y > 15 and self.rect.x < 1125:
                    self.image = createImage("ASSETS/SPRITE/BOSS/NONAME/2.png")
                    self.image = pygame.transform.scale(self.image, (53, 76))
                    self.rect.y -= self.speedMove+1
                    self.rect.x += self.speedMove + 5
                else:
                    self.moveStep = 3
            case 3:
                if self.rect.y < 630 and self.rect.x > 25:
                    self.image = createImage("ASSETS/SPRITE/BOSS/NONAME/2.png")
                    self.image = pygame.transform.scale(self.image, (53, 76))
                    self.image = pygame.transform.flip(self.image, True, False)
                    self.rect.y += self.speedMove
                    self.rect.x -= self.speedMove
                else:
                    self.moveStep = randint(0, 2)
