from random import randint

import pygame.transform

import group_config
from NOTFICATION import notfication_manager
from config_script import *
from SCENE import scene_config, scene_game, scene_game_score, scene_novel, scene_select_music, scene_menu, \
    scene_boss_fight, scene_tutorial
from font_config import LoadingFont, skillFont
from ENEMY import create_enemy
from take_screenshot import take


def loading():

    for ball_rosa in group_config.ball_rosa_group:
        ball_rosa.kill()
    for shota in group_config.shota_group:
        shota.kill()
    for shutle in group_config.shutle_group:
        shutle.kill()
    for soulBall in group_config.ball_soul:
        soulBall.kill()
    for magical in group_config.magical_girl_group:
        magical.kill()
    for bullet_sim in group_config.simple_bullet:
        bullet_sim.kill()
    for p_b in group_config.player_bullet:
        p_b.kill()
    for ab in group_config.anyBullet:
        ab.kill()
    for pistol in group_config.pistol_bullet:
        pistol.kill()
    for crystal in group_config.item_group:
        crystal.kill()
    for eg in group_config.effect_group:
        eg.kill()
    for bomb in group_config.bomb_player:
        bomb.kill()

    FPS: int = 60

    pygame.mixer.music.set_volume(0.08)

    TEXT_SET = ("", "", "")

    match LANGUAGE_SET:
        case "UA":
            TEXT_SET = (TEXT_LOADING_UA[0], TEXT_LOADING_UA[1], TEXT_LOADING_UA[2])
        case "EN":
            TEXT_SET = (TEXT_LOADING_EN[0], TEXT_LOADING_EN[1], TEXT_LOADING_EN[2])

    selectLoading: int = randint(0, 2)

    LoadingText = LoadingFont.render(TEXT_SET[0], scene_config.AA_TEXT, (255, 255, 255))

    match selectLoading:
        case 1:
            LoadingText = LoadingFont.render(TEXT_SET[1], scene_config.AA_TEXT, (255, 255, 255))
        case 2:
            LoadingText = LoadingFont.render(TEXT_SET[2], scene_config.AA_TEXT, (255, 255, 255))
    LoadingTextRect = LoadingText.get_rect(center=(980, 655))

    LoadingIcon = createImage("ASSETS/SPRITE/UI/LOADING/4.png")
    LoadingIcon = pygame.transform.scale(LoadingIcon, (63, 63))

    angleImage = 0
    angleSpeed = randint(1, 4)

    ImageLoad = createImage("ASSETS/SPRITE/UI/LOADING/2.png")
    ImageLoad = pygame.transform.scale(ImageLoad, (328, 328))
    match selectLoading:
        case 1:
            ImageLoad = createImage("ASSETS/SPRITE/UI/LOADING/1.png")
            ImageLoad = pygame.transform.scale(ImageLoad, (328, 328))
        case 2:
            ImageLoad = createImage("ASSETS/SPRITE/UI/LOADING/6.png")
            ImageLoad = pygame.transform.scale(ImageLoad, (328, 328))
    ImageLoadRect = ImageLoad.get_rect(center=(640, 545))

    timeWait: int = 0

    match selectLoading:
        case 1:
            LoadingIcon = createImage("ASSETS/SPRITE/UI/LOADING/5.png")
            LoadingIcon = pygame.transform.scale(LoadingIcon, (69, 69))
        case 2:
            LoadingIcon = createImage("ASSETS/SPRITE/UI/LOADING/7.png")
            LoadingIcon = pygame.transform.scale(LoadingIcon, (69, 69))
    LoadingIconRect = LoadingIcon.get_rect(center=(1150, 650))

    for b in group_config.back_effect_group:
        scene_game.createEffect = 0
        scene_game_score.createEffect = 0
        b.kill()

    TXTTips = ["Утримуйте Клавішу K Щоб Стріляти", "Утримуйте Клавіши K + L або K + J щоб стріляти у бока",
               "Підберайте Сині Кулі Щоб Підвищувати Показник ЕКСП", "Підберайте Жовті Кулі Щоб Підвищувати Кількість Набоїв",
               "Натисніть I щоб випустити бомбу"]

    if LANGUAGE_SET == "EN":
        TXTTips = ["Hold K Or R1 To Shoot", "Hold K + L or K + J to shoot sideways",
                   "Pick up Blue Orbs to Increase EXP", "Pick up Yellow Orbs to Increase the Number Bullet",
                   "Press I to shoot bomb"]

    tips = skillFont.render(TXTTips[randint(0, len(TXTTips)-1)], scene_config.AA_TEXT, (255, 255, 255))

    if scene_game.LEVEL == 9:
        pygame.mixer.music.stop()

    while 1:
        for e in pygame.event.get():
            if e.type == pygame.KEYDOWN and e.key == pygame.K_F6:
                take()
        match timeWait:
            case 45:
                if scene_config.GameMode < 1:
                    match scene_config.isMoveBackEffect:
                        case True:
                            if scene_game.LEVEL < 8 or scene_game.LEVEL > 14:
                                for i in range(scene_config.TimeCountLevelEffect * 2):
                                    scene_game.createEffectLevel()
                                    scene_game.group_config.back_effect_group.update()
                            else:
                                if scene_game.LEVEL == 8:
                                    for i in range(scene_config.TimeCountLevelEffect * 7):
                                        scene_game.createShowEffect()
                                        scene_game.group_config.back_effect_group.update()
                                else:
                                    for i in range(scene_config.TimeCountLevelEffect * 2):
                                        scene_game.createCitadelEffect()
                        case False:
                            if scene_game.LEVEL < 8 or scene_game.LEVEL > 14:
                                for i in range(scene_config.TimeCountLevelEffect * 2):
                                    scene_game.createEffectLevel()
                                    scene_game.group_config.back_effect_group.update()
                            else:
                                if scene_game.LEVEL == 8:
                                    for i in range(scene_config.TimeCountLevelEffect * 6):
                                        scene_game.createShowEffect()
                                        scene_game.group_config.back_effect_group.update()
                                else:
                                    for i in range(scene_config.TimeCountLevelEffect * 2):
                                        scene_game.createCitadelEffect()
                                        scene_game.group_config.back_effect_group.update()
                else:
                    match scene_config.isMoveBackEffect:
                        case True:
                            if scene_config.SELECT_BACK == 0:
                                for i in range(scene_config.TimeCountLevelEffect * 2):
                                    scene_game.createEffectLevel()
                                    scene_game.group_config.back_effect_group.update()
                            else:
                                if scene_config.SELECT_BACK == 1:
                                    for i in range(scene_config.TimeCountLevelEffect * 6):
                                        scene_game.createShowEffect()
                                        scene_game.group_config.back_effect_group.update()
                                else:
                                    for i in range(scene_config.TimeCountLevelEffect * 2):
                                        scene_game.createCitadelEffect()
                        case False:
                            if scene_config.SELECT_BACK == 0:
                                for i in range(scene_config.TimeCountLevelEffect * 2):
                                    scene_game.createEffectLevel()
                                    scene_game.group_config.back_effect_group.update()
                            else:
                                if scene_config.SELECT_BACK == 1:
                                    for i in range(scene_config.TimeCountLevelEffect * 6):
                                        scene_game.createShowEffect()
                                        scene_game.group_config.back_effect_group.update()
                                else:
                                    for i in range(scene_config.TimeCountLevelEffect * 2):
                                        scene_game.createCitadelEffect()
                                        scene_game.group_config.back_effect_group.update()
                timeWait += 1
            case 80:
                if scene_game.LEVEL < 8 or scene_game.LEVEL > 14 and scene_game.LEVEL != 18:
                    for i in range(9):
                        create_enemy.createFly()
                    for i in range(45):
                        create_enemy.createBall()
                    for i in range(randint(2, 3)):
                        create_enemy.createBall(1)
                    if scene_game.LEVEL >= 1 or scene_config.GameMode == 1 or scene_config.GameMode == 2:
                        for i in range(20):
                            create_enemy.createShota()
                    if scene_game.LEVEL >= 2 or scene_config.GameMode == 1 or scene_config.GameMode == 2:
                        for i in range(28):
                            create_enemy.createShutlePatrol()
                            create_enemy.createBallSoul()
                    if scene_game.LEVEL >= 3 or scene_config.GameMode == 1 or scene_config.GameMode == 2:
                        for i in range(20):
                            create_enemy.createMagicalGirl()
                    if scene_game.LEVEL >= 4 or scene_config.GameMode == 1 or scene_config.GameMode == 2:
                        for i in range(15):
                            create_enemy.createShutleItem()
                        for i in range(16):
                            create_enemy.createShutleRocket()
                    if scene_game.LEVEL >= 5 or scene_config.GameMode == 1 or scene_config.GameMode == 2:
                        for i in range(20):
                            create_enemy.createEva()
                            create_enemy.createCardGirl()
                    if scene_game.LEVEL >= 6 or scene_config.GameMode == 1 or scene_config.GameMode == 2:
                        for i in range(14):
                            create_enemy.createWitcher()
                else:
                    if scene_game.LEVEL == 8:
                        for i in range(15):
                            create_enemy.createBall()
                    else:
                        if scene_game.LEVEL > 8 and scene_game.LEVEL < 16 and scene_game.LEVEL != 14:
                            for i in range(30):
                                create_enemy.createMaid()
                                create_enemy.createProtector()
                            for i in range(36):
                                create_enemy.createDrone()
                            for i in range(35):
                                create_enemy.createBall()
                            for i in range(randint(2, 3)):
                                create_enemy.createBall(1)
                timeWait += 1
            case 105:
                pygame.mixer.music.set_volume(0.2)
                match scene_config.GameMode:
                    case 0:
                        if scene_game.LEVEL <= 0:
                            scene_config.switch_scene(scene_novel.sceneStart)
                        else:
                            if scene_game.LEVEL == 14:
                                pygame.mixer.music.load("ASSETS/OST/Tak Tak More More!.wav")
                                pygame.mixer.music.play(-1)
                                pygame.mixer.music.set_volume(0.2)
                                scene_config.switch_scene(scene_boss_fight.scene0)
                            elif scene_game.LEVEL >= 18:
                                pygame.mixer.music.load("ASSETS/OST/Final F Boss Man.wav")
                                pygame.mixer.music.play(-1)
                                pygame.mixer.music.set_volume(0.2)
                                scene_config.switch_scene(scene_boss_fight.scene0)
                            else:
                                if scene_game.LEVEL == 8:
                                    pygame.mixer.music.load("ASSETS/OST/Die City Citadel.wav")
                                    pygame.mixer.music.play(-1)
                                    pygame.mixer.music.set_volume(0.04)
                                if scene_game.LEVEL == 9:
                                    pygame.mixer.music.load("ASSETS/OST/Yak Use Garno Ta Svitlo!.wav")
                                    pygame.mixer.music.play(-1)
                                    pygame.mixer.music.set_volume(0.2)
                                scene_config.switch_scene(scene_game.scene0)
                    case 1:
                        match scene_config.DIFFICULTY:
                            case 0:
                                pygame.mixer.music.load("ASSETS/OST/Heart War.wav")
                                pygame.mixer.music.play(-1)
                                pygame.mixer.music.set_volume(0.3)
                            case 1:
                                pygame.mixer.music.load("ASSETS/OST/Chomu UseTak Pracuye_.wav")
                                pygame.mixer.music.play(-1)
                                pygame.mixer.music.set_volume(0.5)
                            case 2:
                                pygame.mixer.music.load("ASSETS/OST/Ya vbyla svogo hlopca iz-za... nei.wav")
                                pygame.mixer.music.play(-1)
                                pygame.mixer.music.set_volume(0.5)
                        scene_config.switch_scene(scene_game_score.scene0)
                    case 2:
                        try:
                            pygame.mixer.music.load(str(scene_select_music.musicName))
                        except pygame.error:
                            if LANGUAGE_SET == "EN":
                                notfication_manager.notfication_create("ASSETS/SPRITE/UI/Notication/3.png", "Break File")
                            else:
                                notfication_manager.notfication_create("ASSETS/SPRITE/UI/Notication/3.png", "Файл Пошкоджено")
                            scene_config.switch_scene(scene_menu.scene_menu)
                            return 0
                        pygame.mixer.music.play()
                        pygame.mixer.music.set_volume(0.3)
                        scene_config.switch_scene(scene_game_score.scene0)
                    case 3:
                        pygame.mixer.music.load("ASSETS/OST/Tutor Man!.wav")
                        pygame.mixer.music.play(-1)
                        pygame.mixer.music.set_volume(0.1)
                        scene_config.switch_scene(scene_tutorial.tips_one)
                return 0
            case _:
                timeWait += 1

        match selectLoading:
            case 2:
                LoadingIcon = createImage("ASSETS/SPRITE/UI/LOADING/7.png")
                LoadingIcon = pygame.transform.scale(LoadingIcon, (69, 69))
                LoadingIconRect = LoadingIcon.get_rect(center=(1185, 645))
                if LANGUAGE_SET == "EN":
                    LoadingIconRect = LoadingIcon.get_rect(center=(1130, 650))
            case 1:
                LoadingIcon = createImage("ASSETS/SPRITE/UI/LOADING/5.png")
                LoadingIcon = pygame.transform.scale(LoadingIcon, (69, 69))
                LoadingIconRect = LoadingIcon.get_rect(center=(1155, 650))
            case 0:
                LoadingIcon = createImage("ASSETS/SPRITE/UI/LOADING/4.png")
                LoadingIcon = pygame.transform.scale(LoadingIcon, (63, 63))
                LoadingIconRect = LoadingIcon.get_rect(center=(1130, 650))
        angleImage += angleSpeed
        LoadingIcon = pygame.transform.rotate(LoadingIcon, angleImage)

        sc.fill((0, 0, 0))
        sc.blit(LoadingText, LoadingTextRect)
        sc.blit(LoadingIcon, LoadingIconRect)
        sc.blit(tips, tips.get_rect(center=(WINDOWS_SIZE[0]//2, 50)))
        sc.blit(ImageLoad, ImageLoadRect)
        pygame.display.update()
        clock.tick(FPS)
