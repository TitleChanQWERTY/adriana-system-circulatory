from random import randint

from SCENE import scene_config, scene_select_profile
from config_script import *

iconGame = createImage("ASSETS/SPRITE/ICON/png/1.png")

pygame.display.set_icon(iconGame)

Titles = ("Also Try Erra: Exordium", "Hell is Real", "Also Try Deputinization", "I Love My Girlfriend",
          "My Heart Is Broken", "Please buy the game", "We Stand by Ukraine", "Putin is DIE!", "How Do You Do",
          "Я хочу тебе!", "Спробуй Нижню Аттаку",
          "Хах ти такий класний", "Западенець?", "Горить Москва!", "Також Спробуйте Ігровари",
          "Хей! У ТЕБЕ ВСЕ ВИЙДЕ!", "Вона не винна!",
          "Fup!", "Dont Make Rule 34!", "І.м Тараса Григоровича Шевченка", "Мене Звати Богдан",
          "FUCK FUCK FUCK!",
          "В усьому винна росія!", "Also try ... i dont know what you can try!", "Завітайте до Ігроварів",
          "тут був пупсік kemgoblin", "хм", "Doom's Like Game...", "Колись, мою гру хтось купить...",
          "Мене Ґвалтувала Дівчина", "Повністю Вільна Гра!!!", "Я Завжди Повертаюсь", "I'm always comeback", "Good Card"
          , "Oh You Come Back", "Available on itch.io!", "Так так так", "Yes Yes Yes", "Also Try LIFEROOT.bat",
          "Наступна частина вийде восені!", "For You And Your Wife")

pygame.display.set_caption(TITLE + Titles[randint(0, len(Titles) - 1)])
pygame.mouse.set_visible(False)

if isFullScreen:
    sc = pygame.display.set_mode(WINDOWS_SIZE,
                                 pygame.FULLSCREEN, pygame.HWSURFACE | pygame.OPENGL | pygame.DOUBLEBUF, vsync=1)

scene_config.switch_scene(scene_select_profile.scene)
while scene_config.select_scene is not None:
    var = scene_config.select_scene()

pygame.quit()
