import config_script
from SCENE import scene_config
import ad_system


def change():
    config_script.config.sections()
    config_script.config["DEFAULT"] = {"language": scene_config.LANGUAGE_SET,
                                       "fullscreen": str(scene_config.isFullScreen),
                                       "aa_text": str(scene_config.AA_TEXT),
                                       "show_fps": str(scene_config.FPS_SHOW)}
    config_script.config["OTHER"] = {"inertia": str(scene_config.p.isInertia),
                                     "particle": str(scene_config.isParticle),
                                     "moveBackEffect": str(scene_config.isMoveBackEffect),
                                     "countbackeffect": str(scene_config.TimeCountLevelEffect),
                                     "showposindicator": str(scene_config.isShowPos),
                                     "rotateobject": str(scene_config.isRotateObject),
                                     "ad_animation": str(ad_system.isAnimAD),
                                     "trash_count": str(scene_config.TRASH_COUNT),
                                     "ad_gameplay": str(scene_config.isCreateAD),
                                     "light_effect": str(scene_config.isLight)}
    with open("CONFIG.ini", "w") as configfile:
        config_script.config.write(configfile)
