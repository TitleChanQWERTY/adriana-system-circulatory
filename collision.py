from pygame import mixer

import effect_system
import group_config
from random import randint
from SCENE import scene_config
from ITEM import create_item
from effect_system import effect_anim, effect_bodies, effect_take
from config_script import WINDOWS_SIZE
from sfx_compilation import TAKE_ITEM_EXP, DEATH_BALL, TAKE_ITEM_AMMO, TAKE_ITEM_CRYSTAL, TELEPORT, \
    DAMAGE_SHUTLE, DEATH_PEOPLE
import particle
import itertools

timeDamage: int = 0

fileEffectOne = ("ASSETS/SPRITE/EFFECT/1/1.png", "ASSETS/SPRITE/EFFECT/1/2.png", "ASSETS/SPRITE/EFFECT/1/3.png",
                 "ASSETS"
                 "/SPRITE/EFFECT/1/4.png",
                 "ASSETS/SPRITE/EFFECT/1/5.png", "ASSETS/SPRITE/EFFECT/1/6.png")

fileEffectTwo = ("ASSETS/SPRITE/EFFECT/3/1.png", "ASSETS/SPRITE/EFFECT/3/2.png", "ASSETS/SPRITE/EFFECT/3/3.png",
                 "ASSETS"
                 "/SPRITE/EFFECT/3/4.png",
                 "ASSETS/SPRITE/EFFECT/3/5.png", "ASSETS/SPRITE/EFFECT/3/6.png")

fileEffectThree = ("ASSETS/SPRITE/EFFECT/4/1.png", "ASSETS/SPRITE/EFFECT/4/2.png", "ASSETS/SPRITE/EFFECT/4/3.png")

fileEffectFour = (
    "ASSETS/SPRITE/EFFECT/Boom/1.png", "ASSETS/SPRITE/EFFECT/Boom/2.png", "ASSETS/SPRITE/EFFECT/Boom/3.png",
    "ASSETS/SPRITE/EFFECT/Boom/4.png")

fileEffectFive = (
    "ASSETS/SPRITE/EFFECT/BoomNeon/1.png", "ASSETS/SPRITE/EFFECT/BoomNeon/2.png", "ASSETS/SPRITE/EFFECT/BoomNeon/3.png",
    "ASSETS/SPRITE/EFFECT/BoomNeon/4.png")

fileEffectSix = (
    "ASSETS/SPRITE/EFFECT/pickUp/1.png", "ASSETS/SPRITE/EFFECT/pickUp/2.png", "ASSETS/SPRITE/EFFECT/pickUp/3.png",
    "ASSETS/SPRITE/EFFECT/pickUp/4.png", "ASSETS/SPRITE/EFFECT/pickUp/5.png", "ASSETS/SPRITE/EFFECT/pickUp/6.png")

bodies = ("ASSETS/SPRITE/EFFECT/bodies/1.bmp", "ASSETS/SPRITE/EFFECT/bodies/2.bmp", "ASSETS/SPRITE/EFFECT/bodies/3.bmp")

fileMetal = ("ASSETS/SPRITE/EFFECT/6.bmp", "ASSETS/SPRITE/EFFECT/10.bmp", "ASSETS/SPRITE/EFFECT/11.bmp",
             "ASSETS/SPRITE/EFFECT/15.bmp")


def createEffectSimple(x, y, fileEffect, effect=6, size=55, type_effect=0):
    return effect_anim(x + 23, y + 25, fileEffect, effect, size, type_effect)


def createEffectBodies(x, y, filename, size=24, isMetal=False):
    return effect_bodies(x, y, filename, size, isMetal)


def createEffectTake(x, y, filename, size=(16, 16)):
    return effect_take(x, y, filename, size)


def createParticle(x, y, pos=0, filename="ASSETS/SPRITE/EFFECT/2/1.png", isAnim=True, size=26):
    particle.particle_simple(x + 24, y + 25, pos, filename, isAnim, size)


isKillBullet: bool = True


def collisionItem():
    for item in group_config.item_group:
        if scene_config.p.rect.collidepoint(item.rect.center):
            match item.idItem:
                case 3:
                    TELEPORT.play()
                    createEffectSimple(item.rect.x, item.rect.y, fileEffectSix, 6, 31)
                    ScoreSet = 35 + scene_config.p.MultipleScore
                    scene_config.Score += ScoreSet
                    create_item.createTXT(item.rect.x + 15, item.rect.y + 15, "+ " + str(ScoreSet) + " SCORE",
                                          (255, 255, 0))
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                    scene_config.p.rect.x = scene_config.p.centerX = randint(150, 965)
                    scene_config.p.rect.y = scene_config.p.centerY = randint(150, 620)
                    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/white_teleport.png")
                    if scene_config.isParticle:
                        for i in range(2):
                            createParticle(item.rect.x, item.rect.y, i, "ASSETS/SPRITE/EFFECT/7.bmp", False, 24)
                    item.kill()
        if scene_config.p.rect.colliderect(item.rect):
            match item.idItem:
                case 0:
                    TAKE_ITEM_EXP.play()
                    effect_system.EffectSize(item.rect.centerx, item.rect.centery, "ASSETS/SPRITE/EFFECT/pickUp/7.png",
                                             0, 10)
                    create_item.createTXT(item.rect.x + 15, item.rect.y + 15, "+10 EXP")
                    scene_config.p.EXP += 10
                    scene_config.p.check_exp()
                    ScoreSet = 25 + scene_config.p.MultipleScore
                    scene_config.Score += ScoreSet
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                    if scene_config.isParticle:
                        for i in range(2):
                            createParticle(item.rect.x, item.rect.y, i, "ASSETS/SPRITE/EFFECT/7.bmp", False, 24)
                    item.kill()
                case 1:
                    TAKE_ITEM_EXP.play()
                    effect_system.EffectSize(item.rect.centerx, item.rect.centery, "ASSETS/SPRITE/EFFECT/pickUp/7.png",
                                             0, 10)
                    ScoreSet = 45 + scene_config.p.MultipleScore
                    scene_config.Score += ScoreSet
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                    create_item.createTXT(item.rect.x + 15, item.rect.y + 15, "+20 EXP")
                    scene_config.p.EXP += 20
                    scene_config.p.check_exp()
                    if scene_config.isParticle:
                        for i in range(2):
                            createParticle(item.rect.x, item.rect.y, i, "ASSETS/SPRITE/EFFECT/7.bmp", False, 25)
                    item.kill()
                case 2:
                    TAKE_ITEM_AMMO.play()
                    effect_system.EffectSize(item.rect.centerx, item.rect.centery, "ASSETS/SPRITE/EFFECT/pickUp/8.png",
                                             0, 10)
                    create_item.createTXT(item.rect.x + 15, item.rect.y + 15, "+150 AMMO")
                    scene_config.p.Ammo += 150
                    ScoreSet = 10 + scene_config.p.MultipleScore - 1
                    scene_config.Score += ScoreSet
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                    if scene_config.isParticle:
                        for i in itertools.islice(itertools.count(0), 2):
                            createParticle(item.rect.x, item.rect.y, i, "ASSETS/SPRITE/EFFECT/7.bmp", False, 24)
                    item.kill()
                case 4:
                    createEffectSimple(item.rect.x, item.rect.y, fileEffectSix, 6, 31)
                    health = randint(45, 95)
                    scene_config.p.Health += health
                    create_item.createTXT(item.rect.x + 15, item.rect.y + 15, "+" + str(health) + " HEALTH",
                                          (255, 255, 255))
                    ScoreSet = 25 + scene_config.p.MultipleScore
                    scene_config.Score += 25 + scene_config.p.MultipleScore
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/green_health.png")
                    item.kill()
                case 5:
                    createEffectSimple(item.rect.x, item.rect.y, fileEffectSix, 6, 31)
                    scene_config.p.Bomb += 1
                    create_item.createTXT(item.rect.x + 15, item.rect.y + 15, "+1",
                                          (255, 255, 255))
                    ScoreSet = 55 + scene_config.p.MultipleScore
                    scene_config.Score += 55 + scene_config.p.MultipleScore
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                    item.kill()
                case 6:
                    TAKE_ITEM_CRYSTAL.play()
                    create_item.createTXT(item.rect.x + 15, item.rect.y + 15, "+1 CRYSTAL", (63, 222, 255))
                    createEffectSimple(item.rect.x, item.rect.y, fileEffectSix, 6, 31)
                    scene_config.p.Health -= 2
                    for i in range(4):
                        createEffectTake(scene_config.p.rect.x + 17, scene_config.p.rect.y + 15,
                                         "ASSETS/SPRITE/UI/ICON/DamageHealth.png")
                    create_item.createTXT(WINDOWS_SIZE[0] // 2, 696, "-2", (255, 0, 0))
                    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/syan.png", 15)
                    scene_config.CRYSTAL_COUNT += 1
                    ScoreSet = 125 + scene_config.p.MultipleScore
                    scene_config.Score += 125 + scene_config.p.MultipleScore
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                    item.kill()


timeTakeDamage: int = 0

timeBoom: int = 0


def collisionBullet():
    global timeDamage, timeTakeDamage, timeBoom

    for player_b in group_config.player_bullet:
        for ballRosa in group_config.ball_rosa_group:
            if ballRosa.rect.collidepoint(player_b.rect.center):
                DEATH_BALL.play()
                luck: int = randint(0, 3)
                if luck == 0 or luck == 1:
                    if scene_config.p.EXP < 500:
                        create_item.create_exp(ballRosa.rect.centerx, ballRosa.rect.centery, 0)
                elif luck == 3:
                    if scene_config.p.Health <= 155:
                        create_item.createX_CUSE(ballRosa.rect.x + 15, ballRosa.rect.y + 10)
                ScoreSet = 15 + scene_config.p.MultipleScore
                scene_config.Score += 15 + scene_config.p.MultipleScore
                create_item.createTXT(90
                                      , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet+scene_config.p.MultipleScore), (255, 255, 0), 1)
                create_item.createTXT(ballRosa.rect.x + 15, ballRosa.rect.y + 15, "+" + str(ScoreSet+scene_config.p.MultipleScore), (255, 255, 0))
                createEffectSimple(ballRosa.rect.x, ballRosa.rect.y, fileEffectOne, 6, 55, 1)
                if scene_config.isParticle:
                    particle.particleSystem(ballRosa.rect.centerx, ballRosa.rect.centery, (17, 1),
                                            scene_config.TRASH_COUNT, "ASSETS/SPRITE/ENEMY/line/1.png")
                    particle.particleSystem(ballRosa.rect.centerx, ballRosa.rect.centery, (13, 13), 3,
                                            "ASSETS/SPRITE/BULLET/AreaDie.png")
                if isKillBullet:
                    player_b.kill()
                ballRosa.timeMove = 0
                ballRosa.isMove = False
                ballRosa.rect.y = -200
                ballRosa.rect.x = -200
                scene_config.KILL_COUNT += 1
        for ballSoul in group_config.ball_soul:
            if ballSoul.rect.collidepoint(player_b.rect.center):
                DEATH_BALL.play()
                if scene_config.p.EXP <= 250:
                    create_item.createTXT(ballSoul.rect.centerx
                                          , ballSoul.rect.centery, "-" + str(scene_config.p.damageBullet),
                                          (255, 0, 0), 0)
                ballSoul.image.set_alpha(10)
                ballSoul.Health -= scene_config.p.damageBullet
                if isKillBullet:
                    player_b.kill()

        for Shutle in group_config.shutle_group:
            if Shutle.rect.collidepoint(player_b.rect.center):
                Shutle.image.set_alpha(0)
                if scene_config.p.EXP <= 250:
                    create_item.createTXT(Shutle.rect.centerx
                                          , Shutle.rect.centery, "-" + str(scene_config.p.damageBullet),
                                          (255, 0, 0), 0)
                if Shutle.idS == 1 or 2:
                    Shutle.isTakeDamage = True
                if timeBoom >= 8:
                    createEffectSimple(Shutle.rect.x + 5, Shutle.rect.y + 25, fileEffectFour, 4, 44)
                    DAMAGE_SHUTLE.play()
                    if scene_config.isParticle:
                        particle.particleSystem(Shutle.rect.centerx, Shutle.rect.centery - randint(1, 6),
                                                (randint(15, 20), randint(15, 20)),
                                                scene_config.TRASH_COUNT, fileMetal[randint(0, 3)], (0, 0, 0))
                    timeBoom = 0
                else:
                    timeBoom += 1
                Shutle.Health -= scene_config.p.damageBullet
                if isKillBullet:
                    player_b.kill()

        for shota in group_config.shota_group:
            if shota.rect.collidepoint(player_b.rect.center):
                DEATH_PEOPLE.play()
                if scene_config.p.EXP <= 250:
                    create_item.createTXT(shota.rect.centerx
                                          , shota.rect.centery, "-" + str(scene_config.p.damageBullet),
                                          (255, 0, 0), 0)
                shota.isTakeDamage = True
                shota.Health -= scene_config.p.damageBullet
                effect_system.BloodSplash(shota.rect.centerx, shota.rect.centery)
                if isKillBullet:
                    player_b.kill()
        for type_one in group_config.magical_girl_group:
            if type_one.rect.collidepoint(player_b.rect.center):
                DEATH_PEOPLE.play()
                type_one.image.set_alpha(150)
                if scene_config.p.EXP <= 250:
                    create_item.createTXT(type_one.rect.centerx
                                          , type_one.rect.centery, "-" + str(scene_config.p.damageBullet),
                                          (255, 0, 0), 0)
                type_one.isTakeDamage = True
                type_one.Health -= scene_config.p.damageBullet
                effect_system.BloodSplash(type_one.rect.centerx, type_one.rect.centery)
                if isKillBullet:
                    player_b.kill()

    if timeDamage >= 116:
        scene_config.p.image.set_alpha(300)
        for e_b in group_config.simple_bullet:
            if e_b.rect.collidepoint(scene_config.p.rect.center):
                filename = ["ASSETS/SFX/PLAYER_TAKEDAMAGE_FIRST.wav", "ASSETS/SFX/PLAYER_TAKEDAMAGE_SECOND.wav",
                            "ASSETS/SFX/PLAYER_TAKEDAMAGE_THREE.wav"]
                PLAYER_TAKEDAMAGE = mixer.Sound(filename[randint(0, 2)])
                PLAYER_TAKEDAMAGE.play()
                if scene_config.p.EXP > 0:
                    scene_config.p.EXP -= 5
                damage = None
                match scene_config.DIFFICULTY:
                    case 0:
                        damage = 10
                    case 1:
                        damage = 23
                    case 2:
                        damage = 30
                scene_config.p.Health -= damage
                for i in range(4):
                    createEffectTake(scene_config.p.rect.x + 17, scene_config.p.rect.y + 15,
                                     "ASSETS/SPRITE/UI/ICON/DamageHealth.png")
                scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/red.png", 11)
                create_item.createTXT(WINDOWS_SIZE[0] // 2 - 1, 695, "-" + str(damage) + " HEALTH", (255, 0, 0))
                particle.particleSystem(scene_config.p.rect.centerx, scene_config.p.rect.centery, (18, 18),
                                        3, "ASSETS/SPRITE/EFFECT/player.png", (2, 2, 2))
                e_b.kill()
                timeDamage = 0
        for e_b in group_config.pistol_bullet:
            if e_b.rect.collidepoint(scene_config.p.rect.center):
                filename = ["ASSETS/SFX/PLAYER_TAKEDAMAGE_FIRST.wav", "ASSETS/SFX/PLAYER_TAKEDAMAGE_SECOND.wav",
                            "ASSETS/SFX/PLAYER_TAKEDAMAGE_THREE.wav"]
                PLAYER_TAKEDAMAGE = mixer.Sound(filename[randint(0, 2)])
                PLAYER_TAKEDAMAGE.play()
                if scene_config.p.EXP > 0:
                    scene_config.p.EXP -= 5

                damage = None

                match scene_config.DIFFICULTY:
                    case 0:
                        damage = randint(15, 19)
                    case 1:
                        damage = randint(25, 35)
                    case 2:
                        damage = randint(39, 42)
                scene_config.p.Health -= damage
                for i in range(4):
                    createEffectTake(scene_config.p.rect.x + 17, scene_config.p.rect.y + 15,
                                     "ASSETS/SPRITE/UI/ICON/DamageHealth.png")
                scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/red.png", 12)
                particle.particleSystem(scene_config.p.rect.centerx, scene_config.p.rect.centery, (18, 18),
                                        4, "ASSETS/SPRITE/EFFECT/player.png", (2, 2, 2))
                create_item.createTXT(WINDOWS_SIZE[0] // 2 - 1, 695, "-" + str(damage) + " HEALTH", (255, 0, 0))
                e_b.kill()
                timeDamage = 0

        for e_b in group_config.anyBullet:
            if e_b.rect.collidepoint(scene_config.p.rect.center):
                filename = ["ASSETS/SFX/PLAYER_TAKEDAMAGE_FIRST.wav", "ASSETS/SFX/PLAYER_TAKEDAMAGE_SECOND.wav",
                            "ASSETS/SFX/PLAYER_TAKEDAMAGE_THREE.wav"]
                PLAYER_TAKEDAMAGE = mixer.Sound(filename[randint(0, 2)])
                PLAYER_TAKEDAMAGE.play()
                if scene_config.p.EXP > 5:
                    scene_config.p.EXP -= 12
                damage = None
                match scene_config.DIFFICULTY:
                    case 0:
                        damage = 23
                    case 1:
                        damage = 35
                    case 2:
                        damage = 45
                scene_config.p.Health -= damage
                for i in range(4):
                    createEffectTake(scene_config.p.rect.x + 17, scene_config.p.rect.y + 15,
                                     "ASSETS/SPRITE/UI/ICON/DamageHealth.png")
                scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/red.png", 9)
                particle.particleSystem(scene_config.p.rect.centerx, scene_config.p.rect.centery, (18, 18),
                                        5, "ASSETS/SPRITE/EFFECT/player.png", (2, 2, 2))
                create_item.createTXT(WINDOWS_SIZE[0] // 2 - 1, 695, "-" + str(damage) + " HEALTH", (255, 0, 0))
                e_b.kill()
                timeDamage = 0
    else:
        if timeDamage < 30:
            for e_b in group_config.anyBullet:
                e_b.kill()
            for e_b in group_config.pistol_bullet:
                e_b.kill()
            for e_b in group_config.simple_bullet:
                e_b.kill()
        scene_config.p.image.set_alpha(125)
        timeDamage += 0.5


def collisionBombPlayer():
    global timeBoom
    for player_bomb in group_config.bomb_player:
        if player_bomb.rect.y > 14:
            for e_b in group_config.anyBullet:
                if e_b.rect.colliderect(player_bomb.rect):
                    e_b.kill()
            for e_b in group_config.pistol_bullet:
                if e_b.rect.colliderect(player_bomb.rect):
                    e_b.kill()
            for e_b in group_config.simple_bullet:
                if e_b.rect.colliderect(player_bomb.rect):
                    e_b.kill()
            for ballRosa in group_config.ball_rosa_group:
                if ballRosa.rect.colliderect(player_bomb.rect):
                    DEATH_BALL.play()
                    ScoreSet = 15 + scene_config.p.MultipleScore
                    scene_config.Score += 15 + scene_config.p.MultipleScore
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                    create_item.createTXT(ballRosa.rect.x + 15, ballRosa.rect.y + 15, "+" + str(ScoreSet),
                                          (255, 255, 0))
                    createEffectSimple(ballRosa.rect.x, ballRosa.rect.y, fileEffectOne, 6, 55, 1)
                    if scene_config.isParticle:
                        particle.particleSystem(ballRosa.rect.centerx, ballRosa.rect.centery, (17, 1),
                                                scene_config.TRASH_COUNT, "ASSETS/SPRITE/ENEMY/line/1.png")
                        particle.particleSystem(ballRosa.rect.centerx, ballRosa.rect.centery, (13, 13), 3,
                                                "ASSETS/SPRITE/BULLET/AreaDie.png")
                    ballRosa.timeMove = 0
                    ballRosa.isMove = False
                    ballRosa.rect.y = -200
                    ballRosa.rect.x = -200
                    scene_config.KILL_COUNT += 1
            for ballSoul in group_config.ball_soul:
                if ballSoul.rect.colliderect(player_bomb.rect):
                    ballSoul.image.set_alpha(10)
                    ballSoul.Health -= scene_config.p.damageBullet

            for Shutle in group_config.shutle_group:
                if Shutle.rect.colliderect(player_bomb.rect):
                    if Shutle.idS == 1 or 2:
                        Shutle.isTakeDamage = True
                    if timeBoom >= 8:
                        createEffectSimple(Shutle.rect.x + 5, Shutle.rect.y + 25, fileEffectFour, 4, 44)
                        DAMAGE_SHUTLE.play()
                        particle.particleSystem(Shutle.rect.centerx, Shutle.rect.centery - randint(1, 6),
                                                (randint(15, 20), randint(15, 20)),
                                                scene_config.TRASH_COUNT, fileMetal[randint(0, 3)], (0, 0, 0))
                        timeBoom = 0
                    else:
                        timeBoom += 1
                    Shutle.Health -= scene_config.p.damageBullet
                    Shutle.image.set_alpha(0)

            for shota in group_config.shota_group:
                if shota.rect.colliderect(player_bomb.rect):
                    shota.isTakeDamage = True
                    shota.Health -= scene_config.p.damageBullet
            for type_one in group_config.magical_girl_group:
                if type_one.rect.colliderect(player_bomb.rect):
                    type_one.isTakeDamage = True
                    type_one.image.set_alpha(15)
                    type_one.Health -= scene_config.p.damageBullet
                    type_one.image.set_alpha(300)
