from pygame import transform

import group_config
from PLAYER import player
from SCENE import scene_config, scene_game, scene_game_score
from config_script import WINDOWS_SIZE, createImage
from SKILLTREE import SkillTree


def reset():
    for ball_rosa in group_config.ball_rosa_group:
        ball_rosa.kill()
    for shota in group_config.shota_group:
        shota.kill()
    for shutle in group_config.shutle_group:
        shutle.kill()
    for soulBall in group_config.ball_soul:
        soulBall.kill()
    for magical in group_config.magical_girl_group:
        magical.kill()
    for bullet_sim in group_config.simple_bullet:
        bullet_sim.kill()
    for p_b in group_config.player_bullet:
        p_b.kill()
    for ab in group_config.anyBullet:
        ab.kill()
    for pistol in group_config.pistol_bullet:
        pistol.kill()
    for crystal in group_config.item_group:
        crystal.kill()
    for skill in SkillTree.slot_group:
        skill.image = createImage("ASSETS/SPRITE/UI/SkillTree/1.png")
        skill.image = transform.scale(skill.image, (87, 82))
        skill.isBuy = False
    for boss in group_config.bossGroup:
        boss.kill()

    scene_game.createEffect = 0
    scene_game.timeCreateEffect = 0
    scene_config.isDrawPlayer = True
    scene_config.p = player.Player(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2, 8.5, "ASSETS/SPRITE/PLAYER/QWERTY.png")
    scene_game.LEVEL = 0
    scene_config.CRYSTAL_COUNT = 0
    scene_config.CRYSTAL_MAX = 0
    scene_config.KILL_COUNT = 0
    scene_config.LEVEL_TASK = 2
    scene_config.Score = 0
