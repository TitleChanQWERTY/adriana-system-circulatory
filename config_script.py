import pygame
import configparser
from os import path

pygame.init()
pygame.joystick.init()
joysticks = [pygame.joystick.Joystick(x) for x in range(pygame.joystick.get_count())]

WINDOWS_SIZE = (1280, 720)
TITLE: str = "Adriana::System: Circulatory: "

config = configparser.ConfigParser()

config.read("CONFIG.ini")
config.sections()

if not path.isfile("CONFIG.ini"):
    config.sections()
    config["DEFAULT"] = {"language": "EN",
                         "fullscreen": "False",
                         "aa_text": "False",
                         "show_fps": "True"}
    config["OTHER"] = {"inertia": "True",
                       "particle": "True",
                       "moveBackEffect": "True",
                       "countbackeffect": "250",
                       "showposindicator": "True",
                       "rotateobject": "True",
                       "ad_animation": "True",
                       "trash_count": "5",
                       "ad_gameplay": "True",
                       "light_effect": "True"}
    with open("CONFIG.ini", "w") as configfile:
        config.write(configfile)

LANGUAGE_SET = config["DEFAULT"]["language"]
isFullScreen = config.getboolean("DEFAULT", "fullscreen")

TEXT_LEVEL_EN = ("Kill Count: ", "EXP: ", "Ammo: ", "SCORE: ", "Adriana MAX!", "Crystal Count: ")
TEXT_LEVEL_UA = ("Вбито: ", "ЕКСП: ", "НАБОЇ: ", "БАЛИ: ", "МАКСИМАЛЬНА Адріана!", "Зібрано Кристалів: ")

TEXT_MENU_EN = ("PLAY", "SCORE", "OPTIONS", "EXIT", "ABOUT")
TEXT_MENU_UA = ("ГРАТИ", "РЕКОРДИ", "НАЛАШТУВАННЯ", "ВИЙТИ", "ПРО")

TEXT_MODE_EN = ("STORY", "SCORE", "MUSIC", "TUTORIAL", "SELECT MODE:")
TEXT_MODE_UA = ("ІСТОРІЯ", "РЕКОРД", "МУЗИКАЛЬНИЙ", "ТУТОРІАЛ", "ОБЕРІТЬ РЕЖИМ:")

TEXT_RANK_EN = ("Your Rank:", "Kill Count: ", "Health: ", "Score: ", "Press Any Button (Please)")
TEXT_RANK_UA = ("Твій Ранг:", "Вбито: ", "Рівень Здоров'я: ", "Бали: ", "Натисніть Будь Яку Клавішу (Будь-ласка)")

TEXT_DIE_EN = ("CONGRATULATIONS", "YOUR ARE DIE!", "Try Again", "Exit Main Main", "Exit Game", "Time Over")
TEXT_DIE_UA = ("ВІТАЮ", "ТИ ПОМЕР!", "Спробувати Ще Раз", "Вийти В Головне Меню", "Вийти З Гри", "Час Вичерпано")

TEXT_OPTION_EN = ("Other", "Regenerate Game:", "Language:", "Anti-Alaising Text:", "Show FPS:", "EXIT",
                  "Full-screen:")
TEXT_OPTION_UA = ("ІНШІ", "Регенерувати Гру:", "Мова:", "Згладжування Тексту:", "Відображати FPS:", "ВИЙТИ",
                  "Повний Екран: ")

TEXT_OPTION_OTHER_EN = ("Inertia Player:", "Particle:", "Move Back Effect", "Count Back Particle: ", "Show Pointer:",
                        "Rotate Object:", "Animation AD:", "Trash Count: ", "\"AD\":", "Light Effect:")
TEXT_OPTION_OTHER_UA = ("Інерція Гравця:", "Партікли:", "Рух Задніх Ефектів:", "Кількість Задніх Партіклів: ",
                        "Відображати Вказівник:", "Обертати Об'єкти:", "Анімація Реклами:", "Кількість Сміття: ",
                        "\"Реклама\":", "Ефект Світла:")

TEXT_WARNING_EN = ("Requsting Restart Game", "Press E To Continue")
TEXT_WARNING_UA = ("Потрібно Перезагрузити Гру", "Натисніть \"E\" Щоб Продовжити")

TEXT_LOADING_EN = (":TO PRAY!", ":YANDERE KILL", "CREATE PEOPLE")
TEXT_LOADING_UA = (":МОЛИМОСЬ!", ":ЯНДЕРЕ ВБИВАЄ", "НАРОДЖУЄМО ЛЮДЕЙ")

TEXT_NOTFICATION_EN = ("Press Y To open skill tree", "New Achivments")
TEXT_NOTFICATION_UA = ("Для Відкриття Древо Вмінь: Y", "Нове Досягнення")

TEXT_TASK_EN = ("Kill Enemy For The Time: ", "You will hold on?", "Collect  Crystal: ")
TEXT_TASK_UA = ("ВБИЙТЕ ВОРОГІВ ЗА ВІДВЕДЕНИЙ ЧАС: ", "ПРОТРИМАЄШСЯ?", "ЗБЕРИ КРИСТАЛИ: ")

TEXT_SKILL_EN = (
    "DAMAGE BULLET: 6200", "BULLET AWESOME: 8500", "AUTO REGENERATION: 9600", "+10 BOMB: 3300",
    "AUTO AMMO REGEN: 10100")
TEXT_SKILL_UA = ("БОЛЮЧІ КУЛІ: 6200", "НЕЗЛАМНІ КУЛІ: 8500", "АВТО РЕГЕНЕРАЦІЯ: 9600", "+10 БОМБ: 3300",
                 "АВТОМАТИЧНЕ ПОПОВНЕННЯ КУЛЬ: 10100")

sc = pygame.display.set_mode((WINDOWS_SIZE[0], WINDOWS_SIZE[1]))

clock = pygame.time.Clock()


def createImage(filename):
    return pygame.image.load(filename).convert_alpha()
