from random import randint

import pygame

import particle
from ITEM import exp, create_item
from SCENE import scene_config
from collision import createEffectSimple, fileEffectOne
from group_config import ball_soul

from BULLET import bullet_enemy
from config_script import createImage, WINDOWS_SIZE
from sfx_compilation import DEATH_BALL

fileBullet = ("ASSETS/SPRITE/BULLET/ENEMY/simple/1.png", "ASSETS/SPRITE/BULLET/ENEMY/simple/2.png")


class Ball_soul(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_SOUL/1.png")
        self.size: int = 35
        self.image = pygame.transform.scale(self.image, (self.size, self.size))
        self.rect = self.image.get_rect(center=(-300, -300))
        self.add(ball_soul)

        self.Health: int = 100

        match scene_config.DIFFICULTY:
            case 1:
                self.Health: int = 155
            case 2:
                self.Health: int = 235

        self.maxDown: int = randint(75, 140)

        self.Frame: int = 0

        self.TimeShot: int = 0
        self.shootType: int = 0
        self.timeDown: int = 0

        self.timeLeft: int = 0

        self.selectBack: int = randint(0, 1)

        self.isMove: bool = False

        self.timeMove: int = 0
        match scene_config.DIFFICULTY:
            case 0:
                self.timeMoveMax = randint(95, 6465)
            case 1:
                self.timeMoveMax = randint(75, 6085)
            case 2:
                self.timeMoveMax = self.timeMoveMax = randint(15, 4450)
        for i in ball_soul:
            if i.timeMoveMax == self.timeMoveMax:
                match scene_config.DIFFICULTY:
                    case 0:
                        self.timeMoveMax = randint(95, 6565)
                    case 1:
                        self.timeMoveMax = randint(75, 6085)
                    case 2:
                        self.timeMoveMax = self.timeMoveMax = randint(20, 5050)

    def createBullet(self):
        match self.TimeShot:
            case 35:
                bullet_enemy.BulletHell(self.rect.x + 21, self.rect.y + 27, "ASSETS/SPRITE/BULLET/ENEMY/10.bmp",
                                        self.shootType, 9)
                if self.shootType < 8:
                    self.shootType += 1
                else:
                    self.shootType = 1
                self.TimeShot = 0
            case _:
                self.TimeShot += 1

    def animation(self):
        match self.Frame:
            case 10:
                self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_SOUL/1.png")
                self.image = pygame.transform.scale(self.image, (self.size, self.size))
            case 20:
                self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_SOUL/2.png")
                self.image = pygame.transform.scale(self.image, (self.size, self.size))
            case 30:
                self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_SOUL/3.png")
                self.image = pygame.transform.scale(self.image, (self.size, self.size))
            case 40:
                self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_SOUL/4.png")
                self.image = pygame.transform.scale(self.image, (self.size, self.size))
            case 50:
                self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_SOUL/3.png")
                self.image = pygame.transform.scale(self.image, (self.size, self.size))
                self.image = pygame.transform.flip(self.image, True, False)
            case 60:
                self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_SOUL/2.png")
                self.image = pygame.transform.scale(self.image, (self.size, self.size))
                self.image = pygame.transform.flip(self.image, True, False)
                self.Frame = 0

        self.Frame += 1

    def update(self):
        if self.timeMove == self.timeMoveMax * 6:
            if not self.isMove:
                self.rect.x = randint(120, 990)
                self.rect.y = -15
                self.isMove = True
        else:
            self.timeMove += 1
        if self.isMove:
            if self.Health <= 0:
                if scene_config.p.EXP < 500:
                    exp.exp_item(self.rect.centerx, self.rect.centery, 0)
                createEffectSimple(self.rect.x, self.rect.y, fileEffectOne, 6, 55, 1)
                if scene_config.isParticle:
                    particle.particleSystem(self.rect.centerx, self.rect.centery, (17, 1),
                                            scene_config.TRASH_COUNT, "ASSETS/SPRITE/ENEMY/line/1.png")
                    particle.particleSystem(self.rect.centerx, self.rect.centery, (13, 13), 3,
                                            "ASSETS/SPRITE/BULLET/AreaDie.png")
                ScoreSet = 20 + scene_config.p.MultipleScore
                scene_config.Score += 20 + scene_config.p.MultipleScore
                create_item.createTXT(90
                                      , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                create_item.createTXT(self.rect.x + 15, self.rect.y + 15, "+" + str(ScoreSet),
                                      (255, 255, 0))
                self.timeMove = 0
                self.isMove = False
                self.timeDown = 0
                self.timeLeft = 0
                self.Health = 145
                self.rect.y = -300
                self.rect.x = -300
                scene_config.KILL_COUNT += 1
            self.animation()
            self.createBullet()
            if self.timeLeft <= 590:
                self.timeLeft += 1
                if self.timeDown < self.maxDown:
                    self.timeDown += 1
                    self.rect.y += 4
            else:
                match self.selectBack:
                    case 0:
                        if self.rect.x > -20:
                            self.rect.x -= 4
                        else:
                            self.timeDown = 0
                            self.timeMove = 0
                            self.timeLeft = 0
                            self.isMove = False
                            self.rect.y = -300
                            self.rect.x = -300
                    case 1:
                        if self.rect.x < 1280:
                            self.rect.x += 4
                        else:
                            self.timeDown = 0
                            self.timeMove = 0
                            self.timeLeft = 0
                            self.isMove = False
                            self.rect.y = -300
                            self.rect.x = -300
