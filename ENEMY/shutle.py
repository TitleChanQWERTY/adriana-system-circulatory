from random import randint

import pygame

import particle
from SCENE import scene_config
from collision import createEffectSimple, createParticle, fileMetal, createEffectBodies, fileEffectFive, fileEffectFour
from config_script import createImage, WINDOWS_SIZE
from group_config import shutle_group
from BULLET import bullet_enemy
from ITEM import create_item, exp

from effect_system import effect_simple
from sfx_compilation import DIE_SHUTLE


class patrol(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage("ASSETS/SPRITE/ENEMY/SHUTLE/PATRULE/1.bmp")
        self.size: int = randint(39, 42)
        self.image = pygame.transform.scale(self.image, (self.size, self.size + 5))
        self.rect = self.image.get_rect(center=(-200, -200))
        self.add(shutle_group)

        self.idS: int = 0

        self.Health: int = 685

        self.posStart: int = randint(0, 3)

        self.Frame: int = 0
        self.speed: int = 5

        self.ForceDown: int = 1
        self.isTakeDamageFrame = 0
        self.isTakeDamage = False

        self.timeSpawn: int = 0
        self.timeSpawnMAX = randint(15, 75)

        self.pos = randint(0, 2)
        self.timePos: int = 0

        self.timeShot: int = 0

        self.isShot: int = 0

        self.isMove: bool = False

        self.timeMove: int = 0
        match scene_config.DIFFICULTY:
            case 0:
                self.timeMoveMax = self.timeMoveMax = randint(125, 6800)
            case 1:
                self.timeMoveMax = self.timeMoveMax = randint(95, 3055)
            case 2:
                self.timeMoveMax = self.timeMoveMax = randint(75, 2050)
        for i in shutle_group:
            if i.timeMoveMax == self.timeMoveMax:
                match scene_config.DIFFICULTY:
                    case 0:
                        self.timeMoveMax = self.timeMoveMax = randint(100, 7805)
                    case 1:
                        self.timeMoveMax = self.timeMoveMax = randint(100, 3045)
                    case 2:
                        self.timeMoveMax = self.timeMoveMax = randint(65, 1050)

        match self.posStart:
            case 1:
                self.image = pygame.transform.flip(self.image, False, True)
            case 2:
                self.image = pygame.transform.rotate(self.image, 90)
            case 3:
                self.image = pygame.transform.rotate(self.image, -90)

    def createMuzzle(self, x, y, filename, pos):
        return effect_simple(x, y, filename, pos)

    def createBullet(self, x, y):
        match self.timeShot:
            case 14:
                self.timeShot = 0
                match self.posStart:
                    case 0:
                        self.createMuzzle(x, y + 24, "ASSETS/SPRITE/EFFECT/Muzzle Flash/3_5.png", 0)
                    case 1:
                        self.createMuzzle(x, y - 24, "ASSETS/SPRITE/EFFECT/Muzzle Flash/3_5.png", 2)
                    case 2:
                        self.createMuzzle(x + 24, y - 4, "ASSETS/SPRITE/EFFECT/Muzzle Flash/3.png", 0)
                    case 3:
                        self.createMuzzle(x - 24, y - 4, "ASSETS/SPRITE/EFFECT/Muzzle Flash/3.png", 1)
                bullet_enemy.Best_Bullet(x, y, "ASSETS/SPRITE/BULLET/ENEMY/1.bmp", self.posStart)
            case _:
                self.timeShot += 1

    def update(self):
        if self.timeMove == self.timeMoveMax * 6:
            if not self.isMove:
                match self.posStart:
                    case 0:
                        self.rect = self.image.get_rect(center=(randint(45, 1005), 5))
                    case 1:
                        self.rect = self.image.get_rect(center=(randint(45, 1005), 780))
                    case 2:
                        self.rect = self.image.get_rect(center=(5, randint(25, 560)))
                    case 3:
                        self.rect = self.image.get_rect(center=(1280, randint(25, 560)))
                self.isMove = True
        else:
            self.timeMove += 1
        if self.isMove:
            if self.Health <= 0:
                if scene_config.p.EXP < 500:
                    exp.exp_item(self.rect.centerx, self.rect.centery, 1)
                createEffectSimple(self.rect.x + 20, self.rect.y + 10, fileEffectFive, 4, 79, 1)
                if scene_config.isParticle:
                    createParticle(self.rect.x, self.rect.y, scene_config.TRASH_COUNT)
                scene_config.KILL_COUNT += 2
                ScoreSet = 35 + scene_config.p.MultipleScore
                scene_config.Score += 35 + scene_config.p.MultipleScore
                create_item.createTXT(90
                                      , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                if scene_config.isParticle:
                    createEffectBodies(self.rect.x + randint(0, 55), self.rect.y + randint(0, 55),
                                       fileMetal[randint(0, 3)], randint(10, 27), True)
                    particle.particleSystem(self.rect.centerx, self.rect.centery - randint(1, 6), (24, 26),
                                            scene_config.TRASH_COUNT, "ASSETS/SPRITE/EFFECT/7.bmp", (23, 45, 50))
                self.timeMove = 0
                self.isMove = False
                self.rect.y = -400
                self.rect.x = -400
                self.Health = 695
                DIE_SHUTLE.play()
            match self.isShot:
                case 0:
                    match self.posStart:
                        case 0:
                            self.createBullet(self.rect.x + 21, self.rect.y + 48)
                        case 1:
                            self.createBullet(self.rect.x + 21, self.rect.y - 10)
                        case 2:
                            self.createBullet(self.rect.x + 49, self.rect.y + 25)
                        case 3:
                            self.createBullet(self.rect.x - 1, self.rect.y + 25)
            match self.posStart:
                case 0:
                    self.rect.y += self.ForceDown
                case 1:
                    self.rect.y -= self.ForceDown
                case 2:
                    self.rect.x += self.ForceDown
                case 3:
                    self.rect.x -= self.ForceDown
            match self.posStart:
                case 0:
                    if self.rect.y >= 780:
                        self.Health: int = 685
                        self.timeMove = 0
                        self.isMove = False
                        self.rect.y = -200
                        self.rect.x = -200
                case 1:
                    if self.rect.y <= -40:
                        self.timeMove = 0
                        self.Health: int = 685
                        self.isMove = False
                        self.rect.y = -200
                        self.rect.x = -200
                case 2:
                    if self.rect.x >= 1280:
                        self.timeMove = 0
                        self.Health: int = 685
                        self.isMove = False
                        self.rect.y = -200
                        self.rect.x = -200
                case 3:
                    if self.rect.x <= -40:
                        self.timeMove = 0
                        self.Health: int = 685
                        self.isMove = False
                        self.rect.y = -200
                        self.rect.x = -200
            if self.isTakeDamage:
                if self.isTakeDamageFrame < 1.5:
                    self.isTakeDamageFrame += 0.5
                else:
                    self.image.set_alpha(300)
                    self.isTakeDamageFrame = 0
                    self.isTakeDamage = False


class item_shutle(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage("ASSETS/SPRITE/ENEMY/SHUTLE/ITEM/1.png")
        self.image = pygame.transform.scale(self.image, (54, 54))
        self.rect = self.image.get_rect(center=(-200, -200))
        self.add(shutle_group)

        self.Health: int = 250
        self.isTakeDamage: bool = False
        self.isTakeDamageFrame: float = 0

        self.timeItem: int = 0

        self.idS: int = 1

        self.item = randint(0, 2)

        self.isCreateItem: bool = True

        self.Frame: int = 0

        self.isMove: bool = False

        self.timeMove: int = 0

        match scene_config.DIFFICULTY:
            case 0:
                self.timeMoveMax = randint(55, 6000)
            case 1:
                self.timeMoveMax = randint(70, 6500)
            case 2:
                self.timeMoveMax = self.timeMoveMax = randint(105, 3450)
        for i in shutle_group:
            if i.timeMoveMax == self.timeMoveMax:
                match scene_config.DIFFICULTY:
                    case 0:
                        self.timeMoveMax = randint(55, 6000)
                    case 1:
                        self.timeMoveMax = randint(70, 6500)
                    case 2:
                        self.timeMoveMax = self.timeMoveMax = randint(105, 3450)

        self.yRand = randint(95, 455)

    def update(self):
        self.image.set_alpha(300)
        if self.timeMove == self.timeMoveMax * 6:
            if not self.isMove:
                self.rect.x = -20
                self.rect.y = self.yRand
                self.isMove = True
        else:
            self.timeMove += 1
        if self.isMove:
            if self.Health <= 0:
                if scene_config.p.EXP < 500:
                    exp.exp_item(self.rect.centerx, self.rect.centery, 1)
                createEffectSimple(self.rect.x + 20, self.rect.y + 10, fileEffectFour, 4, 79, 1)
                if scene_config.isParticle:
                    createParticle(self.rect.x, self.rect.y, scene_config.TRASH_COUNT)
                scene_config.KILL_COUNT += 2
                ScoreSet = 35 + scene_config.p.MultipleScore
                scene_config.Score += 35 + scene_config.p.MultipleScore
                create_item.createTXT(90
                                      , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                if scene_config.isParticle:
                    createEffectBodies(self.rect.x + randint(0, 55), self.rect.y + randint(0, 55),
                                       fileMetal[randint(0, 3)], randint(10, 27), True)
                    particle.particleSystem(self.rect.centerx, self.rect.centery - randint(1, 6), (24, 26),
                                            scene_config.TRASH_COUNT, "ASSETS/SPRITE/EFFECT/7.bmp", (23, 45, 50))
                self.timeMove = 0
                self.isMove = False
                self.rect.y = -400
                self.rect.x = -400
                self.Frame = 0
                self.Health = 120
                self.timeItem = 0
                DIE_SHUTLE.play()
            if self.isTakeDamage:
                if self.isTakeDamageFrame < 2:
                    self.isTakeDamageFrame += 0.5
                    self.image = createImage("ASSETS/SPRITE/ENEMY/SHUTLE/ITEM/damage.png")
                    self.image = pygame.transform.scale(self.image, (54, 54))
                else:
                    if self.Frame >= 5:
                        self.image = createImage("ASSETS/SPRITE/ENEMY/SHUTLE/ITEM/open/3.png")
                    else:
                        self.image = createImage("ASSETS/SPRITE/ENEMY/SHUTLE/ITEM/1.png")
                    self.image = pygame.transform.scale(self.image, (54, 54))
                    self.isTakeDamageFrame = 0
                    self.isTakeDamage = False
            match self.isCreateItem:
                case True:
                    match self.timeItem:
                        case 105:
                            match self.Frame:
                                case 15:
                                    self.image = createImage("ASSETS/SPRITE/ENEMY/SHUTLE/ITEM/1.png")
                                    self.image = pygame.transform.scale(self.image, (54, 54))
                                case 25:
                                    self.image = createImage(
                                        "ASSETS/SPRITE/ENEMY/SHUTLE/ITEM/open/2.png")
                                    self.image = pygame.transform.scale(self.image, (54, 54))
                                case 45:
                                    self.image = createImage(
                                        "ASSETS/SPRITE/ENEMY/SHUTLE/ITEM/open/3.png")
                                    self.image = pygame.transform.scale(self.image, (54, 54))
                                    match self.item:
                                        case 0:
                                            create_item.createX_CUSE(self.rect.x + 40, self.rect.y + 47)
                                        case 1:
                                            create_item.create_exp(self.rect.x + 40, self.rect.y + 47, 1)
                                        case 2:
                                            create_item.create_ammo(self.rect.x + 40, self.rect.y + 47)
                                    self.isCreateItem = False
                            self.Frame += 1
                        case _:
                            self.timeItem += 1
            self.rect.x += 3
            if self.rect.x > 1295:
                self.Frame = 0
                self.image = createImage("ASSETS/SPRITE/ENEMY/SHUTLE/ITEM/1.png")
                self.image = pygame.transform.scale(self.image, (54, 54))
                self.item = randint(0, 2)
                self.Health: int = 250
                self.timeItem = 0
                self.isCreateItem = True
                self.timeMove = 0
                self.isMove = False
                self.rect.y = -200
                self.rect.x = -200


class rocket(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage("ASSETS/SPRITE/ENEMY/SHUTLE/ROCKET/1.bmp")
        self.image = pygame.transform.scale(self.image, (77, 77))
        self.rect = self.image.get_rect(center=(-250, -250))
        self.image = pygame.transform.flip(self.image, False, True)
        self.add(shutle_group)

        self.Health: int = 810
        self.isTakeDamage: bool = False
        self.isTakeDamageFrame: float = 0

        self.isMoveStandart: bool = False

        self.timePos: int = 0

        self.idS: int = 2

        self.pos = randint(0, 3)

        self.timeShoot: int = 0
        self.Ammo: int = 4
        self.reload: int = 0

        self.Frame: int = 0

        self.isMove: bool = False

        self.timeMove: int = 0
        match scene_config.DIFFICULTY:
            case 0:
                self.timeMoveMax = randint(290, 5000)
            case 1:
                self.timeMoveMax = randint(200, 2990)
            case 2:
                self.timeMoveMax = self.timeMoveMax = randint(100, 2450)
        for i in shutle_group:
            if i.timeMoveMax == self.timeMoveMax:
                match scene_config.DIFFICULTY:
                    case 0:
                        self.timeMoveMax = randint(295, 5400)
                    case 1:
                        self.timeMoveMax = randint(240, 3000)
                    case 2:
                        self.timeMoveMax = self.timeMoveMax = randint(150, 2450)

    def createMuzzle(self):
        return effect_simple(self.rect.x + 48, self.rect.y + 94, "ASSETS/SPRITE/EFFECT/Muzzle Flash/1_5.png", 0, 54)

    def creteBullet(self):
        if self.Ammo > 0:
            match self.timeShoot:
                case 7:
                    self.timeShoot = 0
                    self.Ammo -= 1
                    self.createMuzzle()
                    bullet_enemy.pistolBullet(self.rect.x + 47, self.rect.y + 65,
                                              "ASSETS/SPRITE/BULLET/ENEMY/7.bmp", 9, 24, False)
                case _:
                    self.timeShoot += 1
        else:
            match self.reload:
                case 150:
                    self.reload = 0
                    self.Ammo = 4
                case _:
                    self.reload += 1

    def update(self):
        if self.timeMove == self.timeMoveMax * 6:
            if not self.isMove:
                self.rect = self.image.get_rect(center=(-15, randint(35, 250)))
                self.isMove = True
        else:
            self.timeMove += 1
        if self.isMove:
            if self.Health <= 0:
                if scene_config.p.EXP < 500:
                    exp.exp_item(self.rect.centerx, self.rect.centery, 1)
                createEffectSimple(self.rect.x + 20, self.rect.y + 10, fileEffectFour, 4, 79, 1)
                if scene_config.isParticle:
                    createParticle(self.rect.x, self.rect.y, scene_config.TRASH_COUNT)
                scene_config.KILL_COUNT += 2
                ScoreSet = 35 + scene_config.p.MultipleScore
                scene_config.Score += 35 + scene_config.p.MultipleScore
                create_item.createTXT(90
                                      , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                if scene_config.isParticle:
                    createEffectBodies(self.rect.x + randint(0, 55), self.rect.y + randint(0, 55),
                                       fileMetal[randint(0, 3)], randint(10, 27), True)
                    particle.particleSystem(self.rect.centerx, self.rect.centery - randint(1, 6), (24, 26),
                                            scene_config.TRASH_COUNT, "ASSETS/SPRITE/EFFECT/7.bmp", (23, 45, 50))
                self.timeMove = 0
                self.isMove = False
                self.rect.y = -400
                self.rect.x = -400
                self.Health = 815
                DIE_SHUTLE.play()
            if self.isTakeDamage:
                if self.isTakeDamageFrame < 2:
                    self.isTakeDamageFrame += 0.5
                    self.image = createImage("ASSETS/SPRITE/ENEMY/SHUTLE/ROCKET/damage.bmp")
                    self.image = pygame.transform.scale(self.image, (80, 80))
                    self.image = pygame.transform.flip(self.image, False, True)
                else:
                    self.image = createImage("ASSETS/SPRITE/ENEMY/SHUTLE/ROCKET/1.bmp")
                    self.image = pygame.transform.scale(self.image, (77, 77))
                    self.image = pygame.transform.flip(self.image, False, True)
                    self.isTakeDamageFrame = 0
                    self.isTakeDamage = False
            else:
                if self.Frame > 2:
                    self.image = createImage("ASSETS/SPRITE/ENEMY/SHUTLE/ROCKET/2.bmp")
                    self.image = pygame.transform.scale(self.image, (77, 77))
                    self.image = pygame.transform.flip(self.image, False, True)
                    self.Frame = 0
                else:
                    self.image = createImage("ASSETS/SPRITE/ENEMY/SHUTLE/ROCKET/1.bmp")
                    self.image = pygame.transform.scale(self.image, (77, 77))
                    self.image = pygame.transform.flip(self.image, False, True)
                    self.Frame += 1
            self.creteBullet()
            self.image.set_alpha(300)
            if self.rect.x < 125 and not self.isMoveStandart:
                self.rect.y += 1
                self.rect.x += 1
                self.timeMove += 1
            else:
                self.isMoveStandart = True
                match self.timePos:
                    case 175:
                        self.timePos = 0
                        self.pos = randint(0, 3)
                    case _:
                        self.timePos += 1
                self.move()

    def move(self):
        match self.pos:
            case 0:
                if self.rect.y < 650 and self.rect.x < 1200:
                    self.rect.y += 1
                    self.rect.x += 2
                else:
                    self.pos = 1
            case 1:
                if self.rect.y > 35 and self.rect.x > 35:
                    self.rect.y -= 1
                    self.rect.x -= 1
                else:
                    self.pos = 0
            case 2:
                if self.rect.y > 35 and self.rect.x < 1200:
                    self.rect.y -= 1
                    self.rect.x += 1
                else:
                    self.pos = 3
            case 3:
                if self.rect.y < 600 and self.rect.x > 35:
                    self.rect.y += 1
                    self.rect.x -= 1
                else:
                    self.pos = 2


class Drone_Citadel(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/ENEMY/SHUTLE/DRONE_CITADEL/1.png")
        self.image = pygame.transform.scale(self.image, (54, 54))
        self.rect = self.image.get_rect(center=(-700, -700))
        self.add(shutle_group)

        self.idS: int = 3

        self.Health: int = 950

        self.Frame: int = 0

        self.timeShoot: int = 0

        self.isTakeDamage: bool = False
        self.isTakeDamageFrame: float = 0

        self.isMove: bool = False
        self.timeMove: int = 0
        match scene_config.DIFFICULTY:
            case 0:
                self.timeMoveMax = randint(225, 2900)
            case 1:
                self.timeMoveMax = randint(140, 2300)
            case 2:
                self.timeMoveMax = self.timeMoveMax = randint(100, 1950)
        for i in shutle_group:
            if i.timeMoveMax == self.timeMoveMax:
                match scene_config.DIFFICULTY:
                    case 0:
                        self.timeMoveMax = randint(225, 2900)
                    case 1:
                        self.timeMoveMax = randint(140, 2300)
                    case 2:
                        self.timeMoveMax = self.timeMoveMax = randint(100, 1950)

    def animation(self):
        match self.Frame:
            case 4:
                self.image = createImage("ASSETS/SPRITE/ENEMY/SHUTLE/DRONE_CITADEL/1.png")
                self.image = pygame.transform.scale(self.image, (54, 54))
                self.Frame = 0
            case 2:
                self.image = createImage("ASSETS/SPRITE/ENEMY/SHUTLE/DRONE_CITADEL/2.png")
                self.image = pygame.transform.scale(self.image, (54, 54))
                self.Frame += 1
            case _:
                self.Frame += 1

    def createBullet(self):
        if self.timeShoot >= 5:
            self.timeShoot = 0
            bullet_enemy.pistolBullet(self.rect.x + 27, self.rect.y - 6,
                                      "ASSETS/SPRITE/BULLET/ENEMY/9.png", randint(5, 30), 10, True, 1, 1)
        else:
            self.timeShoot += 1

    def update(self):
        if not self.isMove:
            if self.timeMove == self.timeMoveMax * 6:
                self.rect = self.image.get_rect(center=(randint(95, 1190), 700))
                self.isMove = True
            else:
                self.timeMove += 1
        else:
            if self.Health <= 0:
                if scene_config.p.EXP < 500:
                    exp.exp_item(self.rect.centerx, self.rect.centery, 1)
                createEffectSimple(self.rect.x + 20, self.rect.y + 10, fileEffectFour, 4, 79, 1)
                if scene_config.isParticle:
                    createParticle(self.rect.x, self.rect.y, scene_config.TRASH_COUNT)
                scene_config.KILL_COUNT += 2
                ScoreSet = 35 + scene_config.p.MultipleScore
                scene_config.Score += 35 + scene_config.p.MultipleScore
                create_item.createTXT(90
                                      , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                if scene_config.isParticle:
                    createEffectBodies(self.rect.x + randint(0, 55), self.rect.y + randint(0, 55),
                                       fileMetal[randint(0, 3)], randint(10, 27), True)
                    particle.particleSystem(self.rect.centerx, self.rect.centery - randint(1, 6), (24, 26),
                                            scene_config.TRASH_COUNT, "ASSETS/SPRITE/EFFECT/7.bmp", (23, 45, 50))
                self.timeMove = 0
                self.isMove = False
                self.rect.y = -400
                self.rect.x = -400
                self.Health = 950
                DIE_SHUTLE.play()
            if self.isTakeDamage:
                if self.isTakeDamageFrame < 2:
                    self.isTakeDamageFrame += 0.5
                    self.image = createImage("ASSETS/SPRITE/ENEMY/SHUTLE/DRONE_CITADEL/damage.png")
                    self.image = pygame.transform.scale(self.image, (52, 52))
                else:
                    self.image = createImage("ASSETS/SPRITE/ENEMY/SHUTLE/DRONE_CITADEL/1.png")
                    self.image = pygame.transform.scale(self.image, (52, 52))
                    self.isTakeDamageFrame = 0
                    self.isTakeDamage = False
            else:
                self.animation()

            self.createBullet()

            if self.rect.y > -15:
                self.rect.y -= 2
            else:
                self.timeMove = 0
                self.Health = 950
                self.isMove = False
                self.rect.y = -500
                self.rect.x = -500
