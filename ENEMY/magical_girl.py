from random import randint

from pygame import BLEND_RGB_ADD

import effect_system
import particle
from ITEM import create_item
from SCENE import scene_config
from collision import createEffectBodies, bodies, createEffectSimple, fileEffectOne
from glow import createCircle
from group_config import magical_girl_group
from BULLET import bullet_enemy
from config_script import createImage, sc, WINDOWS_SIZE

import pygame

from sfx_compilation import DEATH_PEOPLE


class Witcher(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/ENEMY/post_remember/1.bmp")
        self.image = pygame.transform.scale(self.image, (71, 70))
        self.rect = self.image.get_rect(center=(-500, -500))
        self.add(magical_girl_group)

        self.id: int = 0

        self.Health: int = 450

        self.isTakeDamage: bool = False
        self.isTakeDamageFrame = 0

        self.selectMovePos: int = randint(0, 3)
        self.timeSelect: int = 0

        self.isMove: bool = False

        self.timeMove: int = 0
        self.timeMoveMax = randint(400, 3900)
        for i in magical_girl_group:
            if i.timeMoveMax == self.timeMoveMax:
                self.timeMoveMax = randint(350, 4000)

        self.timeShoot: int = 0
        self.maxShoot = randint(250, 550)

        self.timeSpawnShow: int = 0

    def shoot(self):
        if self.timeShoot > self.maxShoot:
            for i in range(9):
                for b in range(4):
                    bullet_enemy.BulletHell(self.rect.x + 28, self.rect.y + 25, "ASSETS/SPRITE/BULLET/ENEMY/3.png",
                                            i, 25, randint(10, 95))
            self.timeShoot = 0
            self.maxShoot = randint(450, 1000)
        else:
            self.timeShoot += 1

    def update(self):
        if not self.isMove:
            if self.timeMove > self.timeMoveMax * 6:
                self.rect = self.image.get_rect(center=(randint(45, 1000), randint(25, 450)))
                particle.particleSystem(self.rect.centerx, self.rect.centery, (18, 18), 9,
                                        "ASSETS/SPRITE/EFFECT/12.bmp", (24, 24, 0))
                self.isMove = True
            else:
                self.timeMove += 1

        else:
            if self.Health <= 0:
                if scene_config.p.EXP < 500:
                    create_item.create_exp(self.rect.centerx, self.rect.centery, 1)
                effect_system.BloodSplash(self.rect.centerx, self.rect.centery)
                createEffectBodies(self.rect.centerx, self.rect.centery, bodies[randint(0, 2)])
                createEffectSimple(self.rect.x, self.rect.y, fileEffectOne, 6, 55, 1)
                scene_config.KILL_COUNT += 1
                self.Health = 450
                self.timeMove = 0
                self.isMove = False
                self.rect.y = -200
                self.rect.x = -200
                ScoreSet = 55 + scene_config.p.MultipleScore
                scene_config.Score += 55 + scene_config.p.MultipleScore
                create_item.createTXT(90
                                      , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
            if self.timeSpawnShow < 6:
                sc.blit(createCircle(150, (255, 255, 0)),
                        (self.rect.centerx - 150, self.rect.centery - 150),
                        special_flags=BLEND_RGB_ADD)
                self.timeSpawnShow += 1
            self.shoot()
            if self.timeSelect > 90:
                self.timeSelect = 0
                self.selectMovePos = randint(0, 3)
            else:
                self.timeSelect += 1
            match self.selectMovePos:
                case 0:
                    if self.rect.y < 550 and self.rect.x < 1000:
                        self.rect.x += 2
                        self.rect.y += 2
                    else:
                        self.selectMovePos = 1
                case 1:
                    if self.rect.y > 25 and self.rect.x > 55:
                        self.rect.x -= 2
                        self.rect.y -= 2
                    else:
                        self.selectMovePos = 0
                case 2:
                    if self.rect.y > 25 and self.rect.x < 1000:
                        self.rect.x += 2
                        self.rect.y -= 1
                    else:
                        self.selectMovePos = 3
                case 3:
                    if self.rect.y < 550 and self.rect.x > 55:
                        self.rect.x -= 2
                        self.rect.y += 1
                    else:
                        self.selectMovePos = 2

            if self.isTakeDamage:
                if self.isTakeDamageFrame < 1.5:
                    self.isTakeDamageFrame += 0.5
                    self.image = createImage("ASSETS/SPRITE/ENEMY/post_remember/damage.bmp")
                    self.image = pygame.transform.scale(self.image, (71, 70))
                else:
                    self.image = createImage("ASSETS/SPRITE/ENEMY/post_remember/1.bmp")
                    self.image = pygame.transform.scale(self.image, (71, 70))
                    self.isTakeDamageFrame = 0
                    self.isTakeDamage = False


class type_one(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage("ASSETS/SPRITE/ENEMY/MagicalGirl/1/1.png")
        self.image = pygame.transform.scale(self.image, (91, 91))
        self.posRun: int = randint(0, 1)
        self.rect = self.image.get_rect(center=(-50, -50))
        self.add(magical_girl_group)

        self.id: int = 0

        self.Health: int = 145
        self.isTakeDamage: bool = False
        self.isTakeDamageFrame: float = 0

        self.upTime = randint(50, 95)
        self.upWork: int = 0
        self.timeWait: int = 0

        self.pos: int = 0

        self.timeShot: int = 0

        self.timeStartShot: int = 0

        self.timeLive: int = 0

        self.isMove: bool = False

        self.timeMove: int = 0
        self.timeMoveMax = randint(475, 3200)
        for i in magical_girl_group:
            if i.timeMoveMax == self.timeMoveMax:
                self.timeMoveMax = randint(475, 3200)

    def bullet(self):
        match self.timeStartShot:
            case 95:
                match self.timeShot:
                    case 105:
                        self.timeStartShot = 0
                        self.timeShot = 0
                        return bullet_enemy.pistolBullet(self.rect.x + 45, self.rect.y,
                                                         "ASSETS/SPRITE/BULLET/ENEMY/6.png", 7, 64, False)
                self.timeShot += 1
            case _:
                self.timeStartShot += 1

    def update(self):
        if self.timeMove == self.timeMoveMax * 6:
            if not self.isMove:
                match self.posRun:
                    case 0:
                        self.rect = self.image.get_rect(center=(-10, randint(320, 560)))
                    case 1:
                        self.rect = self.image.get_rect(center=(1290, randint(320, 560)))
                self.isMove = True
        else:
            self.timeMove += 1
        if self.isMove:
            if self.Health <= 0:
                DEATH_PEOPLE.play()
                if scene_config.p.EXP < 500:
                    create_item.create_exp(self.rect.centerx, self.rect.centery, 1)
                effect_system.BloodSplash(self.rect.centerx, self.rect.centery)
                createEffectBodies(self.rect.centerx, self.rect.centery, bodies[randint(0, 2)])
                createEffectSimple(self.rect.x, self.rect.y, fileEffectOne, 6, 55, 1)
                scene_config.KILL_COUNT += 1
                self.Health = 450
                self.timeMove = 0
                self.isMove = False
                self.rect.y = -200
                self.rect.x = -200
                ScoreSet = 55 + scene_config.p.MultipleScore
                scene_config.Score += 55 + scene_config.p.MultipleScore
                create_item.createTXT(90
                                      , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
            if self.timeLive <= 1150:
                self.timeLive += 1
                if self.upWork < self.upTime:
                    match self.posRun:
                        case 0:
                            self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/MagicalGirl/1/2.png").convert_alpha()
                            self.image = pygame.transform.scale(self.image, (91, 91))
                            self.rect.y -= 3
                            self.rect.x += 2
                            self.upWork += 1
                        case 1:
                            self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/MagicalGirl/1/2.png").convert_alpha()
                            self.image = pygame.transform.scale(self.image, (91, 91))
                            self.image = pygame.transform.flip(self.image, True, False)
                            self.rect.y -= 3
                            self.rect.x -= 2
                            self.upWork += 1
                else:
                    match self.timeWait:
                        case 115:
                            self.timeWait = 0
                            self.pos = randint(0, 5)
                        case _:
                            self.bullet()
                            self.timeWait += 1
                    self.move()
            else:
                if self.rect.y > -30:
                    self.rect.y -= 4
                else:
                    self.timeMove = 0
                    self.isMove = False
                    self.rect.y = -400
                    self.rect.x = -400
            if self.isTakeDamage:
                if self.isTakeDamageFrame < 1.5:
                    self.isTakeDamageFrame += 0.5
                    self.image = createImage("ASSETS/SPRITE/ENEMY/MagicalGirl/1/damage.png")
                    self.image = pygame.transform.scale(self.image, (91, 91))
                else:
                    self.image = createImage("ASSETS/SPRITE/ENEMY/MagicalGirl/1/1.png")
                    self.image = pygame.transform.scale(self.image, (91, 91))
                    self.isTakeDamageFrame = 0
                    self.isTakeDamage = False

    def move(self):
        match self.pos:
            case 0:
                if self.rect.x >= 15 and self.rect.y >= 20:
                    self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/MagicalGirl/1/2.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (91, 91))
                    self.image = pygame.transform.flip(self.image, True, False)
                    self.rect.y -= 3
                    self.rect.x -= 2
                else:
                    self.pos = 1
            case 1:
                if self.rect.x <= 1200 and self.rect.y <= 600:
                    self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/MagicalGirl/1/2.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (91, 91))
                    self.rect.y += 3
                    self.rect.x += 2
                else:
                    self.pos = 4
            case 2:
                self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/MagicalGirl/1/1.png").convert_alpha()
                self.image = pygame.transform.scale(self.image, (91, 91))
            case 3:
                if self.rect.x >= 35:
                    self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/MagicalGirl/1/2.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (91, 91))
                    self.image = pygame.transform.flip(self.image, True, False)
                    self.rect.x -= 2
                else:
                    self.pos = 0
            case 4:
                if self.rect.x <= 1200:
                    self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/MagicalGirl/1/2.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (91, 91))
                    self.rect.x += 2
                else:
                    self.pos = 0
            case _:
                self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/MagicalGirl/1/1.png").convert_alpha()
                self.image = pygame.transform.scale(self.image, (91, 91))
