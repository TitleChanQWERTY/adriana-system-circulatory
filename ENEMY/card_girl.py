from random import randint

import effect_system
from ITEM import create_item
from SCENE import scene_config
from collision import createEffectBodies, bodies
from config_script import WINDOWS_SIZE
from group_config import magical_girl_group
from BULLET import bullet_enemy

import pygame

from sfx_compilation import DEATH_PEOPLE


class card_girl(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load_extended("ASSETS/SPRITE/ENEMY/Card_Girl/1.png").convert_alpha()
        self.image = pygame.transform.scale(self.image, (64, 64))
        self.rect = self.image.get_rect(center=(-500, -500))
        self.add(magical_girl_group)

        self.id: int = 1

        self.Health = randint(150, 230)

        match scene_config.DIFFICULTY:
            case 1:
                self.Health = randint(170, 290)
            case 1:
                self.Health = randint(195, 320)

        self.posRun = randint(0, 1)
        self.startPos: bool = True

        self.time: int = 0
        self.timeMax: int = randint(35, 55)

        self.pos: int = randint(0, 3)
        self.timePos: int = 0

        self.timeShoot: int = 0
        self.MaxTimeShoot: int = 2

        self.EXP = randint(1, 10)

        self.Ammo = 0
        self.timeReload = 0

        self.isMove: bool = False

        self.timeMove: int = 0
        match scene_config.DIFFICULTY:
            case 0:
                self.timeMoveMax = randint(995, 5465)
            case 1:
                self.timeMoveMax = randint(725, 4265)
            case 2:
                self.timeMoveMax = self.timeMoveMax = randint(555, 3450)
        for i in magical_girl_group:
            if i.timeMoveMax == self.timeMoveMax:
                match scene_config.DIFFICULTY:
                    case 0:
                        self.timeMoveMax = randint(995, 5465)
                    case 1:
                        self.timeMoveMax = randint(725, 4265)
                    case 2:
                        self.timeMoveMax = self.timeMoveMax = randint(555, 3450)

    def shoot(self):
        if self.Ammo > 0:
            match self.timeShoot:
                case self.MaxTimeShoot:
                    self.Ammo -= 1
                    self.timeShoot = 0
                    return bullet_enemy.pistolBullet(self.rect.x+33, self.rect.y+45, "ASSETS/SPRITE/BULLET/ENEMY/8.png")
                case _:
                    self.timeShoot += 1
        else:
            match self.timeReload:
                case 50:
                    self.timeReload = 0
                    self.Ammo = 5
                case _:
                    self.timeReload += 1

    def update(self):
        if self.timeMove == self.timeMoveMax * 6:
            if not self.isMove:
                match self.posRun:
                    case 0:
                        self.rect = self.image.get_rect(center=(-30, randint(45, 590)))
                    case 1:
                        self.rect = self.image.get_rect(center=(1200, randint(45, 590)))
                self.isMove = True
        else:
            self.timeMove += 1
        if self.isMove:
            if self.Health <= 0:
                DEATH_PEOPLE.play()
                if scene_config.p.EXP < 500:
                    create_item.create_exp(self.rect.centerx, self.rect.centery, 1)
                effect_system.BloodSplash(self.rect.centerx, self.rect.centery)
                createEffectBodies(self.rect.centerx, self.rect.centery, bodies[randint(0, 2)])
                scene_config.KILL_COUNT += 1
                create_item.create_bomb(self.rect.centerx, self.rect.centery)
                self.Health = randint(150, 230)
                self.timeMove = 0
                self.isMove = False
                self.rect.y = -200
                self.rect.x = -200
                ScoreSet = 55 + scene_config.p.MultipleScore
                scene_config.Score += 55 + scene_config.p.MultipleScore
                create_item.createTXT(90, WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
            self.shoot()
            self.image.set_alpha(350)
            match self.startPos:
                case True:
                    match self.posRun:
                        case 0:
                            if self.time < self.timeMax:
                                self.time += 1
                                self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/Card_Girl/2.png").convert_alpha()
                                self.image = pygame.transform.scale(self.image, (64, 64))
                                self.image = pygame.transform.flip(self.image, True, False)
                                self.rect.x += 1
                                self.rect.y += 1
                            else:
                                self.startPos = False
                        case 1:
                            if self.time < self.timeMax:
                                self.time += 1
                                self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/Card_Girl/2.png").convert_alpha()
                                self.image = pygame.transform.scale(self.image, (64, 64))
                                self.image = pygame.transform.flip(self.image, False, False)
                                self.rect.x -= 1
                                self.rect.y -= 1
                            else:
                                self.startPos = False
                case False:
                    match self.timePos:
                        case 35:
                            self.timePos = 0
                            self.pos = randint(0, 3)
                        case _:
                            self.timePos += 1
                    self.move()

    def move(self):
        match self.pos:
            case 0:
                if self.rect.x < 1200 and self.rect.y < 640:
                    self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/Card_Girl/2.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (64, 64))
                    self.image = pygame.transform.flip(self.image, True, False)
                    self.rect.x += 1
                    self.rect.y += 1
                else:
                    self.pos = 1
            case 1:
                if self.rect.x > 45 and self.rect.y > 95:
                    self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/Card_Girl/2.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (64, 64))
                    self.image = pygame.transform.flip(self.image, False, False)
                    self.rect.x -= 1
                    self.rect.y -= 1
                else:
                    self.pos = 0
            case 2:
                if self.rect.x < 1200:
                    self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/Card_Girl/2.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (64, 64))
                    self.image = pygame.transform.flip(self.image, True, False)
                    self.rect.x += 2
                else:
                    self.pos = 1
            case 3:
                if self.rect.x > 40:
                    self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/Card_Girl/2.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (64, 64))
                    self.image = pygame.transform.flip(self.image, False, False)
                    self.rect.x -= 2
                else:
                    self.pos = 2
