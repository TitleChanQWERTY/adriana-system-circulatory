from random import randint

import pygame

from BULLET import bullet_enemy
from ITEM import exp, Ammo, create_item
from collision import createEffectBodies, bodies, createEffectSimple, fileEffectOne
from config_script import createImage, WINDOWS_SIZE
from SCENE import scene_config
from group_config import shota_group
from sfx_compilation import DEATH_PEOPLE


class Protecter(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.typeShoot: int = randint(0, 1)
        if self.typeShoot == 0:
            self.image = createImage("ASSETS/SPRITE/ENEMY/protecter/1.png")
        else:
            self.image = createImage("ASSETS/SPRITE/ENEMY/protecter/3.png")
        self.image = pygame.transform.scale(self.image, (55, 55))
        self.rect = self.image.get_rect(center=(-500, -500))
        self.type = 3

        self.Health: int = 1095

        self.isTakeDamage: bool = False
        self.isTakeDamageFrame: float = 0

        self.timeShoot: int = 0
        self.countBullet: int = 0

        self.add(shota_group)
        self.isMove: bool = False

        self.luckS = randint(0, 1)

        self.timeMove: int = 0
        match scene_config.DIFFICULTY:
            case 0:
                self.timeMoveMax = self.timeMoveMax = randint(355, 2795)
            case 1:
                self.timeMoveMax = self.timeMoveMax = randint(236, 2375)
            case 2:
                self.timeMoveMax = self.timeMoveMax = randint(199, 2355)
        for i in shota_group:
            if i.timeMoveMax == self.timeMoveMax:
                match scene_config.DIFFICULTY:
                    case 0:
                        self.timeMoveMax = self.timeMoveMax = randint(355, 2695)
                    case 1:
                        self.timeMoveMax = self.timeMoveMax = randint(236, 2265)
                    case 2:
                        self.timeMoveMax = self.timeMoveMax = randint(205, 2155)

    def shoot(self):
        if self.timeShoot >= 55:
            if self.countBullet < 8:
                self.countBullet += 1
                if self.typeShoot == 1:
                    bullet_enemy.BulletHell(self.rect.centerx, self.rect.centery, "ASSETS/SPRITE/BULLET/ENEMY/2.png",
                                            self.countBullet, 29)
            else:
                self.countBullet = 0
                if self.typeShoot == 1:
                    self.timeShoot = 0
            if self.typeShoot == 0:
                bullet_enemy.BulletHell(self.rect.centerx, self.rect.centery, "ASSETS/SPRITE/BULLET/ENEMY/16.bmp",
                                        self.countBullet, 20)
                self.timeShoot = 0
        else:
            self.timeShoot += 1

    def update(self):
        if self.timeMove == self.timeMoveMax * 6 and not self.isMove:
            self.rect.x = scene_config.p.rect.x
            self.rect.y = -30
            self.isMove = True
        else:
            self.timeMove += 1
        if self.isMove:
            if self.Health <= 0:
                DEATH_PEOPLE.play()
                if self.luckS == 0:
                    if scene_config.p.EXP < 500:
                        exp.exp_item(self.rect.centerx, self.rect.centery, 0)
                else:
                    Ammo.ammo(self.rect.centerx, self.rect.centery)
                createEffectBodies(self.rect.centerx, self.rect.centery, bodies[randint(0, 2)])
                createEffectBodies(self.rect.centerx, self.rect.centery, "ASSETS/SPRITE/EFFECT/bodies/4.bmp")
                createEffectSimple(self.rect.x, self.rect.y, fileEffectOne, 6, 55, 1)
                scene_config.KILL_COUNT += 1
                self.timeMove = 0
                self.isMove = False
                self.rect.y = -200
                self.rect.x = -200
                self.Health = 1095
                ScoreSet = 45 + scene_config.p.MultipleScore
                scene_config.Score += 45 + scene_config.p.MultipleScore
                create_item.createTXT(90
                                      , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
            self.shoot()
            if self.rect.y > 695:
                self.timeMove = 0
                self.isMove = False
                self.rect.y = -900
                self.rect.x = -900
                self.Health = 1090
            self.rect.y += 2
