from random import randint

import pygame

from ITEM import exp, Ammo, create_item
from collision import createEffectBodies, bodies, createEffectSimple, fileEffectOne
from config_script import createImage, WINDOWS_SIZE
from group_config import shota_group
from BULLET import bullet_enemy
from SCENE import scene_config
from sfx_compilation import SHOOT_ENEMY_FIRST, DEATH_PEOPLE


class eva(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/ENEMY/EVA/1.png")
        self.scale = randint(48, 59)
        self.image = pygame.transform.scale(self.image, (self.scale, self.scale))
        self.rect = self.image.get_rect(center=(-500, -500))

        self.type: int = 1

        self.Health: int = 250

        self.add(shota_group)

        self.timeUp: int = 0
        self.timeUpMax: int = randint(105, 185)

        self.timeShoot: int = 0
        self.preTimeShoot: int = 0
        self.shootTimeOther: int = 0
        self.isShoot: bool = False

        self.timeFrame: int = 0
        self.isFrameDown: bool = False

        self.speedDown: int = 0
        self.speedUp: int = 0

        self.switchPos: int = 0
        self.isSwitchPos: bool = False
        self.posMove: int = 0

        self.timeLive: int = 0

        self.Frame: int = 0

        self.isMove: bool = False

        self.timeMove: int = 0
        match scene_config.DIFFICULTY:
            case 0:
                self.timeMoveMax = self.timeMoveMax = randint(990, 4590)
            case 1:
                self.timeMoveMax = self.timeMoveMax = randint(895, 4495)
            case 2:
                self.timeMoveMax = self.timeMoveMax = randint(675, 3895)
        for i in shota_group:
            if i.timeMoveMax == self.timeMoveMax:
                match scene_config.DIFFICULTY:
                    case 0:
                        self.timeMoveMax = self.timeMoveMax = randint(790, 4490)
                    case 1:
                        self.timeMoveMax = self.timeMoveMax = randint(895, 4495)
                    case 2:
                        self.timeMoveMax = self.timeMoveMax = randint(675, 3890)

    def createBullet(self):
        if self.preTimeShoot >= 200:
            if self.timeShoot < 45 and self.isShoot:
                self.timeShoot += 1
                if self.timeShoot > 45:
                    self.isShoot = False
                match self.shootTimeOther:
                    case 11:
                        SHOOT_ENEMY_FIRST.play()
                        if scene_config.DIFFICULTY == 0:
                            bullet_enemy.BulletHell(self.rect.x + 28, self.rect.y + 25,
                                                    "ASSETS/SPRITE/BULLET/ENEMY/10.bmp",
                                                    1)
                            bullet_enemy.BulletHell(self.rect.x + 28, self.rect.y + 25,
                                                    "ASSETS/SPRITE/BULLET/ENEMY/10.bmp",
                                                    5)
                        if scene_config.DIFFICULTY == 1:
                            bullet_enemy.BulletHell(self.rect.x + 28, self.rect.y + 25,
                                                    "ASSETS/SPRITE/BULLET/ENEMY/10.bmp",
                                                    1)
                            bullet_enemy.BulletHell(self.rect.x + 28, self.rect.y + 25,
                                                    "ASSETS/SPRITE/BULLET/ENEMY/10.bmp",
                                                    3)
                            bullet_enemy.BulletHell(self.rect.x + 28, self.rect.y + 25,
                                                    "ASSETS/SPRITE/BULLET/ENEMY/10.bmp",
                                                    5)
                            bullet_enemy.BulletHell(self.rect.x + 28, self.rect.y + 25,
                                                    "ASSETS/SPRITE/BULLET/ENEMY/10.bmp",
                                                    7)
                        if scene_config.DIFFICULTY == 2:
                            for i in range(9):
                                bullet_enemy.BulletHell(self.rect.x + 28, self.rect.y + 25, "ASSETS/SPRITE/BULLET/ENEMY/10.bmp",
                                                        i)
                        self.shootTimeOther = 0
                    case _:
                        self.shootTimeOther += 1
            else:
                if self.timeShoot > 0:
                    self.timeShoot -= 1
                else:
                    self.isShoot = True
        else:
            self.preTimeShoot += 1

    def update(self):
        if self.timeMove == self.timeMoveMax * 6:
            if not self.isMove:
                self.rect.x = randint(135, 935)
                self.rect.y = 750
                self.isMove = True
        else:
            self.timeMove += 1
        if self.isMove:
            if self.Health <= 0:
                DEATH_PEOPLE.play()
                luckS = randint(0, 1)
                if luckS == 0:
                    if scene_config.p.EXP < 500:
                        exp.exp_item(self.rect.centerx, self.rect.centery, 0)
                    else:
                        Ammo.ammo(self.rect.centerx, self.rect.centery)
                    create_item.create_teleporter(self.rect.centerx, self.rect.centery)
                    createEffectBodies(self.rect.centerx, self.rect.centery, bodies[randint(0, 2)])
                    createEffectSimple(self.rect.x, self.rect.y, fileEffectOne, 6, 55, 1)
                    createEffectBodies(self.rect.centerx, self.rect.centery, "ASSETS/SPRITE/EFFECT/bodies/4.bmp")
                    scene_config.KILL_COUNT += 1
                    self.timeUp = 0
                    self.timeMove = 0
                    self.isMove = False
                    self.rect.y = -950
                    self.rect.x = -950
                    self.Health = 250
                    ScoreSet = 45 + scene_config.p.MultipleScore
                    scene_config.Score += 45 + scene_config.p.MultipleScore
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
            if self.Frame < 4:
                self.Frame += 1
                self.image = createImage("ASSETS/SPRITE/ENEMY/EVA/1.png")
                self.image = pygame.transform.scale(self.image, (self.scale, self.scale))
            else:
                self.image = createImage("ASSETS/SPRITE/ENEMY/EVA/2.png")
                self.image = pygame.transform.scale(self.image, (self.scale, self.scale))
                self.Frame = 0
            if self.timeLive <= 1450:
                self.createBullet()
                if self.timeUp < self.timeUpMax:
                    self.timeUp += 1
                    self.rect.y -= 3
                else:
                    if self.switchPos <= 125 and not self.isSwitchPos:
                        if self.switchPos >= 125:
                            self.posMove = randint(0, 1)
                            self.isSwitchPos = True
                        self.switchPos += 1
                        if self.timeFrame < 25 and self.isFrameDown:
                            self.timeFrame += 1
                            if self.timeFrame >= 25:
                                self.isFrameDown = False
                            if self.speedDown < 1:
                                self.speedDown += 0.1
                            self.rect.y += self.speedDown
                        else:
                            self.speedDown = 0
                            if self.timeFrame > 0:
                                self.timeFrame -= 1
                                if self.speedUp < 1:
                                    self.speedUp += 0.1
                                self.rect.y -= self.speedUp
                            else:
                                self.speedUp = 0
                                self.isFrameDown = True
                    else:
                        if self.switchPos > 0:
                            match self.posMove:
                                case 0:
                                    if self.rect.x > 85:
                                        self.rect.x -= 2
                                    else:
                                        self.posMove = 1
                                case 1:
                                    if self.rect.x < 1090:
                                        self.rect.x += 2
                                    else:
                                        self.posMove = 0
                            self.switchPos -= 1
                        else:
                            self.isSwitchPos = False
                self.timeLive += 1
            else:
                match self.posMove:
                    case 0:
                        if self.rect.x > -25:
                            self.rect.x -= 3
                        else:
                            self.timeUp = 0
                            self.Health = 250
                            self.timeMove = 0
                            self.isMove = False
                            self.rect.y = -200
                            self.rect.x = -200
                    case 1:
                        if self.rect.x < 1290:
                            self.rect.x += 3
                        else:
                            self.timeUp = 0
                            self.Health = 250
                            self.timeMove = 0
                            self.isMove = False
                            self.rect.y = -200
                            self.rect.x = -200
