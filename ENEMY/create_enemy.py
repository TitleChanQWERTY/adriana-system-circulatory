from ENEMY import rosa_ball, shota, shutle, ball_soul, magical_girl, card_girl, Eva, Fly_Blood, maid, protecter


def createBall(type=0):
    return rosa_ball.ball(type)


def createShota():
    return shota.Shota()


def createShutlePatrol():
    return shutle.patrol()


def createBallSoul():
    return ball_soul.Ball_soul()


def createMagicalGirl():
    return magical_girl.type_one()


def createShutleRocket():
    return shutle.rocket()


def createShutleItem():
    return shutle.item_shutle()


def createDrone():
    return shutle.Drone_Citadel()


def createCardGirl():
    return card_girl.card_girl()


def createEva():
    return Eva.eva()


def createFly():
    return Fly_Blood.fly()


def createWitcher():
    return magical_girl.Witcher()


def createMaid():
    return maid.Simple()


def createProtector():
    protecter.Protecter()
