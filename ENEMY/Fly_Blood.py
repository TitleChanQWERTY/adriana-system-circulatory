from random import randint

import pygame
from pygame import BLEND_RGB_ADD

from glow import createCircle
from group_config import ball_rosa_group
from BULLET import bullet_enemy
from config_script import createImage, sc
from SCENE import scene_config


class fly(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.filename = ("ASSETS/SPRITE/ENEMY/Fly_Blood/1.png", "ASSETS/SPRITE/ENEMY/Fly_Blood/2.png")
        self.image = createImage(self.filename[0])
        self.image = pygame.transform.scale(self.image, (21, 21))
        self.rect = self.image.get_rect(center=(-500, -500))

        self.add(ball_rosa_group)

        self.isAnim: bool = False

        self.size = 21
        self.radius = self.size * 2

        self.posMove: int = randint(0, 3)
        self.Frame: int = 1
        self.setPos: int = 0

        self.Alpha: int = 0
        self.image.set_alpha(self.Alpha)

        self.timeShoot: int = 0

        self.isMove: bool = False
        self.timeMove: int = 0
        match scene_config.DIFFICULTY:
            case 0:
                self.timeMoveMax = self.timeMoveMax = randint(120, 2985)
            case 1:
                self.timeMoveMax = self.timeMoveMax = randint(105, 2405)
            case 2:
                self.timeMoveMax = self.timeMoveMax = randint(95, 1915)

    def shoot(self):
        if self.timeShoot >= 150:
            if scene_config.p.rect.centery > self.rect.centery:
                bullet_enemy.BulletHell(self.rect.x + 15, self.rect.y + 27, "ASSETS/SPRITE/BULLET/ENEMY/13.bmp", 1, 22, 0, 10)
                bullet_enemy.BulletHell(self.rect.x + 15, self.rect.y + 27, "ASSETS/SPRITE/BULLET/ENEMY/13.bmp", 2, 23, 0, 10)
                bullet_enemy.BulletHell(self.rect.x + 15, self.rect.y + 27, "ASSETS/SPRITE/BULLET/ENEMY/13.bmp", 8, 23, 0, 10)
            else:
                bullet_enemy.BulletHell(self.rect.x + 15, self.rect.y + 27, "ASSETS/SPRITE/BULLET/ENEMY/13.bmp", 5, 22, 0, 10)
                bullet_enemy.BulletHell(self.rect.x + 15, self.rect.y + 27, "ASSETS/SPRITE/BULLET/ENEMY/13.bmp", 4, 23, 0, 10)
                bullet_enemy.BulletHell(self.rect.x + 15, self.rect.y + 27, "ASSETS/SPRITE/BULLET/ENEMY/13.bmp", 6, 23, 0, 10)
            self.timeShoot = 0
        else:
            self.timeShoot += 1

    def update(self):
        if self.timeMove == self.timeMoveMax*6:
            if not self.isMove:
                self.rect.x = randint(120, 960)
                self.rect.y = randint(115, 566)
                self.isMove = True
        else:
            self.timeMove += 1
        if self.isMove:
            if self.Alpha < 285:
                self.Alpha += 2
                self.image.set_alpha(self.Alpha)
            else:
                if scene_config.isLight:
                    if self.radius < self.size * 2 - 1:
                        self.radius += 6
                    else:
                        self.radius = 4
                    sc.blit(createCircle(self.radius, (25, 0, 0)),
                            (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                            special_flags=BLEND_RGB_ADD)
                self.shoot()
                match self.Frame:
                    case 2:
                        self.image = createImage(self.filename[0])
                        self.image = pygame.transform.scale(self.image, (21, 21))
                    case 4:
                        self.image = createImage(self.filename[1])
                        self.image = pygame.transform.scale(self.image, (21, 21))
                        self.Frame = 0
                self.Frame += 1
                if self.setPos >= 135:
                    self.setPos = 0
                    self.posMove: int = randint(0, 3)
                else:
                    self.setPos += 1
                self.move()

    def move(self):
        match self.posMove:
            case 0:
                if self.rect.x < 1090 and self.rect.y > 35:
                    self.rect.x += 2
                    self.rect.y -= 2
                else:
                    self.posMove = 1
            case 1:
                if self.rect.x > 35 and self.rect.y < 600:
                    self.rect.x -= 2
                    self.rect.y += 2
                else:
                    self.posMove = 0
            case 2:
                if self.rect.x > 35 and self.rect.y > 35:
                    self.rect.x -= 2
                    self.rect.y -= 2
                else:
                    self.posMove = 3
            case 3:
                if self.rect.x < 1090 and self.rect.y < 600:
                    self.rect.x += 2
                    self.rect.y += 2
                else:
                    self.posMove = 2
