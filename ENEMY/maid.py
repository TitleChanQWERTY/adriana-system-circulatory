import pygame

from ITEM import exp, Ammo, create_item
from collision import createEffectBodies, bodies, createEffectSimple, fileEffectOne
from group_config import shota_group
from random import randint
from BULLET import bullet_enemy
from SCENE import scene_config

from effect_system import effect_simple

from config_script import createImage, WINDOWS_SIZE
from sfx_compilation import DEATH_PEOPLE


class Simple(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage("ASSETS/SPRITE/ENEMY/MAID/1.png")
        self.image = pygame.transform.scale(self.image, (40, 54))
        self.rect = self.image.get_rect(center=(-400, -400))
        self.add(shota_group)

        self.type: int = 2

        self.Health: int = 1060

        self.isTakeDamage: bool = False
        self.isTakeDamageFrame: float = 0

        self.speed: int = randint(1, 5)
        self.maxTimeDown = randint(50, 95)
        self.timeDown: int = 0

        self.timePos: int = 0

        self.pos: int = 0

        self.timeShoot: int = 0
        self.maxShoot: int = randint(150, 250)

        self.timeLeave: int = 0
        self.posLeave = randint(0, 1)

        self.isMove: bool = False

        self.luckS = randint(0, 1)

        self.timeMove: int = 0
        match scene_config.DIFFICULTY:
            case 0:
                self.timeMoveMax = self.timeMoveMax = randint(195, 2700)
            case 1:
                self.timeMoveMax = self.timeMoveMax = randint(165, 2255)
            case 2:
                self.timeMoveMax = self.timeMoveMax = randint(137, 2050)
        for i in shota_group:
            if i.timeMoveMax == self.timeMoveMax:
                match scene_config.DIFFICULTY:
                    case 0:
                        self.timeMoveMax = self.timeMoveMax = randint(195, 2700)
                    case 1:
                        self.timeMoveMax = self.timeMoveMax = randint(165, 2255)
                    case 2:
                        self.timeMoveMax = self.timeMoveMax = randint(137, 2050)

    def createMuzzle(self, x, y, pos=0):
        return effect_simple(x, y, "ASSETS/SPRITE/EFFECT/Muzzle Flash/1_5.png", pos, 44)

    def createBullet(self):
        if self.timeShoot > self.maxShoot:
            for i in range(19):
                bullet_enemy.BulletHell(self.rect.x + 28, self.rect.y + 25, "ASSETS/SPRITE/BULLET/ENEMY/12.png",
                                        i, 17, randint(5, 155))
            self.timeShoot = 0
            self.maxShoot = randint(255, 750)
        else:
            self.timeShoot += 1

    def update(self):
        if self.timeMove == self.timeMoveMax * 6 and not self.isMove:
            self.rect.x = randint(200, 900)
            self.rect.y = -35
            self.isMove = True
        else:
            self.timeMove += 1
        if self.isMove:
            if self.Health <= 0:
                DEATH_PEOPLE.play()
                if self.luckS == 0:
                    if scene_config.p.EXP < 500:
                        exp.exp_item(self.rect.centerx, self.rect.centery, 0)
                else:
                    Ammo.ammo(self.rect.centerx, self.rect.centery)
                createEffectBodies(self.rect.centerx, self.rect.centery, bodies[randint(0, 2)])
                createEffectBodies(self.rect.centerx, self.rect.centery, "ASSETS/SPRITE/EFFECT/bodies/4.bmp")
                createEffectSimple(self.rect.x, self.rect.y, fileEffectOne, 6, 55, 1)
                scene_config.KILL_COUNT += 1
                self.timePos = 0
                self.timeMove = 0
                self.isMove = False
                self.rect.y = -200
                self.rect.x = -200
                self.Health = 1070
                ScoreSet = 45 + scene_config.p.MultipleScore
                scene_config.Score += 45 + scene_config.p.MultipleScore
                create_item.createTXT(90
                                      , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
            if self.timeDown < self.maxTimeDown:
                self.rect.y += self.speed
                self.timeDown += 1
            else:
                self.createBullet()
                if self.timePos >= 35:
                    self.timePos = 0
                    self.pos = randint(0, 5)
                else:
                    self.timePos += 1
                self.move()

            if self.isTakeDamage:
                if self.isTakeDamageFrame < 2:
                    self.isTakeDamageFrame += 0.5
                    self.image = createImage("ASSETS/SPRITE/ENEMY/MAID/damage.png")
                    self.image = pygame.transform.scale(self.image, (40, 54))
                else:
                    self.image = createImage("ASSETS/SPRITE/ENEMY/MAID/1.png")
                    self.image = pygame.transform.scale(self.image, (40, 54))
                    self.isTakeDamageFrame = 0
                    self.isTakeDamage = False

    def move(self):
        self.image = createImage("ASSETS/SPRITE/ENEMY/MAID/1.png")
        self.image = pygame.transform.scale(self.image, (40, 54))
        if self.timeLeave < 1155:
            self.timeLeave += 1
            match self.pos:
                case 0:
                    if self.rect.y >= 35 and self.rect.x >= 25:
                        if not self.isTakeDamage:
                            self.image = createImage("ASSETS/SPRITE/ENEMY/MAID/2.png")
                            self.image = pygame.transform.scale(self.image, (40, 54))
                            self.image = pygame.transform.flip(self.image, True, False)
                        self.rect.y -= self.speed
                        self.rect.x -= self.speed
                    else:
                        self.pos = 1
                case 1:
                    if self.rect.y <= 600 and self.rect.x <= 1200:
                        if not self.isTakeDamage:
                            self.image = createImage("ASSETS/SPRITE/ENEMY/MAID/2.png")
                            self.image = pygame.transform.scale(self.image, (40, 54))
                        self.rect.y += self.speed
                        self.rect.x += self.speed
                    else:
                        self.pos = 0
                case 2:
                    if self.rect.y <= 600:
                        self.rect.y += self.speed
                    else:
                        self.pos = 3
                case 3:
                    if self.rect.y >= 35:
                        self.rect.y -= self.speed
                    else:
                        self.pos = 2
        else:
            match self.posLeave:
                case 0:
                    if self.rect.x >= -30:
                        if not self.isTakeDamage:
                            self.image = createImage("ASSETS/SPRITE/ENEMY/MAID/2.png")
                            self.image = pygame.transform.scale(self.image, (40, 54))
                            self.image = pygame.transform.flip(self.image, True, False)
                        self.rect.x -= self.speed
                    else:
                        self.timeMove = 0
                        self.timeDown = 0
                        self.isMove = False
                        self.rect.y = -400
                        self.rect.x = -400
                        self.Health = 1065
                case 1:
                    if self.rect.x <= 1295:
                        if not self.isTakeDamage:
                            self.image = createImage("ASSETS/SPRITE/ENEMY/MAID/2.png")
                            self.image = pygame.transform.scale(self.image, (40, 54))
                        self.rect.x += self.speed
                    else:
                        self.timeMove = 0
                        self.timeDown = 0
                        self.timePos = 0
                        self.isMove = False
                        self.rect.y = -400
                        self.rect.x = -400
                        self.Health = 1065
