import pygame

from ITEM import exp, Ammo, create_item
from collision import createEffectBodies, bodies, createEffectSimple, fileEffectOne
from group_config import shota_group
from random import randint
from BULLET import bullet_enemy
from SCENE import scene_config

from effect_system import effect_simple

from config_script import createImage, WINDOWS_SIZE


class Shota(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage("ASSETS/SPRITE/ENEMY/Shota/1.png")
        self.Size: int = 66
        self.image = pygame.transform.scale(self.image, (self.Size, self.Size))
        self.rect = self.image.get_rect(center=(-400, -400))
        self.add(shota_group)

        self.type: int = 0

        self.Health: int = randint(75, 85)

        match scene_config.DIFFICULTY:
            case 1:
                self.Health: int = randint(85, 95)
            case 2:
                self.Health: int = randint(95, 115)

        self.isTakeDamage: bool = False
        self.isTakeDamageFrame: float = 0

        self.Frame: int = 0
        self.speed: int = randint(2, 4)
        self.maxTimeDown = randint(180, 195)
        self.timeDown: int = 0

        self.timePos: int = 0

        self.pos: int = 0

        self.spawnBullet: int = 0

        self.timeLeave: int = 0
        self.posLeave = randint(0, 1)

        self.isMove: bool = False

        self.luckS = randint(0, 1)

        self.timeMove: int = 0
        match scene_config.DIFFICULTY:
            case 0:
                self.timeMoveMax = self.timeMoveMax = randint(120, 4750)
            case 1:
                self.timeMoveMax = self.timeMoveMax = randint(100, 3650)
            case 2:
                self.timeMoveMax = self.timeMoveMax = randint(35, 1205)
        for i in shota_group:
            if i.timeMoveMax == self.timeMoveMax:
                match scene_config.DIFFICULTY:
                    case 0:
                        self.timeMoveMax = self.timeMoveMax = randint(125, 4800)
                    case 1:
                        self.timeMoveMax = self.timeMoveMax = randint(100, 3700)
                    case 2:
                        self.timeMoveMax = self.timeMoveMax = randint(45, 1200)

    def Animation(self):
        if not self.isTakeDamage:
            if self.pos != 2 or 1:
                match self.Frame:
                    case 10:
                        self.image = pygame.image.load_extended("ASSETS/SPRITE/ENEMY/Shota/1.png").convert_alpha()
                        self.image = pygame.transform.scale(self.image, (self.Size, self.Size))
                    case 50:
                        self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/Shota/2.png").convert_alpha()
                        self.image = pygame.transform.scale(self.image, (self.Size, self.Size))
                        self.Frame = 0
                self.Frame += 1

    def createMuzzle(self, x, y, pos=0):
        return effect_simple(x, y, "ASSETS/SPRITE/EFFECT/Muzzle Flash/1_5.png", pos, 44)

    def createBullet(self):
        match self.spawnBullet:
            case 43:
                self.spawnBullet = 0
                if scene_config.p.rect.centery > self.rect.centery:
                    self.createMuzzle(self.rect.x + 35, self.rect.y + 89)
                    bullet_enemy.pistolBullet(self.rect.centerx, self.rect.centery+10,
                                                         "ASSETS/SPRITE/BULLET/ENEMY/4.bmp", 11)
                else:
                    self.createMuzzle(self.rect.x + 35, self.rect.y + 7, 2)
                    bullet_enemy.pistolBullet(self.rect.centerx, self.rect.centery-10,
                                                         "ASSETS/SPRITE/BULLET/ENEMY/4.bmp", 11, 10, False, 1)
            case _:
                self.spawnBullet += 1

    def update(self):
        if self.timeMove == self.timeMoveMax * 6:
            if not self.isMove:
                self.rect.x = randint(200, 900)
                self.rect.y = -25
                self.isMove = True
        else:
            self.timeMove += 1
        if self.isMove:
            if self.Health <= 0:
                if self.luckS == 0:
                    if scene_config.p.EXP < 500:
                        exp.exp_item(self.rect.centerx, self.rect.centery, 0)
                else:
                    Ammo.ammo(self.rect.centerx, self.rect.centery)
                create_item.create_teleporter(self.rect.centerx, self.rect.centery)
                createEffectBodies(self.rect.centerx, self.rect.centery, bodies[randint(0, 2)])
                createEffectBodies(self.rect.centerx, self.rect.centery, "ASSETS/SPRITE/EFFECT/bodies/4.bmp")
                createEffectSimple(self.rect.x, self.rect.y, fileEffectOne, 6, 55, 1)
                scene_config.KILL_COUNT += 1
                self.timeMove = 0
                self.timeLeave = 0
                self.isMove = False
                self.rect.y = -950
                self.rect.x = -950
                self.Health = randint(75, 85)

                match scene_config.DIFFICULTY:
                    case 1:
                        self.Health = randint(90, 105)
                    case 2:
                        self.Health = randint(100, 125)
                ScoreSet = 45 + scene_config.p.MultipleScore
                scene_config.Score += 45 + scene_config.p.MultipleScore
                create_item.createTXT(90
                                        , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
            if self.isTakeDamage:
                if self.isTakeDamageFrame < 1.5:
                    self.isTakeDamageFrame += 0.5
                    self.image = createImage("ASSETS/SPRITE/ENEMY/Shota/Damage.png")
                    self.image = pygame.transform.scale(self.image, (69, 69))
                else:
                    self.image = createImage("ASSETS/SPRITE/ENEMY/Shota/1.png")
                    self.image = pygame.transform.scale(self.image, (self.Size, self.Size))
                    self.isTakeDamageFrame = 0
                    self.isTakeDamage = False
            self.Animation()
            if self.timeDown < self.maxTimeDown:
                self.rect.y += self.speed
                self.timeDown += 1
            else:
                self.createBullet()
                if self.timePos >= 35:
                    self.timePos = 0
                    self.pos = randint(0, 5)
                else:
                    self.timePos += 1
                self.move()

    def move(self):
        if self.timeLeave < 1155:
            self.timeLeave += 1
            match self.pos:
                case 0:
                    if self.rect.y >= 35 and self.rect.x >= 25:
                        if not self.isTakeDamage:
                            self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/Shota/Right.png").convert_alpha()
                            self.image = pygame.transform.scale(self.image, (self.Size-6, self.Size))
                            self.image = pygame.transform.flip(self.image, True, False)
                        self.rect.y -= self.speed
                        self.rect.x -= self.speed
                    else:
                        self.pos = 1
                case 1:
                    if self.rect.y <= 600 and self.rect.x <= 1200:
                        if not self.isTakeDamage:
                            self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/Shota/Right.png").convert_alpha()
                            self.image = pygame.transform.scale(self.image, (self.Size-6, self.Size))
                        self.rect.y += self.speed
                        self.rect.x += self.speed
                    else:
                        self.pos = 0
                case 2:
                    if self.rect.y <= 600:
                        self.rect.y += self.speed
                    else:
                        self.pos = 3
                case 3:
                    if self.rect.y >= 35:
                        self.rect.y -= self.speed
                    else:
                        self.pos = 2
        else:
            match self.posLeave:
                case 0:
                    if self.rect.x >= -30:
                        if not self.isTakeDamage:
                            self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/Shota/Right.png").convert_alpha()
                            self.image = pygame.transform.scale(self.image, (self.Size-6, self.Size))
                            self.image = pygame.transform.flip(self.image, True, False)
                        self.rect.x -= self.speed
                    else:
                        self.timeMove = 0
                        self.timeDown = 0
                        self.timeLeave = 0
                        self.isMove = False
                        self.rect.y = -200
                        self.rect.x = -200
                case 1:
                    if self.rect.x <= 1295:
                        if not self.isTakeDamage:
                            self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/Shota/Right.png").convert_alpha()
                            self.image = pygame.transform.scale(self.image, (self.Size-6, self.Size))
                        self.rect.x += self.speed
                    else:
                        self.timeMove = 0
                        self.timeDown = 0
                        self.timePos = 0
                        self.timeLeave = 0
                        self.isMove = False
                        self.rect.y = -200
                        self.rect.x = -200
