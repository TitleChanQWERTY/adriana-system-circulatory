from random import randint
from BULLET import bullet_enemy

import pygame
from group_config import ball_rosa_group
from SCENE import scene_config
from config_script import createImage

bullet = ["ASSETS/SPRITE/BULLET/ENEMY/simple/1.png", "ASSETS/SPRITE/BULLET/ENEMY/simple/2.png"]


class ball(pygame.sprite.Sprite):
    def __init__(self, type):
        pygame.sprite.Sprite.__init__(self)
        self.filename = ("ASSETS/SPRITE/ENEMY/ballRosaAdriana.bmp", "ASSETS/SPRITE/ENEMY/ballRosa.bmp")
        self.typeBall = randint(0, 1)
        self.image = createImage(self.filename[self.typeBall])
        self.image = pygame.transform.scale(self.image, (29, 30))
        self.rect = self.image.get_rect(center=(-200, -200))
        self.add(ball_rosa_group)
        self.MoveSelect: int = randint(0, 2)
        self.timerEnd: int = randint(105, 555)

        self.time: int = 0

        self.isMove: bool = False

        self.timeMove: int = 0
        if type == 0:
            match scene_config.DIFFICULTY:
                case 0:
                    self.timeMoveMax = self.timeMoveMax = randint(25, 4800)
                case 1:
                    self.timeMoveMax = self.timeMoveMax = randint(15, 2800)
                case 2:
                    self.timeMoveMax = self.timeMoveMax = randint(5, 2000)
            for i in ball_rosa_group:
                if i.timeMoveMax == self.timeMoveMax:
                    match scene_config.DIFFICULTY:
                        case 0:
                            self.timeMoveMax = self.timeMoveMax = randint(25, 4800)
                        case 1:
                            self.timeMoveMax = self.timeMoveMax = randint(15, 2200)
                        case 2:
                            self.timeMoveMax = self.timeMoveMax = randint(5, 1905)
        else:
            self.timeMoveMax = self.timeMoveMax = randint(30, 70)

    def createBullet(self):
        match self.time:
            case 105:
                self.time = 50
                match self.typeBall:
                    case 1:
                        return bullet_enemy.simpleBullet(self.rect.x + 14, self.rect.y + 25, bullet, True, 5)
                    case 0:
                        return bullet_enemy.pistolBullet(self.rect.x + 14, self.rect.y + 25,
                                                         "ASSETS/SPRITE/BULLET/ENEMY/4.bmp")
            case _:
                self.time += 1

    def update(self):
        if self.timeMove == self.timeMoveMax * 6:
            if not self.isMove:
                self.rect.x = randint(135, 955)
                self.rect.y = -5
                self.isMove = True
        else:
            self.timeMove += 1
        if self.isMove:
            self.createBullet()
            match self.MoveSelect:
                case 0:
                    if self.rect.y <= 685:
                        self.rect.y += 2
                    else:
                        self.timeMove = 0
                        self.isMove = False
                        self.rect.y = -200
                        self.rect.x = -200
                case 1:
                    if self.rect.y < self.timerEnd:
                        self.rect.y += 2
                    else:
                        if self.rect.x >= 5:
                            self.rect.x -= 2
                        else:
                            self.timeMove = 0
                            self.isMove = False
                            self.rect.y = -200
                            self.rect.x = -200
                case 2:
                    if self.rect.y < self.timerEnd:
                        self.rect.y += 2
                    else:
                        if self.rect.x <= 1285:
                            self.rect.x += 2
                        else:
                            self.timeMove = 0
                            self.isMove = False
                            self.rect.y = -200
                            self.rect.x = -200
