from random import randint

import pygame
from pygame import BLEND_RGB_ADD

from SCENE import scene_config
from glow import createCircle
from group_config import effect_group
from config_script import createImage, sc


class Particle(pygame.sprite.Sprite):
    def __init__(self, x, y, size, filename, color):
        super().__init__()
        self.image = createImage(filename)
        self.image = pygame.transform.scale(self.image, size)
        self.rect = self.image.get_rect(center=(x, y))
        self.add(effect_group)

        self.MaxTime: int = randint(100, 300)
        self.time: int = 0
        self.radius = size[0] - 6

        self.size = size
        self.filename = filename

        self.posMoveX = randint(-10, 35) / 9 - 1
        self.posY = randint(0, 1)

        if self.posY == 1:
            self.posMoveY = randint(0, 6)
        else:
            self.posMoveY = randint(-6, 0)

        self.x, self.y = x, y

        self.angle: int = 0
        self.angleRot = randint(-10, 45) / 5

        self.color = color

    def update(self):
        if scene_config.isLight:
            sc.blit(createCircle(self.radius, self.color),
                    (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                    special_flags=BLEND_RGB_ADD)
        self.y += self.posMoveY+1
        self.x += self.posMoveX
        self.rect = self.image.get_rect(center=(self.x, self.y))
        self.time += 1
        if self.time >= self.MaxTime or self.rect.y > 690 or self.rect.y < 5:
            self.kill()


def particleSystem(x, y, size, count, filename, color=(35, 35, 35)):
    for i in range(count):
        Particle(x, y, size, filename, color)


class particle_simple(pygame.sprite.Sprite):
    def __init__(self, x, y, pos, filename="ASSETS/SPRITE/EFFECT/2/1.png", isAnim=True, size=20):
        pygame.sprite.Sprite.__init__(self)
        self.filename = filename
        self.image = createImage(self.filename)
        self.image = pygame.transform.scale(self.image, (size, size))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(effect_group)

        self.Frame: int = 0

        self.TimeKill: int = 0

        self.Alpha: int = 250

        self.pos: int = pos

        self.isAnim: bool = isAnim

        self.size: int = size

    def update(self):
        if self.isAnim:
            match self.Frame:
                case 10:
                    self.image = createImage("ASSETS/SPRITE/EFFECT/2/1.png")
                case 20:
                    self.image = createImage("ASSETS/SPRITE/EFFECT/2/2.png")
                    self.Frame = 0
            self.Frame += 1
            self.Alpha -= 7
            self.image.set_alpha(self.Alpha)
            self.image = pygame.transform.scale(self.image, (self.size, self.size))
        else:
            self.Alpha -= 10
            self.image.set_alpha(self.Alpha)

        match self.TimeKill:
            case 95:
                self.kill()
            case _:
                self.TimeKill += 1

        match self.pos:
            case 0:
                self.rect.y -= 10
            case 1:
                self.rect.y += 10
            case 2:
                self.rect.x -= 10
            case 3:
                self.rect.x += 10
