from random import randint

import pygame
from pygame import BLEND_RGB_ADD

from glow import createCircle
from group_config import simple_bullet, pistol_bullet, anyBullet
from SCENE import scene_config
from config_script import createImage, sc


class RandomBullet(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, size):
        super().__init__()
        self.moveInt = randint(0, 3)
        self.image = createImage(filename)
        self.image = pygame.transform.scale(self.image, (size, size))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(anyBullet)

        self.radius = size

        self.timeSet: int = 0

    def update(self):
        if scene_config.isLight:
            sc.blit(createCircle(self.radius, (40, 0, 0)),
                    (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                    special_flags=BLEND_RGB_ADD)
        self.move()
        if self.timeSet > 55:
            self.moveInt = randint(0, 3)
            self.timeSet = 0
        self.timeSet += 1

    def move(self):
        match self.moveInt:
            case 0:
                if self.rect.x < 1290 and self.rect.y > 0:
                    self.rect.x += 2
                    self.rect.y -= 2
                else:
                    self.kill()
            case 1:
                if self.rect.x > 0 and self.rect.y < 750:
                    self.rect.x -= 2
                    self.rect.y += 2
                else:
                    self.kill()
            case 2:
                if self.rect.x > 0 and self.rect.y > 0:
                    self.rect.x -= 2
                    self.rect.y -= 2
                else:
                    self.kill()
            case 3:
                if self.rect.x < 1290 and self.rect.y < 750:
                    self.rect.x += 2
                    self.rect.y += 2
                else:
                    self.kill()


class BulletHell(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, posFly, size=10, time=0, exta_light=5):
        super().__init__()
        self.image = pygame.image.load(filename).convert_alpha()
        self.image = pygame.transform.scale(self.image, (size, size))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(simple_bullet)

        self.filename = filename
        self.x = x
        self.y = y

        self.size: int = size
        self.radius: int = size * 2 - exta_light

        self.pos: int = posFly

        self.time: int = 0
        self.maxTime: int = time

        self.xForce: int = 0

        self.extra_light = exta_light

    def update(self):
        if scene_config.isLight:
            sc.blit(createCircle(self.radius, (35, 0, 0)),
                    (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                    special_flags=BLEND_RGB_ADD)
        if self.time > self.maxTime:
            match self.pos:
                case 1:
                    if self.rect.y < 705:
                        self.rect.y += 8
                    else:
                        self.kill()
                case 2:
                    if self.rect.y < 705:
                        self.y += 7
                        if self.xForce <= 6:
                            self.xForce += 1
                        self.x -= self.xForce
                        self.image = pygame.image.load(self.filename).convert_alpha()
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                        self.image = pygame.transform.rotate(self.image, -30)
                        self.rect = self.image.get_rect(center=(self.x, self.y))
                    else:
                        self.kill()
                case 3:
                    if self.rect.x > -10:
                        self.x -= 8
                        self.image = pygame.image.load(self.filename).convert_alpha()
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                        self.image = pygame.transform.rotate(self.image, -90)
                        self.rect = self.image.get_rect(center=(self.x, self.y))
                    else:
                        self.kill()
                case 4:
                    if self.rect.y > -10:
                        self.y -= 7
                        if self.xForce <= 6:
                            self.xForce += 1
                        self.x -= self.xForce
                        self.image = pygame.image.load(self.filename).convert_alpha()
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                        self.image = pygame.transform.rotate(self.image, -130)
                        self.rect = self.image.get_rect(center=(self.x, self.y))
                    else:
                        self.kill()
                case 5:
                    if self.rect.y > -10:
                        self.rect.y -= 8
                        self.image = pygame.image.load(self.filename).convert_alpha()
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                        self.image = pygame.transform.flip(self.image, False, True)
                    else:
                        self.kill()
                case 6:
                    if self.rect.y > -10:
                        self.y -= 7
                        if self.xForce <= 6:
                            self.xForce += 1
                        self.x += self.xForce
                        self.image = pygame.image.load(self.filename).convert_alpha()
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                        self.image = pygame.transform.rotate(self.image, 130)
                        self.rect = self.image.get_rect(center=(self.x, self.y))
                    else:
                        self.kill()
                case 7:
                    if self.rect.x < 1235:
                        self.x += 8
                        self.image = pygame.image.load(self.filename).convert_alpha()
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                        self.image = pygame.transform.rotate(self.image, -90)
                        self.rect = self.image.get_rect(center=(self.x, self.y))
                    else:
                        self.kill()
                case 8:
                    if self.rect.y < 725:
                        self.y += 7
                        if self.xForce <= 6:
                            self.xForce += 1
                        self.x += self.xForce
                        self.image = pygame.image.load(self.filename).convert_alpha()
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                        self.image = pygame.transform.rotate(self.image, 30)
                        self.rect = self.image.get_rect(center=(self.x, self.y))
                    else:
                        self.kill()
                case _:
                    self.kill()
        else:
            self.time += 1


class simpleBullet(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, isAnim=False, speed=3):
        super().__init__()
        if isAnim:
            self.filename: [] = filename
            self.image = createImage(self.filename[0])
            self.image = pygame.transform.scale(self.image, (16, 16))
        else:
            self.filename = filename
            self.image = createImage(self.filename)
            self.image = pygame.transform.scale(self.image, (9, 9))
            self.image = pygame.transform.flip(self.image, False, True)
        self.rect = self.image.get_rect(center=(x, y))
        self.add(simple_bullet)

        self.radius = 16 * 2

        self.speed: int = speed

        self.Frame: int = 0

        self.isAnim: bool = isAnim

        self.posRun: int = 0

        self.autoAim: int = randint(0, 1)

        if self.autoAim == 1:
            if self.rect.centerx < scene_config.p.rect.centerx and self.rect.centery < scene_config.p.rect.centery:
                self.posRun = 1
            if self.rect.centerx > scene_config.p.rect.centerx and self.rect.centery < scene_config.p.rect.centery:
                self.posRun = 2

        self.timeAutoAim = 0

    def update(self):
        if scene_config.isLight:
            sc.blit(createCircle(self.radius, (20, 0, 0)),
                    (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                    special_flags=BLEND_RGB_ADD)
        if self.isAnim:
            if self.rect.y <= 750:
                self.rect.y += self.speed
            else:
                self.kill()
            if self.posRun == 1:
                self.rect.x += self.speed-1
            else:
                self.rect.x -= self.speed-1

            match self.Frame:
                case 11:
                    self.Frame = 0
                    self.image = createImage("ASSETS/SPRITE/BULLET/ENEMY/simple/1.png")
                    self.image = pygame.transform.scale(self.image, (16, 16))
                case 5:
                    self.image = createImage("ASSETS/SPRITE/BULLET/ENEMY/simple/2.png")
                    self.image = pygame.transform.scale(self.image, (16, 16))
            self.Frame += 1
        else:
            if self.rect.y <= 790:
                self.rect.y += self.speed + 2
            else:
                self.kill()


class Best_Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, pos, size=(15, 29)):
        super().__init__()
        self.image = pygame.image.load(filename).convert_alpha()
        self.image = pygame.transform.scale(self.image, size)
        self.rect = self.image.get_rect(center=(x, y))
        self.add(anyBullet)

        self.size = size[1] - 5

        self.radius = self.size * 2

        self.pos = pos

        match self.pos:
            case 2:
                self.image = pygame.transform.rotate(self.image, 90)
            case 3:
                self.image = pygame.transform.rotate(self.image, -90)

        self.speed = 6

    def update(self):
        match self.pos:
            case 0:
                if self.rect.y <= 675:
                    self.rect.y += self.speed
                else:
                    self.kill()
            case 1:
                if self.rect.y >= 7:
                    self.rect.y -= self.speed
                else:
                    self.kill()
            case 2:
                if self.rect.x >= 7:
                    self.rect.x += self.speed
                else:
                    self.kill()
            case 3:
                if self.rect.x <= 1265:
                    self.rect.x -= self.speed
                else:
                    self.kill()
        if scene_config.isLight:
            sc.blit(createCircle(self.radius, (25, 0, 0)),
                    (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                    special_flags=BLEND_RGB_ADD)


class pistolBullet(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, speed=5, size=11, flip=True, pos=0, type=0):
        pygame.sprite.Sprite.__init__(self)
        self.filename = filename
        self.image = createImage(self.filename)
        self.image = pygame.transform.scale(self.image, (size, size + 1))
        if type == 1: self.image = pygame.transform.scale(self.image, (size, size + size + 5))
        self.image = pygame.transform.flip(self.image, False, flip)
        self.rect = self.image.get_rect(center=(x, y))
        self.add(pistol_bullet)

        self.size = size

        self.radius = self.size * 2

        self.speed: int = speed

        self.pos = pos

    def update(self):
        if scene_config.isLight:
            sc.blit(createCircle(self.radius, (20, 0, 0)),
                    (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                    special_flags=BLEND_RGB_ADD)
        match self.pos:
            case 0:
                if self.rect.y <= 760:
                    self.rect.y += self.speed + 1
                else:
                    self.kill()
            case 1:
                if self.rect.y > 10:
                    self.rect.y -= self.speed + 1
                else:
                    self.kill()
