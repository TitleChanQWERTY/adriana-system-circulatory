import pygame

from config_script import createImage
from group_config import bomb_player
from SCENE import scene_config
from sfx_compilation import BOMB_BOOM


class AreaDie(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/BULLET/AreaDie.png")
        self.size: int = 7
        self.image = pygame.transform.scale(self.image, (self.size, self.size))
        self.x: int = x
        self.y: int = y
        self.rect = self.image.get_rect(center=(self.x, self.y))
        self.add(bomb_player)

        self.Alpha: int = 300

        self.timeLive: int = 0

    def update(self):
        if self.timeLive <= 36:
            self.timeLive += 1
            if self.size < 309:
                self.size += 28
            self.image = createImage("ASSETS/SPRITE/BULLET/AreaDie.png")
            self.image = pygame.transform.scale(self.image, (self.size, self.size))
            self.rect = self.image.get_rect(center=(self.x, self.y))
        else:
            if self.Alpha < 8:
                self.kill()
            else:
                self.Alpha -= 10
                self.image.set_alpha(self.Alpha)


class BombPlayer(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/ITEM/bomb.png")
        self.image = pygame.transform.scale(self.image, (20, 20))
        self.x: int = x
        self.y: int = y
        self.rect = self.image.get_rect(center=(self.x, self.y))
        self.add(bomb_player)

        self.isUp: bool = True

        self.angle: int = 0

        self.timeUp: int = 0

    def update(self):
        if self.timeUp < 30:
            self.timeUp += 1
            if self.rect.y > 10 and self.isUp:
                self.y -= 8
            else:
                self.isUp = False
                self.y += 22
            self.image = createImage("ASSETS/SPRITE/ITEM/bomb.png")
            self.image = pygame.transform.scale(self.image, (20, 20))
            self.rect = self.image.get_rect(center=(self.x, self.y))
            if scene_config.isRotateObject:
                self.angle += 25
                self.image = pygame.transform.rotate(self.image, self.angle)

        else:
            BOMB_BOOM.play()
            AreaDie(self.rect.x, self.rect.y)
            self.kill()
