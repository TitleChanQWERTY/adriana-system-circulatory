from random import randint

import pygame
from pygame import BLEND_RGB_ADD

from glow import createCircle
from SCENE import scene_config
from group_config import player_bullet
from config_script import createImage, sc


class bullet_start(pygame.sprite.Sprite):
    def __init__(self, x, y, speed, rot, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/1/1.bmp")
        self.image = pygame.transform.scale(self.image, (19, 19))

        self.pos: int = pos
        if self.pos == 1:
            self.rect = self.image.get_rect(center=(x - 1, y + 19))
        else:
            self.rect = self.image.get_rect(center=(x, y))

        self.add(player_bullet)
        self.speed: int = speed
        self.rot: int = rot
        self.Frame: int = 0

        self.radius = 20

    def update(self):
        if scene_config.isLight:
            sc.blit(createCircle(self.radius, (24, 27, 59)),
                    (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                    special_flags=BLEND_RGB_ADD)

        if self.pos == 0:
            if self.rot == 0:
                self.rect.y -= self.speed
            elif self.rot == 1:
                self.rect.y -= self.speed
                self.rect.x += self.speed - 3
            elif self.rot == 2:
                self.rect.y -= self.speed
                self.rect.x -= self.speed - 3
            if self.rect.y <= 11:
                self.kill()
        else:
            if self.rect.y > 655:
                self.kill()
            self.rect.y += self.speed
        match self.Frame:
            case 12:
                self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/1/1.bmp")
                self.image = pygame.transform.scale(self.image, (19, 19))
                self.Frame = 0
            case 6:
                self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/1/2.bmp")
                self.image = pygame.transform.scale(self.image, (19, 19))
        self.Frame += 1


class bullet_plasma(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/3.bmp")
        self.image = pygame.transform.scale(self.image, (14, 13))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(player_bullet)
        self.speedMove: int = 0

        self.radius = 6 * 2

    def update(self):
        if scene_config.isLight:
            sc.blit(createCircle(self.radius, (44, 77, 79)),
                    (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                    special_flags=BLEND_RGB_ADD)

        self.speedMove += 1
        self.rect.y -= self.speedMove
        if self.rect.y < 12:
            self.kill()


class bullet_pos_auto(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.add(player_bullet)
        self.pos: int = randint(0, 1)

        self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/6.png")
        self.image = pygame.transform.scale(self.image, (16, 16))
        self.rect = self.image.get_rect(center=(x, y))
        if self.pos == 0:
            self.image = pygame.transform.flip(self.image, True, False)

    def update(self):
        if self.pos == 0:
            self.rect.y -= 15
            self.rect.x -= 11
        else:
            self.rect.y -= 15
            self.rect.x += 11
        if self.rect.y <= 10:
            self.kill()


class bullet_pistol(pygame.sprite.Sprite):
    def __init__(self, x, y, pos=0, filename="ASSETS/SPRITE/BULLET/PLAYER/2/up.bmp", speed=20):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage(filename)
        self.image = pygame.transform.scale(self.image, (9, 10))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(player_bullet)
        self.pos: int = pos
        self.speed: int = speed

        self.radius: int = 16

    def update(self):
        if scene_config.isLight:
            sc.blit(createCircle(self.radius, (75, 75, 75)),
                    (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                    special_flags=BLEND_RGB_ADD)

        if self.rect.y < 16:
            self.kill()
        match self.pos:
            case 0:
                self.rect.y -= self.speed
            case 1:
                self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/2/left.png")
                self.image = pygame.transform.scale(self.image, (11, 11))
                self.rect.x -= self.speed + 1
                self.rect.y -= self.speed
            case 2:
                self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/2/right.png")
                self.image = pygame.transform.scale(self.image, (11, 11))
                self.rect.x += self.speed + 1
                self.rect.y -= self.speed


class bulletShotgun(pygame.sprite.Sprite):
    def __init__(self, x, y, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/10.bmp")
        self.image = pygame.transform.scale(self.image, (8, 9))
        self.rect = self.image.get_rect(center=(x, y))

        self.add(player_bullet)

        self.pos: int = pos

        self.xSpeed: int = 1

    def update(self):
        if self.rect.y < 12:
            self.kill()
        match self.pos:
            case 1:
                self.rect.y -= 16
                self.rect.x += self.xSpeed
            case 2:
                self.rect.y -= 16
                self.rect.x -= self.xSpeed
            case _:
                self.rect.y -= 18
        self.xSpeed += 1
        self.xSpeed = max(5, 0)
