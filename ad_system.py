from random import randint
from group_config import ad_group
from SCENE import scene_config
from config_script import createImage, config
from sfx_compilation import CLOSE_AD

import pygame

filename = ("ASSETS/SPRITE/UI/AD/1.bmp", "ASSETS/SPRITE/UI/AD/2.bmp", "ASSETS/SPRITE/UI/AD/3.bmp",
            "ASSETS/SPRITE/UI/AD/4.bmp", "ASSETS/SPRITE/UI/AD/5.bmp", "ASSETS/SPRITE/UI/AD/6.bmp",
            "ASSETS/SPRITE/UI/AD/7.bmp", "ASSETS/SPRITE/UI/AD/8.bmp")

isAnimAD = config.getboolean("OTHER", "ad_animation")


class ad(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.select = randint(0, len(filename)-1)
        self.image = createImage(filename[self.select])
        self.size: int = 16
        self.image = pygame.transform.scale(self.image, (self.size, self.size))
        self.x = randint(145, 1100)
        self.y = randint(100, 445)
        self.rect = self.image.get_rect(center=(self.x, self.y))
        self.add(ad_group)

        self.isAnimClose: bool = False
        self.joyIsClose: bool = False

    def update(self):

        key = pygame.key.get_pressed()

        if key[pygame.K_q] or self.joyIsClose:
            self.isAnimClose = True

        match self.isAnimClose:
            case True:
                scene_config.isFillAd = False
                if isAnimAD:
                    if self.size >= 27:
                        self.size -= 24
                        self.image = createImage(filename[self.select])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                        self.rect = self.image.get_rect(center=(self.x, self.y))
                    else:
                        scene_config.Score += 150
                        CLOSE_AD.play()
                        self.kill()
                else:
                    scene_config.Score += 145
                    self.kill()
            case False:
                scene_config.isFillAd = True
                if isAnimAD:
                    if self.size < 206:
                        self.size += 19
                        self.image = createImage(filename[self.select])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                        self.rect = self.image.get_rect(center=(self.x, self.y))
                else:
                    self.size = 206
                    self.image = createImage(filename[self.select])
                    self.image = pygame.transform.scale(self.image, (self.size, self.size))
