from random import randint

import pygame

import group_config
from SCENE import scene_config
from BULLET import bullet_player, player_bomb
from config_script import createImage, sc, WINDOWS_SIZE, LANGUAGE_SET, config
from sfx_compilation import SHOOT_PLAYER_FIRST, SHOOT_PLAYER_SECOND
from key_setup import PLAYER_MOVE, PLAYER_SHOOT
import itertools


class Player(pygame.sprite.Sprite):
    def __init__(self, x, y, speed, filename):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage(filename)
        self.image = pygame.transform.scale(self.image, (17, 27))
        self.rect = self.image.get_rect(center=(x, y))
        self.centerX = float(self.rect.centerx)
        self.centerY = float(self.rect.centery)
        self.speed: int = speed
        self.EXP: int = 0
        self.MultipleScore: int = 0
        self.Health: int = 300
        self.Bomb: int = randint(2, 4)
        self.Ammo: int = randint(900, 1000)
        self.damageBullet: int = randint(2, 5)
        self.ExtraDamageBullet: int = 0
        self.damageEnemySlow: int = 0

        self.isAutoRegen: bool = False
        self.timeRegen: int = 0

        self.autoBullet: bool = False
        self.timeAmmo: int = 0

        # Force
        self.isInertia = config.getboolean("OTHER", "inertia")
        self.ForceMove = [0, 0, 0, 0]

        self.timeShoot = [0, 0, 0, 0, 0, 0, 0]
        self.timeShootMax: int = 25

        self.FrameTXT: int = 0

        self.TEXT_SET_UA = ("МАКСИМАЛЬНЕ САЛО!", "МАКСИМАЛЬНИЙ РІВЕНЬ", "МАКСИМАЛЬНА ДУША", "ЗАПОВНЕНИЙ КРОВ'Ю")
        self.fonTxt = pygame.font.Font("ASSETS/FONT/Alice-Regular.ttf", 35)
        if LANGUAGE_SET == "EN":
            self.TEXT_SET_UA = ("MAX LEVEL!", "FULL CRAZY!", "LETS GO!", "MORE EXP PLEASE!")
        self.TextMax = self.fonTxt.render(self.TEXT_SET_UA[randint(0, len(self.TEXT_SET_UA) - 1)], False,
                                          (51, 255, 255))
        self.TextMax.set_alpha(0)
        self.AlphaTxt: int = 0

    def createBomb(self):
        self.Bomb -= 1
        player_bomb.BombPlayer(self.rect.centerx, self.rect.centery - 5)

    def update(self):
        if self.EXP < 0:
            self.EXP = 0
        if self.Ammo < 0:
            self.Ammo = 0

        key = pygame.key.get_pressed()

        if key[PLAYER_SHOOT[0]]:
            if self.timeShoot[0] > self.timeShootMax:
                if self.EXP <= 120 or self.Ammo < 1:
                    SHOOT_PLAYER_SECOND.play()
                if key[pygame.K_SPACE]:
                    self.timeShoot[0] = 0
                    bullet_player.bullet_start(self.rect.x + 8, self.rect.y + 10, 10, 0, 1)
                else:
                    if key[PLAYER_SHOOT[1]]:
                        self.timeShoot[0] = 0
                        bullet_player.bullet_start(self.rect.x + 8, self.rect.y + 10, 10, 2, 0)
                    elif key[PLAYER_SHOOT[2]]:
                        self.timeShoot[0] = 0
                        bullet_player.bullet_start(self.rect.x + 8, self.rect.y + 10, 10, 1, 0)
                    else:
                        self.timeShoot[0] = 0
                        bullet_player.bullet_start(self.rect.x + 8, self.rect.y + 10, 10, 0, 0)
            else:
                self.timeShoot[0] += 1
            if self.Ammo > 0:
                if self.EXP >= 250:
                    if self.timeShoot[2] >= 25:
                        self.Ammo -= 1
                        self.timeShoot[2] = 0
                        bullet_player.bullet_pos_auto(self.rect.x + 11, self.rect.y + 10)
                    else:
                        self.timeShoot[2] += 1

                if self.timeShoot[3] >= 5:
                    if self.EXP >= 300:
                        self.Ammo -= 1
                        self.timeShoot[3] = 0
                        bullet_player.bullet_pistol(self.rect.x + 3, self.rect.y + 25, 0,
                                                    "ASSETS/SPRITE/BULLET/PLAYER/7.bmp", 26)
                        bullet_player.bullet_pistol(self.rect.x + 16, self.rect.y + 25, 0,
                                                    "ASSETS/SPRITE/BULLET/PLAYER/7.bmp", 26)
                    if self.EXP >= 65:
                        self.Ammo -= 1
                        self.timeShoot[3] = 0
                        bullet_player.bullet_plasma(self.rect.x + 10, self.rect.y + 10)
                else:
                    self.timeShoot[3] += 1
                if self.EXP >= 120:
                    if self.timeShoot[5] >= 9:
                        self.Ammo -= 1
                        if key[PLAYER_SHOOT[2]]:
                            self.timeShoot[5] = 0
                            bullet_player.bullet_pistol(self.rect.x + 21, self.rect.y + 10, 1)
                        elif key[PLAYER_SHOOT[1]]:
                            self.timeShoot[5] = 0
                            bullet_player.bullet_pistol(self.rect.x + 8, self.rect.y + 10, 2)
                        else:
                            self.timeShoot[5] = 0
                            bullet_player.bullet_pistol(self.rect.x + 3, self.rect.y + 10, 0)
                            bullet_player.bullet_pistol(self.rect.x + 17, self.rect.y + 10, 0)
                        SHOOT_PLAYER_FIRST.play()
                    else:
                        self.timeShoot[5] += 1
                if self.EXP >= 165:
                    if self.timeShoot[6] >= 11:
                        self.Ammo -= 2
                        self.timeShoot[6] = 0
                        for i in itertools.islice(itertools.count(0), 3):
                            bullet_player.bulletShotgun(self.rect.x + 9, self.rect.y + 10, i)
                    else:
                        self.timeShoot[6] += 1
        if self.isInertia:
            self.inertia_move(key)
        else:
            self.simple_move(key)

        if self.isAutoRegen and self.Health <= 100 and self.timeRegen >= 995:
            self.timeRegen = 0
            self.Health += randint(15, 25)
        else:
            if self.isAutoRegen:
                self.timeRegen += 1
        if self.autoBullet and self.Ammo <= 350 and self.timeAmmo >= 1500:
            self.timeAmmo = 0
            self.Ammo += 155
        else:
            if self.autoBullet:
                self.timeAmmo += 1
        if self.EXP >= 500:
            if self.FrameTXT < 1:
                self.TextMax = self.fonTxt.render(self.TEXT_SET_UA[randint(0, len(self.TEXT_SET_UA) - 1)], False,
                                                  (51, 255, 255))
                self.FrameTXT += 1
            if self.FrameTXT <= 140:
                self.FrameTXT += 1
                if self.AlphaTxt < 300:
                    self.AlphaTxt += 10
                    self.TextMax.set_alpha(self.AlphaTxt)
                sc.blit(self.TextMax, self.TextMax.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
        else:
            self.AlphaTxt = 0
            self.FrameTXT = 0

    def simple_move(self, key):
        if key[pygame.K_LSHIFT]:
            self.speed = 4
        else:
            self.speed = 6
        if key[PLAYER_MOVE[0]] and self.rect.y > 20:
            self.rect.y -= self.speed
            if scene_config.isMoveBackEffect:
                for i in group_config.back_effect_group:
                    i.rect.y += self.speed
        if key[PLAYER_MOVE[1]] and self.rect.y < 660:
            self.rect.y += self.speed
            if scene_config.isMoveBackEffect:
                for i in group_config.back_effect_group:
                    i.rect.y -= self.speed
        if key[PLAYER_MOVE[2]] and self.rect.x > 25:
            self.rect.x -= self.speed
        if key[PLAYER_MOVE[3]] and self.rect.x < 1225:
            self.rect.x += self.speed

    def inertia_move(self, key):
        if self.rect.y > 25:
            if key[PLAYER_MOVE[0]]:
                if self.ForceMove[0] < self.speed:
                    self.ForceMove[0] += 0.5
                self.centerY -= self.ForceMove[0]
                if scene_config.isMoveBackEffect and self.rect.y > 90:
                    for i in group_config.back_effect_group:
                        i.rect.y += self.ForceMove[0] - 1
            else:
                if self.ForceMove[0] > 0 and self.rect.y > 30:
                    self.ForceMove[0] -= 0.5
                    self.centerY -= self.ForceMove[0]
                    if scene_config.isMoveBackEffect and self.rect.y > 90:
                        for i in group_config.back_effect_group:
                            i.rect.y += self.ForceMove[0] - 1
                else:
                    self.ForceMove[0] = 0
        if self.rect.y < 660:
            if key[PLAYER_MOVE[1]]:
                if self.ForceMove[1] < self.speed:
                    self.ForceMove[1] += 0.5
                self.centerY += self.ForceMove[1]
                if scene_config.isMoveBackEffect and self.rect.y < 580:
                    for i in group_config.back_effect_group:
                        i.rect.y -= self.ForceMove[1] - 1
            else:
                if self.ForceMove[1] > 0 and self.rect.y < 655:
                    self.ForceMove[1] -= 0.5
                    self.centerY += self.ForceMove[1]
                    if scene_config.isMoveBackEffect and self.rect.y < 580:
                        for i in group_config.back_effect_group:
                            i.rect.y -= self.ForceMove[1] - 2
                else:
                    self.ForceMove[1] = 0
        if self.rect.x > 30:
            if key[PLAYER_MOVE[2]]:
                if self.ForceMove[2] < self.speed:
                    self.ForceMove[2] += 0.5
                self.centerX -= self.ForceMove[2]
            else:
                if self.ForceMove[2] > 0 and self.rect.x > 35:
                    self.ForceMove[2] -= 0.5
                    self.centerX -= self.ForceMove[2]
                else:
                    self.ForceMove[2] = 0
        if self.rect.x < 1225:
            if key[PLAYER_MOVE[3]]:
                if self.ForceMove[3] < self.speed:
                    self.ForceMove[3] += 0.5
                self.centerX += self.ForceMove[3]
            else:
                if self.ForceMove[3] > 0 and self.rect.x < 1200:
                    self.ForceMove[3] -= 0.5
                    self.centerX += self.ForceMove[3]
                else:
                    self.ForceMove[3] = 0
        self.rect.centerx = self.centerX
        self.rect.centery = self.centerY

    def check_exp(self):
        if self.EXP >= 45:
            self.timeShootMax = 6
        else:
            self.timeShootMax = 20
        if self.EXP >= 85:
            self.damageBullet = scene_config.AngleSpeed = int(self.EXP / 190 * 15 + self.ExtraDamageBullet)
