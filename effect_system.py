from random import randint

import pygame
from pygame import BLEND_RGB_ADD

from glow import createCircle
from group_config import effect_group, back_effect_group, fill_group, bodies_effect_group
from config_script import WINDOWS_SIZE, createImage, sc
from SCENE import scene_config
import time


class BloodSplash(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/EFFECT/BloodSplash/1.png")
        self.image = pygame.transform.scale(self.image, (43, 43))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(effect_group)

        self.frame: int = 0

    def update(self):
        match self.frame:
            case 0:
                self.image = createImage("ASSETS/SPRITE/EFFECT/BloodSplash/1.png")
            case 8:
                self.image = createImage("ASSETS/SPRITE/EFFECT/BloodSplash/2.png")
            case 11:
                self.image = createImage("ASSETS/SPRITE/EFFECT/BloodSplash/3.png")
            case 16:
                self.image = createImage("ASSETS/SPRITE/EFFECT/BloodSplash/4.png")
            case 18:
                self.kill()
        self.image = pygame.transform.scale(self.image, (43, 43))
        self.frame += 1


class EffectSize(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, size, sizemax):
        super().__init__()
        self.filename = filename
        self.size = size
        self.image = createImage(self.filename)
        self.image = pygame.transform.scale(self.image, (self.size, self.size))
        self.x, self.y = x, y
        self.rect = self.image.get_rect(center=(self.x, self.y))
        self.add(effect_group)

        self.time = 0
        self.maxTime = sizemax

        self.alpha = 305

    def update(self):
        if scene_config.isLight:
            sc.blit(createCircle(self.size, (29, 29, 29)),
                    (self.rect.centerx - self.size, self.rect.centery - self.size),
                    special_flags=BLEND_RGB_ADD)
        if self.time > self.maxTime:
            self.kill()
        self.size += 10
        self.image = createImage(self.filename)
        self.image = pygame.transform.scale(self.image, (self.size, self.size))
        self.rect = self.image.get_rect(center=(self.x, self.y))
        self.alpha -= 30
        self.image.set_alpha(self.alpha)
        self.time += 1


class effect_bodies(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, size=24, isMetal=False):
        pygame.sprite.Sprite.__init__(self)
        self.filename = filename
        self.size: int = size
        self.image = createImage(self.filename)
        self.image = pygame.transform.scale(self.image, (self.size, self.size))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(bodies_effect_group)

        self.timeLife: int = 0
        self.MaxTimeLife = randint(95, 400)
        self.pos: int = randint(0, 5)

        self.Alpha: int = randint(140, 350)

        self.timePos: int = 50

        self.isMetal: bool = isMetal

        self.x: int = x
        self.y: int = y
        self.angle: int = 0

    def update(self):
        if self.timeLife < self.MaxTimeLife:
            self.timeLife += 1
        else:
            self.kill()

        if self.isMetal:
            if self.timePos >= 45:
                self.timePos = 0
                self.pos = randint(0, 5)
            else:
                self.timePos += 1

        match self.pos:
            case 0:
                self.y -= 1
                self.x -= 1
            case 1:
                self.y += 1
                self.x += 1
            case 2:
                self.y += 1
                self.x -= 1
            case 3:
                self.y -= 1
                self.x += 1
            case 4:
                self.y -= 1
            case 5:
                self.y += 1
        if scene_config.isRotateObject:
            self.image = createImage(self.filename)
            self.image = pygame.transform.scale(self.image, (self.size, self.size))
            self.angle += 4
            self.image = pygame.transform.rotate(self.image, self.angle)
        self.rect = self.image.get_rect(center=(self.x, self.y))
        self.image.set_alpha(self.Alpha)


class effect_simple(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, pos=0, size=64):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage(filename)
        self.image = pygame.transform.scale(self.image, (size, size))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(effect_group)

        match pos:
            case 1:
                self.image = pygame.transform.flip(self.image, True, False)
            case 2:
                self.image = pygame.transform.flip(self.image, False, True)

        self.timeKill: int = 0

    def update(self):
        if self.timeKill >= 4:
                self.kill()
        self.timeKill += 1


class effect_anim(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, frame=6, size=64, type_effect=0):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage(filename[0])
        self.image = pygame.transform.scale(self.image, (size, size))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(effect_group)
        self.filename: [] = filename

        self.type = frame

        self.size: int = size
        self.Frame: int = 0
        self.Alpha: int = 250

        self.radius: int = 1
        self.color = [255, 255, 255]

        self.type_effect: int = type_effect

    def update(self):
        if self.type_effect == 1:
            self.radius += 20
            if self.radius <= 145:
                sc.blit(createCircle(self.radius, self.color),
                        (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                        special_flags=BLEND_RGB_ADD)
            self.color[0] -= 15
            self.color[1] -= 20
            self.color[2] -= 20
        match self.type:
            case 6:
                match self.Frame:
                    case 6:
                        self.image = createImage(self.filename[1])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 14:
                        self.image = createImage(self.filename[2])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 25:
                        self.image = createImage(self.filename[3])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 30:
                        self.image = createImage(self.filename[4])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 35:
                        self.image = createImage(self.filename[5])
                        self.image = pygame.transform.scale(self.image, (self.size + 1, self.size + 1))
                    case 39:
                        self.kill()
                self.Frame += 1
                self.image.set_alpha(self.Alpha)
                self.Alpha -= 19
            case 3:
                match self.Frame:
                    case 5:
                        self.image = createImage(self.filename[1])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 9:
                        self.image = createImage(self.filename[2])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 10:
                        self.kill()
                self.Frame += 1
                self.image.set_alpha(self.Alpha)
                self.Alpha -= 5
            case 4:
                match self.Frame:
                    case 6:
                        self.image = createImage(self.filename[1])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 11:
                        self.image = createImage(self.filename[2])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 16:
                        self.image = createImage(self.filename[3])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 23:
                        self.kill()
                self.Frame += 1
                self.image.set_alpha(self.Alpha)
                self.Alpha -= 5


class effect_take(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, size=(24, 24)):
        super().__init__()
        self.image = createImage(filename)
        self.image = pygame.transform.scale(self.image, size)
        self.rect = self.image.get_rect(center=(x, y))
        self.add(effect_group)

        self.alpha: int = 300

        self.posMove: int = randint(0, 2)

        self.lifeTime: int = 0

    def update(self):
        if self.lifeTime <= 25:
            match self.posMove:
                case 0:
                    self.rect.y -= 4
                case 1:
                    self.rect.y -= 4
                    self.rect.x -= 2
                case 2:
                    self.rect.y -= 4
                    self.rect.x += 2
            self.alpha -= 6
            self.image.set_alpha(self.alpha)
        else:
            self.kill()


class EffectLevelSnow(pygame.sprite.Sprite):
    def __init__(self, y):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/BULLET/ENEMY/10.bmp")
        self.image = pygame.transform.scale(self.image, (6, 6))
        self.rect = self.image.get_rect(center=(-50, y))
        self.add(back_effect_group)

        self.speedX: int = randint(4, 5)

        self.image.set_alpha(randint(25, 100))

        self.y = y

    def update(self):
        if self.rect.y < 6000:
            self.rect.y += self.speedX
            self.rect.x += self.speedX
        else:
            self.rect = self.image.get_rect(center=(-50, self.y))


class EffectLevelCitadel(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/EFFECT/LEVEL/2.png")
        self.image = pygame.transform.scale(self.image, (9, 9))
        self.rect = self.image.get_rect(center=(randint(25, 1205), randint(-5000, 5000)))

        self.add(back_effect_group)

        self.time: int = 0

        self.timeMax: int = randint(250, 750)

        self.alpha = randint(50, 140)

        self.radius: int = 9
        self.timeRadius: int = 0

    def update(self):
        if scene_config.isLight:
            if self.timeRadius > 40:
                self.radius = 27
                self.timeRadius = 0
            else:
                self.timeRadius += 1
                self.radius = 9
            sc.blit(createCircle(self.radius, (30, 30, 0)),
                    (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                    special_flags=BLEND_RGB_ADD)
        if self.rect.y < 5000:
            self.rect.y += 1
        else:
            self.rect = self.image.get_rect(center=(randint(25, 1200), randint(-6000, 5000)))
        if self.time > self.timeMax:
            if self.alpha > 0:
                self.alpha -= 6
                self.image.set_alpha(self.alpha)
            else:
                self.rect = self.image.get_rect(center=(randint(25, 1200), randint(-5000, 5000)))
                self.alpha = randint(75, 250)
                self.image.set_alpha(self.alpha)
                self.time = 0
                self.timeMax: int = randint(200, 750)
        else:
            self.time += 1


class EffectLevel(pygame.sprite.Sprite):
    def __init__(self, y):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/EFFECT/LEVEL/1/1.bmp")
        self.image = pygame.transform.scale(self.image, (29, 29))
        self.add(back_effect_group)
        self.Frame = 0

        self.speed = randint(1, 5)

        posSelect = randint(0, 1)
        self.pos = posSelect

        self.Alpha = randint(1, 75)
        self.image.set_alpha(self.Alpha)

        self.xMinus = randint(15, 575)

        match self.pos:
            case 0:
                self.rect = self.image.get_rect(center=(-15, y))
                self.rect.x -= self.xMinus
            case 1:
                self.image = pygame.transform.flip(self.image, True, False)
                self.rect = self.image.get_rect(center=(1280, y))
                self.rect.x += self.xMinus

    def update(self):
        if self.rect.y < -1250:
            self.rect.y = randint(-955, 1350)
            self.rect.x = -20
        if self.rect.y > 2950:
            self.rect.y = randint(-555, 1350)
            self.rect.x = 1275
        match self.pos:
            case 0:
                if self.rect.x <= 1270:
                    self.rect.x += self.speed
                else:
                    self.pos = 1
            case 1:
                if self.rect.x >= -20:
                    self.rect.x -= self.speed
                else:
                    self.pos = 0


class colorFill(pygame.sprite.Sprite):
    def __init__(self, filename, force=10):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(filename).convert()
        self.image = pygame.transform.scale(self.image, WINDOWS_SIZE)
        self.rect = self.image.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2))
        self.add(fill_group)

        self.Alpha: int = 265

        self.force = force

        self.prev_time = time.time()

    def update(self):

        dt = time.time() - self.prev_time
        dt *= 60
        self.prev_time = time.time()

        if self.Alpha > 2:
            self.Alpha -= self.force * dt
            self.image.set_alpha(self.Alpha)
        else:
            self.remove()
            self.kill()
