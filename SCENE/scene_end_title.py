from random import randint

import pygame
from config_script import sc, clock, createImage, WINDOWS_SIZE
from SCENE import scene_config, scene_menu, scene_score


def scene():
    running: bool = True
    FPS: int = 60

    font1 = pygame.font.Font("ASSETS/FONT/FFFFORWA.TTF", 13)

    font2 = pygame.font.Font("ASSETS/FONT/Alice-Regular.ttf", 20)

    txtRand = ["With Love For Your Body!", "Please Love Adriana", "Good see you", "Please Donate Me",
               "Russian War Criminal", "Zero is cute!", "QWERTY IS NOT GIRL OR TRANS!"]

    txtGame = font1.render("Adriana::System: Circulatory: "+txtRand[randint(0, len(txtRand)-1)], scene_config.AA_TEXT,
                           (255, 255, 255))
    txtGame.set_alpha(0)

    txtCreator = font2.render("STAT::TEAM (publisher solo indie, and GNU game)", scene_config.AA_TEXT, (255, 255, 255))
    txtCreator.set_alpha(0)

    filename = ["ASSETS/SPRITE/UI/CreditsImage/1.jpg", "ASSETS/SPRITE/UI/CreditsImage/2.png",
                "ASSETS/SPRITE/UI/CreditsImage/3.png", "ASSETS/SPRITE/UI/CreditsImage/4.png",
                "ASSETS/SPRITE/UI/CreditsImage/5.png", "ASSETS/SPRITE/UI/CreditsImage/6.png",
                "ASSETS/SPRITE/UI/CreditsImage/7.png", "ASSETS/SPRITE/UI/CreditsImage/8.png",
                "ASSETS/SPRITE/UI/CreditsImage/9.png", "ASSETS/SPRITE/UI/CreditsImage/16.png",
                "ASSETS/SPRITE/UI/CreditsImage/11.png", "ASSETS/SPRITE/UI/CreditsImage/12.png",
                "ASSETS/SPRITE/UI/CreditsImage/13.png", "ASSETS/SPRITE/UI/CreditsImage/14.png",
                "ASSETS/SPRITE/UI/CreditsImage/15.png", "ASSETS/SPRITE/UI/CreditsImage/10.png"]

    image = createImage(filename[randint(0, len(filename)-1)])
    image = pygame.transform.scale(image, (557, 350))
    image.set_alpha(0)

    frame: int = 0
    frameImage: int = 0
    alphaImg: int = 0
    frameGame: int = 0
    alphaGame = 0
    alpha: int = 0
    yGame: int = WINDOWS_SIZE[1]//2
    typeTXT: int = 0

    scene_score.SCORE_STORY = scene_config.Score

    pygame.mixer.music.load("ASSETS/OST/Finally end this game.wav")
    pygame.mixer.music.play(-1)
    pygame.mixer.music.set_volume(0.75)

    scene_score.re_write()

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
        sc.fill((0, 0, 0))
        if frameGame >= 200:
            if alpha < 250 and frame < 150:
                alpha += 5
                frame += 1
                txtCreator.set_alpha(alpha)
            else:
                if alpha > 0:
                    alpha -= 5
                else:
                    frame = 0
                    alpha = 0
                    match typeTXT:
                        case 0:
                            txtCreator = font2.render("Creator^", scene_config.AA_TEXT, (255, 255, 255))
                        case 1:
                            txtCreator = font2.render("Idea -> TitleChanQWERTY", scene_config.AA_TEXT, (255, 255, 255))
                        case 2:
                            txtCreator = font2.render("Programmer -> TitleChanQWERTY", scene_config.AA_TEXT, (255, 255, 255))
                        case 3:
                            txtCreator = font2.render("Art Design -> TitleChanQWERTY", scene_config.AA_TEXT, (255, 255, 255))
                        case 4:
                            txtCreator = font2.render("Music -> TitleChanQWERTY", scene_config.AA_TEXT,
                                                      (255, 255, 255))
                        case 5:
                            txtCreator = font2.render("Testers -> Arficord, LoveJoy", scene_config.AA_TEXT,
                                                      (255, 255, 255))
                        case 6:
                            txtCreator = font2.render("Special Thanks^", scene_config.AA_TEXT,
                                                      (255, 255, 255))
                        case 7:
                            txtCreator = font2.render("Ігровари", scene_config.AA_TEXT,
                                                      (255, 255, 255))
                        case 8:
                            txtCreator = font2.render("Arficord", scene_config.AA_TEXT,
                                                      (255, 255, 255))
                        case 9:
                            txtCreator = font2.render("kemgoblin", scene_config.AA_TEXT,
                                                      (255, 255, 255))
                        case 10:
                            txtCreator = font2.render("BodaMat", scene_config.AA_TEXT,
                                                      (255, 255, 255))
                        case 11:
                            txtCreator = font2.render("LoveJoy", scene_config.AA_TEXT,
                                                      (255, 255, 255))
                        case 12:
                            txtCreator = font2.render("Tracker_UA", scene_config.AA_TEXT,
                                                      (255, 255, 255))
                        case 13:
                            txtCreator = font2.render("Max( Parado 10)", scene_config.AA_TEXT,
                                                      (255, 255, 255))
                        case 14:
                            txtCreator = font2.render("Bludmoon", scene_config.AA_TEXT,
                                                      (255, 255, 255))
                        case 15:
                            txtCreator = font2.render("AND", scene_config.AA_TEXT,
                                                      (255, 255, 255))
                        case 16:
                            txtCreator = font2.render("You - Тобі Пупсік", scene_config.AA_TEXT,
                                                      (255, 255, 255))
                        case 17:
                            txtCreator = font2.render("Thanks for playing", scene_config.AA_TEXT,
                                                      (255, 255, 255))
                        case 18:
                            txtCreator = font2.render("Your Score -> "+str(scene_score.SCORE_STORY), scene_config.AA_TEXT,
                                                      (255, 255, 255))
                        case 19:
                            running = False
                            scene_config.switch_scene(scene_menu.scene_menu)
                    typeTXT += 1
                txtCreator.set_alpha(alpha)
            if alphaImg < 250 and frameImage < 84:
                alphaImg += 10
                frameImage += 1
                image.set_alpha(alphaImg)
            else:
                if alphaImg > 0:
                    alphaImg -= 5
                    image.set_alpha(alphaImg)
                else:
                    frameImage = 0
                    alphaImg = 0
                    image = createImage(filename[randint(0, len(filename) - 1)])
                    image = pygame.transform.scale(image, (557, 350))
                    image.set_alpha(0)
            if typeTXT < 16:
                sc.blit(image, image.get_rect(center=(WINDOWS_SIZE[0] // 2 - 350, WINDOWS_SIZE[1]//2)))
            sc.blit(txtCreator, txtCreator.get_rect(center=(WINDOWS_SIZE[0] // 2 + 300, WINDOWS_SIZE[1]//2)))
        if frameGame >= 130:
            if yGame < 655:
                yGame += 5
        if frameGame >= 50:
            if alphaGame < 250:
                alphaGame += 10
                txtGame.set_alpha(alphaGame)
        frameGame += 1
        sc.blit(txtGame, txtGame.get_rect(center=(WINDOWS_SIZE[0] // 2 + 300, yGame)))
        pygame.display.update()
        clock.tick(FPS)
