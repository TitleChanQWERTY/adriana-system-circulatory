import pygame.mixer

from SCENE import scene_config, scene_game, scene_end_title
from config_script import *
from font_config import NovelTextName, NovelText, FontKILL
from LOADING import loading

KeyText = FontKILL.render("Натисність клавішу SPACE", scene_config.AA_TEXT, (255, 255, 255))

match LANGUAGE_SET:
    case "EN":
        KeyText = FontKILL.render("Press Key SPACE", scene_config.AA_TEXT, (255, 255, 255))
KeyTextRect = KeyText.get_rect(center=(1045, 690))
KeyText.set_alpha(120)


def sceneStart():
    running: bool = True
    FPS: int = 60

    FrameImage = pygame.image.load("ASSETS/SPRITE/UI/Novel/Frame.png").convert_alpha()
    FrameImage = pygame.transform.scale(FrameImage, (WINDOWS_SIZE[0], 266))
    FrameImageRect = FrameImage.get_rect(center=(WINDOWS_SIZE[0] // 2, 660))
    FrameImage.set_alpha(295)

    NameText = NovelTextName.render("???", scene_config.AA_TEXT, (255, 255, 255))
    NameRect = NameText.get_rect(center=(315, 545))

    TextSet = NovelText.render("Пані, ми зібрали кров з дітей ось в цю баночку.",
                               scene_config.AA_TEXT, (255, 255, 255))
    TextSetRect = TextSet.get_rect(center=(455, 605))

    select = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.KEYDOWN and e.key == pygame.K_SPACE:
                select += 1

        sc.fill((0, 0, 0))
        match select:
            case 0:
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Пані, ми зібрали кров з дітей ось в цю баночку.",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Miss, we have get kids blood in this bottle",
                            scene_config.AA_TEXT, (255, 255, 255))
            case 1:
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Чудово.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Good.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 2:
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Давай сюди!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Take me this!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 3:
                TextSet = NovelText.render(
                    "...",
                    scene_config.AA_TEXT, (255, 255, 255))
            case 4:
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Чудово! Тепер, знайдіть мені щось краще! Ви вже надоїли з цим дитячим лайном!",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Perfectly! Now, find me something better! You've had enough of this childish shit!",
                            scene_config.AA_TEXT, (255, 255, 255))
            case 5:
                running = False
                pygame.mixer.music.load("ASSETS/OST/Ya Ne Tvoja Divchina.wav")
                pygame.mixer.music.play(-1)
                pygame.mixer.music.set_volume(0.3)
                scene_config.switch_scene(scene_game.scene0)
        sc.blit(FrameImage, FrameImageRect)
        sc.blit(TextSet, TextSetRect)
        sc.blit(KeyText, KeyTextRect)
        sc.blit(NameText, NameRect)
        pygame.display.update()
        clock.tick(FPS)


def scene0():
    running: bool = True
    FPS: int = 60

    pygame.mixer.music.stop()

    FrameImage = pygame.image.load("ASSETS/SPRITE/UI/Novel/Frame.png").convert_alpha()
    FrameImage = pygame.transform.scale(FrameImage, (WINDOWS_SIZE[0], 266))
    FrameImageRect = FrameImage.get_rect(center=(WINDOWS_SIZE[0] // 2, 660))
    FrameImage.set_alpha(295)

    NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
    NameRect = NameText.get_rect(center=(290, 544))

    TextSet = NovelText.render("Вай вай вай! Що за дічь відбувається?. Чому усі наче як бухали з друзями!?",
                               scene_config.AA_TEXT, (255, 255, 255))
    TextSetRect = TextSet.get_rect(center=(455, 605))

    PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
    PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))

    select = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.JOYBUTTONUP and e.button == 7:
                select += 1
            if e.type == pygame.KEYDOWN and e.key == pygame.K_SPACE:
                select += 1

        sc.fill((0, 0, 0))
        match select:
            case 0:
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Так стоп! Чому це сьогодні всі в напруженому стані? Не вже щось трапиолося?",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "So stop! Why is everyone in a tense state today? Didn't something happen already?",
                            scene_config.AA_TEXT, (255, 255, 255))
            case 1:
                NameText = NovelTextName.render("ZERO:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Самій цікаво.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 2:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Наче як нічого серйозного не було",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("As if nothing serious had happened",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 3:
                NameText = NovelTextName.render("ZERO:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/ZERO/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (458, 668))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Ну я чула, точніше бачила, як лежали трупи людей, і з них наче викачили усю кров.",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Well, I heard, or rather saw, how the corpses of people were lying, and it was as if all the blood had been drained from them.",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 4:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Ну це вже мінняє сенс картини.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Well, this already changes the meaning of the picture.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 5:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/ZERO/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (458, 668))
                NameText = NovelTextName.render("ZERO:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Так, згодна. Але мені все-одно. Нехай хоч геноцид чистилища настане.",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Yes, I agree. But I don't care. Let the genocide of purgatory come.",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 6:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Давай просто не звертати на це уваги і все.",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Let's just ignore it and that's it.", scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 7:
                pygame.mixer.music.load("ASSETS/OST/Tvoja Dusha Stala Mojeju.wav")
                pygame.mixer.music.play(-1)
                pygame.mixer.music.set_volume(0.4)
                running = False
                scene_config.switch_scene(loading.loading)
        sc.blit(FrameImage, FrameImageRect)
        sc.blit(NameText, NameRect)
        sc.blit(TextSet, TextSetRect)
        sc.blit(KeyText, KeyTextRect)
        pygame.display.update()
        clock.tick(FPS)


def scene1():
    running: bool = True
    FPS: int = 60

    pygame.mixer.music.stop()

    FrameImage = pygame.image.load("ASSETS/SPRITE/UI/Novel/Frame.png").convert_alpha()
    FrameImage = pygame.transform.scale(FrameImage, (WINDOWS_SIZE[0], 266))
    FrameImageRect = FrameImage.get_rect(center=(WINDOWS_SIZE[0] // 2, 660))
    FrameImage.set_alpha(285)

    NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
    NameRect = NameText.get_rect(center=(290, 544))

    TextSet = NovelText.render("ХЕЙ!",
                               scene_config.AA_TEXT, (255, 255, 255))
    TextSetRect = TextSet.get_rect(center=(60, 615))

    select: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.JOYBUTTONUP and e.button == 7:
                select += 1
            if e.type == pygame.KEYDOWN and e.key == pygame.K_SPACE:
                select += 1

        sc.fill((0, 0, 0))
        match select:
            case 0:
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("ХЕЙ!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("HEY!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 1:
                NameText = NovelTextName.render("Іоанн:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Га?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Hm?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 2:
                NameText = NovelTextName.render("Ioann:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/IOANN/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (388, 538))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("О, привіт!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Hello!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 3:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Привіт. Як справи?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Hello. How are you?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 4:
                NameText = NovelTextName.render("Ioann:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/IOANN/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (388, 538))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Та наче не погано. А в тебе?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("But it's not bad. What about you?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 5:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Чудово. Як бачиш, трупів біля мене багато.",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Perfectly. As you can see, there are many corpses near me.",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 6:
                NameText = NovelTextName.render("Іоанн:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/IOANN/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (388, 538))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Ех. Останні новини зовсім мене не радіють.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Eh. The latest news does not make me happy at all.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 7:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 8:
                NameText = NovelTextName.render("Іоанн:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/IOANN/3.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (388, 538))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("А ти не чув, що усі шукають якихось дивних людей?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Haven't you heard that everyone is looking for some strange people?",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 9:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("З усіх дивних, я тут бачу тільки тебе.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Of all the strange ones, I see only you here.",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 10:
                NameText = NovelTextName.render("Іоанн:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/IOANN/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (388, 538))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Іронічно...",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Ironically...",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 11:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Піди знайди когось ще. Просто я нічого про це не знаю і сам.",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Go find someone else. I just don't know anything about it myself.",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 12:
                NameText = NovelTextName.render("Ioann:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/IOANN/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (388, 538))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Добре. Давай удачі.",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Alright. Good Luck",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 14:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Ну а я навідаюсь особисто до Данни",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Well, I personally visit Danna",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 13:
                NameText = NovelTextName.render("", scene_config.AA_TEXT, (255, 255, 255))
                TextSet = NovelText.render(
                    "...",
                    scene_config.AA_TEXT, (255, 255, 255))
            case 15:
                pygame.mixer.music.load("ASSETS/OST/Dobre vizmi ce.wav")
                pygame.mixer.music.play(-1)
                pygame.mixer.music.set_volume(0.5)
                running = False
                scene_config.switch_scene(loading.loading)

        sc.blit(FrameImage, FrameImageRect)
        sc.blit(NameText, NameRect)
        sc.blit(TextSet, TextSetRect)
        sc.blit(KeyText, KeyTextRect)
        pygame.display.update()
        clock.tick(FPS)


def scene2():
    running: bool = True
    FPS: int = 60

    pygame.mixer.music.stop()

    FrameImage = pygame.image.load("ASSETS/SPRITE/UI/Novel/Frame.png").convert_alpha()
    FrameImage = pygame.transform.scale(FrameImage, (WINDOWS_SIZE[0], 266))
    FrameImageRect = FrameImage.get_rect(center=(WINDOWS_SIZE[0] // 2, 660))
    FrameImage.set_alpha(285)

    NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
    NameRect = NameText.get_rect(center=(290, 544))

    TextSet = NovelText.render("ХЕЙ!",
                               scene_config.AA_TEXT, (255, 255, 255))
    TextSetRect = TextSet.get_rect(center=(60, 615))

    select: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.JOYBUTTONUP and e.button == 7:
                select += 1
            if e.type == pygame.KEYDOWN and e.key == pygame.K_SPACE:
                select += 1

        sc.fill((0, 0, 0))
        match select:
            case 0:
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 1:
                NameText = NovelTextName.render("Anna:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 2:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (378, 538))
                PortraitCharacter = pygame.transform.flip(PortraitCharacter, True, False)
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 3:
                NameText = NovelTextName.render("Anna:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/Anna/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (318, 532))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Блять! Ти довго будеш мовчати",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Fuck! Were are you start talking",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 4:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (378, 538))
                PortraitCharacter = pygame.transform.flip(PortraitCharacter, True, False)
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Боюсь, що ти можеш мене вдарити",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Scary, what you hit me",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 5:
                NameText = NovelTextName.render("Anna:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/Anna/3.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (318, 532))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Давай кажи.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Let's talk",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 6:
                NameText = NovelTextName.render("QWERTY", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (378, 538))
                PortraitCharacter = pygame.transform.flip(PortraitCharacter, True, False)
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Ти ж чула про ту новину, що хтось десь...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("You heard that someone is somewhere...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 7:
                NameText = NovelTextName.render("Anna:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/Anna/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (318, 532))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Все все. На тобі карту, іди, і сам розбирайся",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("That's all. Take a map, go and figure it out yourself",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 8:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/3.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (378, 538))
                PortraitCharacter = pygame.transform.flip(PortraitCharacter, True, False)
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("А чого така доброта?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "And why such kindness?",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 9:
                NameText = NovelTextName.render("", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "...",
                            scene_config.AA_TEXT, (255, 255, 255))
            case 10:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (378, 538))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Втікла...",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Escape...",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 11:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "OK",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "OK",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 12:
                NameText = NovelTextName.render("Anna:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/Anna/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (318, 532))
                PortraitCharacter = pygame.transform.flip(PortraitCharacter, True, False)
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Тільки тобі треба пройти, через мертве місто!",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Only you have to go through the dead city!",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 13:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/3.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "ЩО!? НІ!",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "WHAT!? NO!",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 14:
                running = False
                scene_config.switch_scene(loading.loading)

        sc.blit(FrameImage, FrameImageRect)
        sc.blit(NameText, NameRect)
        sc.blit(TextSet, TextSetRect)
        sc.blit(KeyText, KeyTextRect)
        pygame.display.update()
        clock.tick(FPS)


def scene3():
    running: bool = True
    FPS: int = 60

    pygame.mixer.music.stop()

    FrameImage = pygame.image.load("ASSETS/SPRITE/UI/Novel/Frame.png").convert_alpha()
    FrameImage = pygame.transform.scale(FrameImage, (WINDOWS_SIZE[0], 266))
    FrameImageRect = FrameImage.get_rect(center=(WINDOWS_SIZE[0] // 2, 660))
    FrameImage.set_alpha(285)

    NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
    NameRect = NameText.get_rect(center=(290, 544))

    TextSet = NovelText.render("...",
                               scene_config.AA_TEXT, (255, 255, 255))
    TextSetRect = TextSet.get_rect(center=(45, 615))

    select: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.JOYBUTTONUP and e.button == 7:
                select += 1
            if e.type == pygame.KEYDOWN and e.key == pygame.K_SPACE:
                select += 1

        sc.fill((0, 0, 0))
        match select:
            case 0:
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 1:
                NameText = NovelTextName.render("Danna:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 2:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (378, 538))
                PortraitCharacter = pygame.transform.flip(PortraitCharacter, True, False)
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 3:
                NameText = NovelTextName.render("Danna:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/DANNA/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (348, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 4:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (378, 538))
                PortraitCharacter = pygame.transform.flip(PortraitCharacter, True, False)
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Вам коли небудь, лице розбивали?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Have you ever had your face smashed?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 5:
                NameText = NovelTextName.render("Danna:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/DANNA/3.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (348, 498))
                PortraitCharacter = pygame.transform.flip(PortraitCharacter, True, False)
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Було Діло",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Yes",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 6:
                NameText = NovelTextName.render("QWERTY", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (378, 538))
                PortraitCharacter = pygame.transform.flip(PortraitCharacter, True, False)
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Крч. Або ви кажете мені, де та сама, або я вам лице розибаваю.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Either you tell me where she is, or I'll smash your face. "
                                                   "And I am not afraid that you are the most important here,",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 7:
                NameText = NovelTextName.render("QWERTY", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (378, 538))
                PortraitCharacter = pygame.transform.flip(PortraitCharacter, True, False)
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "І мене не лякає що ви найголовніша тут, і у вас недоторканість вищого рівння",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("and you have inviolability of the highest level",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 8:
                NameText = NovelTextName.render("Danna:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/DANNA/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (348, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Ну давай. Краще Помахаємось хуями песик.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Common. Let's shake our dicks, doggies.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 9:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/3.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (378, 538))
                PortraitCharacter = pygame.transform.flip(PortraitCharacter, True, False)
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "...",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 10:
                running = False
                scene_config.switch_scene(loading.loading)

        sc.blit(FrameImage, FrameImageRect)
        sc.blit(NameText, NameRect)
        sc.blit(TextSet, TextSetRect)
        sc.blit(KeyText, KeyTextRect)
        pygame.display.update()
        clock.tick(FPS)


def scene4():
    running: bool = True
    FPS: int = 60

    pygame.mixer.music.stop()

    FrameImage = pygame.image.load("ASSETS/SPRITE/UI/Novel/Frame.png").convert_alpha()
    FrameImage = pygame.transform.scale(FrameImage, (WINDOWS_SIZE[0], 266))
    FrameImageRect = FrameImage.get_rect(center=(WINDOWS_SIZE[0] // 2, 660))
    FrameImage.set_alpha(285)

    NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
    NameRect = NameText.get_rect(center=(290, 544))

    TextSet = NovelText.render("...",
                               scene_config.AA_TEXT, (255, 255, 255))
    TextSetRect = TextSet.get_rect(center=(45, 615))

    select: int = 0

    pygame.mixer.music.stop()

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.JOYBUTTONUP and e.button == 7:
                select += 1
            if e.type == pygame.KEYDOWN and e.key == pygame.K_SPACE:
                select += 1

        sc.fill((0, 0, 0))
        match select:
            case 0:
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Приємно мати з вами діло Данна.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Thank's You Danna",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 1:
                NameText = NovelTextName.render("Danna:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Так так.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 2:
                NameText = NovelTextName.render("Danna:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Іди вже, і надери їй дупу.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Go ahead and kick her ass.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 3:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Данна. А чого ви цим не займаєтесь? Ви ж головне в місті, і повині слідкувати за цим.",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Danna Why aren't you doing this? You are the main thing in the city, and you must follow that.",
                            scene_config.AA_TEXT, (255, 255, 255))
            case 4:
                NameText = NovelTextName.render("Danna:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("По перше, мені лінь",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("First, I'm lazy",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 5:
                NameText = NovelTextName.render("Danna:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("По друге. У мене таких, як води у морі.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Second. I have such as the waters in the sea.",
                            scene_config.AA_TEXT, (255, 255, 255))
            case 6:
                NameText = NovelTextName.render("QWERTY", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Хах. Іронічно.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Hah. Ironically.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 7:
                NameText = NovelTextName.render("Danna:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Крч. Якщо ти зупиниш ці усі вбивства, я не буду відправляти поліцію, на пошуки тебе як злочинця.",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "If you stop all these murders, I won't send the police to find you as a criminal.",
                            scene_config.AA_TEXT, (255, 255, 255))
            case 8:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Тобто. Вам буде все-одно на те, що я вбиваю \"невиних\" людей?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("That is, Would you care that I kill \"innocent\" people?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 9:
                NameText = NovelTextName.render("Danna:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Так пупс",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Yes",
                            scene_config.AA_TEXT, (255, 255, 255))
            case 10:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Чудово)",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Good)",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 11:
                scene_game.LEVEL += 1
                pygame.mixer.music.load("ASSETS/OST/Teper bude veselo, meni.wav")
                pygame.mixer.music.play(-1)
                running = False
                scene_config.switch_scene(loading.loading)

        sc.blit(FrameImage, FrameImageRect)
        sc.blit(NameText, NameRect)
        sc.blit(TextSet, TextSetRect)
        sc.blit(KeyText, KeyTextRect)
        pygame.display.update()
        clock.tick(FPS)


def scene5():
    running: bool = True
    FPS: int = 60

    pygame.mixer.music.stop()

    FrameImage = pygame.image.load("ASSETS/SPRITE/UI/Novel/Frame.png").convert_alpha()
    FrameImage = pygame.transform.scale(FrameImage, (WINDOWS_SIZE[0], 266))
    FrameImageRect = FrameImage.get_rect(center=(WINDOWS_SIZE[0] // 2, 660))
    FrameImage.set_alpha(295)

    NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
    NameRect = NameText.get_rect(center=(290, 546))

    TextSet = NovelText.render("Та де вона?",
                               scene_config.AA_TEXT, (255, 255, 255))
    TextSetRect = TextSet.get_rect(center=(155, 604))

    PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
    PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))

    select = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.JOYBUTTONUP and e.button == 7:
                select += 1
            if e.type == pygame.KEYDOWN and e.key == pygame.K_SPACE:
                select += 1

        sc.fill((0, 0, 0))
        match select:
            case 0:
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Та де вона?",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "And where is she?",
                            scene_config.AA_TEXT, (255, 255, 255))
            case 1:
                NameText = NovelTextName.render("ZERO:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Самій цікаво.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 2:
                NameText = NovelTextName.render("NONAME:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Привіт Пупс!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Hello fucked stuped man!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 3:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (458, 668))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "(Тихий Сміх)",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "(Quiet Laughter)",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 4:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("А що це у тебе маска така велика? І чому ти її носиш?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Why is your mask so big? And why are you wearing it?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 5:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/ZERO/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (458, 668))
                NameText = NovelTextName.render("ZERO:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Вона в ній така смішна",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Yes, I agree. They could, I don't know, torture small children.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 6:
                NameText = NovelTextName.render("NONAME:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/NONAME/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 548))
                PortraitCharacter = pygame.transform.flip(PortraitCharacter, True, False)
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "ХЕЙ! Не смійтесь з мене!",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Hey! Dont laugh on me!", scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 7:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Так чого ти її носиш?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("So why are you wearing it?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 8:
                NameText = NovelTextName.render("NONAME:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/NONAME/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 548))
                PortraitCharacter = pygame.transform.flip(PortraitCharacter, True, False)
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Тому що в мене розірвана щелепа",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Because my jaw is broken", scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 9:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Покажи!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Show Me!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 10:
                NameText = NovelTextName.render("", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 11:
                NameText = NovelTextName.render("NONAME:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/NONAME/3.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 548))
                PortraitCharacter = pygame.transform.flip(PortraitCharacter, True, False)
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Подобається?",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Like This?", scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 12:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Дуже",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Very",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 13:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Так чого ти збираєш кров людей, і випиваєш її?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("So why do you collect people's blood and drink it?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 14:
                NameText = NovelTextName.render("NONAME:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/NONAME/3.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 548))
                PortraitCharacter = pygame.transform.flip(PortraitCharacter, True, False)
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Тому що тільки це допомагає моєму тілу віндовлюватись.",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Because only this helps my body heal.", scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 15:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Отже. Зараз ми будемо бити твоє лице, до поки ти не померш!)",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("So. Now we're going to beat your face until you die!)",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 16:
                NameText = NovelTextName.render("NONAME:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/NONAME/4.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 548))
                PortraitCharacter = pygame.transform.flip(PortraitCharacter, True, False)
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "НІ!",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "NO!", scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 17:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("так",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("yes",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 18:
                running = False
                scene_config.switch_scene(loading.loading)
        sc.blit(FrameImage, FrameImageRect)
        sc.blit(NameText, NameRect)
        sc.blit(TextSet, TextSetRect)
        sc.blit(KeyText, KeyTextRect)
        pygame.display.update()
        clock.tick(FPS)


def scene6():
    running: bool = True
    FPS: int = 60

    pygame.mixer.music.stop()

    FrameImage = pygame.image.load("ASSETS/SPRITE/UI/Novel/Frame.png").convert_alpha()
    FrameImage = pygame.transform.scale(FrameImage, (WINDOWS_SIZE[0], 266))
    FrameImageRect = FrameImage.get_rect(center=(WINDOWS_SIZE[0] // 2, 660))
    FrameImage.set_alpha(295)

    NameText = NovelTextName.render("", scene_config.AA_TEXT, (255, 255, 255))
    NameRect = NameText.get_rect(center=(290, 544))

    TextSet = NovelText.render("Та де вона?",
                               scene_config.AA_TEXT, (255, 255, 255))
    TextSetRect = TextSet.get_rect(center=(155, 605))

    PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
    PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))

    select = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.JOYBUTTONUP and e.button == 7:
                select += 1
            if e.type == pygame.KEYDOWN and e.key == pygame.K_SPACE:
                select += 1

        sc.fill((0, 0, 0))
        match select:
            case 0:
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "...",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "...",
                            scene_config.AA_TEXT, (255, 255, 255))
            case 1:
                NameText = NovelTextName.render("", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 2:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Нарешті. Тепер можна іти спокійно спати",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Finally. Now i can go to sleep!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 3:
                NameText = NovelTextName.render("ZERO:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/CutScene/2/1.png").convert()
                PortraitCharacter = pygame.transform.scale(PortraitCharacter, (1558, 1498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Так. Я вже треті сутки не спала",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "So. I have not slept for the third day",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter,
                        PortraitCharacter.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
            case 4:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/CutScene/2/1.png").convert()
                PortraitCharacter = pygame.transform.scale(PortraitCharacter, (1558, 1498))
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("От би якомога швидше поїсти.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("I would like to eat as soon as possible.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter,
                        PortraitCharacter.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
            case 5:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/CutScene/2/1.png").convert()
                PortraitCharacter = pygame.transform.scale(PortraitCharacter, (1558, 1498))
                NameText = NovelTextName.render("ZERO:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Ех.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Eh",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter,
                        PortraitCharacter.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
            case 6:
                NameText = NovelTextName.render("ZERO:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/CutScene/2/2.png").convert()
                PortraitCharacter = pygame.transform.scale(PortraitCharacter, (958, 868))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Скажи гарно весить",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "It weighs well", scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter,
                        PortraitCharacter.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 150)))
            case 7:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/CutScene/2/2.png").convert()
                PortraitCharacter = pygame.transform.scale(PortraitCharacter, (958, 868))
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Так. Не погано повісив",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("So. Not a bad hang",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter,
                        PortraitCharacter.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 150)))
            case 8:
                NameText = NovelTextName.render("ZERO:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/CutScene/2/2.png").convert()
                PortraitCharacter = pygame.transform.scale(PortraitCharacter, (958, 868))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Пішли додому?",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Go To Home?", scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter,
                        PortraitCharacter.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
            case 9:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/CutScene/2/2.png").convert()
                PortraitCharacter = pygame.transform.scale(PortraitCharacter, (958, 868))
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Пішли",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Lets go",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter,
                        PortraitCharacter.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
            case 10:
                NameText = NovelTextName.render("", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 11:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/CutScene/2/3.png").convert()
                PortraitCharacter = pygame.transform.scale(PortraitCharacter, (958, 868))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Цікаво, що буде завтра?",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Wondering what will happen tomorrow?", scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter,
                        PortraitCharacter.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
            case 12:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/CutScene/2/3.png").convert()
                PortraitCharacter = pygame.transform.scale(PortraitCharacter, (958, 868))
                NameText = NovelTextName.render("ZERO:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Завтра ти будеш почувати себе погано!)",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Tomorrow you will feel bad!)",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter,
                        PortraitCharacter.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
            case 13:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/CutScene/2/3.png").convert()
                PortraitCharacter = pygame.transform.scale(PortraitCharacter, (958, 868))
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Чому?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Why?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter,
                        PortraitCharacter.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
            case 14:
                NameText = NovelTextName.render("ZERO:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/CutScene/2/3.png").convert()
                PortraitCharacter = pygame.transform.scale(PortraitCharacter, (958, 868))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Секрет)",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Secret)", scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter,
                        PortraitCharacter.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
            case 15:
                NameText = NovelTextName.render("", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Ось і настав кінець цієї дурної історії. Сподіваюсь, вона вас нічому не навчила.",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "That's the end of this stupid story. I hope she didn't teach you anything.",
                            scene_config.AA_TEXT, (255, 255, 255))
            case 16:
                NameText = NovelTextName.render("TitleChanQWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                NameRect = NameText.get_rect(center=(204, 544))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Так Стоп! Це вже кінець!?",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Yes Stop! Is this the end already!?", scene_config.AA_TEXT, (255, 255, 255))
            case 17:
                NameText = NovelTextName.render("TitleChanQWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Ех. Треба було робити довше сюжетний режим",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Eh. It was necessary to make the story mode longer",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 18:
                NameText = NovelTextName.render("TitleChanQWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Почекай не йди від мене!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Wait don't leave me!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 19:
                NameText = NovelTextName.render("TitleChanQWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Почекай!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Wait!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 20:
                NameText = NovelTextName.render("TitleChanQWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("ні",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("no",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 21:
                NameText = NovelTextName.render("TitleChanQWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Добре іди",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Alright go.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 22:
                NameText = NovelTextName.render("TitleChanQWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Тільки пограй в інші режими добре?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Just play the other modes ok?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 23:
                running = False
                scene_config.switch_scene(scene_end_title.scene)
        sc.blit(FrameImage, FrameImageRect)
        sc.blit(NameText, NameRect)
        sc.blit(TextSet, TextSetRect)
        sc.blit(KeyText, KeyTextRect)
        pygame.display.update()
        clock.tick(FPS)
