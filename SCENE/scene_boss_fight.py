from random import randint

import config_script
import effect_system
import group_config
import take_screenshot
from ENEMY import create_enemy
from ITEM import create_item
from SCENE import scene_config, scene_menu, scene_die, scene_cutscene, scene_game, scene_novel
from BOSS import danna, noname
from LOADING import loading
from collision import collisionBullet, collisionBombPlayer, isKillBullet
import collision
from config_script import *
from font_config import *
from key_setup import PLAYER_SHOOT
from sfx_compilation import ENTER_PAUSE, SELECT_MENU, ENTER_SELECT, EXIT_PAUSE, DAMAGE_SHUTLE
import reset_data
import pygame

TEXT_SET = ("", "", "", "", "", "")
TEXT_SET_RANK = ("", "", "", "", "")
TEXT_SET_TASK = ("", "", "")

ExpText = FontEXP.render(TEXT_SET[1] + str(scene_config.p.EXP), scene_config.AA_TEXT, (255, 255, 255))
ExpTextRect = ExpText.get_rect(center=(32, 345))

textAlpha: int = 150

HealthText = HealthFont.render(str(scene_config.p.Health), scene_config.AA_TEXT, (255, 255, 255))
HealthTextRect = HealthText.get_rect(center=(WINDOWS_SIZE[0] // 2, 697))

ScoreText = ScoreFont.render(TEXT_SET[3] + str(scene_config.Score), scene_config.AA_TEXT, (255, 255, 0))
ScoreTextRect = ScoreText.get_rect(center=(50, WINDOWS_SIZE[0] // 2 - 598))

AmmoText = AmmoFont.render(TEXT_SET[2] + str(scene_config.p.Ammo), scene_config.AA_TEXT, (255, 255, 255))
AmmoTextRect = AmmoText.get_rect(center=(WINDOWS_SIZE[0] // 2 + 270, WINDOWS_SIZE[0] // 2 - 600))

MultipleScoreTXT = ScoreFont.render("X" + str(scene_config.p.MultipleScore), scene_config.AA_TEXT, (245, 245, 245))
MultipleScoreRect = MultipleScoreTXT.get_rect(center=(240, WINDOWS_SIZE[0] // 2 - 598))

match config_script.LANGUAGE_SET:
    case "UA":
        TEXT_SET = (config_script.TEXT_LEVEL_UA[0], config_script.TEXT_LEVEL_UA[1], config_script.TEXT_LEVEL_UA[2],
                    config_script.TEXT_LEVEL_UA[3], config_script.TEXT_LEVEL_UA[4], config_script.TEXT_LEVEL_UA[5])
        TEXT_SET_RANK = (
            config_script.TEXT_RANK_UA[0], config_script.TEXT_RANK_UA[1], config_script.TEXT_RANK_UA[2],
            config_script.TEXT_RANK_UA[3], config_script.TEXT_RANK_UA[4])
        TEXT_SET_TASK = (config_script.TEXT_TASK_UA[0], config_script.TEXT_TASK_UA[1], config_script.TEXT_TASK_UA[2])
    case "EN":
        TEXT_SET = (config_script.TEXT_LEVEL_EN[0], config_script.TEXT_LEVEL_EN[1], config_script.TEXT_LEVEL_EN[2],
                    config_script.TEXT_LEVEL_EN[3], config_script.TEXT_LEVEL_EN[4], config_script.TEXT_LEVEL_EN[5])
        TEXT_SET_RANK = (
            config_script.TEXT_RANK_EN[0], config_script.TEXT_RANK_EN[1], config_script.TEXT_RANK_EN[2],
            config_script.TEXT_RANK_EN[3], config_script.TEXT_RANK_EN[4])
        TEXT_SET_TASK = (config_script.TEXT_TASK_EN[0], config_script.TEXT_TASK_EN[1], config_script.TEXT_TASK_EN[2])

bombCount = AmmoFont.render("Bomb: " + str(scene_config.p.Bomb), scene_config.AA_TEXT, (255, 255, 255))


def textUpdate():
    global ExpText, textAlpha, HealthText, AmmoText, ScoreText, TEXT_SET, TEXT_SET_RANK, \
        AmmoTextRect, ExpTextRect, HealthTextRect, MultipleScoreTXT, ScoreTextRect, bombCount

    if scene_config.p.EXP < 500:
        ExpText = FontEXP.render(TEXT_SET[1] + str(scene_config.p.EXP), scene_config.AA_TEXT, (255, 255, 255))
    else:
        ExpText = FontEXP.render(TEXT_SET[1] + "MAX!", scene_config.AA_TEXT, (255, 255, 255))
    ExpTextRect = ExpText.get_rect(center=(WINDOWS_SIZE[0] // 2 - 270, WINDOWS_SIZE[0] // 2 - 600))
    AmmoText = AmmoFont.render(TEXT_SET[2] + str(scene_config.p.Ammo), scene_config.AA_TEXT, (255, 255, 255))
    ScoreText = ScoreFont.render(TEXT_SET[3] + str(scene_config.Score), scene_config.AA_TEXT, (255, 255, 0))
    ScoreText.set_alpha(150)
    HealthText = HealthFont.render(str(scene_config.p.Health), scene_config.AA_TEXT, (255, 255, 255))
    HealthTextRect = HealthText.get_rect(center=(WINDOWS_SIZE[0] // 2, 697))
    MultipleScoreTXT = ScoreFont.render("X" + str(scene_config.p.MultipleScore), scene_config.AA_TEXT, (245, 245, 245))
    bombCount = AmmoFont.render("Bomb: " + str(scene_config.p.Bomb), scene_config.AA_TEXT, (255, 255, 255))


BombIcon = createImage("ASSETS/SPRITE/ITEM/bomb.png")
BombIcon = pygame.transform.scale(BombIcon, (17, 17))

heartUI = createImage("ASSETS/SPRITE/UI/ICON/HeartUI.png")
heartUI = pygame.transform.scale(heartUI, (55, 54))
heartUIRect = heartUI.get_rect(center=(WINDOWS_SIZE[0] // 2, 696))


def updateUI():
    global heartUI
    heartUI.set_alpha(scene_config.p.Health)


timeCreateEffect: int = 0
createEffect: int = 0


def createEffectLevel():
    global timeCreateEffect, createEffect
    if createEffect < scene_config.TimeCountLevelEffect:
        match timeCreateEffect:
            case 5:
                createEffect += 1
                timeCreateEffect = 0
                match scene_config.isMoveBackEffect:
                    case True:
                        return effect_system.EffectLevel(randint(-5000, 5000))
                    case False:
                        return effect_system.EffectLevel(randint(5, 740))
            case _:
                timeCreateEffect += 1


def scene0_draw():
    group_config.back_effect_group.draw(sc)
    if scene_config.p.EXP >= 70: sc.blit(scene_config.PlayerProgresInOne, scene_config.PlayerProgresInOneRect)
    if scene_config.p.EXP >= 150: sc.blit(scene_config.PlayerProgresInTWO, scene_config.PlayerProgresInTWORect)
    if scene_config.isDrawPlayer: sc.blit(scene_config.p.image, scene_config.p.rect)
    group_config.bodies_effect_group.draw(sc)
    group_config.player_bullet.draw(sc)
    group_config.item_group.draw(sc)
    group_config.shota_group.draw(sc)
    group_config.bossGroup.draw(sc)
    group_config.simple_bullet.draw(sc)
    group_config.pistol_bullet.draw(sc)
    group_config.anyBullet.draw(sc)
    group_config.bomb_player.draw(sc)
    group_config.effect_group.draw(sc)
    group_config.TextEffect_group.draw(sc)
    group_config.notfication_group.draw(sc)
    if scene_config.p.EXP >= 405: sc.blit(scene_config.playerShowIndicator, scene_config.playerShowIndicatorRect)
    for n in group_config.notfication_group: sc.blit(n.text, n.rectTxt)


def scene0_update():
    if scene_config.isDrawPlayer:
        scene_config.p.update()
    group_config.ad_group.update()
    group_config.bodies_effect_group.update()
    group_config.anyBullet.update()
    group_config.fill_group.update()
    group_config.bomb_player.update()
    group_config.pistol_bullet.update()
    group_config.back_effect_group.update()
    group_config.effect_group.update()
    group_config.TextEffect_group.update()
    group_config.player_bullet.update()
    group_config.item_group.update()
    group_config.simple_bullet.update()
    group_config.notfication_group.update()
    group_config.bossGroup.update()
    textUpdate()
    updateUI()
    scene_config.playerPosSet()
    scene_config.createAd()


def bossCollision():
    for boss in group_config.bossGroup:
        for bullet in group_config.player_bullet:
            if boss.rect.collidepoint(bullet.rect.center):
                damage = scene_config.p.damageBullet - randint(0, 11)
                boss.Health -= damage
                create_item.createTXT(boss.rect.x + 20, boss.rect.y + 10,
                                      "-" + str(damage), (255, 0, 0))
                effect_system.BloodSplash(boss.rect.centerx, boss.rect.centery)
                if isKillBullet:
                    bullet.kill()
        if boss.rect.colliderect(scene_config.p.rect) and collision.timeDamage >= 95:
            if scene_config.p.EXP > 0:
                scene_config.p.EXP -= 15

            damage = None

            match scene_config.DIFFICULTY:
                case 0:
                        damage = randint(5, 10)
                case 1:
                        damage = randint(6, 11)
                case 2:
                        damage = randint(9, 15)
            scene_config.p.Health -= damage
            collision.timeDamage = 5
            for i in range(4):
                collision.createEffectTake(scene_config.p.rect.x + 17, scene_config.p.rect.y + 15,
                                               "ASSETS/SPRITE/UI/ICON/DamageHealth.png")
            scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/red.png", 11)
            create_item.createTXT(WINDOWS_SIZE[0] // 2 - 1, 695, "-" + str(damage) + " HEALTH", (255, 0, 0))


def scene0():
    for i in range(2):
        create_item.create_exp(randint(100, 1120), randint(140, 495), 1)
        create_item.create_ammo(randint(90, 1200), randint(140, 495))
        create_item.create_ammo(randint(90, 1200), randint(140, 495))

    BlackFill = pygame.image.load("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png").convert()
    BlackFill = pygame.transform.scale(BlackFill, WINDOWS_SIZE)
    BlackFillRect = BlackFill.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2))
    FrameFillBlack: int = 0
    BlackFill.set_alpha(FrameFillBlack)

    isUpdate: bool = True
    running: bool = True

    FPS: int = 60

    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png")

    fpsText = fpsFont.render(str(int(clock.get_fps())), False, (255, 255, 255))
    fpsRect = fpsText.get_rect(center=(43, 680))

    timeDie: int = 0

    iconPause = ("ASSETS/SPRITE/UI/PAUSE MENU/ICON/1.png", "ASSETS/SPRITE/UI/PAUSE MENU/ICON/2.png",
                 "ASSETS/SPRITE/UI/PAUSE MENU/ICON/3.png", "ASSETS/SPRITE/UI/PAUSE MENU/ICON/4.png")

    PauseIcon = config_script.createImage(iconPause[0])
    PauseIcon = pygame.transform.scale(PauseIcon, (129, 129))
    AnglePauseIcon: int = 0
    selectIcon: int = 0

    FillImagePause = pygame.image.load("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png").convert()
    FillImagePause = pygame.transform.scale(FillImagePause, scene_config.WINDOWS_SIZE)
    FillImagePauseRect = FillImagePause.get_rect(
        center=(scene_config.WINDOWS_SIZE[0] // 2, scene_config.WINDOWS_SIZE[1] // 2))
    FillPauseAlpha: int = 0
    FillImagePause.set_alpha(FillPauseAlpha)

    TextPause = ("", "", "", "")

    match config_script.LANGUAGE_SET:
        case "UA":
            TextPause = ("Вертайся Пупсік", "Почати З Початку", "Вийти", "УВАГА, ПАУЗА НА СЦЕНІ!")
        case "EN":
            TextPause = ("Back To Game", "Restart Level", "Exit", "WARNING, PAUSE IN THE SCENE!")

    BackToGameText = TextOption.render(TextPause[0], scene_config.AA_TEXT, (255, 255, 255))
    BackToGameTextRect = BackToGameText.get_rect(center=(185, 295))

    RestartGameText = TextOption.render(TextPause[1], scene_config.AA_TEXT, (255, 255, 255))
    RestartGameTextRect = RestartGameText.get_rect(center=(186, 365))

    ExitGameText = TextOption.render(TextPause[2], scene_config.AA_TEXT, (255, 255, 255))
    ExitGameTextRect = ExitGameText.get_rect(center=(186, 445))

    WarningTextPause = FinishFont.render(TextPause[3], scene_config.AA_TEXT, (255, 255, 0))
    WarningTextPauseRect = WarningTextPause.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2, 85))

    ArrowImage = config_script.createImage("ASSETS/SPRITE/UI/BUTTON/ARROW/1.png")
    ArrowImage = pygame.transform.scale(ArrowImage, (34, 34))
    ArrowImageRect = ArrowImage.get_rect(center=(55, 295))

    selectPause: int = 0

    FillAd = pygame.image.load("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png").convert()
    FillAd = pygame.transform.scale(FillAd, config_script.WINDOWS_SIZE)
    FillAdRect = FillAd.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2, config_script.WINDOWS_SIZE[1] // 2))

    shieldImage = createImage("ASSETS/SPRITE/UI/shield.png")
    shieldImage = pygame.transform.scale(shieldImage, (57, 58))
    shieldImage.set_alpha(165)

    FillAdAlpha: int = 0

    if scene_game.LEVEL < 18:
        danna.Danna()
        for i in range(20):
            create_enemy.createMaid()
    elif scene_game.LEVEL >= 18:
        noname.NoName()

    timeEnd: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.select_scene = None
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6: take_screenshot.take()
                if e.key == PLAYER_SHOOT[3] and scene_config.p.Bomb > 0:
                    scene_config.p.createBomb()
                if e.key == pygame.K_ESCAPE:
                    match isUpdate:
                        case False:
                            pygame.mixer.music.unpause()
                            EXIT_PAUSE.play()
                            selectIcon = randint(0, 3)
                            isUpdate = True
                        case True:
                            pygame.mixer.music.pause()
                            ENTER_PAUSE.play()
                            FillPauseAlpha = 0
                            isUpdate = False
                if e.key == pygame.K_DOWN and not isUpdate:
                    SELECT_MENU.play()
                    match selectPause:
                        case 0:
                            selectPause = 1
                        case 1:
                            selectPause = 2
                        case 2:
                            selectPause = 0
                if e.key == pygame.K_UP and not isUpdate:
                    SELECT_MENU.play()
                    match selectPause:
                        case 0:
                            selectPause = 2
                        case 1:
                            selectPause = 0
                        case 2:
                            selectPause = 1
                if not isUpdate:
                    if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                        ENTER_SELECT.play()
                        match selectPause:
                            case 0:
                                pygame.mixer.music.unpause()
                                isUpdate = True
                            case 1:
                                reset_data.reset()
                                running = False
                                scene_config.switch_scene(loading.loading)
                            case 2:
                                pygame.mixer.music.load("ASSETS/OST/Moje Shita Ce Stragdanya.wav")
                                pygame.mixer.music.play()
                                pygame.mixer.music.set_volume(0.10)
                                reset_data.reset()
                                running = False
                                scene_config.switch_scene(scene_menu.scene_menu)

        # draw
        sc.fill((8, 0, 24))
        scene0_draw()
        sc.blit(ExpText, ExpTextRect)
        pygame.draw.rect(sc, (0, 0, 0), (0, 0, WINDOWS_SIZE[0], WINDOWS_SIZE[1]), 19)
        pygame.draw.rect(sc, (255, 255, 255), (19, 15, 1245, 685), 1)
        if scene_config.isShowPos: sc.blit(scene_config.PlayerPosImage, scene_config.PlayerPosRect)
        sc.blit(heartUI, heartUIRect)
        sc.blit(HealthText, HealthTextRect)
        if collision.timeDamage < 115:
            sc.blit(shieldImage, heartUIRect)
        if scene_config.p.EXP >= 65:
            sc.blit(AmmoText, AmmoTextRect)

        sc.blit(ScoreText, ScoreTextRect)
        sc.blit(MultipleScoreTXT, MultipleScoreRect)
        sc.blit(bombCount, bombCount.get_rect(center=(WINDOWS_SIZE[0] // 2 + 545, WINDOWS_SIZE[1] // 2 + 315)))
        sc.blit(BombIcon, BombIcon.get_rect(center=(WINDOWS_SIZE[0] // 2 + 495, WINDOWS_SIZE[1] // 2 + 315)))
        if scene_config.FPS_SHOW:
            fpsText = fpsFont.render(str(int(clock.get_fps())), False, (255, 255, 255))
            sc.blit(fpsText, fpsRect)
        match isUpdate:
            case True:
                if scene_config.p.Health <= 0:
                    scene_config.p.Health = 0
                    scene_config.isDrawPlayer = False
                    if timeDie >= 285:
                        scene_config.switch_scene(scene_die.scene_die)
                        running = False
                    if timeDie >= 85:
                        FrameFillBlack += 2
                        BlackFill.set_alpha(FrameFillBlack)
                        sc.blit(BlackFill, BlackFillRect)
                    timeDie += 1

                if scene_config.isFillAd:
                    FillAdAlpha += 0.5
                    FillAd.set_alpha(FillAdAlpha)
                    sc.blit(FillAd, FillAdRect)
                else:
                    FillAdAlpha = 0

                group_config.ad_group.draw(sc)
                group_config.fill_group.draw(sc)

                # update
                if scene_config.isDrawPlayer:
                    collisionBombPlayer()
                    collisionBullet()
                    bossCollision()
                scene0_update()
                group_config.shota_group.update()
                for boss in group_config.bossGroup:
                    if boss.Health <= 1:
                        if timeEnd > 19:
                            boss.kill()
                            running = False
                            if scene_game.LEVEL < 18:
                                scene_config.switch_scene(scene_cutscene.scene0)
                            else:
                                scene_config.switch_scene(scene_novel.scene6)
                        else:
                            if timeEnd < 16:
                                scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/white.png", 7)
                            DAMAGE_SHUTLE.play()
                        timeEnd += 1

            case False:
                match selectPause:
                    case 0:
                        ArrowImageRect = ArrowImage.get_rect(center=(55, BackToGameTextRect.y + 15))
                    case 1:
                        ArrowImageRect = ArrowImage.get_rect(center=(50, RestartGameTextRect.y + 15))
                    case 2:
                        ArrowImageRect = ArrowImage.get_rect(center=(110, ExitGameTextRect.y + 15))
                if FillPauseAlpha <= 175:
                    FillPauseAlpha += 12
                    FillImagePause.set_alpha(FillPauseAlpha)
                PauseIcon = config_script.createImage(iconPause[selectIcon])
                PauseIcon = pygame.transform.scale(PauseIcon, (129, 129))
                AnglePauseIcon += 1
                PauseIcon = pygame.transform.rotate(PauseIcon, AnglePauseIcon)
                PauseIconRect = PauseIcon.get_rect(center=(980, config_script.WINDOWS_SIZE[1] // 2))
                sc.blit(FillImagePause, FillImagePauseRect)
                sc.blit(PauseIcon, PauseIconRect)
                sc.blit(BackToGameText, BackToGameTextRect)
                sc.blit(RestartGameText, RestartGameTextRect)
                sc.blit(ExitGameText, ExitGameTextRect)
                sc.blit(ArrowImage, ArrowImageRect)
                sc.blit(WarningTextPause, WarningTextPauseRect)
        pygame.display.update()
        clock.tick(FPS)
