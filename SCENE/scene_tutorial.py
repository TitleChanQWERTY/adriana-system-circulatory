from random import randint

import collision
import config_script
import effect_system
import group_config
import sfx_compilation
import take_screenshot
from BULLET import bullet_enemy
from ENEMY import create_enemy
from ITEM import create_item
from SCENE import scene_config, scene_menu, scene_select_music
from LOADING import loading
from collision import collisionBullet, collisionBombPlayer
from config_script import *
from font_config import *
from key_setup import PLAYER_SHOOT
from sfx_compilation import ENTER_PAUSE, SELECT_MENU, ENTER_SELECT, EXIT_PAUSE
import reset_data
import pygame

TEXT_SET = ("", "", "", "", "", "")
TEXT_SET_RANK = ("", "", "", "", "")
TEXT_SET_TASK = ("", "", "")

ExpText = FontEXP.render(TEXT_SET[1] + str(scene_config.p.EXP), scene_config.AA_TEXT, (255, 255, 255))
ExpTextRect = ExpText.get_rect(center=(32, 345))

textAlpha: int = 150

HealthText = HealthFont.render(str(scene_config.p.Health), scene_config.AA_TEXT, (255, 255, 255))
HealthTextRect = HealthText.get_rect(center=(WINDOWS_SIZE[0] // 2, 697))

ScoreText = ScoreFont.render(TEXT_SET[3] + str(scene_config.Score), scene_config.AA_TEXT, (255, 255, 0))
ScoreTextRect = ScoreText.get_rect(center=(50, WINDOWS_SIZE[0] // 2 - 598))

AmmoText = AmmoFont.render(TEXT_SET[2] + str(scene_config.p.Ammo), scene_config.AA_TEXT, (255, 255, 255))
AmmoTextRect = AmmoText.get_rect(center=(WINDOWS_SIZE[0] // 2 + 270, WINDOWS_SIZE[0] // 2 - 600))

CrystalCount = FontKILL.render(TEXT_SET[5] + str(scene_config.CRYSTAL_COUNT) + "/" + str(scene_config.CRYSTAL_MAX),
                               scene_config.AA_TEXT, (255, 255, 255))
CrystalCountRect = CrystalCount.get_rect(center=(WINDOWS_SIZE[0] // 2 - 500, WINDOWS_SIZE[0] // 2 + 40))

MultipleScoreTXT = ScoreFont.render("X" + str(scene_config.p.MultipleScore), scene_config.AA_TEXT, (245, 245, 245))
MultipleScoreRect = MultipleScoreTXT.get_rect(center=(240, WINDOWS_SIZE[0] // 2 - 598))

match config_script.LANGUAGE_SET:
    case "UA":
        TEXT_SET = (config_script.TEXT_LEVEL_UA[0], config_script.TEXT_LEVEL_UA[1], config_script.TEXT_LEVEL_UA[2],
                    config_script.TEXT_LEVEL_UA[3], config_script.TEXT_LEVEL_UA[4], config_script.TEXT_LEVEL_UA[5])
        TEXT_SET_RANK = (
            config_script.TEXT_RANK_UA[0], config_script.TEXT_RANK_UA[1], config_script.TEXT_RANK_UA[2],
            config_script.TEXT_RANK_UA[3], config_script.TEXT_RANK_UA[4])
        TEXT_SET_TASK = (config_script.TEXT_TASK_UA[0], config_script.TEXT_TASK_UA[1], config_script.TEXT_TASK_UA[2])
    case "EN":
        TEXT_SET = (config_script.TEXT_LEVEL_EN[0], config_script.TEXT_LEVEL_EN[1], config_script.TEXT_LEVEL_EN[2],
                    config_script.TEXT_LEVEL_EN[3], config_script.TEXT_LEVEL_EN[4], config_script.TEXT_LEVEL_EN[5])
        TEXT_SET_RANK = (
            config_script.TEXT_RANK_EN[0], config_script.TEXT_RANK_EN[1], config_script.TEXT_RANK_EN[2],
            config_script.TEXT_RANK_EN[3], config_script.TEXT_RANK_EN[4])
        TEXT_SET_TASK = (config_script.TEXT_TASK_EN[0], config_script.TEXT_TASK_EN[1], config_script.TEXT_TASK_EN[2])

bombCount = AmmoFont.render("Bomb: " + str(scene_config.p.Bomb), scene_config.AA_TEXT, (255, 255, 255))


def textUpdate():
    global ExpText, textAlpha, HealthText, AmmoText, ScoreText, TEXT_SET, TEXT_SET_RANK, \
        AmmoTextRect, CrystalCount, CrystalCountRect, ExpTextRect, HealthTextRect, MultipleScoreTXT, ScoreTextRect, bombCount

    CrystalCount = FontKILL.render(str(scene_select_music.musicName), scene_config.AA_TEXT, (255, 255, 255))
    CrystalCountRect = CrystalCount.get_rect(center=(WINDOWS_SIZE[0] // 2 - 301, WINDOWS_SIZE[0] // 2 + 39))

    if scene_config.p.EXP < 500:
        ExpText = FontEXP.render(TEXT_SET[1] + str(scene_config.p.EXP), scene_config.AA_TEXT, (255, 255, 255))
    else:
        ExpText = FontEXP.render(TEXT_SET[1] + "MAX!", scene_config.AA_TEXT, (255, 255, 255))
    ExpTextRect = ExpText.get_rect(center=(WINDOWS_SIZE[0] // 2 - 270, WINDOWS_SIZE[0] // 2 - 600))
    AmmoText = AmmoFont.render(TEXT_SET[2] + str(scene_config.p.Ammo), scene_config.AA_TEXT, (255, 255, 255))
    ScoreText = ScoreFont.render(TEXT_SET[3] + str(scene_config.Score), scene_config.AA_TEXT, (255, 255, 0))
    ScoreText.set_alpha(150)
    if textAlpha >= 50:
        textAlpha -= 2
    else:
        textAlpha = 150
    CrystalCount.set_alpha(textAlpha)
    HealthText = HealthFont.render(str(scene_config.p.Health), scene_config.AA_TEXT, (255, 255, 255))
    HealthTextRect = HealthText.get_rect(center=(WINDOWS_SIZE[0] // 2, 697))
    MultipleScoreTXT = ScoreFont.render("X" + str(scene_config.p.MultipleScore), scene_config.AA_TEXT, (245, 245, 245))
    bombCount = AmmoFont.render("Bomb: " + str(scene_config.p.Bomb), scene_config.AA_TEXT, (255, 255, 255))


BombIcon = createImage("ASSETS/SPRITE/ITEM/bomb.png")
BombIcon = pygame.transform.scale(BombIcon, (17, 17))

heartUI = createImage("ASSETS/SPRITE/UI/ICON/HeartUI.png")
heartUI = pygame.transform.scale(heartUI, (55, 54))
heartUIRect = heartUI.get_rect(center=(WINDOWS_SIZE[0] // 2, 696))


def updateUI():
    global heartUI
    heartUI.set_alpha(scene_config.p.Health)


timeCreateEffect: int = 0
createEffect: int = 0


def createEffectLevel():
    global timeCreateEffect, createEffect
    if createEffect < scene_config.TimeCountLevelEffect:
        match timeCreateEffect:
            case 5:
                createEffect += 1
                timeCreateEffect = 0
                match scene_config.isMoveBackEffect:
                    case True:
                        return effect_system.EffectLevel(randint(-5000, 5000))
                    case False:
                        return effect_system.EffectLevel(randint(5, 740))
            case _:
                timeCreateEffect += 1


def scene0_draw():
    group_config.back_effect_group.draw(sc)
    group_config.TextEffect_group.draw(sc)
    if scene_config.p.EXP >= 70: sc.blit(scene_config.PlayerProgresInOne, scene_config.PlayerProgresInOneRect)
    if scene_config.p.EXP >= 150: sc.blit(scene_config.PlayerProgresInTWO, scene_config.PlayerProgresInTWORect)
    if scene_config.isDrawPlayer: sc.blit(scene_config.p.image, scene_config.p.rect)
    group_config.bodies_effect_group.draw(sc)
    group_config.player_bullet.draw(sc)
    group_config.item_group.draw(sc)
    group_config.ball_rosa_group.draw(sc)
    group_config.shota_group.draw(sc)
    group_config.shutle_group.draw(sc)
    group_config.ball_soul.draw(sc)
    group_config.magical_girl_group.draw(sc)
    group_config.simple_bullet.draw(sc)
    group_config.pistol_bullet.draw(sc)
    group_config.anyBullet.draw(sc)
    group_config.bomb_player.draw(sc)
    group_config.effect_group.draw(sc)
    group_config.notfication_group.draw(sc)
    if scene_config.p.EXP >= 405: sc.blit(scene_config.playerShowIndicator, scene_config.playerShowIndicatorRect)
    for n in group_config.notfication_group: sc.blit(n.text, n.rectTxt)


def scene0_update():
    if scene_config.isDrawPlayer:
        scene_config.p.update()
    group_config.ad_group.update()
    group_config.bodies_effect_group.update()
    group_config.anyBullet.update()
    group_config.fill_group.update()
    group_config.bomb_player.update()
    group_config.pistol_bullet.update()
    group_config.back_effect_group.update()
    group_config.effect_group.update()
    group_config.TextEffect_group.update()
    group_config.player_bullet.update()
    group_config.item_group.update()
    group_config.simple_bullet.update()
    group_config.notfication_group.update()
    textUpdate()
    updateUI()
    scene_config.playerPosSet()
    scene_config.createAd()


task_tutor: int = 0


class Shota(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage("ASSETS/SPRITE/ENEMY/Shota/1.png")
        self.Size: int = 66
        self.image = pygame.transform.scale(self.image, (self.Size, self.Size))
        self.rect = self.image.get_rect(center=(randint(45, 950), -10))
        self.add(group_config.shota_group)

        self.type: int = 0

        self.Health: int = 1

        self.isTakeDamage: bool = False
        self.isTakeDamageFrame: float = 0

        self.Frame: int = 0
        self.speed: int = 1
        self.maxTimeDown = randint(180, 195)
        self.timeDown: int = 0

        self.timePos: int = 0

        self.pos: int = 0

        self.spawnBullet: int = 0
        self.ammo: int = 5
        self.reload: int = 0

        self.timeLeave: int = 0
        self.posLeave = randint(0, 1)

    def Animation(self):
        if not self.isTakeDamage:
            if self.pos != 2 or 1:
                match self.Frame:
                    case 10:
                        self.image = pygame.image.load_extended("ASSETS/SPRITE/ENEMY/Shota/1.png").convert_alpha()
                        self.image = pygame.transform.scale(self.image, (self.Size, self.Size))
                    case 50:
                        self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/Shota/2.png").convert_alpha()
                        self.image = pygame.transform.scale(self.image, (self.Size, self.Size))
                        self.Frame = 0
                self.Frame += 1

    def createBullet(self):
        match self.spawnBullet:
            case 45:
                if self.ammo > 0:
                    self.spawnBullet = 0
                    self.ammo -= 1
                    if scene_config.p.rect.centery > self.rect.centery:
                        return bullet_enemy.pistolBullet(self.rect.x + 35, self.rect.y + 65,
                                                         "ASSETS/SPRITE/BULLET/ENEMY/4.bmp", 11)
                    else:
                        return bullet_enemy.pistolBullet(self.rect.x + 35, self.rect.y + 5,
                                                         "ASSETS/SPRITE/BULLET/ENEMY/4.bmp", 11, 10, False, 1)
                else:
                    match self.reload:
                        case 200:
                            self.reload = 0
                            self.ammo = 7
                        case _:
                            self.reload += 1
            case _:
                self.spawnBullet += 1

    def update(self):
        if self.isTakeDamage:
            if self.isTakeDamageFrame < 1.5:
                self.isTakeDamageFrame += 0.5
                self.image = createImage("ASSETS/SPRITE/ENEMY/Shota/Damage.png")
                self.image = pygame.transform.scale(self.image, (69, 69))
            else:
                self.image = createImage("ASSETS/SPRITE/ENEMY/Shota/1.png")
                self.image = pygame.transform.scale(self.image, (self.Size, self.Size))
                self.isTakeDamageFrame = 0
                self.isTakeDamage = False
        self.Animation()
        if self.timeDown < self.maxTimeDown:
            self.rect.y += self.speed
            self.timeDown += 1
        else:
            self.createBullet()
            if self.timePos >= 35:
                self.timePos = 0
                self.pos = randint(0, 5)
            else:
                self.timePos += 1
            self.move()

    def move(self):
        if self.timeLeave < 1155:
            self.timeLeave += 1
            match self.pos:
                case 0:
                    if self.rect.y >= 35 and self.rect.x >= 25:
                        if not self.isTakeDamage:
                            self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/Shota/Right.png").convert_alpha()
                            self.image = pygame.transform.scale(self.image, (self.Size - 6, self.Size))
                            self.image = pygame.transform.flip(self.image, True, False)
                        self.rect.y -= self.speed
                        self.rect.x -= self.speed
                    else:
                        self.pos = 1
                case 1:
                    if self.rect.y <= 600 and self.rect.x <= 1200:
                        if not self.isTakeDamage:
                            self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/Shota/Right.png").convert_alpha()
                            self.image = pygame.transform.scale(self.image, (self.Size - 6, self.Size))
                        self.rect.y += self.speed
                        self.rect.x += self.speed
                    else:
                        self.pos = 0
                case 2:
                    if self.rect.y <= 600:
                        self.rect.y += self.speed
                    else:
                        self.pos = 3
                case 3:
                    if self.rect.y >= 35:
                        self.rect.y -= self.speed
                    else:
                        self.pos = 2
        else:
            match self.posLeave:
                case 0:
                    if self.rect.x >= -30:
                        if not self.isTakeDamage:
                            self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/Shota/Right.png").convert_alpha()
                            self.image = pygame.transform.scale(self.image, (self.Size - 6, self.Size))
                            self.image = pygame.transform.flip(self.image, True, False)
                        self.rect.x -= self.speed
                    else:
                        self.kill()
                case 1:
                    if self.rect.x <= 1295:
                        if not self.isTakeDamage:
                            self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/Shota/Right.png").convert_alpha()
                            self.image = pygame.transform.scale(self.image, (self.Size - 6, self.Size))
                        self.rect.x += self.speed
                    else:
                        self.kill()


def scene0():
    global task_tutor

    if task_tutor == 2:
        for i in range(15):
            create_item.create_exp(randint(105, 750), randint(55, 550), 0)
            create_item.create_exp(randint(105, 750), randint(55, 550), 1)
    if task_tutor == 3:
        for i in range(10):
            create_item.create_ammo(randint(105, 750), randint(55, 550))
    BlackFill = pygame.image.load("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png").convert()
    BlackFill = pygame.transform.scale(BlackFill, WINDOWS_SIZE)
    BlackFillRect = BlackFill.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2))
    FrameFillBlack: int = 0
    BlackFill.set_alpha(FrameFillBlack)

    LevelRang: str = "-"

    isRank: bool = False
    ColorRank = (255, 255, 255)
    RankTextLevel = RankFont.render(LevelRang, scene_config.AA_TEXT, ColorRank)
    RankRectLevel = RankTextLevel.get_rect(center=(1165, 100))

    isUpdate: bool = True
    running: bool = True

    FPS: int = 60

    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png")

    fpsText = fpsFont.render(str(int(clock.get_fps())), False, (255, 255, 255))
    fpsRect = fpsText.get_rect(center=(43, 680))

    iconPause = ("ASSETS/SPRITE/UI/PAUSE MENU/ICON/1.png", "ASSETS/SPRITE/UI/PAUSE MENU/ICON/2.png",
                 "ASSETS/SPRITE/UI/PAUSE MENU/ICON/3.png", "ASSETS/SPRITE/UI/PAUSE MENU/ICON/4.png")

    PauseIcon = config_script.createImage(iconPause[0])
    PauseIcon = pygame.transform.scale(PauseIcon, (129, 129))
    AnglePauseIcon: int = 0
    selectIcon: int = 0

    FillImagePause = pygame.image.load("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png").convert()
    FillImagePause = pygame.transform.scale(FillImagePause, scene_config.WINDOWS_SIZE)
    FillImagePauseRect = FillImagePause.get_rect(
        center=(scene_config.WINDOWS_SIZE[0] // 2, scene_config.WINDOWS_SIZE[1] // 2))
    FillPauseAlpha: int = 0
    FillImagePause.set_alpha(FillPauseAlpha)

    TextPause = ("", "", "", "")

    match config_script.LANGUAGE_SET:
        case "UA":
            TextPause = ("Вертайся Пупсік", "Почати З Початку", "Вийти", "УВАГА, ПАУЗА НА СЦЕНІ!")
        case "EN":
            TextPause = ("Back To Game", "Restart Level", "Exit", "WARNING, PAUSE IN THE SCENE!")

    BackToGameText = TextOption.render(TextPause[0], scene_config.AA_TEXT, (255, 255, 255))
    BackToGameTextRect = BackToGameText.get_rect(center=(185, 295))

    RestartGameText = TextOption.render(TextPause[1], scene_config.AA_TEXT, (255, 255, 255))
    RestartGameTextRect = RestartGameText.get_rect(center=(186, 365))

    ExitGameText = TextOption.render(TextPause[2], scene_config.AA_TEXT, (255, 255, 255))
    ExitGameTextRect = ExitGameText.get_rect(center=(186, 445))

    WarningTextPause = FinishFont.render(TextPause[3], scene_config.AA_TEXT, (255, 255, 0))
    WarningTextPauseRect = WarningTextPause.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2, 85))

    ArrowImage = config_script.createImage("ASSETS/SPRITE/UI/BUTTON/ARROW/1.png")
    ArrowImage = pygame.transform.scale(ArrowImage, (34, 34))
    ArrowImageRect = ArrowImage.get_rect(center=(55, 295))

    TEXT_SET_RANK_FONT = "Ранг:"

    if config_script.LANGUAGE_SET == "EN": TEXT_SET_RANK_FONT = "RANK:"

    RankText = ScoreFont.render(TEXT_SET_RANK_FONT, scene_config.AA_TEXT, (255, 255, 255))
    RankTextRect = RankText.get_rect(center=(1165, 70))

    selectPause: int = 0

    FillAd = pygame.image.load("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png").convert()
    FillAd = pygame.transform.scale(FillAd, config_script.WINDOWS_SIZE)
    FillAdRect = FillAd.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2, config_script.WINDOWS_SIZE[1] // 2))

    shieldImage = createImage("ASSETS/SPRITE/UI/shield.png")
    shieldImage = pygame.transform.scale(shieldImage, (57, 58))
    shieldImage.set_alpha(165)

    FillAdAlpha: int = 0

    location = "78h843n123dg"

    locationTXT = FontKILL.render(location, scene_config.AA_TEXT, (255, 255, 255))
    locationTXT.set_alpha(95)

    timer: int = 0

    countMove: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.select_scene = None
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6: take_screenshot.take()
                if e.key == pygame.K_w:
                    countMove += 1
                if e.key == pygame.K_s:
                    countMove += 1
                if e.key == pygame.K_a:
                    countMove += 1
                if e.key == pygame.K_d:
                    countMove += 1
                if e.key == PLAYER_SHOOT[3] and scene_config.p.Bomb > 0:
                    sfx_compilation.SHOOT_BOMB.play()
                    scene_config.p.createBomb()
                if e.key == pygame.K_ESCAPE:
                    match isUpdate:
                        case False:
                            pygame.mixer.music.unpause()
                            EXIT_PAUSE.play()
                            selectIcon = randint(0, 3)
                            isUpdate = True
                        case True:
                            pygame.mixer.music.pause()
                            ENTER_PAUSE.play()
                            FillPauseAlpha = 0
                            isUpdate = False
                if e.key == pygame.K_DOWN and not isUpdate:
                    SELECT_MENU.play()
                    match selectPause:
                        case 0:
                            selectPause = 1
                        case 1:
                            selectPause = 2
                        case 2:
                            selectPause = 0
                if e.key == pygame.K_UP and not isUpdate:
                    SELECT_MENU.play()
                    match selectPause:
                        case 0:
                            selectPause = 2
                        case 1:
                            selectPause = 0
                        case 2:
                            selectPause = 1
                if e.key == pygame.K_e and not isUpdate or e.key == pygame.K_RETURN and not isUpdate:
                    ENTER_SELECT.play()
                    match selectPause:
                        case 0:
                            pygame.mixer.music.unpause()
                            isUpdate = True
                        case 1:
                            reset_data.reset()
                            running = False
                            scene_config.switch_scene(loading.loading)
                        case 2:
                            pygame.mixer.music.load("ASSETS/OST/Moje Shita Ce Stragdanya.wav")
                            pygame.mixer.music.play(-1)
                            pygame.mixer.music.set_volume(0.10)
                            reset_data.reset()
                            running = False
                            scene_config.switch_scene(scene_menu.scene_menu)

        # draw
        sc.fill((8, 0, 24))
        scene0_draw()
        sc.blit(ExpText, ExpTextRect)
        pygame.draw.rect(sc, (0, 0, 0), (0, 0, WINDOWS_SIZE[0], WINDOWS_SIZE[1]), 19)
        pygame.draw.rect(sc, (255, 255, 255), (19, 15, 1245, 685), 1)
        if scene_config.isShowPos: sc.blit(scene_config.PlayerPosImage, scene_config.PlayerPosRect)
        sc.blit(heartUI, heartUIRect)
        sc.blit(HealthText, HealthTextRect)
        if collision.timeDamage < 115:
            sc.blit(shieldImage, heartUIRect)
        if scene_config.p.EXP >= 65: sc.blit(AmmoText, AmmoTextRect)

        sc.blit(ScoreText, ScoreTextRect)
        sc.blit(MultipleScoreTXT, MultipleScoreRect)
        sc.blit(bombCount, bombCount.get_rect(center=(WINDOWS_SIZE[0] // 2 + 545, WINDOWS_SIZE[1] // 2 + 315)))
        sc.blit(BombIcon, BombIcon.get_rect(center=(WINDOWS_SIZE[0] // 2 + 495, WINDOWS_SIZE[1] // 2 + 315)))
        match scene_config.FPS_SHOW:
            case True:
                fpsText = fpsFont.render(str(int(clock.get_fps())), False, (255, 255, 255))
                sc.blit(fpsText, fpsRect)
        match isUpdate:
            case True:
                collisionBullet()
                collisionBombPlayer()
                group_config.shota_group.update()
                match task_tutor:
                    case 0:
                        if countMove == 18:
                            for i in range(2):
                                Shota()
                            countMove += 1
                        if countMove >= 20:
                            task_tutor += 1
                            running = False
                            scene_config.switch_scene(tips_two)
                    case 1:
                        if timer >= 555:
                            task_tutor += 1
                            for shota in group_config.shota_group:
                                shota.kill()
                            running = False
                            scene_config.switch_scene(tips_tree)
                        timer += 1
                    case 2:
                        if timer <= 575:
                            timer += 1
                        else:
                            task_tutor += 1
                            running = False
                            scene_config.switch_scene(tips_four)
                    case 3:
                        if timer <= 550:
                            timer += 1
                        else:
                            task_tutor += 1
                            running = False
                            scene_config.switch_scene(tips_five)

                match isRank:
                    case True:
                        sc.blit(RankText, RankTextRect)
                        sc.blit(RankTextLevel, RankRectLevel)
                if scene_config.isFillAd:
                    FillAdAlpha += 0.5
                    FillAd.set_alpha(FillAdAlpha)
                    sc.blit(FillAd, FillAdRect)
                else:
                    FillAdAlpha = 0

                group_config.ad_group.draw(sc)
                group_config.fill_group.draw(sc)

                # update
                if scene_config.isDrawPlayer and pygame.mixer.music.get_busy():
                    collisionBombPlayer()
                    collisionBullet()
                scene0_update()
                RankTextLevel = RankFont.render(LevelRang, scene_config.AA_TEXT, ColorRank)
            case False:
                match selectPause:
                    case 0:
                        ArrowImageRect = ArrowImage.get_rect(center=(55, BackToGameTextRect.y + 15))
                    case 1:
                        ArrowImageRect = ArrowImage.get_rect(center=(50, RestartGameTextRect.y + 15))
                    case 2:
                        ArrowImageRect = ArrowImage.get_rect(center=(110, ExitGameTextRect.y + 15))
                if FillPauseAlpha <= 175:
                    FillPauseAlpha += 12
                    FillImagePause.set_alpha(FillPauseAlpha)
                PauseIcon = config_script.createImage(iconPause[selectIcon])
                PauseIcon = pygame.transform.scale(PauseIcon, (129, 129))
                AnglePauseIcon += 1
                PauseIcon = pygame.transform.rotate(PauseIcon, AnglePauseIcon)
                PauseIconRect = PauseIcon.get_rect(center=(980, config_script.WINDOWS_SIZE[1] // 2))
                sc.blit(FillImagePause, FillImagePauseRect)
                sc.blit(PauseIcon, PauseIconRect)
                sc.blit(BackToGameText, BackToGameTextRect)
                sc.blit(RestartGameText, RestartGameTextRect)
                sc.blit(ExitGameText, ExitGameTextRect)
                sc.blit(ArrowImage, ArrowImageRect)
                sc.blit(WarningTextPause, WarningTextPauseRect)
        pygame.display.update()
        clock.tick(FPS)


titlechan = createImage("ASSETS/SPRITE/UI/VisualNovel/TitleChan/1.png")
titlechan = pygame.transform.scale(titlechan, (358, 565))

KeyText = FontKILL.render("Натисність клавішу SPACE", scene_config.AA_TEXT, (255, 255, 255))

match LANGUAGE_SET:
    case "EN":
        KeyText = FontKILL.render("Press Key SPACE", scene_config.AA_TEXT, (255, 255, 255))
KeyTextRect = KeyText.get_rect(center=(1045, 690))
KeyText.set_alpha(120)


def tips_one():
    global titlechan
    running = True
    FPS: int = 60

    fill_image = createImage("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png")
    fill_image = pygame.transform.scale(fill_image, WINDOWS_SIZE)
    fill_image.set_alpha(0)
    fill_image_alpha = 0

    FrameImage = pygame.image.load("ASSETS/SPRITE/UI/Novel/Frame.png").convert_alpha()
    FrameImage = pygame.transform.scale(FrameImage, (WINDOWS_SIZE[0], 266))
    FrameImageRect = FrameImage.get_rect(center=(WINDOWS_SIZE[0] // 2, 660))
    FrameImage.set_alpha(295)

    NameText = NovelTextName.render("TitleChanQWERTY", scene_config.AA_TEXT, (255, 255, 255))
    NameRect = NameText.get_rect(center=(306, 546))

    TextSet = NovelText.render("Привіт! Вітаю тебе в Adriana::Sysetm: Circulatory.",
                               scene_config.AA_TEXT, (255, 255, 255))
    TextSetRect = TextSet.get_rect(center=(295, 605))

    select: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if fill_image_alpha >= 180:
                if e.type == pygame.JOYBUTTONUP and e.button == 7:
                    select += 1
                if e.type == pygame.KEYDOWN and e.key == pygame.K_SPACE:
                    select += 1
        sc.fill((0, 0, 0))
        scene0_draw()
        sc.blit(ExpText, ExpTextRect)
        pygame.draw.rect(sc, (0, 0, 0), (0, 0, WINDOWS_SIZE[0], WINDOWS_SIZE[1]), 19)
        pygame.draw.rect(sc, (255, 255, 255), (19, 15, 1245, 685), 1)
        if scene_config.isShowPos: sc.blit(scene_config.PlayerPosImage, scene_config.PlayerPosRect)
        sc.blit(heartUI, heartUIRect)
        sc.blit(HealthText, HealthTextRect)
        if scene_config.p.EXP >= 65: sc.blit(AmmoText, AmmoTextRect)
        sc.blit(ScoreText, ScoreTextRect)
        sc.blit(MultipleScoreTXT, MultipleScoreRect)
        sc.blit(bombCount, bombCount.get_rect(center=(WINDOWS_SIZE[0] // 2 + 545, WINDOWS_SIZE[1] // 2 + 315)))
        sc.blit(BombIcon, BombIcon.get_rect(center=(WINDOWS_SIZE[0] // 2 + 495, WINDOWS_SIZE[1] // 2 + 315)))
        sc.blit(fill_image, fill_image.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
        match select:
            case 0:
                titlechan = createImage("ASSETS/SPRITE/UI/VisualNovel/TitleChan/2.png")
                titlechan = pygame.transform.scale(titlechan, (358, 565))
                if LANGUAGE_SET == "UA":
                    TextSet = NovelText.render("Привіт! Вітаю тебе в Adriana::Sysetm: Circulatory.",
                                               scene_config.AA_TEXT, (255, 255, 255))
                else:
                    TextSet = NovelText.render("Hello! Welcome to Adriana::Sysetm: Circulatory.",
                                               scene_config.AA_TEXT, (255, 255, 255))
            case 1:
                if LANGUAGE_SET == "UA":
                    TextSet = NovelText.render("Давай спочатку розберемося, як тобі рухатись.",
                                               scene_config.AA_TEXT, (255, 255, 255))
                else:
                    TextSet = NovelText.render("Let's first understand how you move.",
                                               scene_config.AA_TEXT, (255, 255, 255))
            case 2:
                if LANGUAGE_SET == "UA":
                    TextSet = NovelText.render("Рухайся завдяки WASD",
                                               scene_config.AA_TEXT, (255, 255, 255))
                else:
                    TextSet = NovelText.render("Move thanks to WASD",
                                               scene_config.AA_TEXT, (255, 255, 255))
            case 3:
                if LANGUAGE_SET == "UA":
                    TextSet = NovelText.render("А ще, коли ти бачиш якісь дивні віконця, натискай Q",
                                               scene_config.AA_TEXT, (255, 255, 255))
                else:
                    TextSet = NovelText.render("Also, when you see some strange windows, press Q",
                                               scene_config.AA_TEXT, (255, 255, 255))
            case 4:
                running = False
                scene_config.switch_scene(scene0)
        if fill_image_alpha >= 180:
            sc.blit(titlechan, titlechan.get_rect(center=(190, 300)))
            sc.blit(FrameImage, FrameImageRect)
            sc.blit(NameText, NameRect)
            sc.blit(KeyText, KeyTextRect)
            sc.blit(TextSet, TextSetRect)
        else:
            fill_image_alpha += 5
            fill_image.set_alpha(fill_image_alpha)
        pygame.display.update()
        clock.tick(FPS)


def tips_two():
    global titlechan
    running = True
    FPS: int = 60

    fill_image = createImage("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png")
    fill_image = pygame.transform.scale(fill_image, WINDOWS_SIZE)
    fill_image.set_alpha(0)
    fill_image_alpha = 0

    FrameImage = pygame.image.load("ASSETS/SPRITE/UI/Novel/Frame.png").convert_alpha()
    FrameImage = pygame.transform.scale(FrameImage, (WINDOWS_SIZE[0], 266))
    FrameImageRect = FrameImage.get_rect(center=(WINDOWS_SIZE[0] // 2, 660))
    FrameImage.set_alpha(295)

    NameText = NovelTextName.render("TitleChanQWERTY", scene_config.AA_TEXT, (255, 255, 255))
    NameRect = NameText.get_rect(center=(306, 546))

    TextSet = NovelText.render("Оу. Хто це там? Їх треба знищити, поки вони нічого не накоїли.",
                               scene_config.AA_TEXT, (255, 255, 255))
    TextSetRect = TextSet.get_rect(center=(335, 605))

    select: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if fill_image_alpha >= 180:
                if e.type == pygame.JOYBUTTONUP and e.button == 7:
                    select += 1
                if e.type == pygame.KEYDOWN and e.key == pygame.K_SPACE:
                    select += 1
        sc.fill((0, 0, 0))
        scene0_draw()
        sc.blit(ExpText, ExpTextRect)
        pygame.draw.rect(sc, (0, 0, 0), (0, 0, WINDOWS_SIZE[0], WINDOWS_SIZE[1]), 19)
        pygame.draw.rect(sc, (255, 255, 255), (19, 15, 1245, 685), 1)
        if scene_config.isShowPos: sc.blit(scene_config.PlayerPosImage, scene_config.PlayerPosRect)
        sc.blit(heartUI, heartUIRect)
        sc.blit(HealthText, HealthTextRect)
        if scene_config.p.EXP >= 65: sc.blit(AmmoText, AmmoTextRect)
        sc.blit(ScoreText, ScoreTextRect)
        sc.blit(MultipleScoreTXT, MultipleScoreRect)
        sc.blit(bombCount, bombCount.get_rect(center=(WINDOWS_SIZE[0] // 2 + 545, WINDOWS_SIZE[1] // 2 + 315)))
        sc.blit(BombIcon, BombIcon.get_rect(center=(WINDOWS_SIZE[0] // 2 + 495, WINDOWS_SIZE[1] // 2 + 315)))
        sc.blit(fill_image, fill_image.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
        match select:
            case 0:
                if LANGUAGE_SET == "UA":
                    titlechan = createImage("ASSETS/SPRITE/UI/VisualNovel/TitleChan/3.png")
                    titlechan = pygame.transform.scale(titlechan, (358, 565))
                    TextSet = NovelText.render("Оу. Хто це там? Їх треба знищити, поки вони нічого не накоїли.",
                                               scene_config.AA_TEXT, (255, 255, 255))
                else:
                    TextSet = NovelText.render("Oh Who is that there? They must be destroyed before they do anything.",
                                               scene_config.AA_TEXT, (255, 255, 255))
            case 1:
                titlechan = createImage("ASSETS/SPRITE/UI/VisualNovel/TitleChan/1.png")
                titlechan = pygame.transform.scale(titlechan, (358, 565))
                if LANGUAGE_SET == "UA":
                    TextSet = NovelText.render("Але як?",
                                               scene_config.AA_TEXT, (255, 255, 255))
                else:
                    TextSet = NovelText.render("But How?",
                                               scene_config.AA_TEXT, (255, 255, 255))
            case 2:
                titlechan = createImage("ASSETS/SPRITE/UI/VisualNovel/TitleChan/2.png")
                titlechan = pygame.transform.scale(titlechan, (358, 565))
                if LANGUAGE_SET == "UA":
                    TextSet = NovelText.render("Натисни і тримай клавішу K",
                                               scene_config.AA_TEXT, (255, 255, 255))
                else:
                    TextSet = NovelText.render("Press and hold the K",
                                               scene_config.AA_TEXT, (255, 255, 255))
            case 3:
                running = False
                scene_config.switch_scene(scene0)
        if fill_image_alpha >= 180:
            sc.blit(titlechan, titlechan.get_rect(center=(190, 300)))
            sc.blit(FrameImage, FrameImageRect)
            sc.blit(NameText, NameRect)
            sc.blit(KeyText, KeyTextRect)
            sc.blit(TextSet, TextSetRect)
        else:
            fill_image_alpha += 5
            fill_image.set_alpha(fill_image_alpha)
        pygame.display.update()
        clock.tick(FPS)


def tips_tree():
    global titlechan
    running = True
    FPS: int = 60

    fill_image = createImage("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png")
    fill_image = pygame.transform.scale(fill_image, WINDOWS_SIZE)
    fill_image.set_alpha(0)
    fill_image_alpha = 0

    FrameImage = pygame.image.load("ASSETS/SPRITE/UI/Novel/Frame.png").convert_alpha()
    FrameImage = pygame.transform.scale(FrameImage, (WINDOWS_SIZE[0], 266))
    FrameImageRect = FrameImage.get_rect(center=(WINDOWS_SIZE[0] // 2, 660))
    FrameImage.set_alpha(295)

    NameText = NovelTextName.render("TitleChanQWERTY", scene_config.AA_TEXT, (255, 255, 255))
    NameRect = NameText.get_rect(center=(306, 546))

    TextSet = NovelText.render("Фух вспіли.",
                               scene_config.AA_TEXT, (255, 255, 255))
    TextSetRect = TextSet.get_rect(center=(295, 605))

    select: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if fill_image_alpha >= 180:
                if e.type == pygame.JOYBUTTONUP and e.button == 7:
                    select += 1
                if e.type == pygame.KEYDOWN and e.key == pygame.K_SPACE:
                    select += 1
        sc.fill((0, 0, 0))
        scene0_draw()
        sc.blit(ExpText, ExpTextRect)
        pygame.draw.rect(sc, (0, 0, 0), (0, 0, WINDOWS_SIZE[0], WINDOWS_SIZE[1]), 19)
        pygame.draw.rect(sc, (255, 255, 255), (19, 15, 1245, 685), 1)
        if scene_config.isShowPos: sc.blit(scene_config.PlayerPosImage, scene_config.PlayerPosRect)
        sc.blit(heartUI, heartUIRect)
        sc.blit(HealthText, HealthTextRect)
        if scene_config.p.EXP >= 65: sc.blit(AmmoText, AmmoTextRect)
        sc.blit(ScoreText, ScoreTextRect)
        sc.blit(MultipleScoreTXT, MultipleScoreRect)
        sc.blit(bombCount, bombCount.get_rect(center=(WINDOWS_SIZE[0] // 2 + 545, WINDOWS_SIZE[1] // 2 + 315)))
        sc.blit(BombIcon, BombIcon.get_rect(center=(WINDOWS_SIZE[0] // 2 + 495, WINDOWS_SIZE[1] // 2 + 315)))
        sc.blit(fill_image, fill_image.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
        match select:
            case 0:
                titlechan = createImage("ASSETS/SPRITE/UI/VisualNovel/TitleChan/1.png")
                titlechan = pygame.transform.scale(titlechan, (358, 565))
                if LANGUAGE_SET == "UA":
                    TextSet = NovelText.render("І так...",
                                               scene_config.AA_TEXT, (255, 255, 255))
                else:
                    TextSet = NovelText.render("So...",
                                               scene_config.AA_TEXT, (255, 255, 255))
            case 1:
                titlechan = createImage("ASSETS/SPRITE/UI/VisualNovel/TitleChan/1.png")
                titlechan = pygame.transform.scale(titlechan, (358, 565))
                if LANGUAGE_SET == "UA":
                    TextSet = NovelText.render("бачив якісь сині кулі? Їх треба збирати.",
                                               scene_config.AA_TEXT, (255, 255, 255))
                else:
                    TextSet = NovelText.render("seen any blue orbs? They must be collected.",
                                               scene_config.AA_TEXT, (255, 255, 255))
            case 2:
                titlechan = createImage("ASSETS/SPRITE/UI/VisualNovel/TitleChan/2.png")
                titlechan = pygame.transform.scale(titlechan, (358, 565))
                if LANGUAGE_SET == "UA":
                    TextSet = NovelText.render(
                        "Вони покращують силу твого персонажа, за рахунок підвищення показника ЕКСП",
                        scene_config.AA_TEXT, (255, 255, 255))
                else:
                    TextSet = NovelText.render(
                        "They improve the strength of your character by increasing the EXP indicator",
                        scene_config.AA_TEXT, (255, 255, 255))
            case 3:
                running = False
                scene_config.switch_scene(scene0)
        if fill_image_alpha >= 180:
            sc.blit(titlechan, titlechan.get_rect(center=(190, 300)))
            sc.blit(FrameImage, FrameImageRect)
            sc.blit(NameText, NameRect)
            sc.blit(KeyText, KeyTextRect)
            sc.blit(TextSet, TextSetRect)
        else:
            fill_image_alpha += 5
            fill_image.set_alpha(fill_image_alpha)
        pygame.display.update()
        clock.tick(FPS)


def tips_four():
    global titlechan
    running = True
    FPS: int = 60

    fill_image = createImage("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png")
    fill_image = pygame.transform.scale(fill_image, WINDOWS_SIZE)
    fill_image.set_alpha(0)
    fill_image_alpha = 0

    FrameImage = pygame.image.load("ASSETS/SPRITE/UI/Novel/Frame.png").convert_alpha()
    FrameImage = pygame.transform.scale(FrameImage, (WINDOWS_SIZE[0], 266))
    FrameImageRect = FrameImage.get_rect(center=(WINDOWS_SIZE[0] // 2, 660))
    FrameImage.set_alpha(295)

    NameText = NovelTextName.render("TitleChanQWERTY", scene_config.AA_TEXT, (255, 255, 255))
    NameRect = NameText.get_rect(center=(306, 546))

    TextSet = NovelText.render("Або, можливо ти бачив жовті кулі.",
                               scene_config.AA_TEXT, (255, 255, 255))
    TextSetRect = TextSet.get_rect(center=(295, 605))

    select: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if fill_image_alpha >= 180:
                if e.type == pygame.JOYBUTTONUP and e.button == 7:
                    select += 1
                if e.type == pygame.KEYDOWN and e.key == pygame.K_SPACE:
                    select += 1
        sc.fill((0, 0, 0))
        scene0_draw()
        sc.blit(ExpText, ExpTextRect)
        pygame.draw.rect(sc, (0, 0, 0), (0, 0, WINDOWS_SIZE[0], WINDOWS_SIZE[1]), 19)
        pygame.draw.rect(sc, (255, 255, 255), (19, 15, 1245, 685), 1)
        if scene_config.isShowPos: sc.blit(scene_config.PlayerPosImage, scene_config.PlayerPosRect)
        sc.blit(heartUI, heartUIRect)
        sc.blit(HealthText, HealthTextRect)
        if scene_config.p.EXP >= 65: sc.blit(AmmoText, AmmoTextRect)
        sc.blit(ScoreText, ScoreTextRect)
        sc.blit(MultipleScoreTXT, MultipleScoreRect)
        sc.blit(bombCount, bombCount.get_rect(center=(WINDOWS_SIZE[0] // 2 + 545, WINDOWS_SIZE[1] // 2 + 315)))
        sc.blit(BombIcon, BombIcon.get_rect(center=(WINDOWS_SIZE[0] // 2 + 495, WINDOWS_SIZE[1] // 2 + 315)))
        sc.blit(fill_image, fill_image.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
        match select:
            case 0:
                titlechan = createImage("ASSETS/SPRITE/UI/VisualNovel/TitleChan/2.png")
                titlechan = pygame.transform.scale(titlechan, (358, 565))
                if LANGUAGE_SET == "UA":
                    TextSet = NovelText.render("Або, можливо ти бачив жовті кулі.",
                                               scene_config.AA_TEXT, (255, 255, 255))
                else:
                    TextSet = NovelText.render("Or maybe you saw yellow balls.",
                                               scene_config.AA_TEXT, (255, 255, 255))
            case 1:
                if LANGUAGE_SET == "UA":
                    TextSet = NovelText.render("Їх теж треба збирати, щоб не закінчувалися набої.",
                                               scene_config.AA_TEXT, (255, 255, 255))
                else:
                    TextSet = NovelText.render("They must also be collected so that the cartridges do not run out.",
                                               scene_config.AA_TEXT, (255, 255, 255))
            case 2:
                titlechan = createImage("ASSETS/SPRITE/UI/VisualNovel/TitleChan/1.png")
                titlechan = pygame.transform.scale(titlechan, (358, 565))
                if LANGUAGE_SET == "UA":
                    TextSet = NovelText.render(
                        "В іншому випадку, ви не можете стріляти покращеними кулями",
                        scene_config.AA_TEXT, (255, 255, 255))
                else:
                    TextSet = NovelText.render(
                        "Otherwise, you cannot shoot enhanced bullets",
                        scene_config.AA_TEXT, (255, 255, 255))
            case 3:
                running = False
                scene_config.switch_scene(scene0)
        if fill_image_alpha >= 180:
            sc.blit(titlechan, titlechan.get_rect(center=(190, 300)))
            sc.blit(FrameImage, FrameImageRect)
            sc.blit(NameText, NameRect)
            sc.blit(KeyText, KeyTextRect)
            sc.blit(TextSet, TextSetRect)
        else:
            fill_image_alpha += 5
            fill_image.set_alpha(fill_image_alpha)
        pygame.display.update()
        clock.tick(FPS)


def tips_five():
    global titlechan
    running = True
    FPS: int = 60

    fill_image = createImage("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png")
    fill_image = pygame.transform.scale(fill_image, WINDOWS_SIZE)
    fill_image.set_alpha(0)
    fill_image_alpha = 0

    FrameImage = pygame.image.load("ASSETS/SPRITE/UI/Novel/Frame.png").convert_alpha()
    FrameImage = pygame.transform.scale(FrameImage, (WINDOWS_SIZE[0], 266))
    FrameImageRect = FrameImage.get_rect(center=(WINDOWS_SIZE[0] // 2, 660))
    FrameImage.set_alpha(295)

    NameText = NovelTextName.render("TitleChanQWERTY", scene_config.AA_TEXT, (255, 255, 255))
    NameRect = NameText.get_rect(center=(306, 546))

    TextSet = NovelText.render("Або, можливо ти бачив жовті кулі.",
                               scene_config.AA_TEXT, (255, 255, 255))
    TextSetRect = TextSet.get_rect(center=(295, 605))

    select: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if fill_image_alpha >= 180:
                if e.type == pygame.JOYBUTTONUP and e.button == 7:
                    select += 1
                if e.type == pygame.KEYDOWN and e.key == pygame.K_SPACE:
                    select += 1
        sc.fill((0, 0, 0))
        scene0_draw()
        sc.blit(ExpText, ExpTextRect)
        pygame.draw.rect(sc, (0, 0, 0), (0, 0, WINDOWS_SIZE[0], WINDOWS_SIZE[1]), 19)
        pygame.draw.rect(sc, (255, 255, 255), (19, 15, 1245, 685), 1)
        if scene_config.isShowPos: sc.blit(scene_config.PlayerPosImage, scene_config.PlayerPosRect)
        sc.blit(heartUI, heartUIRect)
        sc.blit(HealthText, HealthTextRect)
        if scene_config.p.EXP >= 65: sc.blit(AmmoText, AmmoTextRect)
        sc.blit(ScoreText, ScoreTextRect)
        sc.blit(MultipleScoreTXT, MultipleScoreRect)
        sc.blit(bombCount, bombCount.get_rect(center=(WINDOWS_SIZE[0] // 2 + 545, WINDOWS_SIZE[1] // 2 + 315)))
        sc.blit(BombIcon, BombIcon.get_rect(center=(WINDOWS_SIZE[0] // 2 + 495, WINDOWS_SIZE[1] // 2 + 315)))
        sc.blit(fill_image, fill_image.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
        match select:
            case 0:
                titlechan = createImage("ASSETS/SPRITE/UI/VisualNovel/TitleChan/2.png")
                titlechan = pygame.transform.scale(titlechan, (358, 565))
                if LANGUAGE_SET == "UA":
                    TextSet = NovelText.render("Вітаю! Тепер ти знаєш основні речі в цій грі!",
                                               scene_config.AA_TEXT, (255, 255, 255))
                else:
                    TextSet = NovelText.render("Congratulations! Now you know the basics of this game!",
                                               scene_config.AA_TEXT, (255, 255, 255))
            case 1:
                titlechan = createImage("ASSETS/SPRITE/UI/VisualNovel/TitleChan/1.png")
                titlechan = pygame.transform.scale(titlechan, (358, 565))
                if LANGUAGE_SET == "UA":
                    TextSet = NovelText.render("Щож. Тепер я залишаю тебе.",
                                               scene_config.AA_TEXT, (255, 255, 255))
                else:
                    TextSet = NovelText.render("Well Now I'm leaving you.",
                                               scene_config.AA_TEXT, (255, 255, 255))
            case 2:
                titlechan = createImage("ASSETS/SPRITE/UI/VisualNovel/TitleChan/2.png")
                titlechan = pygame.transform.scale(titlechan, (358, 565))
                if LANGUAGE_SET == "UA":
                    TextSet = NovelText.render(
                        "Я дозволяю, залишитись тобі, і трішечкі потренуватись. Удачі!",
                        scene_config.AA_TEXT, (255, 255, 255))
                else:
                    TextSet = NovelText.render(
                        "I allow you to stay and practice a little. Good luck!",
                        scene_config.AA_TEXT, (255, 255, 255))
            case 3:
                for i in range(55):
                    create_enemy.createShota()
                running = False
                scene_config.switch_scene(scene0)
        if fill_image_alpha >= 180:
            sc.blit(titlechan, titlechan.get_rect(center=(190, 300)))
            sc.blit(FrameImage, FrameImageRect)
            sc.blit(NameText, NameRect)
            sc.blit(KeyText, KeyTextRect)
            sc.blit(TextSet, TextSetRect)
        else:
            fill_image_alpha += 5
            fill_image.set_alpha(fill_image_alpha)
        pygame.display.update()
        clock.tick(FPS)
