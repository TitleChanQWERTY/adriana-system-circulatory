from random import randint

import pygame.transform

import ad_system
import effect_system
from PLAYER import player
from config_script import *
from ITEM import create_item

select_scene: None = None

p = player.Player(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2, 8.5, "ASSETS/SPRITE/PLAYER/QWERTY.png")
isDrawPlayer: bool = True

globalRank: str = '-'
Score: int = 0

isShowPos = config.getboolean("OTHER", "showposindicator")

PlayerPosImage = pygame.image.load("ASSETS/SPRITE/UI/ICON/PlayerPos.png").convert_alpha()
PlayerPosImage = pygame.transform.scale(PlayerPosImage, (31, 43))
PlayerPosRect = PlayerPosImage.get_rect(center=(p.rect.x, 689))
PlayerPosImage.set_alpha(85)

PlayerProgresInOne = createImage("ASSETS/SPRITE/PLAYER/Indicator.png")
PlayerProgresInOne = pygame.transform.scale(PlayerProgresInOne, (14, 14))
PlayerProgresInOneRect = PlayerProgresInOne.get_rect(center=(p.rect.x + 15, p.rect.y + 11))
AngleOne: int = 0
AngleSpeed: int = 1

PlayerProgresInTWO = createImage("ASSETS/SPRITE/PLAYER/Indicator.png")
PlayerProgresInTWO = pygame.transform.scale(PlayerProgresInTWO, (14, 14))
PlayerProgresInTWORect = PlayerProgresInTWO.get_rect(center=(p.rect.x - 5, p.rect.y + 11))

playerShowIndicator = createImage("ASSETS/SPRITE/PLAYER/IndicatorShow.png")
playerShowIndicator = pygame.transform.scale(playerShowIndicator, (86, 86))
playerShowIndicatorRect = playerShowIndicator.get_rect(center=(p.rect.centerx, p.rect.centery))
angleInd: int = 0


def createFill(filename, force=10):
    return effect_system.colorFill(filename, force)


def playerPosSet():
    global PlayerPosRect, PlayerProgresInOneRect, PlayerProgresInOne, AngleOne, PlayerProgresInTWO, \
        PlayerProgresInTWORect, playerShowIndicatorRect, playerShowIndicator, angleInd
    PlayerPosRect = PlayerPosImage.get_rect(center=(p.rect.x + 6, 689))
    PlayerPosImage.set_alpha(p.rect.y - 35)

    PlayerProgresInOne = createImage("ASSETS/SPRITE/PLAYER/Indicator.png")
    PlayerProgresInOne = pygame.transform.scale(PlayerProgresInOne, (14, 14))
    AngleOne += AngleSpeed
    PlayerProgresInOne = pygame.transform.rotate(PlayerProgresInOne, AngleOne)
    PlayerProgresInOneRect = PlayerProgresInOne.get_rect(center=(p.rect.x + 27, p.rect.y + 11))

    PlayerProgresInTWO = createImage("ASSETS/SPRITE/PLAYER/Indicator.png")
    PlayerProgresInTWO = pygame.transform.scale(PlayerProgresInTWO, (14, 14))
    AngleOne += AngleSpeed-1
    PlayerProgresInTWO = pygame.transform.rotate(PlayerProgresInTWO, AngleOne)
    PlayerProgresInTWORect = PlayerProgresInTWO.get_rect(center=(p.rect.x - 9, p.rect.y + 11))

    if p.EXP >= 405:
        playerShowIndicator = createImage("ASSETS/SPRITE/PLAYER/IndicatorShow.png")
        playerShowIndicator = pygame.transform.scale(playerShowIndicator, (86, 86))
        angleInd -= 3
        playerShowIndicator = pygame.transform.rotate(playerShowIndicator, angleInd)
        playerShowIndicatorRect = playerShowIndicator.get_rect(center=(p.rect.centerx, p.rect.centery))


timeCreateAdMax: int = randint(365, 650)
timeCreateAd: int = 0

isCreateAD = config.getboolean("OTHER", "ad_gameplay")


def createAd():
    global timeCreateAd
    if isCreateAD:
        if timeCreateAd > timeCreateAdMax:
            timeCreateAd = 0
            return ad_system.ad()
        else:
            timeCreateAd += 1


isMoveBackEffect = config.getboolean("OTHER", "moveBackEffect")
TimeCountLevelEffect = config.getint("OTHER", "countbackeffect")

isParticle = config.getboolean("OTHER", "particle")
isParticleMore: bool = True
isRotateObject = config.getboolean("OTHER", "rotateobject")
isLight = config.getboolean("OTHER", "light_effect")

timeCreateCrystal: int = 0
timeCreateCrystalMax: int = randint(100, 150)


def createCrystal():
    global timeCreateCrystal
    if timeCreateCrystal >= timeCreateCrystalMax:
        create_item.create_crystal_task()
        timeCreateCrystal = 0
    else:
        timeCreateCrystal += 1


LEVEL_TASK: int = 2

isFillAd: bool = False

KILL_COUNT: int = 0
MAX_KILL: int = 0
CRYSTAL_COUNT: int = 0
CRYSTAL_MAX: int = 0

TRASH_COUNT = config.getint("OTHER", "trash_count")

isVisibleKey: bool = False

AA_TEXT = config.getboolean("DEFAULT", "aa_text")

FPS_SHOW = config.getboolean("DEFAULT", "show_fps")

DIFFICULTY: int = 0

GameMode: int = 0

SELECT_BACK: int = 0


def switch_scene(scene):
    global select_scene
    select_scene = scene
