import pygame.mixer

import take_screenshot
from SCENE import scene_menu, scene_config
from LOADING import loading
from config_script import *
from sfx_compilation import PISTOL_SHOOT, DEATH_PEOPLE
import reset_data

TEXT_SET = ("", "", "", "", "")

match LANGUAGE_SET:
    case "UA":
        TEXT_SET = (TEXT_DIE_UA[0], TEXT_DIE_UA[1], TEXT_DIE_UA[2], TEXT_DIE_UA[3], TEXT_DIE_UA[4], TEXT_DIE_UA[5])
    case "EN":
        TEXT_SET = (TEXT_DIE_EN[0], TEXT_DIE_EN[1], TEXT_DIE_EN[2], TEXT_DIE_EN[3], TEXT_DIE_EN[4], TEXT_DIE_EN[5])


def scene_die():
    running: bool = True
    FPS: int = 60

    dieImage = pygame.image.load("ASSETS/SPRITE/PLAYER/Die/Ioann/1.png").convert_alpha()
    dieImage = pygame.transform.scale(dieImage, (158, 158))
    dieImageRect = dieImage.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2))

    DieFrame = 0

    alphaImage = 250
    isAnim = True
    isAlpha = False

    arrowImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/Arrow_simple.png").convert_alpha()
    arrowImage = pygame.transform.scale(arrowImage, (29, 27))
    arrowImageRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 122, 336))

    DieTextAlpha = [15, 0]

    DieFont = pygame.font.Font("ASSETS/FONT/Alice-Regular.ttf", 39)
    DieText = DieFont.render(TEXT_SET[0], scene_config.AA_TEXT, (255, 255, 0))
    DieTextRect = DieText.get_rect(center=(WINDOWS_SIZE[0] // 2, 100))
    DieText.set_alpha(DieTextAlpha[0])

    DieText2 = DieFont.render(TEXT_SET[1], scene_config.AA_TEXT, (255, 0, 0))
    DieTextRect2 = DieText2.get_rect(center=(WINDOWS_SIZE[0] // 2, 180))
    DieText2.set_alpha(DieTextAlpha[1])

    fontPlay = pygame.font.Font("ASSETS/FONT/Alice-Regular.ttf", 22)

    playText = fontPlay.render(TEXT_SET[2], scene_config.AA_TEXT, (200, 0, 0))
    playTextRect = playText.get_rect(center=(WINDOWS_SIZE[0] // 2 + 250, 335))

    menuText = fontPlay.render(TEXT_SET[3], scene_config.AA_TEXT, (255, 255, 255))
    menuTextRect = menuText.get_rect(center=(WINDOWS_SIZE[0] // 2 + 250, 385))

    exitText = fontPlay.render(TEXT_SET[4], scene_config.AA_TEXT, (255, 255, 0))
    exitTextRect = exitText.get_rect(center=(WINDOWS_SIZE[0] // 2 + 250, 435))

    select: int = 0

    reset_data.reset()

    pygame.mixer.music.stop()

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                scene_config.switch_scene(None)
                running = False
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    take_screenshot.take()
                if e.key == pygame.K_DOWN:
                    match select:
                        case 0:
                            select = 1
                        case 1:
                            select = 2
                        case 2:
                            select = 0
                if e.key == pygame.K_UP:
                    match select:
                        case 0:
                            select = 2
                        case 1:
                            select = 0
                        case 2:
                            select = 1
                if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                    match select:
                        case 0:
                            scene_config.switch_scene(loading.loading)
                            running = False
                        case 1:
                            pygame.mixer.music.load("ASSETS/OST/Moje Shita Ce Stragdanya.wav")
                            pygame.mixer.music.play()
                            pygame.mixer.music.set_volume(0.10)
                            scene_config.switch_scene(scene_menu.scene_menu)
                            running = False
                        case 2:
                            scene_config.switch_scene(None)
                            running = False

        sc.fill((17, 0, 0))
        match isAnim:
            case True:
                match DieFrame:
                    case 35:
                        dieImage = pygame.image.load("ASSETS/SPRITE/PLAYER/Die/Ioann/1.png").convert_alpha()
                    case 75:
                        dieImage = pygame.image.load("ASSETS/SPRITE/PLAYER/Die/Ioann/2.png").convert_alpha()
                    case 80:
                        dieImage = pygame.image.load("ASSETS/SPRITE/PLAYER/Die/Ioann/3.png").convert_alpha()
                    case 85:
                        PISTOL_SHOOT.play()
                        DEATH_PEOPLE.play()
                        dieImage = pygame.image.load("ASSETS/SPRITE/PLAYER/Die/Ioann/4.png").convert_alpha()
                    case 90:
                        dieImage = pygame.image.load("ASSETS/SPRITE/PLAYER/Die/Ioann/5.png").convert_alpha()
                    case 91:
                        dieImage = pygame.image.load("ASSETS/SPRITE/PLAYER/Die/Ioann/6.png").convert_alpha()
                    case 91:
                        dieImage = pygame.image.load("ASSETS/SPRITE/PLAYER/Die/Ioann/6.png").convert_alpha()
                    case 100:
                        dieImage = pygame.image.load("ASSETS/SPRITE/PLAYER/Die/Ioann/7.png").convert_alpha()
                    case 110:
                        dieImage = pygame.image.load("ASSETS/SPRITE/PLAYER/Die/Ioann/8.png").convert_alpha()
                    case 115:
                        isAnim = False
                DieFrame += 1
                dieImage = pygame.transform.scale(dieImage, (158, 158))
            case False:
                if DieText.get_alpha() < 250:
                    DieTextAlpha[0] += 5
                    DieText.set_alpha(DieTextAlpha[0])
                else:
                    if DieText2.get_alpha() < 220:
                        DieTextAlpha[1] += 2
                        DieText2.set_alpha(DieTextAlpha[1])
                    else:
                        match select:
                            case 0:
                                arrowImageRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 + 122, 336))
                            case 1:
                                arrowImageRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 + 105, 386))
                            case 2:
                                arrowImageRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 + 145, 435))
                        sc.blit(arrowImage, arrowImageRect)
                        sc.blit(playText, playTextRect)
                        sc.blit(menuText, menuTextRect)
                        sc.blit(exitText, exitTextRect)
                sc.blit(DieText, DieTextRect)
                sc.blit(DieText2, DieTextRect2)
        sc.blit(dieImage, dieImageRect)
        pygame.display.update()
        clock.tick(FPS)


def scene_time_over():
    running: bool = True
    FPS: int = 60

    arrowImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/Arrow_simple.png").convert_alpha()
    arrowImage = pygame.transform.scale(arrowImage, (29, 27))
    arrowImageRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 122, 304))

    sizeImage: int = 16
    imagePlayerDie = pygame.image.load("ASSETS/SPRITE/PLAYER/TimeOver/Ioann/1.png").convert_alpha()
    imagePlayerDie = pygame.transform.scale(imagePlayerDie, (sizeImage, sizeImage))
    imagePlayerDieRect = imagePlayerDie.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2))

    Frame: int = 0

    isAnim: bool = True

    timeStart: int = 0

    colorBack = (0, 0, 0)

    fontPlay = pygame.font.Font("ASSETS/FONT/Alice-Regular.ttf", 22)

    DieFont = pygame.font.Font("ASSETS/FONT/Alice-Regular.ttf", 39)
    DieText = DieFont.render(TEXT_SET[5], scene_config.AA_TEXT, (255, 245, 0))
    DieTextRect = DieText.get_rect(center=(WINDOWS_SIZE[0] // 2, 115))
    DieText.set_alpha(0)

    playText = fontPlay.render(TEXT_SET[2], scene_config.AA_TEXT, (200, 0, 0))
    playTextRect = playText.get_rect(center=(WINDOWS_SIZE[0] // 2 + 5, 304))

    menuText = fontPlay.render(TEXT_SET[3], scene_config.AA_TEXT, (255, 255, 255))
    menuTextRect = menuText.get_rect(center=(WINDOWS_SIZE[0] // 2 + 215, 385))

    exitText = fontPlay.render(TEXT_SET[4], scene_config.AA_TEXT, (255, 255, 0))
    exitTextRect = exitText.get_rect(center=(WINDOWS_SIZE[0] // 2 - 155, 385))

    AlphaText: int = 0

    playText.set_alpha(AlphaText)
    menuText.set_alpha(AlphaText)
    exitText.set_alpha(AlphaText)

    pos: int = 0

    reset_data.reset()

    pygame.mixer.music.stop()

    die_sound = pygame.mixer.Sound("ASSETS/SFX/BODY_DIE.wav")
    die_sound.set_volume(0.45)
    cep_sound = pygame.mixer.Sound("ASSETS/SFX/CEPI.wav")

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                scene_config.switch_scene(None)
                running = False          
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    take_screenshot.take()
                if e.key == pygame.K_DOWN:
                    match pos:
                        case 0:
                            pos = 1
                        case 1:
                            pos = 2
                        case 2:
                            pos = 0
                if e.key == pygame.K_UP:
                    match pos:
                        case 0:
                            pos = 2
                        case 1:
                            pos = 0
                        case 2:
                            pos = 1
                if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                    match pos:
                        case 0:
                            scene_config.switch_scene(loading.loading)
                            running = False
                        case 1:
                            scene_config.switch_scene(None)
                            running = False
                        case 2:
                            pygame.mixer.music.load("ASSETS/OST/Moje Shita Ce Stragdanya.wav")
                            pygame.mixer.music.play()
                            pygame.mixer.music.set_volume(0.10)
                            scene_config.switch_scene(scene_menu.scene_menu)
                            running = False

        sc.fill(colorBack)

        if timeStart < 115:
            timeStart += 1
            sizeImage += 1
            imagePlayerDie = pygame.image.load("ASSETS/SPRITE/PLAYER/TimeOver/Ioann/1.png").convert_alpha()
            imagePlayerDie = pygame.transform.scale(imagePlayerDie, (sizeImage, sizeImage))
            imagePlayerDieRect = imagePlayerDie.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2))
        else:
            match isAnim:
                case True:
                    match Frame:
                        case 5:
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/1.png").convert_alpha()
                        case 10:
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/2.png").convert_alpha()
                        case 15:
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/3.png").convert_alpha()
                        case 20:
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/4.png").convert_alpha()
                        case 25:
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/5.png").convert_alpha()
                        case 30:
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/6.png").convert_alpha()
                        case 35:
                            DEATH_PEOPLE.play()
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/7.png").convert_alpha()
                        case 40:
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/8.png").convert_alpha()
                        case 95:
                            cep_sound.play()
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/9.png").convert_alpha()
                        case 100:
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/10.png").convert_alpha()
                        case 105:
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/11.png").convert_alpha()
                        case 110:
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/12.png").convert_alpha()
                        case 145:
                            colorBack = (65, 0, 0)
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/13.png").convert_alpha()
                        case 150:
                            die_sound.play()
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/14.png").convert_alpha()
                        case 155:
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/15.png").convert_alpha()
                        case 160:
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/16.png").convert_alpha()
                        case 165:
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/17.png").convert_alpha()
                        case 170:
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/18.png").convert_alpha()
                        case 175:
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/19.png").convert_alpha()
                        case 180:
                            imagePlayerDie = pygame.image.load(
                                "ASSETS/SPRITE/PLAYER/TimeOver/Ioann/20.png").convert_alpha()
                        case 225:
                            isAnim = False
                    Frame += 1
                    imagePlayerDie = pygame.transform.scale(imagePlayerDie, (sizeImage, sizeImage))
                case False:
                    if AlphaText < 245:
                        AlphaText += 5
                        playText.set_alpha(AlphaText)
                        menuText.set_alpha(AlphaText)
                        exitText.set_alpha(AlphaText)
                        DieText.set_alpha(AlphaText)
                    match pos:
                        case 0:
                            arrowImageRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 120, 304))
                        case 1:
                            arrowImageRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 235, 385))
                        case 2:
                            arrowImageRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 + 75, 385))
                    sc.blit(arrowImage, arrowImageRect)
                    sc.blit(playText, playTextRect)
                    sc.blit(menuText, menuTextRect)
                    sc.blit(exitText, exitTextRect)
                    sc.blit(DieText, DieTextRect)
        sc.blit(imagePlayerDie, imagePlayerDieRect)
        pygame.display.update()
        clock.tick(FPS)
