import pygame

from change_op import change
import take_screenshot
from SCENE import scene_config, scene_select_setings, scene_menu
from NOTFICATION import notfication_manager
from font_config import TextOption, FontKILL
from sfx_compilation import SELECT_MENU, ENTER_SELECT
import reset_data
from group_config import fill_group, notfication_group
from config_script import LANGUAGE_SET, isFullScreen, TEXT_OPTION_EN, TEXT_OPTION_UA, WINDOWS_SIZE, sc, clock, \
    TEXT_WARNING_UA, \
    TEXT_WARNING_EN, createImage

isRegen = False
RegenFrame: int = 0

TEXT_SET = ("", "", "", "", "", "", "", '')

fileRegen = "ASSETS/SPRITE/UI/BUTTON/ButtonGenUKR/1.png"

match LANGUAGE_SET:
    case "UA":
        TEXT_SET = (TEXT_OPTION_UA[0], TEXT_OPTION_UA[1], TEXT_OPTION_UA[2],
                    TEXT_OPTION_UA[3], TEXT_OPTION_UA[4], TEXT_OPTION_UA[5],
                    TEXT_WARNING_UA[0], TEXT_WARNING_UA[1], TEXT_OPTION_UA[6])
    case "EN":
        fileRegen = "ASSETS/SPRITE/UI/BUTTON/ButtonGen/1.png"
        TEXT_SET = (TEXT_OPTION_EN[0], TEXT_OPTION_EN[1], TEXT_OPTION_EN[2],
                    TEXT_OPTION_EN[3], TEXT_OPTION_EN[4], TEXT_OPTION_EN[5],
                    TEXT_WARNING_EN[0], TEXT_WARNING_EN[1], TEXT_OPTION_EN[6])

RegenImage = pygame.image.load(fileRegen).convert_alpha()
RegenImage = pygame.transform.scale(RegenImage, (44, 43))
RegenImageRect = RegenImage.get_rect(center=(WINDOWS_SIZE[0] // 2 + 130, 455))
angleRegen = 0

sizeWindow = 1
WindowImage = pygame.image.load("ASSETS/SPRITE/UI/WINDOW.png").convert_alpha()
WindowImage = pygame.transform.scale(WindowImage, (sizeWindow, sizeWindow))
WindowImageRect = WindowImage.get_rect(center=(scene_config.WINDOWS_SIZE[0] // 2, scene_config.WINDOWS_SIZE[1] // 2))
FrameWindow = 0

windowLangText = TextOption.render(TEXT_SET[6], scene_config.AA_TEXT, (250, 250, 250))
windowLangRect = windowLangText.get_rect(
    center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 30))

TextContinueLang = TextOption.render(TEXT_SET[7], scene_config.AA_TEXT, (255, 255, 255))
TextContinueLangRect = TextContinueLang.get_rect(
    center=(WINDOWS_SIZE[0] // 2 + 10, WINDOWS_SIZE[1] // 2 + 90))

Lang_set = LANGUAGE_SET
isFull_Screen = isFullScreen


def option():
    global isRegen, FrameWindow, sizeWindow, WindowImage, WindowImageRect, TextContinueLangRect, Lang_set, isFull_Screen
    running: bool = True
    FPS: int = 60

    select: int = 0

    RegenText = TextOption.render(TEXT_SET[1], scene_config.AA_TEXT, (255, 255, 255))
    RegenTextRect = RegenText.get_rect(center=(WINDOWS_SIZE[0] // 2, 455))

    FullsceenText = TextOption.render(TEXT_SET[8], scene_config.AA_TEXT, (255, 255, 255))
    FullsceenTextRect = FullsceenText.get_rect(center=(WINDOWS_SIZE[0] // 2, 305))

    AAText = TextOption.render(TEXT_SET[3], scene_config.AA_TEXT, (255, 255, 255))
    AATextRect = AAText.get_rect(center=(WINDOWS_SIZE[0] // 2, 355))

    FPSText = TextOption.render(TEXT_SET[4], scene_config.AA_TEXT, (255, 255, 255))
    FPSTextRect = AAText.get_rect(center=(WINDOWS_SIZE[0] // 2 + 30, 405))

    arrowImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ARROW/1.png").convert_alpha()
    arrowImage = pygame.transform.scale(arrowImage, (32, 32))
    arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 65, 245))

    AATextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png").convert_alpha()
    AATextBoolImage = pygame.transform.scale(AATextBoolImage, (64, 64))
    AATextBoolRect = AATextBoolImage.get_rect(center=(WINDOWS_SIZE[0] // 2 + 145, 325))

    FSTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png").convert_alpha()
    FSTextBoolImage = pygame.transform.scale(FSTextBoolImage, (64, 64))
    FSTextBoolRect = FSTextBoolImage.get_rect(center=(WINDOWS_SIZE[0] // 2 + 127, 305))

    FPSTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png").convert_alpha()
    FPSTextBoolImage = pygame.transform.scale(FPSTextBoolImage, (64, 64))
    FPSTextBoolRect = FPSTextBoolImage.get_rect(center=(WINDOWS_SIZE[0] // 2 + 145, 405))

    LangTXT = TextOption.render("МОВА:", scene_config.AA_TEXT, (255, 255, 255))
    LangTextRect = LangTXT.get_rect(center=(WINDOWS_SIZE[0] // 2, 245))

    LanguageImage = pygame.image.load("ASSETS/SPRITE/UI/FLAG/UK.png").convert_alpha()
    LanguageImage = pygame.transform.scale(LanguageImage, (39, 34))
    LanguageImageRect = LanguageImage.get_rect(center=(WINDOWS_SIZE[0] // 2 + 90, 245))

    if Lang_set == "EN":
        TextContinueLangRect = TextContinueLang.get_rect(
            center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 60))
        LanguageImage = pygame.image.load("ASSETS/SPRITE/UI/FLAG/US.png").convert_alpha()
        LanguageImage = pygame.transform.scale(LanguageImage, (39, 34))
        LanguageImageRect = LanguageImage.get_rect(center=(WINDOWS_SIZE[0] // 2 + 90, 245))
        FPSTextRect = AAText.get_rect(center=(WINDOWS_SIZE[0] // 2 + 50, 405))
        LangTXT = TextOption.render("LANGUAGE:", scene_config.AA_TEXT, (255, 255, 255))
        LangTextRect = LangTXT.get_rect(center=(WINDOWS_SIZE[0] // 2-10, 245))
        FPSTextBoolRect = FPSTextBoolImage.get_rect(center=(WINDOWS_SIZE[0] // 2 + 100, 405))
        arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 95, 245))
        AATextBoolRect = AATextBoolImage.get_rect(center=(WINDOWS_SIZE[0] // 2 + 145, 355))
        FullsceenTextRect = FullsceenText.get_rect(center=(WINDOWS_SIZE[0] // 2, 304))
    else:
        AATextBoolRect = AATextBoolImage.get_rect(center=(WINDOWS_SIZE[0] // 2 + 175, 355))

    isKey: bool = True

    ColorSelect = (0, 0, 200)

    yRectSelect: int = 245

    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/white.png", 16)

    TextQUIT = FontKILL.render(":QUIT", scene_config.AA_TEXT, (255, 255, 255))
    TextQUITRect = TextQUIT.get_rect(center=(1130, 680))

    QKeyImage = createImage("ASSETS/SPRITE/UI/KEY/Q.png")
    QKeyImage = pygame.transform.scale(QKeyImage, (31, 31))
    QKeyImageRect = QKeyImage.get_rect(center=(1082, 680))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    take_screenshot.take()
                if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                    ENTER_SELECT.play()
                    match select:
                        case 0:
                            if scene_config.LANGUAGE_SET == "EN":
                                notfication_manager.notfication_create("ASSETS/SPRITE/UI/Notication/4.png",
                                                                       "Please, Restart Game!: UA")
                                scene_config.LANGUAGE_SET = "UA"
                            else:
                                notfication_manager.notfication_create("ASSETS/SPRITE/UI/Notication/4.png",
                                                                       "Перезапустіть Гру!: ENG")
                                scene_config.LANGUAGE_SET = "EN"
                        case 1:
                            match scene_config.isFullScreen:
                                case False:
                                    scene_config.isFullScreen = True
                                    scene_config.sc = pygame.display.set_mode((scene_config.WINDOWS_SIZE[0],
                                                                               scene_config.WINDOWS_SIZE[1]),
                                                                              pygame.FULLSCREEN, pygame.OPENGL, vsync=1)
                                case True:
                                    scene_config.isFullScreen = False
                                    scene_config.sc = pygame.display.set_mode(
                                        (scene_config.WINDOWS_SIZE[0], scene_config.WINDOWS_SIZE[1]))
                        case 2:
                            match scene_config.AA_TEXT:
                                case False:
                                    scene_config.AA_TEXT = True
                                case True:
                                    scene_config.AA_TEXT = False
                        case 3:
                            match scene_config.FPS_SHOW:
                                case False:
                                    scene_config.FPS_SHOW = True
                                case True:
                                    scene_config.FPS_SHOW = False
                        case 4:
                            isRegen = True
                    change()
                if e.key == pygame.K_DOWN and isKey or e.key == pygame.K_s and isKey:
                    SELECT_MENU.play()
                    match select:
                        case 0:
                            select = 1
                        case 1:
                            select = 2
                        case 2:
                            select = 3
                        case 3:
                            select = 4
                        case 4:
                            select = 0
                if e.key == pygame.K_UP and isKey or e.key == pygame.K_w and isKey:
                    SELECT_MENU.play()
                    match select:
                        case 0:
                            select = 4
                        case 1:
                            select = 0
                        case 2:
                            select = 1
                        case 3:
                            select = 2
                        case 4:
                            select = 3
                if e.key == pygame.K_q:
                    scene_config.switch_scene(scene_select_setings.scene)
                    running = False

        match select:
            case 0:
                LangTXT.set_alpha(300)
                FullsceenText.set_alpha(40)
                AAText.set_alpha(40)
                FPSText.set_alpha(40)
                RegenText.set_alpha(40)
                yRectSelect = 295
                arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2-100, 245))
                yRectSelect = 235
            case 1:
                LangTXT.set_alpha(40)
                FullsceenText.set_alpha(300)
                AAText.set_alpha(40)
                FPSText.set_alpha(40)
                RegenText.set_alpha(40)
                yRectSelect = 295
                match scene_config.LANGUAGE_SET:
                    case "UA":
                        arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 145, 305))
                    case "EN":
                        arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 95, 305))
            case 2:
                LangTXT.set_alpha(40)
                FullsceenText.set_alpha(40)
                AAText.set_alpha(300)
                FPSText.set_alpha(40)
                RegenText.set_alpha(40)
                yRectSelect = 345
                match scene_config.LANGUAGE_SET:
                    case "UA":
                        arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 160, 355))
                    case "EN":
                        arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 140, 355))
            case 3:
                LangTXT.set_alpha(40)
                yRectSelect = 395
                FullsceenText.set_alpha(40)
                AAText.set_alpha(40)
                FPSText.set_alpha(300)
                RegenText.set_alpha(40)
                match scene_config.LANGUAGE_SET:
                    case "UA":
                        arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 135, 405))
                    case "EN":
                        arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 95, 405))
            case 4:
                LangTXT.set_alpha(40)
                FullsceenText.set_alpha(40)
                AAText.set_alpha(40)
                FPSText.set_alpha(40)
                RegenText.set_alpha(300)
                yRectSelect = 445
                arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 135, 455))

        match isRegen:
            case True:
                regen()

        match scene_config.AA_TEXT:
            case True:
                AATextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/9.png").convert_alpha()
                AATextBoolImage.set_alpha(300)
                AATextBoolImage = pygame.transform.scale(AATextBoolImage, (64, 64))
            case False:
                AATextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png").convert_alpha()
                AATextBoolImage.set_alpha(100)
                AATextBoolImage = pygame.transform.scale(AATextBoolImage, (64, 64))
        match scene_config.FPS_SHOW:
            case True:
                FPSTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/9.png").convert_alpha()
                FPSTextBoolImage.set_alpha(300)
                FPSTextBoolImage = pygame.transform.scale(FPSTextBoolImage, (64, 64))
            case False:
                FPSTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png").convert_alpha()
                FPSTextBoolImage.set_alpha(100)
                FPSTextBoolImage = pygame.transform.scale(FPSTextBoolImage, (64, 64))
        match scene_config.isFullScreen:
            case True:
                FSTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/9.png").convert_alpha()
                FSTextBoolImage = pygame.transform.scale(FSTextBoolImage, (64, 64))
                FSTextBoolImage.set_alpha(300)
            case False:
                FSTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png").convert_alpha()
                FSTextBoolImage = pygame.transform.scale(FSTextBoolImage, (64, 64))
                FSTextBoolImage.set_alpha(100)

        sc.fill((0, 0, 0))
        pygame.draw.rect(sc, ColorSelect, (0, yRectSelect, WINDOWS_SIZE[0], 22))
        sc.blit(RegenText, RegenTextRect)
        sc.blit(RegenImage, RegenImageRect)
        sc.blit(AAText, AATextRect)
        sc.blit(FPSText, FPSTextRect)
        sc.blit(LangTXT, LangTextRect)
        sc.blit(LanguageImage, LanguageImageRect)
        sc.blit(AATextBoolImage, AATextBoolRect)
        sc.blit(FPSTextBoolImage, FPSTextBoolRect)
        sc.blit(FullsceenText, FullsceenTextRect)
        sc.blit(FSTextBoolImage, FSTextBoolRect)
        sc.blit(arrowImage, arrowRect)
        sc.blit(scene_menu.EKeyImage, scene_menu.EKeyImageRect)
        sc.blit(scene_menu.TextOK, scene_menu.TextOKRect)
        sc.blit(QKeyImage, QKeyImageRect)
        sc.blit(TextQUIT, TextQUITRect)
        notfication_group.draw(sc)
        for n in notfication_group: sc.blit(n.text, n.rectTxt)
        fill_group.draw(sc)
        pygame.display.update()
        clock.tick(FPS)
        fill_group.update()
        notfication_group.update()


def regen():
    global fileRegen, RegenImage, isRegen, RegenFrame, angleRegen, RegenImageRect
    match isRegen:
        case True:
            if RegenFrame <= 30:
                reset_data.reset()
                angleRegen += 15
            else:
                angleRegen = 0
                RegenFrame = 0
                isRegen = False
            RegenImage = pygame.image.load(fileRegen).convert_alpha()
            RegenImage = pygame.transform.scale(RegenImage, (44, 43))
            RegenImage = pygame.transform.rotate(RegenImage, angleRegen)
            RegenImageRect = RegenImage.get_rect(center=(WINDOWS_SIZE[0] // 2 + 130, 455))
            RegenFrame += 1
