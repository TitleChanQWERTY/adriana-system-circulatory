import pygame
from SCENE import scene_config, scene_menu, scene_option, scene_option_other
from config_script import LANGUAGE_SET, sc, clock, WINDOWS_SIZE, createImage
from font_config import FinishFont, FontKILL
from group_config import fill_group
from sfx_compilation import ENTER_SELECT, SELECT_MENU


def scene():
    running: bool = True
    FPS: int = 60

    TEXT_SET = ("ОСНОВНІ", "ІНШІ")

    match LANGUAGE_SET:
        case "EN":
            TEXT_SET = ("MAIN", "OTHER")

    TextMain = FinishFont.render(TEXT_SET[0], scene_config.AA_TEXT, (255, 255, 255))

    TextOther = FinishFont.render(TEXT_SET[1], scene_config.AA_TEXT, (255, 255, 255))

    TextQUIT = FontKILL.render(":QUIT", scene_config.AA_TEXT, (255, 255, 255))
    TextQUITRect = TextQUIT.get_rect(center=(1130, 680))

    QKeyImage = createImage("ASSETS/SPRITE/UI/KEY/Q.png")
    QKeyImage = pygame.transform.scale(QKeyImage, (31, 31))
    QKeyImageRect = QKeyImage.get_rect(center=(1082, 680))

    arrowImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ARROW/1.png").convert_alpha()
    arrowImage = pygame.transform.scale(arrowImage, (32, 32))
    arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 45, WINDOWS_SIZE[1] // 2 - 100))

    select: int = 0

    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/white.png", 12)

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_q:
                    running = False
                    scene_config.switch_scene(scene_menu.scene_menu)
                if e.key == pygame.K_DOWN or e.key == pygame.K_s:
                    SELECT_MENU.play()
                    match select:
                        case 0:
                            select = 1
                        case 1:
                            select = 0
                if e.key == pygame.K_UP or e.key == pygame.K_w:
                    SELECT_MENU.play()
                    match select:
                        case 0:
                            select = 1
                        case 1:
                            select = 0
                if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                    ENTER_SELECT.play()
                    match select:
                        case 0:
                            running = False
                            scene_config.switch_scene(scene_option.option)
                        case 1:
                            running = False
                            scene_config.switch_scene(scene_option_other.op_other)

        match select:
            case 0:
                arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 115, WINDOWS_SIZE[1] // 2 - 100))
            case 1:
                arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 75, WINDOWS_SIZE[1] // 2 + 100))

        sc.fill((0, 0, 0))
        sc.blit(TextMain, TextMain.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 100)))
        sc.blit(TextOther, TextOther.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 100)))
        sc.blit(scene_menu.EKeyImage, scene_menu.EKeyImageRect)
        sc.blit(scene_menu.TextOK, scene_menu.TextOKRect)
        sc.blit(arrowImage, arrowRect)
        sc.blit(QKeyImage, QKeyImageRect)
        sc.blit(TextQUIT, TextQUITRect)
        fill_group.draw(sc)
        pygame.display.update()
        clock.tick(FPS)
        fill_group.update()
