import pygame
from SCENE import scene_config, scene_novel, scene_game
from config_script import createImage, sc, clock, WINDOWS_SIZE
from group_config import fill_group, back_effect_group
from sfx_compilation import DAMAGE_SHUTLE


def scene0():
    running: bool = True
    FPS: int = 60

    Frame: int = 0

    image = createImage("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png")
    image = pygame.transform.scale(image, (1450, 1038))
    image_rect = image.get_rect(center=(WINDOWS_SIZE[0] // 2 + 250, WINDOWS_SIZE[1] // 2 + 15))

    for effect in fill_group:
        effect.kill()
    for back in back_effect_group:
        scene_game.createEffect = 0
        back.kill()

    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/white.png", 9)
    DAMAGE_SHUTLE.play()

    pygame.mixer.music.stop()

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)

        sc.fill((0, 0, 0))
        if Frame == 106:
            image = createImage("ASSETS/SPRITE/UI/VisualNovel/CutScene/1/1.png")
            image = pygame.transform.scale(image, (920, 648))
            image_rect = image.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 57))
        if Frame == 196:
            image = createImage("ASSETS/SPRITE/UI/VisualNovel/CutScene/1/1.png")
            image = pygame.transform.scale(image, (1455, 1038))
            image_rect = image.get_rect(center=(WINDOWS_SIZE[0] // 2-350, WINDOWS_SIZE[1] // 2 + 57))
        if Frame == 285:
            image = createImage("ASSETS/SPRITE/UI/VisualNovel/CutScene/1/1.png")
            image = pygame.transform.scale(image, (1450, 1038))
            image_rect = image.get_rect(center=(WINDOWS_SIZE[0] // 2 + 395, WINDOWS_SIZE[1] // 2 + 15))
        if Frame == 445:
            image = createImage("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png")
            image = pygame.transform.scale(image, (1450, 1038))
            image_rect = image.get_rect(center=(WINDOWS_SIZE[0] // 2 + 250, WINDOWS_SIZE[1] // 2 + 15))
        if Frame == 565:
            image = createImage("ASSETS/SPRITE/UI/VisualNovel/CutScene/1/2.png")
            image = pygame.transform.scale(image, (1450, 1038))
            image_rect = image.get_rect(center=(WINDOWS_SIZE[0] // 2 + 150, WINDOWS_SIZE[1] // 2 + 15))
        if Frame == 635:
            image = createImage("ASSETS/SPRITE/UI/VisualNovel/CutScene/1/3.png")
            image = pygame.transform.scale(image, (1450, 1038))
            image_rect = image.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 15))
        if Frame == 695:
            image = createImage("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png")
            image = pygame.transform.scale(image, (1450, 1038))
            image_rect = image.get_rect(center=(WINDOWS_SIZE[0] // 2 + 250, WINDOWS_SIZE[1] // 2 + 15))
        if Frame == 755:
            image = createImage("ASSETS/SPRITE/UI/VisualNovel/CutScene/1/4.png")
            image = pygame.transform.scale(image, (920, 648))
            image_rect = image.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 47))
        if Frame == 795:
            image = createImage("ASSETS/SPRITE/UI/VisualNovel/CutScene/1/5.png")
            image = pygame.transform.scale(image, (920, 648))
            image_rect = image.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 47))
        if Frame == 850:
            image = createImage("ASSETS/SPRITE/UI/VisualNovel/CutScene/1/5_5.png")
            image = pygame.transform.scale(image, (920, 648))
            image_rect = image.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 47))
        if Frame == 865:
            image = createImage("ASSETS/SPRITE/UI/VisualNovel/CutScene/1/5.png")
            image = pygame.transform.scale(image, (920, 648))
            image_rect = image.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 47))
        if Frame == 885:
            running = False
            scene_config.switch_scene(scene_novel.scene4)
        Frame += 1
        sc.blit(image, image_rect)
        fill_group.draw(sc)
        pygame.display.update()
        clock.tick(FPS)
        fill_group.update()
