import pygame
from SCENE import scene_config, scene_difficulty, scene_SelectMode
from config_script import sc, clock, createImage, WINDOWS_SIZE, LANGUAGE_SET
from font_config import FontKILL
import os

from sfx_compilation import SELECT_MENU
from take_screenshot import take

fontMusic = pygame.font.Font("ASSETS/FONT/FreeSans.woff", 15)

musicName = "Personal_Music/" + ""


class text(pygame.sprite.Sprite):
    def __init__(self, txt, posY, posX, group):
        super().__init__()
        self.textMusic = fontMusic.render(str(txt), scene_config.AA_TEXT, (255, 255, 255))
        self.textMusicRect = self.textMusic.get_rect(center=(posX + 110, posY))
        self.add(group)

    def update(self, event):
        if event.key == pygame.K_DOWN:
            self.textMusicRect.y -= 50
        if event.key == pygame.K_UP:
            self.textMusicRect.y += 50


def scene():
    global musicName
    FPS: int = 60
    running: bool = True

    posTextY: int = 0

    txtMusicGroup = pygame.sprite.Group()

    dir_folder: str = r"Personal_Music"

    res = None

    select: int = 0
    selectMax = 0

    BranchFont = pygame.font.Font("ASSETS/FONT/joystix monospace.ttf", 13)

    try:
        res = os.listdir(dir_folder)
        len_res = len(res)
        for lent in range(len_res):
            selectMax = lent+1
            posTextY += 50
            text(res[lent], posTextY, 120, txtMusicGroup)
    except FileNotFoundError:
        os.mkdir(r"Personal_Music")

    selector = createImage("ASSETS/SPRITE/UI/SkillTree/2.png")
    selector = pygame.transform.scale(selector, (347, 56))
    selectorY = 50

    QKeyImage = createImage("ASSETS/SPRITE/UI/KEY/Q.png")
    QKeyImage = pygame.transform.scale(QKeyImage, (31, 31))
    QKeyImageRect = QKeyImage.get_rect(center=(1082, 680))

    TextQUIT = FontKILL.render(":QUIT", scene_config.AA_TEXT, (255, 255, 255))
    TextQUITRect = TextQUIT.get_rect(center=(1130, 680))

    noteImage = createImage("ASSETS/SPRITE/UI/Note.png")
    noteImage = pygame.transform.scale(noteImage, (85, 85))
    yNote: int = WINDOWS_SIZE[1] // 2 - 150
    FrameNote: int = 0
    angle: int = 0
    isDown: int = 0

    fileTips = "Скопіюйте свою музику в теку \"Personal_Music\""

    if LANGUAGE_SET == "EN":
        fileTips = "Copy your music on \"Personal_Music\" folder"

    txtTips = BranchFont.render(str(fileTips), scene_config.AA_TEXT, (255, 255, 255))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    take()
                if e.key == pygame.K_DOWN:
                    SELECT_MENU.play()
                    if select < selectMax - 13:
                        select += 1
                        txtMusicGroup.update(e)
                    else:
                        if select < selectMax:
                            select += 1
                            selectorY += 50
                if e.key == pygame.K_UP:
                    SELECT_MENU.play()
                    if 0 < select < selectMax - 12:
                        select -= 1
                        txtMusicGroup.update(e)
                    else:
                        if select > 0:
                            selectorY -= 50
                            select -= 1
                if e.key == pygame.K_e and selectMax > 0 or e.key == pygame.K_RETURN and selectMax > 0:
                    for txt in txtMusicGroup:
                        txt.kill()
                    musicName = "Personal_Music/" + str(res[select])
                    running = False
                    scene_config.switch_scene(scene_difficulty.difficulty)
                if e.key == pygame.K_q:
                    for txt in txtMusicGroup:
                        txt.kill()
                    running = False
                    scene_config.switch_scene(scene_SelectMode.scene_select)
        sc.fill((0, 0, 0))
        selectorRect = selector.get_rect(center=(230, selectorY))
        for txt in txtMusicGroup:
            sc.blit(txt.textMusic, txt.textMusicRect)
        sc.blit(selector, selectorRect)
        sc.blit(txtTips, txtTips.get_rect(center=(WINDOWS_SIZE[0] // 2 + 305, 25)))
        sc.blit(QKeyImage, QKeyImageRect)
        sc.blit(TextQUIT, TextQUITRect)
        if selectMax > 0:
            if FrameNote < 35 and isDown == 0:
                yNote -= 1
                FrameNote += 1
            else:
                if FrameNote > 0:
                    FrameNote -= 1
                    isDown = 1
                    yNote += 1
                else:
                    isDown = 0
            if scene_config.isRotateObject:
                noteImage = createImage("ASSETS/SPRITE/UI/Note.png")
                noteImage = pygame.transform.scale(noteImage, (85, 85))
                angle += 1
                noteImage = pygame.transform.rotate(noteImage, angle)
            sc.blit(noteImage, noteImage.get_rect(center=(WINDOWS_SIZE[0] // 2 + 350, yNote)))
        pygame.display.update()
        clock.tick(FPS)
