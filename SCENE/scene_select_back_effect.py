import pygame
from SCENE import scene_config, scene_menu, scene_SelectMode
from config_script import createImage, sc, clock, WINDOWS_SIZE, LANGUAGE_SET
from font_config import FinishFont, FontKILL
from LOADING import loading


def scene():
    running: bool = True
    FPS: int = 60

    TextQUIT = FontKILL.render(":QUIT", scene_config.AA_TEXT, (255, 255, 255))
    TextQUITRect = TextQUIT.get_rect(center=(1130, 680))

    QKeyImage = createImage("ASSETS/SPRITE/UI/KEY/Q.png")
    QKeyImage = pygame.transform.scale(QKeyImage, (31, 31))
    QKeyImageRect = QKeyImage.get_rect(center=(1082, 680))

    selectorImg = createImage("ASSETS/SPRITE/UI/SkillTree/2.png")
    selectorImg = pygame.transform.scale(selectorImg, (325, 258))
    selectorImgRect = selectorImg.get_rect(center=(WINDOWS_SIZE[0] // 2 - 400, WINDOWS_SIZE[1] // 2 - 128))
    selectorImg.set_alpha(115)

    effect_one = createImage("ASSETS/SPRITE/UI/ICON/EFFECT_BACK/1.png")
    effect_two = createImage("ASSETS/SPRITE/UI/ICON/EFFECT_BACK/2.png")
    effect_two = pygame.transform.scale(effect_two, (225, 178))
    effect_three = createImage("ASSETS/SPRITE/UI/ICON/EFFECT_BACK/3.png")

    TEXT_SET = "ОБЕР!ТЬ ЕФФЕКТ ЗАДНЬОГО ФОНУ"

    if LANGUAGE_SET == "EN":
        TEXT_SET = "SELECT BACK EFFECT!"

    txtInfo = FinishFont.render(TEXT_SET, scene_config.AA_TEXT, (195, 195, 195))

    select: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.JOYHATMOTION:
                if e.hat == 0 and e.value == (-1, 0):
                    match select:
                        case 0:
                            select = 2
                        case 1:
                            select = 0
                        case 2:
                            select = 1
                if e.hat == 0 and e.value == (1, 0):
                    match select:
                        case 0:
                            select = 1
                        case 1:
                            select = 2
                        case 2:
                            select = 0
            if e.type == pygame.JOYBUTTONDOWN:
                if e.button == 0:
                    scene_config.SELECT_BACK = select
                    running = False
                    scene_config.switch_scene(loading.loading)
                if e.button == 1:
                    running = False
                    scene_config.switch_scene(scene_SelectMode.scene_select)
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_LEFT:
                    match select:
                        case 0:
                            select = 2
                        case 1:
                            select = 0
                        case 2:
                            select = 1
                if e.key == pygame.K_RIGHT:
                    match select:
                        case 0:
                            select = 1
                        case 1:
                            select = 2
                        case 2:
                            select = 0
                if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                    scene_config.SELECT_BACK = select
                    running = False
                    scene_config.switch_scene(loading.loading)
                if e.key == pygame.K_q:
                    running = False
                    scene_config.switch_scene(scene_SelectMode.scene_select)

        match select:
            case 0:
                selectorImgRect = selectorImg.get_rect(center=(WINDOWS_SIZE[0] // 2 - 400, WINDOWS_SIZE[1] // 2 - 3))
            case 1:
                selectorImgRect = selectorImg.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 3))
            case 2:
                selectorImgRect = selectorImg.get_rect(center=(WINDOWS_SIZE[0] // 2 + 400, WINDOWS_SIZE[1] // 2 - 3))

        sc.fill((0, 0, 0))
        sc.blit(effect_one, effect_one.get_rect(center=(WINDOWS_SIZE[0] // 2 - 400, WINDOWS_SIZE[1] // 2)))
        sc.blit(effect_two, effect_two.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
        sc.blit(effect_three, effect_three.get_rect(center=(WINDOWS_SIZE[0] // 2 + 400, WINDOWS_SIZE[1] // 2)))
        sc.blit(txtInfo, txtInfo.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 295)))
        sc.blit(selectorImg, selectorImgRect)
        sc.blit(scene_menu.EKeyImage, scene_menu.EKeyImageRect)
        sc.blit(scene_menu.TextOK, scene_menu.TextOKRect)
        sc.blit(QKeyImage, QKeyImageRect)
        sc.blit(TextQUIT, TextQUITRect)
        pygame.display.update()
        clock.tick(FPS)
