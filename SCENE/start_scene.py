from random import randint

import pygame

from ITEM import create_item
from config_script import clock, sc, LANGUAGE_SET, createImage, WINDOWS_SIZE
from SCENE import scene_config, scene_menu
from font_config import FontKILL, NovelText
from group_config import item_group


def warningEye():
    running: bool = True
    FPS: int = 60

    image = createImage("ASSETS/SPRITE/UI/eye.png")
    image = pygame.transform.scale(image, (120, 120))

    TEXT_SET = ["УВАГА!", "ЦЯ ГРА БОЛЮЧЯ ДЛЯ ТВОЇХ ОЧЕНЯТ ПУПСІК!", "ЗБЕРЕЖИ СВОЇ ОЧІ БУДЬ-ЛАСКА"]

    if LANGUAGE_SET == "EN":
        TEXT_SET = ["LISTEN HERE!", "THIS GAME VERY BAD FOR YOUR EYES!", "0_o"]

    txtWarning = NovelText.render(TEXT_SET[0], scene_config.AA_TEXT, (255, 0, 0))

    txtWarning2 = NovelText.render(TEXT_SET[1], scene_config.AA_TEXT, (255, 255, 0))

    txtWarning3 = NovelText.render(TEXT_SET[2], scene_config.AA_TEXT, (255, 255, 255))

    frame: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
        sc.fill((0, 0, 0))
        if frame >= 155:
            for item in item_group:
                item.kill()
            running = False
            scene_config.switch_scene(auotor)
        if frame >= 45:
            sc.blit(image, image.get_rect(center=(WINDOWS_SIZE[0] // 2, 110)))
        frame += 1
        sc.blit(txtWarning, txtWarning.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 50)))
        sc.blit(txtWarning2, txtWarning2.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
        sc.blit(txtWarning3, txtWarning3.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 25)))
        pygame.display.update()
        clock.tick(FPS)


def auotor():
    running: bool = True
    FPS: int = 60

    Frame: int = 0
    FrameAlpha: int = 0

    Image = createImage("ASSETS/SPRITE/UI/StartImage/2.png")
    Image = pygame.transform.scale(Image, (433, 112))

    alpha: int = 0

    font1 = pygame.font.Font("ASSETS/FONT/Alice-Regular.ttf", 45)

    TXT = font1.render("Ігровари :)", scene_config.AA_TEXT, (255, 255, 255))

    timer: int = 0

    while running:
        if timer >= 35:
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    running = False
                    scene_config.switch_scene(None)
                if e.type == pygame.KEYDOWN or e.type == pygame.JOYBUTTONUP:
                    running = False
                    scene_config.switch_scene(startStory)
        else:
            timer += 1

        sc.fill((0, 0, 0))

        if FrameAlpha < 110:
            alpha += 4
        if FrameAlpha > 100:
            alpha -= 5
        if FrameAlpha > 205:
            FrameAlpha = 0

        if Frame > 220:
            Image = createImage("ASSETS/SPRITE/UI/StartImage/pygame.png")
            Image = pygame.transform.scale(Image, (833, 192))
            Frame += 4

        if Frame > 990:
            Image = createImage("ASSETS/SPRITE/UI/StartImage/SPECIAL_THANKS/GameVar.png")
            Image = pygame.transform.scale(Image, (228, 228))
            TXT.set_alpha(alpha)
            sc.blit(TXT, TXT.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 150)))
            Frame += 4
        if Frame > 2810:
            running = False
            scene_config.switch_scene(startStory)

        Image.set_alpha(alpha)
        Frame += 1
        FrameAlpha += 1

        sc.blit(Image, Image.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
        pygame.display.update()
        clock.tick(FPS)


def StartGameAnim():
    FPS: int = 60
    running: bool = True

    image = createImage("ASSETS/SPRITE/UI/PAUSE MENU/ICON/4.png")
    size: int = 75
    forceSize: int = 975
    image = pygame.transform.scale(image, (size, size))
    imageRect = image.get_rect(center=(WINDOWS_SIZE[0] // 2, 850))

    isDrawImage = True

    angle: int = 0
    y: int = 850

    Frame: int = 0

    color = (0, 0, 0)

    while running:
        if Frame >= 35:
            if imageRect.y > WINDOWS_SIZE[1] // 2 - 20:
                y -= 30
            else:
                if size <= 19960:
                    size += forceSize
                    if size >= 2000:
                        isDrawImage = False
                        color = (255, 255, 255)
                else:
                    pygame.mixer.music.load("ASSETS/OST/Moje Shita Ce Stragdanya.wav")
                    pygame.mixer.music.play(-1)
                    pygame.mixer.music.set_volume(0.10)
                    running = False
                    scene_config.switch_scene(scene_menu.scene_menu)
        else:
            Frame += 1
        sc.fill(color)
        if isDrawImage:
            image = createImage("ASSETS/SPRITE/UI/PAUSE MENU/ICON/4.png")
            image = pygame.transform.scale(image, (size, size))
            angle += 15
            image = pygame.transform.rotate(image, angle)
            imageRect = image.get_rect(center=(WINDOWS_SIZE[0] // 2, y))
        sc.blit(image, imageRect)
        pygame.display.update()
        clock.tick(FPS)


def startStory():
    FPS: int = 60
    running: bool = True

    palletImage = createImage("ASSETS/SPRITE/UI/StartImage/1.png")
    palletImage = pygame.transform.scale(palletImage, (WINDOWS_SIZE[0] * 4, WINDOWS_SIZE[1] * 4))
    xPallet: int = WINDOWS_SIZE[0] * 2 + 995
    yPallet: int = 1555
    size = [WINDOWS_SIZE[0] * 3, WINDOWS_SIZE[1] * 3]

    Frame: int = 0

    imageFill = createImage("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png")
    imageFill = pygame.transform.scale(imageFill, WINDOWS_SIZE)
    alphaImage: int = 0
    imageFill.set_alpha(alphaImage)

    TXTFont = pygame.font.Font("ASSETS/FONT/Alice-Regular.ttf", 20)
    txt = TXTFont.render("Кожна людина будує своє життя, як вона хоче того.", scene_config.AA_TEXT, (255, 255, 0))
    txtAlpha: int = 0

    TextSkip = FontKILL.render(":SKIP", scene_config.AA_TEXT, (255, 255, 255))
    TextSkipRect = TextSkip.get_rect(center=(1150, 705))

    SpaceKeyImage = createImage("ASSETS/SPRITE/UI/KEY/Space.png")
    SpaceKeyImage = pygame.transform.scale(SpaceKeyImage, (70, 28))
    SpaceKeyImageRect = SpaceKeyImage.get_rect(center=(1082, 705))

    pygame.mixer.music.load("ASSETS/OST/Ti Bula Yak Kvitka Rosa.wav")
    pygame.mixer.music.play(-1)
    pygame.mixer.music.set_volume(0.1)

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if Frame >= 200:
                if e.type == pygame.KEYDOWN and e.key == pygame.K_SPACE:
                    running = False
                    scene_config.switch_scene(StartGameAnim)

        sc.fill((0, 0, 0))
        sc.blit(palletImage, palletImage.get_rect(center=(xPallet, yPallet)))

        if Frame < 805:
            Frame += 1
            xPallet -= 1.5
            txt.set_alpha(txtAlpha)
            if Frame > 655 and LANGUAGE_SET == "UA":
                txt = TXTFont.render("Хтось працює, хтось кохає, хтось просто живе.", scene_config.AA_TEXT,
                                     (255, 255, 0))
            if LANGUAGE_SET == "EN":
                txt = TXTFont.render("Always people building your self, how who can do", scene_config.AA_TEXT,
                                     (255, 255, 0))
                if Frame > 655:
                    txt = TXTFont.render("Some work, some love, some just live.", scene_config.AA_TEXT,
                                         (255, 255, 0))
            if Frame > 415:
                if txtAlpha < 300:
                    txtAlpha += 5
                sc.blit(txt, txt.get_rect(center=(292, 192)))
        else:
            if Frame == 805:
                xPallet = WINDOWS_SIZE[0] * 2 - 395
                yPallet: int = 1000
                Frame += 1
            else:
                if Frame < 1055:
                    Frame += 1
                    yPallet -= 0.5
                    if LANGUAGE_SET == "EN":
                        txt = TXTFont.render("But sooner or later, we will die. And our life thread will break",
                                             scene_config.AA_TEXT, (255, 255, 0))
                    else:
                        txt = TXTFont.render("Але рано чи пізно, ми помремо. І наша життєва ниточка обірветься",
                                             scene_config.AA_TEXT, (255, 255, 0))
                    sc.blit(txt, txt.get_rect(center=(372, 192)))
                else:
                    if Frame == 1055:
                        palletImage = pygame.transform.smoothscale(palletImage,
                                                                   (WINDOWS_SIZE[0] * 3 - 380,
                                                                    WINDOWS_SIZE[1] * 3 - 380))
                        xPallet = WINDOWS_SIZE[0] // 7 - 150
                        yPallet: int = 920
                        Frame += 1
                    else:
                        if Frame < 1925:
                            Frame += 1
                            xPallet -= 1
                            if LANGUAGE_SET == "EN":
                                txt = TXTFont.render("Someone then goes to Paradise. Or, to the hell of puppets",
                                                     scene_config.AA_TEXT,
                                                     (255, 255, 0))
                                if Frame > 1555:
                                    txt = TXTFont.render("We are also judged for all our bad and sinful things in life",
                                                         scene_config.AA_TEXT,
                                                         (255, 255, 0))
                            else:
                                txt = TXTFont.render("Хтось потім потрапляє у Рай, або, у пекло пупсів",
                                                     scene_config.AA_TEXT,
                                                     (255, 255, 0))
                                if Frame > 1545:
                                    txt = TXTFont.render(
                                        "Нас також засуджують за усі наші погані, та грішні речі у житті",
                                        scene_config.AA_TEXT,
                                        (255, 255, 0))
                            sc.blit(txt, txt.get_rect(center=(342, 650)))
                        else:
                            if Frame == 1925:
                                palletImage = pygame.transform.scale(palletImage,
                                                                     (WINDOWS_SIZE[0] * 4 - 50,
                                                                      WINDOWS_SIZE[1] * 4 - 50))
                                xPallet = WINDOWS_SIZE[0] // 7 - 700
                                yPallet: int = 25
                                Frame += 1
                            else:
                                if Frame < 2575:
                                    yPallet -= 0.5
                                    Frame += 1
                                    if LANGUAGE_SET == "EN":
                                        txt = TXTFont.render(
                                            "But. You can end up in purgatory if it's not clear where you're going.",
                                            scene_config.AA_TEXT,
                                            (255, 255, 0))
                                    else:
                                        txt = TXTFont.render(
                                            "Але. Ви можете потрапити в чистилище, якщо не зрозуміло, куди вас відправити.",
                                            scene_config.AA_TEXT,
                                            (255, 255, 0))
                                    sc.blit(txt, txt.get_rect(center=(425, 85)))
                                else:
                                    if Frame == 2575:
                                        palletImage = pygame.transform.scale(palletImage,
                                                                             (WINDOWS_SIZE[0] * 3 - 370,
                                                                              WINDOWS_SIZE[1] * 3 - 370))
                                        xPallet = WINDOWS_SIZE[0] * 2 - 885
                                        yPallet: int = 90
                                        Frame += 1
                                    else:
                                        if Frame < 3000:
                                            yPallet -= 0.5
                                            Frame += 1
                                            if LANGUAGE_SET == "EN":
                                                txt = TXTFont.render(
                                                    "Purgatory has its Goddess, that holy place. The citadel, which stretches far up the mountain, is considered a holy place here.",
                                                    scene_config.AA_TEXT,
                                                    (255, 255, 0))
                                            else:
                                                txt = TXTFont.render(
                                                    "Чистилище має свою Богиню, та святе місце. Святим місцем тут вважається цитадель, котра простягається далеко у гору.",
                                                    scene_config.AA_TEXT,
                                                    (255, 255, 0))
                                            sc.blit(txt, txt.get_rect(center=(650, 670)))
                                        else:
                                            if Frame == 3000:
                                                palletImage = pygame.transform.scale(palletImage,
                                                                                     (WINDOWS_SIZE[0] * 4 - 50,
                                                                                      WINDOWS_SIZE[1] * 4 - 50))
                                                xPallet = WINDOWS_SIZE[0] // 7 - 850
                                                yPallet: int = 195
                                                Frame += 1
                                            else:
                                                if Frame < 3555:
                                                    yPallet += 0.5
                                                    Frame += 1
                                                    if LANGUAGE_SET == "EN":
                                                        txt = TXTFont.render(
                                                            "Purgatory has people who are like kings and queens.",
                                                            scene_config.AA_TEXT,
                                                            (255, 255, 0))
                                                        if Frame >= 3275:
                                                            txt = TXTFont.render(
                                                                "They keep order in the central city, and have relations with the Goddess.",
                                                                scene_config.AA_TEXT,
                                                                (255, 255, 0))
                                                    else:
                                                        txt = TXTFont.render(
                                                            "Чистилище має людей, котрі подібні королям та королевам.",
                                                            scene_config.AA_TEXT,
                                                            (255, 255, 0))
                                                        if Frame >= 3275:
                                                            txt = TXTFont.render(
                                                                "Вони слідкують за порядком у центральному місті, та мають відносини з Богинею.",
                                                                scene_config.AA_TEXT, (255, 255, 0))
                                                    sc.blit(txt, txt.get_rect(center=(645, 570)))
                                                else:
                                                    if Frame == 3555:
                                                        palletImage = createImage("ASSETS/SPRITE/UI/StartImage/1.png")
                                                        palletImage = pygame.transform.scale(palletImage, (
                                                            WINDOWS_SIZE[0] * 5, WINDOWS_SIZE[1] * 5))
                                                        xPallet: int = WINDOWS_SIZE[0] * 2 - 1550
                                                        yPallet: int = 1995
                                                        Frame += 1
                                                    else:
                                                        if Frame < 3905:
                                                            xPallet -= 0.5
                                                            Frame += 1
                                                            if LANGUAGE_SET == "EN":
                                                                txt = TXTFont.render(
                                                                    "Purgatory is a very developed place. In many ways, even more than us.",
                                                                    scene_config.AA_TEXT,
                                                                    (255, 255, 0))
                                                            else:
                                                                txt = TXTFont.render(
                                                                    "Чистилище дуже розвинуте місце. В багатьох аспектах, навіть більше ніж ми",
                                                                    scene_config.AA_TEXT,
                                                                    (255, 255, 0))
                                                            sc.blit(txt, txt.get_rect(center=(645, 85)))
                                                        else:
                                                            if Frame == 3905:
                                                                palletImage = pygame.transform.scale(palletImage,
                                                                                                     (
                                                                                                         WINDOWS_SIZE[
                                                                                                             0] * 3 + 550,
                                                                                                         WINDOWS_SIZE[
                                                                                                             1] * 3 + 550))
                                                                xPallet = WINDOWS_SIZE[0] // 7 - 1450
                                                                yPallet: int = -100
                                                                Frame += 1
                                                            else:
                                                                if Frame < 4250:
                                                                    xPallet += 0.5
                                                                    Frame += 1
                                                                    if LANGUAGE_SET == "EN":
                                                                        txt = TXTFont.render(
                                                                            "Life in this place, pretty good",
                                                                            scene_config.AA_TEXT,
                                                                            (255, 255, 0))
                                                                    else:
                                                                        txt = TXTFont.render(
                                                                            "Життя тут іде своїм шляхом.",
                                                                            scene_config.AA_TEXT,
                                                                            (255, 255, 0))
                                                                    sc.blit(txt, txt.get_rect(center=(645, 85)))
                                                                else:
                                                                    if Frame == 4250:
                                                                        palletImage = createImage(
                                                                            "ASSETS/SPRITE/UI/StartImage/1.png")
                                                                        palletImage = pygame.transform.scale(
                                                                            palletImage, (
                                                                                WINDOWS_SIZE[0] * 6 + 50,
                                                                                WINDOWS_SIZE[1] * 6 + 50))
                                                                        xPallet: int = WINDOWS_SIZE[0] * 2 - 950
                                                                        yPallet: int = 1897
                                                                        Frame += 1
                                                                    else:
                                                                        if Frame < 4850:
                                                                            yPallet -= 0.5
                                                                            Frame += 1
                                                                            if LANGUAGE_SET == "EN":
                                                                                txt = TXTFont.render(
                                                                                    "Every day. No. hours Really interesting stories and events happen here",
                                                                                    scene_config.AA_TEXT,
                                                                                    (255, 255, 0))
                                                                            else:
                                                                                txt = TXTFont.render(
                                                                                    "Кожен день. Ні. Години. Тут трапляються дійсно цікаві історії, та події",
                                                                                    scene_config.AA_TEXT,
                                                                                    (255, 255, 0))
                                                                            sc.blit(txt, txt.get_rect(center=(645, 85)))
                                                                        else:
                                                                            if Frame < 5250:
                                                                                yPallet -= 0.5
                                                                                alphaImage += 3
                                                                                imageFill.set_alpha(alphaImage)
                                                                                Frame += 1
                                                                                sc.blit(txt,
                                                                                        txt.get_rect(center=(645, 85)))
                                                                            else:
                                                                                running = False
                                                                                scene_config.switch_scene(StartGameAnim)

        sc.blit(imageFill, imageFill.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
        if Frame > 4975:
            if LANGUAGE_SET == "EN":
                txt = TXTFont.render(
                    "And we will tell you one of the stories today.",
                    scene_config.AA_TEXT,
                    (255, 255, 0))
            else:
                txt = TXTFont.render(
                    "І одну з історій, ми вам сьогодні розповімо.",
                    scene_config.AA_TEXT,
                    (255, 255, 0))
            if Frame > 5110:
                txtAlpha -= 5
                txt.set_alpha(txtAlpha)
            sc.blit(txt, txt.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
        if Frame >= 395:
            sc.blit(SpaceKeyImage, SpaceKeyImageRect)
            sc.blit(TextSkip, TextSkipRect)
        pygame.display.update()
        clock.tick(FPS)
