import os
import stat

import pygame
from SCENE import scene_config, scene_menu, scene_select_profile
from config_script import sc, clock, LANGUAGE_SET, WINDOWS_SIZE, createImage
from font_config import FontKILL

SCORE_SM: int = 0
SCORE_MM: int = 0
SCORE_STORY: int = 0


def re_write():
    global SCORE_SM, SCORE_MM, SCORE_STORY
    os.remove("PROFILE_PLAYER/" + scene_select_profile.select_profile)
    file = open("PROFILE_PLAYER/" + scene_select_profile.select_profile, "w")
    if scene_config.GameMode == 1:
        file.write(str(SCORE_SM)+"\n"+str(SCORE_MM)+str(SCORE_STORY))
    elif scene_config.GameMode == 2:
        file.write(str(SCORE_SM) + str(SCORE_MM) + "\n" + str(SCORE_STORY))
    else:
        file.write(str(SCORE_SM) + str(SCORE_MM) + str(SCORE_STORY))
    file.close()


def scene():
    running: bool = True
    FPS: int = 60

    TEXT_SET = ("Рекорд Сюжета: ", "Рекорд: ", "Рекорд Музики: ")

    if LANGUAGE_SET == "EN": TEXT_SET = ("Score Story: ", "Score: ", "Score Music: ")

    ScoreFont = pygame.font.Font("ASSETS/FONT/Alice-Regular.ttf", 29)

    Score_S = ScoreFont.render(TEXT_SET[0] + str(SCORE_STORY), scene_config.AA_TEXT, (255, 255, 0))

    Score_SS = ScoreFont.render(TEXT_SET[1] + str(SCORE_SM), scene_config.AA_TEXT, (255, 255, 255))

    Score_SM = ScoreFont.render(TEXT_SET[2] + str(SCORE_MM), scene_config.AA_TEXT, (255, 255, 255))

    TextQUIT = FontKILL.render(":QUIT", scene_config.AA_TEXT, (255, 255, 255))
    TextQUITRect = TextQUIT.get_rect(center=(1135, 680))

    QKeyImage = createImage("ASSETS/SPRITE/UI/KEY/Q.png")
    QKeyImage = pygame.transform.scale(QKeyImage, (31, 31))
    QKeyImageRect = QKeyImage.get_rect(center=(1086, 680))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.KEYDOWN and e.key == pygame.K_q or e.type == pygame.JOYBUTTONDOWN and e.button == 1:
                running = False
                scene_config.switch_scene(scene_menu.scene_menu)

        sc.fill((0, 0, 0))
        sc.blit(Score_S, Score_S.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 55)))
        sc.blit(Score_SS, Score_SS.get_rect(center=(WINDOWS_SIZE[0] // 2 - 350, WINDOWS_SIZE[1] // 2 - 55)))
        sc.blit(Score_SM, Score_SM.get_rect(center=(WINDOWS_SIZE[0] // 2 + 350, WINDOWS_SIZE[1] // 2 - 55)))
        sc.blit(QKeyImage, QKeyImageRect)
        sc.blit(TextQUIT, TextQUITRect)
        pygame.display.update()
        clock.tick(FPS)
