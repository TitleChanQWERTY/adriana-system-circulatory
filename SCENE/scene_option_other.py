import pygame.transform

import change_op
import take_screenshot
from SCENE import scene_config, scene_select_setings, scene_menu
from config_script import *
from font_config import TextOptionOther, FontKILL
from sfx_compilation import SELECT_MENU, ENTER_SELECT
import ad_system
from group_config import fill_group, notfication_group


def op_other():
    running: bool = True
    FPS: int = 60

    TEXT_SET = ("", "", "", "", "", "", "", "", "")

    match LANGUAGE_SET:
        case "UA":
            TEXT_SET = (TEXT_OPTION_OTHER_UA[0], TEXT_OPTION_OTHER_UA[1], TEXT_OPTION_UA[5], TEXT_OPTION_OTHER_UA[2],
                        TEXT_OPTION_OTHER_UA[3], TEXT_OPTION_OTHER_UA[4], TEXT_OPTION_OTHER_UA[5],
                        TEXT_OPTION_OTHER_UA[6], TEXT_OPTION_OTHER_UA[7], TEXT_OPTION_OTHER_UA[8], TEXT_OPTION_OTHER_UA[9])
        case "EN":
            TEXT_SET = (TEXT_OPTION_OTHER_EN[0], TEXT_OPTION_OTHER_EN[1], TEXT_OPTION_EN[5], TEXT_OPTION_OTHER_EN[2],
                        TEXT_OPTION_OTHER_EN[3], TEXT_OPTION_OTHER_EN[4], TEXT_OPTION_OTHER_EN[5],
                        TEXT_OPTION_OTHER_EN[6], TEXT_OPTION_OTHER_EN[7], TEXT_OPTION_OTHER_EN[8], TEXT_OPTION_OTHER_EN[9])

    InertiaText = TextOptionOther.render(TEXT_SET[0], scene_config.AA_TEXT, (255, 255, 255))
    InertiaTextRect = InertiaText.get_rect(center=(155, 225))

    ParticleText = TextOptionOther.render(TEXT_SET[1], scene_config.AA_TEXT, (255, 255, 255))
    ParticleTextRect = ParticleText.get_rect(center=(123, 285))

    isMoveBackText = TextOptionOther.render(TEXT_SET[3], scene_config.AA_TEXT, (255, 255, 255))
    isMoveBackTextRect = isMoveBackText.get_rect(center=(175, 345))

    CountParticle = TextOptionOther.render(TEXT_SET[4] + str(scene_config.TimeCountLevelEffect), scene_config.AA_TEXT,
                                           (255, 255, 255))
    CountParticleRect = CountParticle.get_rect(center=(236, 415))

    textPointer = TextOptionOther.render(TEXT_SET[5], scene_config.AA_TEXT, (255, 255, 255))
    textPointerRect = textPointer.get_rect(center=(200, 485))

    TextRotateObject = TextOptionOther.render(TEXT_SET[6], scene_config.AA_TEXT, (255, 255, 255))
    TextRotateObjectRect = TextRotateObject.get_rect(center=(485, 225))

    TextAnimAD = TextOptionOther.render(TEXT_SET[7], scene_config.AA_TEXT, (255, 255, 255))
    TextAnimADRect = TextAnimAD.get_rect(center=(485, 285))

    arrowImage = createImage("ASSETS/SPRITE/UI/BUTTON/ARROW/1.png")
    arrowImage = pygame.transform.scale(arrowImage, (32, 32))
    arrowRect = arrowImage.get_rect(center=(40, 225))

    InerTextBoolImage = createImage("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png")
    InerTextBoolImage = pygame.transform.scale(InerTextBoolImage, (59, 59))
    InerTextBoolRect = InerTextBoolImage.get_rect(center=(275, 225))

    ParTextBoolImage = createImage("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png")
    ParTextBoolImage = pygame.transform.scale(ParTextBoolImage, (59, 59))
    ParTextBoolRect = ParTextBoolImage.get_rect(center=(215, 285))

    MoveETextBoolImage = createImage("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png")
    MoveETextBoolImage = pygame.transform.scale(MoveETextBoolImage, (59, 59))
    MoveETextBoolRect = MoveETextBoolImage.get_rect(center=(315, 345))

    PoTextBoolImage = createImage("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png")
    PoTextBoolImage = pygame.transform.scale(PoTextBoolImage, (59, 59))
    PoTextBoolRect = PoTextBoolImage.get_rect(center=(365, 485))

    RotTextBoolImage = createImage("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png")
    RotTextBoolImage = pygame.transform.scale(RotTextBoolImage, (59, 59))
    RotTextBoolRect = RotTextBoolImage.get_rect(center=(620, 225))

    ADTextBoolImage = createImage("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png")
    ADTextBoolImage = pygame.transform.scale(ADTextBoolImage, (59, 59))
    ADTextBoolRect = ADTextBoolImage.get_rect(center=(620, 285))
    XAd = 620

    LightTextBoolImage = createImage("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png")
    LightTextBoolImage = pygame.transform.scale(LightTextBoolImage, (59, 59))

    ADBoolImage = createImage("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png")
    ADBoolImage = pygame.transform.scale(ADBoolImage, (59, 59))
    ADBoolRect = ADBoolImage.get_rect(center=(585, 415))

    TextAD = TextOptionOther.render(TEXT_SET[9], scene_config.AA_TEXT, (255, 255, 255))

    TextLight = TextOptionOther.render(TEXT_SET[10], scene_config.AA_TEXT, (255, 255, 255))

    QKeyImage = createImage("ASSETS/SPRITE/UI/KEY/Q.png")
    QKeyImage = pygame.transform.scale(QKeyImage, (31, 31))
    QKeyImageRect = QKeyImage.get_rect(center=(1082, 680))

    ARROW_KEY_LEFT = createImage("ASSETS/SPRITE/UI/KEY/ARROW.png")
    ARROW_KEY_LEFT = pygame.transform.scale(ARROW_KEY_LEFT, (30, 30))

    ARROW_KEY_RIGHT = createImage("ASSETS/SPRITE/UI/KEY/ARROW.png")
    ARROW_KEY_RIGHT = pygame.transform.scale(ARROW_KEY_RIGHT, (30, 30))
    ARROW_KEY_RIGHT = pygame.transform.flip(ARROW_KEY_RIGHT, True, False)

    TextQUIT = FontKILL.render(":QUIT", scene_config.AA_TEXT, (255, 255, 255))
    TextQUITRect = TextQUIT.get_rect(center=(1130, 680))

    isTrashChange: bool = False

    match LANGUAGE_SET:
        case "EN":
            isMoveBackTextRect = isMoveBackText.get_rect(center=(165, 345))
            textPointerRect = textPointer.get_rect(center=(153, 485))
            CountParticleRect = CountParticle.get_rect(center=(199, 415))
            PoTextBoolRect = PoTextBoolImage.get_rect(center=(275, 485))
            RotTextBoolRect = RotTextBoolImage.get_rect(center=(575, 225))
            TextRotateObjectRect = TextRotateObject.get_rect(center=(460, 225))
            ADTextBoolRect = ADTextBoolImage.get_rect(center=(580, 285))
            XAd = 580
            TextAnimADRect = TextAnimAD.get_rect(center=(464, 285))

    select: int = 0

    lockParticle: bool = False
    isSelectParticle: bool = False

    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/white.png", 15)

    while running:
        sc.fill((0, 0, 0))
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    take_screenshot.take()
                if e.key == pygame.K_DOWN and not lockParticle and not isTrashChange or e.key == pygame.K_s and not lockParticle and not isTrashChange:
                    SELECT_MENU.play()
                    match select:
                        case 0:
                            select = 1
                        case 1:
                            select = 2
                        case 2:
                            select = 3
                        case 3:
                            select = 4
                        case 4:
                            select = 5
                        case 5:
                            select = 6
                        case 6:
                            select = 7
                        case 7:
                            select = 8
                        case 8:
                            select = 9
                        case 9:
                            select = 0
                if e.key == pygame.K_UP and not lockParticle and not isTrashChange or e.key == pygame.K_w and not lockParticle and not isTrashChange:
                    SELECT_MENU.play()
                    match select:
                        case 0:
                            select = 9
                        case 1:
                            select = 0
                        case 2:
                            select = 1
                        case 3:
                            select = 2
                        case 4:
                            select = 3
                        case 5:
                            select = 4
                        case 6:
                            select = 5
                        case 7:
                            select = 6
                        case 8:
                            select = 7
                        case 9:
                            select = 8
                if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                    ENTER_SELECT.play()
                    match select:
                        case 0:
                            match scene_config.p.isInertia:
                                case True:
                                    scene_config.p.isInertia = False
                                case False:
                                    scene_config.p.isInertia = True
                        case 1:
                            match scene_config.isParticle:
                                case True:
                                    scene_config.isParticle = False
                                case False:
                                    scene_config.isParticle = True
                        case 2:
                            match scene_config.isMoveBackEffect:
                                case True:
                                    scene_config.isMoveBackEffect = False
                                case False:
                                    scene_config.isMoveBackEffect = True
                        case 3:
                            match lockParticle:
                                case False:
                                    lockParticle = True
                                case True:
                                    lockParticle = False
                        case 4:
                            match scene_config.isShowPos:
                                case False:
                                    scene_config.isShowPos = True
                                case True:
                                    scene_config.isShowPos = False
                        case 5:
                            match scene_config.isRotateObject:
                                case False:
                                    scene_config.isRotateObject = True
                                case True:
                                    scene_config.isRotateObject = False
                        case 6:
                            match ad_system.isAnimAD:
                                case False:
                                    ad_system.isAnimAD = True
                                case True:
                                    ad_system.isAnimAD = False
                        case 7:
                            match isTrashChange:
                                case False:
                                    isTrashChange = True
                                case True:
                                    isTrashChange = False
                        case 8:
                            match scene_config.isCreateAD:
                                case False:
                                    scene_config.isCreateAD = True
                                case True:
                                    scene_config.isCreateAD = False
                        case 9:
                            match scene_config.isLight:
                                case False:
                                    scene_config.isLight = True
                                case True:
                                    scene_config.isLight = False
                    change_op.change()
                key = pygame.key.get_pressed()
                if e.key == pygame.K_LEFT or e.key == pygame.K_d:
                    if lockParticle:
                        if scene_config.TimeCountLevelEffect > 0:
                            if key[pygame.K_x] and scene_config.TimeCountLevelEffect > 10:
                                scene_config.TimeCountLevelEffect -= 10
                            else:
                                scene_config.TimeCountLevelEffect -= 1
                    elif isTrashChange:
                        if key[pygame.K_x] and scene_config.TRASH_COUNT > 5:
                            scene_config.TRASH_COUNT -= 10
                        else:
                            if scene_config.TRASH_COUNT > 0:
                                scene_config.TRASH_COUNT -= 1
                if e.key == pygame.K_RIGHT or e.key == pygame.K_a:
                    if lockParticle:
                        if key[pygame.K_x] and scene_config.TimeCountLevelEffect > 10:
                            scene_config.TimeCountLevelEffect += 10
                        else:
                            scene_config.TimeCountLevelEffect += 1
                    elif isTrashChange:
                        if key[pygame.K_x] and scene_config.TRASH_COUNT > 10:
                            scene_config.TRASH_COUNT += 10
                        else:
                            scene_config.TRASH_COUNT += 1
                if e.key == pygame.K_q:
                    running = False
                    scene_config.switch_scene(scene_select_setings.scene)

        CountParticle = TextOptionOther.render(TEXT_SET[4] + str(scene_config.TimeCountLevelEffect),
                                               scene_config.AA_TEXT, (255, 255, 255))
        TextTrash = TextOptionOther.render(TEXT_SET[8] + str(scene_config.TRASH_COUNT), scene_config.AA_TEXT,
                                           (255, 255, 255))

        match select:
            case 0:
                arrowRect = arrowImage.get_rect(center=(40, 225))
            case 1:
                arrowRect = arrowImage.get_rect(center=(40, 285))
            case 2:
                arrowRect = arrowImage.get_rect(center=(40, 345))
            case 3:
                arrowRect = arrowImage.get_rect(center=(40, 415))
                if lockParticle:
                    sc.blit(ARROW_KEY_LEFT, ARROW_KEY_LEFT.get_rect(center=(460, 415)))
                    sc.blit(ARROW_KEY_RIGHT, ARROW_KEY_RIGHT.get_rect(center=(425, 415)))
            case 4:
                arrowRect = arrowImage.get_rect(center=(40, 485))
            case 5:
                arrowRect = arrowImage.get_rect(center=(365, 225))
            case 6:
                arrowRect = arrowImage.get_rect(center=(365, 285))
            case 7:
                arrowRect = arrowImage.get_rect(center=(370, 350))
                if isTrashChange:
                    sc.blit(ARROW_KEY_LEFT, ARROW_KEY_LEFT.get_rect(center=(665, 350)))
                    sc.blit(ARROW_KEY_RIGHT, ARROW_KEY_RIGHT.get_rect(center=(630, 350)))
            case 8:
                arrowRect = arrowImage.get_rect(center=(424, 415))
            case 9:
                arrowRect = arrowImage.get_rect(center=(421, 484))

        match scene_config.p.isInertia:
            case True:
                InerTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/9.png").convert_alpha()
                InerTextBoolImage = pygame.transform.scale(InerTextBoolImage, (59, 59))
                InerTextBoolRect = InerTextBoolImage.get_rect(center=(275, 225))
                InerTextBoolImage.set_alpha(300)
            case False:
                InerTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png").convert_alpha()
                InerTextBoolImage = pygame.transform.scale(InerTextBoolImage, (56, 56))
                InerTextBoolRect = InerTextBoolImage.get_rect(center=(275, 225))
                InerTextBoolImage.set_alpha(100)

        match ad_system.isAnimAD:
            case True:
                ADTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/9.png").convert_alpha()
                ADTextBoolImage = pygame.transform.scale(ADTextBoolImage, (59, 59))
                ADTextBoolRect = ADTextBoolImage.get_rect(center=(XAd, 285))
                ADTextBoolImage.set_alpha(300)
            case False:
                ADTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png").convert_alpha()
                ADTextBoolImage = pygame.transform.scale(ADTextBoolImage, (59, 59))
                ADTextBoolRect = ADTextBoolImage.get_rect(center=(XAd, 285))
                ADTextBoolImage.set_alpha(100)

        match scene_config.isParticle:
            case True:
                ParTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/9.png").convert_alpha()
                ParTextBoolImage = pygame.transform.scale(ParTextBoolImage, (59, 59))
                ParTextBoolRect = ParTextBoolImage.get_rect(center=(215, 285))
                ParTextBoolImage.set_alpha(300)
            case False:
                ParTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png").convert_alpha()
                ParTextBoolImage = pygame.transform.scale(ParTextBoolImage, (56, 56))
                ParTextBoolRect = ParTextBoolImage.get_rect(center=(215, 285))
                ParTextBoolImage.set_alpha(100)

        match scene_config.isMoveBackEffect:
            case True:
                isSelectParticle = False
                MoveETextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/9.png").convert_alpha()
                MoveETextBoolImage = pygame.transform.scale(MoveETextBoolImage, (59, 59))
                MoveETextBoolImage.set_alpha(300)
            case False:
                isSelectParticle = True
                MoveETextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png").convert_alpha()
                MoveETextBoolImage = pygame.transform.scale(MoveETextBoolImage, (59, 59))
                MoveETextBoolImage.set_alpha(100)
        match scene_config.isShowPos:
            case True:
                PoTextBoolImage = createImage("ASSETS/SPRITE/UI/BUTTON/ButtonBool/9.png")
                PoTextBoolImage = pygame.transform.scale(PoTextBoolImage, (59, 59))
                PoTextBoolImage.set_alpha(300)
            case False:
                PoTextBoolImage = createImage("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png")
                PoTextBoolImage = pygame.transform.scale(PoTextBoolImage, (59, 59))
                PoTextBoolImage.set_alpha(100)
        match scene_config.isRotateObject:
            case True:
                RotTextBoolImage = createImage("ASSETS/SPRITE/UI/BUTTON/ButtonBool/9.png")
                RotTextBoolImage = pygame.transform.scale(RotTextBoolImage, (59, 59))
                RotTextBoolImage.set_alpha(300)
            case False:
                RotTextBoolImage = createImage("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png")
                RotTextBoolImage = pygame.transform.scale(RotTextBoolImage, (59, 59))
                RotTextBoolImage.set_alpha(100)
        match scene_config.isCreateAD:
            case True:
                ADBoolImage = createImage("ASSETS/SPRITE/UI/BUTTON/ButtonBool/9.png")
                ADBoolImage = pygame.transform.scale(ADBoolImage, (59, 59))
                ADBoolImage.set_alpha(300)
            case False:
                ADBoolImage = createImage("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png")
                ADBoolImage = pygame.transform.scale(ADBoolImage, (59, 59))
                ADBoolImage.set_alpha(100)
        match scene_config.isLight:
            case True:
                LightTextBoolImage = createImage("ASSETS/SPRITE/UI/BUTTON/ButtonBool/9.png")
                LightTextBoolImage = pygame.transform.scale(LightTextBoolImage, (59, 59))
                LightTextBoolImage.set_alpha(300)
            case False:
                LightTextBoolImage = createImage("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png")
                LightTextBoolImage = pygame.transform.scale(LightTextBoolImage, (59, 59))
                LightTextBoolImage.set_alpha(100)

        sc.blit(InertiaText, InertiaTextRect)
        sc.blit(ParticleText, ParticleTextRect)
        sc.blit(InerTextBoolImage, InerTextBoolRect)
        sc.blit(ParTextBoolImage, ParTextBoolRect)
        sc.blit(textPointer, textPointerRect)
        sc.blit(TextRotateObject, TextRotateObjectRect)
        sc.blit(isMoveBackText, isMoveBackTextRect)
        sc.blit(MoveETextBoolImage, MoveETextBoolRect)
        sc.blit(CountParticle, CountParticleRect)
        sc.blit(PoTextBoolImage, PoTextBoolRect)
        sc.blit(RotTextBoolImage, RotTextBoolRect)
        sc.blit(QKeyImage, QKeyImageRect)
        sc.blit(TextQUIT, TextQUITRect)
        sc.blit(TextAnimAD, TextAnimADRect)
        sc.blit(ADTextBoolImage, ADTextBoolRect)
        sc.blit(arrowImage, arrowRect)
        sc.blit(scene_menu.EKeyImage, scene_menu.EKeyImageRect)
        sc.blit(scene_menu.TextOK, scene_menu.TextOKRect)
        sc.blit(TextTrash, TextTrash.get_rect(center=(496, 350)))
        sc.blit(TextAD, TextAD.get_rect(center=(495, 415)))
        sc.blit(TextLight, TextLight.get_rect(center=(515, 484)))
        sc.blit(LightTextBoolImage, LightTextBoolImage.get_rect(center=(625, 484)))
        sc.blit(ADBoolImage, ADBoolRect)
        notfication_group.draw(sc)
        for n in notfication_group:
            sc.blit(n.text, n.rectTxt)
        fill_group.draw(sc)
        pygame.display.update()
        clock.tick(FPS)
        fill_group.update()
        notfication_group.update()
