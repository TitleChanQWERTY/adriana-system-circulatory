import os
import stat

import pygame
from SCENE import scene_config, start_scene, scene_score
from config_script import sc, clock, LANGUAGE_SET, WINDOWS_SIZE, createImage
from font_config import FontKILL
from sfx_compilation import SELECT_MENU
from take_screenshot import take

select_profile = ""

fontMusic = pygame.font.Font("ASSETS/FONT/Alice-Regular.ttf", 15)


class text_p(pygame.sprite.Sprite):
    def __init__(self, txt, posY, posX, group):
        super().__init__()
        self.textMusic = fontMusic.render(str(txt), scene_config.AA_TEXT, (255, 255, 255))
        self.textMusicRect = self.textMusic.get_rect(center=(posX + 110, posY))
        self.add(group)

    def update(self, event):
        if event.key == pygame.K_DOWN:
            self.textMusicRect.y -= 50
        if event.key == pygame.K_UP:
            self.textMusicRect.y += 50

    def updateJoy(self, event):
        if event.hat == 0 and event.value == (0, -1):
            self.textMusicRect.y -= 50
        if event.hat == 0 and event.value == (0, 1):
            self.textMusicRect.y += 50


def scene():
    global select_profile
    running: bool = True
    FPS: int = 60

    dir_folder = "PROFILE_PLAYER"

    posTextY: int = 0

    txtMusicGroup = pygame.sprite.Group()

    select: int = 0
    selectMax = 0

    res = None
    len_res = None

    try:
        res = os.listdir(dir_folder)
        len_res = len(res)
        for lent in range(len_res):
            selectMax = lent
            posTextY += 50
            text_p(res[lent], posTextY, 120, txtMusicGroup)
    except FileNotFoundError:
        os.mkdir(r"PROFILE_PLAYER")

    text = ""

    try:
        if len_res > 0:
            active: bool = False
        else:
            active: bool = True
    except TypeError:
        active: bool = True
        var = None

    txt = FontKILL.render(str(text), scene_config.AA_TEXT, (255, 255, 255))

    lang_write = ["НАПИШИ НАЗВУ ПРОФІЛЛЯ:", "НАТИСНИ ENTER ЩОБ ПІДТВЕРДИТИ"]

    if LANGUAGE_SET == "EN":
        lang_write = ["WRITE PROFILE NAME:", "PRESS ENTER TO CREATE"]

    txtWrite = FontKILL.render(lang_write[0], scene_config.AA_TEXT, (255, 255, 255))

    txtNew = FontKILL.render(lang_write[1], scene_config.AA_TEXT, (255, 255, 255))

    selector = createImage("ASSETS/SPRITE/UI/SkillTree/2.png")
    selector = pygame.transform.scale(selector, (347, 56))
    selectorY = 50

    NKeyImage = createImage("ASSETS/SPRITE/UI/KEY/N.png")
    NKeyImage = pygame.transform.scale(NKeyImage, (31, 31))
    NKeyImageRect = NKeyImage.get_rect(center=(1000, 680))

    TextNEW = FontKILL.render(":NEW", scene_config.AA_TEXT, (255, 255, 255))
    TextNEWRect = TextNEW.get_rect(center=(1046, 680))

    QKeyImage = createImage("ASSETS/SPRITE/UI/KEY/E/2.png")
    QKeyImage = pygame.transform.scale(QKeyImage, (31, 31))
    QKeyImageRect = QKeyImage.get_rect(center=(1115, 680))

    TextQUIT = FontKILL.render(":OK", scene_config.AA_TEXT, (255, 255, 255))
    TextQUITRect = TextQUIT.get_rect(center=(1155, 680))

    txtBACKSPACE = "Натисніть  \"<-BACKSPACE\"  Щоб Видалити Профіль."

    if LANGUAGE_SET == "EN":
        txtBACKSPACE = "Press  \"<-BACKSPACE\"  To Remove Profile"

    BACKSPACETXT = FontKILL.render(txtBACKSPACE, scene_config.AA_TEXT, (255, 255, 255))

    txtError = FontKILL.render("ERROR", scene_config.AA_TEXT, (255, 15, 15))

    while running:
        sc.fill((0, 0, 0))
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.KEYDOWN:
                if active:
                    if e.key == pygame.K_ESCAPE:
                        active = False
                        text = ""
                    if e.key == pygame.K_RETURN:
                        try:
                            createProfile(text)
                            active = False
                            running = False
                            scene_config.switch_scene(scene)
                        except PermissionError:
                            text = ""
                            sc.blit(txtError,
                                    txtError.get_rect(center=(WINDOWS_SIZE[0] // 2 + 350, WINDOWS_SIZE[1] // 2 - 55)))
                    elif e.key == pygame.K_BACKSPACE:
                        text = text[:-1]
                    else:
                        text += e.unicode
                    txt = FontKILL.render(str(text), scene_config.AA_TEXT, (255, 255, 255))
                else:
                    try:
                        if len_res > 0:
                            if e.key == pygame.K_RETURN or e.key == pygame.K_e:
                                for txtp in txtMusicGroup:
                                    txtp.kill()
                                select_profile = str(res[select])
                                file = open("PROFILE_PLAYER/" + select_profile, "r")
                                scene_score.SCORE_SM = file.readline()
                                scene_score.SCORE_MM = file.readline()
                                scene_score.SCORE_STORY = file.readline()
                                file.close()
                                running = False
                                scene_config.switch_scene(start_scene.warningEye)
                            if e.key == pygame.K_BACKSPACE:
                                os.remove("PROFILE_PLAYER/" + str(res[select]))
                                running = False
                                scene_config.switch_scene(scene)
                    except TypeError:
                        var = None
                    if e.key == pygame.K_n:
                        active = True
                    if e.key == pygame.K_F6:
                        take()
                    if e.key == pygame.K_DOWN:
                        SELECT_MENU.play()
                        if select < selectMax - 12:
                            select += 1
                            txtMusicGroup.update(e)
                        else:
                            if select < selectMax:
                                select += 1
                                selectorY += 50
                    if e.key == pygame.K_UP:
                        SELECT_MENU.play()
                        if 0 < select < selectMax - 11:
                            select -= 1
                            txtMusicGroup.update(e)
                        else:
                            if select > 0:
                                selectorY -= 50
                                select -= 1

        sc.blit(txt, txt.get_rect(center=(WINDOWS_SIZE[0] // 2 + 350, WINDOWS_SIZE[1] // 2)))
        if active:
            sc.blit(txtWrite, txtWrite.get_rect(center=(WINDOWS_SIZE[0] // 2 + 350, WINDOWS_SIZE[1] // 2 - 35)))
            sc.blit(txtNew, txtNew.get_rect(center=(WINDOWS_SIZE[0] // 2 + 350, WINDOWS_SIZE[1] // 2 + 50)))
        else:
            sc.blit(NKeyImage, NKeyImageRect)
            sc.blit(TextNEW, TextNEWRect)
        try:
            if len_res > 0:
                sc.blit(selector, selector.get_rect(center=(230, selectorY)))
                sc.blit(QKeyImage, QKeyImageRect)
                sc.blit(TextQUIT, TextQUITRect)
        except TypeError:
            var = None
        for txt_p in txtMusicGroup:
            sc.blit(txt_p.textMusic, txt_p.textMusicRect)
        sc.blit(BACKSPACETXT, BACKSPACETXT.get_rect(center=(WINDOWS_SIZE[0] // 2 + 300, 25)))
        pygame.display.update()
        clock.tick(FPS)


def createProfile(profile_name):
    try:
        profile = open("PROFILE_PLAYER/" + str(profile_name), "w")
        profile.write("0 \n0 \n0")
        profile.close()
    except IsADirectoryError:
        print("ERROR")
