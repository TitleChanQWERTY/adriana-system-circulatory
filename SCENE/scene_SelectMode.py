import pygame

import take_screenshot
from LOADING import loading
from SCENE import scene_config, scene_menu, scene_difficulty, scene_game, scene_select_music
from config_script import WINDOWS_SIZE, sc, clock, createImage, TEXT_MODE_EN, TEXT_MODE_UA, LANGUAGE_SET
from sfx_compilation import SELECT_MENU, ENTER_SELECT
from font_config import FontStat, FontKILL, FinishFont
from group_config import fill_group, notfication_group

select: int = 3


def scene_select():
    global select
    running: bool = True
    FPS: int = 60

    TEXT_SET = ("", "", "", "")

    match LANGUAGE_SET:
        case "UA":
            TEXT_SET = (TEXT_MODE_UA[0], TEXT_MODE_UA[1], TEXT_MODE_UA[2], TEXT_MODE_UA[3], TEXT_MODE_UA[4])
        case "EN":
            TEXT_SET = (TEXT_MODE_EN[0], TEXT_MODE_EN[1], TEXT_MODE_EN[2], TEXT_MODE_EN[3], TEXT_MODE_EN[4])

    StoryMode = createImage("ASSETS/SPRITE/UI/MODE/1.png")
    StoryMode = pygame.transform.scale(StoryMode, (100, 100))
    StoryModeRect = StoryMode.get_rect(center=(WINDOWS_SIZE[0] // 2 - 450, WINDOWS_SIZE[1] // 2 - 50))

    ScoreMode = createImage("ASSETS/SPRITE/UI/MODE/2.png")
    ScoreMode = pygame.transform.scale(ScoreMode, (100, 100))
    ScoreModeRect = ScoreMode.get_rect(center=(WINDOWS_SIZE[0] // 2 - 150, WINDOWS_SIZE[1] // 2 - 50))

    MusicMode = createImage("ASSETS/SPRITE/UI/MODE/3.png")
    MusicMode = pygame.transform.scale(MusicMode, (100, 100))
    MusicModeRect = MusicMode.get_rect(center=(WINDOWS_SIZE[0] // 2 + 150, WINDOWS_SIZE[1] // 2 - 50))

    SandMode = createImage("ASSETS/SPRITE/UI/MODE/4.png")
    SandMode = pygame.transform.scale(SandMode, (100, 100))
    SandModeRect = SandMode.get_rect(center=(WINDOWS_SIZE[0] // 2 + 450, WINDOWS_SIZE[1] // 2 - 50))

    SelectImage = createImage("ASSETS/SPRITE/UI/SkillTree/2.png")
    SelectImage = pygame.transform.scale(SelectImage, (158, 158))
    SelectImageRect = SelectImage.get_rect(center=(StoryModeRect.x + 50, StoryModeRect.y + 50))

    TextNameMode = FontStat.render(TEXT_SET[0], scene_config.AA_TEXT, (255, 255, 255))

    QKeyImage = createImage("ASSETS/SPRITE/UI/KEY/Q.png")
    QKeyImage = pygame.transform.scale(QKeyImage, (31, 31))
    QKeyImageRect = QKeyImage.get_rect(center=(1082, 680))

    TextQUIT = FontKILL.render(":QUIT", scene_config.AA_TEXT, (255, 255, 255))
    TextQUITRect = TextQUIT.get_rect(center=(1130, 680))

    TextSelectMode = FinishFont.render(TEXT_SET[4], scene_config.AA_TEXT, (255, 255, 255))

    AlphaIcon = [100, 100, 100, 100]

    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/white.png", 17)

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    take_screenshot.take()
                if e.key == pygame.K_RIGHT:
                    SELECT_MENU.play()
                    match select:
                        case 0:
                            select = 1
                        case 1:
                            select = 2
                        case 2:
                            select = 3
                        case 3:
                            select = 0
                if e.key == pygame.K_LEFT:
                    SELECT_MENU.play()
                    match select:
                        case 0:
                            select = 3
                        case 1:
                            select = 0
                        case 2:
                            select = 1
                        case 3:
                            select = 2
                if e.key == pygame.K_q:
                    running = False
                    scene_config.switch_scene(scene_menu.scene_menu)
                if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                    ENTER_SELECT.play()
                    match select:
                        case 0:
                            scene_game.LEVEL = 0
                            scene_config.GameMode = 0
                            running = False
                            scene_config.switch_scene(scene_difficulty.difficulty)
                        case 1:
                            scene_config.GameMode = 1
                            scene_game.LEVEL = 300
                            running = False
                            scene_config.switch_scene(scene_difficulty.difficulty)
                        case 2:
                            scene_config.GameMode = 2
                            scene_game.LEVEL = 300
                            running = False
                            scene_config.switch_scene(scene_select_music.scene)
                        case 3:
                            scene_config.DIFFICULTY = 0
                            scene_config.GameMode = 3
                            running = False
                            scene_config.switch_scene(loading.loading)

        match select:
            case 0:
                AlphaIcon[0] = 300
                AlphaIcon[1] = 95
                AlphaIcon[2] = 95
                AlphaIcon[3] = 95
                TextNameMode = FontStat.render(TEXT_SET[0], scene_config.AA_TEXT, (255, 255, 255))
                SelectImageRect = SelectImage.get_rect(center=(StoryModeRect.x + 50, StoryModeRect.y + 50))
            case 1:
                AlphaIcon[0] = 95
                AlphaIcon[1] = 300
                AlphaIcon[2] = 95
                AlphaIcon[3] = 95
                TextNameMode = FontStat.render(TEXT_SET[1], scene_config.AA_TEXT, (255, 255, 255))
                SelectImageRect = SelectImage.get_rect(center=(ScoreModeRect.x + 50, ScoreModeRect.y + 50))
            case 2:
                AlphaIcon[0] = 95
                AlphaIcon[1] = 95
                AlphaIcon[2] = 300
                AlphaIcon[3] = 95
                TextNameMode = FontStat.render(TEXT_SET[2], scene_config.AA_TEXT, (255, 255, 255))
                SelectImageRect = SelectImage.get_rect(center=(MusicModeRect.x + 50, MusicModeRect.y + 50))
            case 3:
                AlphaIcon[0] = 95
                AlphaIcon[1] = 95
                AlphaIcon[2] = 95
                AlphaIcon[3] = 300
                TextNameMode = FontStat.render(TEXT_SET[3], scene_config.AA_TEXT, (255, 255, 255))
                SelectImageRect = SelectImage.get_rect(center=(SandModeRect.x + 50, SandModeRect.y + 50))

        StoryMode.set_alpha(AlphaIcon[0])
        ScoreMode.set_alpha(AlphaIcon[1])
        MusicMode.set_alpha(AlphaIcon[2])
        SandMode.set_alpha(AlphaIcon[3])

        sc.fill((0, 0, 0))
        sc.blit(StoryMode, StoryModeRect)
        sc.blit(ScoreMode, ScoreModeRect)
        sc.blit(MusicMode, MusicModeRect)
        sc.blit(SandMode, SandModeRect)
        sc.blit(SelectImage, SelectImageRect)
        sc.blit(TextNameMode, TextNameMode.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 200)))
        sc.blit(scene_menu.EKeyImage, scene_menu.EKeyImageRect)
        sc.blit(QKeyImage, QKeyImageRect)
        sc.blit(TextQUIT, TextQUITRect)
        sc.blit(scene_menu.TextOK, scene_menu.TextOKRect)
        sc.blit(TextSelectMode, TextSelectMode.get_rect(center=(WINDOWS_SIZE[0] // 2, 125)))
        notfication_group.draw(sc)
        for n in notfication_group:
            sc.blit(n.text, n.rectTxt)
        fill_group.draw(sc)
        pygame.display.update()
        clock.tick(FPS)
        fill_group.update()
        notfication_group.update()
