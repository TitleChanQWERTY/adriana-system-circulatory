from random import randint

import pygame.draw

import take_screenshot
from config_script import *
from SCENE import scene_config, scene_select_setings, scene_SelectMode, scene_score, scene_select_profile
from font_config import FontKILL, TextOptionOther, fontMenuText, FontStat, FinishFont
from sfx_compilation import SELECT_MENU, ENTER_SELECT
from group_config import fill_group, notfication_group
from reset_data import reset

EKeyImage = createImage("ASSETS/SPRITE/UI/KEY/E/2.png")
EKeyImage = pygame.transform.scale(EKeyImage, (31, 31))
EKeyImageRect = EKeyImage.get_rect(center=(1190, 680))

TextOK = FontKILL.render(":OK", scene_config.AA_TEXT, (255, 255, 255))
TextOKRect = TextOK.get_rect(center=(1230, 680))


def scene_menu():
    global EKeyImage, EKeyImageRect
    FPS: int = 60

    TEXT_SET = ("", "", "", "")

    match LANGUAGE_SET:
        case "EN":
            TEXT_SET = (TEXT_MENU_EN[0], TEXT_MENU_EN[1], TEXT_MENU_EN[2], TEXT_MENU_EN[3], TEXT_MENU_EN[4])
        case "UA":
            TEXT_SET = (TEXT_MENU_UA[0], TEXT_MENU_UA[1], TEXT_MENU_UA[2], TEXT_MENU_UA[3], TEXT_MENU_UA[4])

    menuTextPlay = fontMenuText.render(TEXT_SET[0], scene_config.AA_TEXT, (255, 255, 255))

    menuTextRecord = fontMenuText.render(TEXT_SET[1], scene_config.AA_TEXT, (255, 255, 255))

    menuTextOption = fontMenuText.render(TEXT_SET[2], scene_config.AA_TEXT, (255, 255, 255))

    menuTextExit = fontMenuText.render(TEXT_SET[3], scene_config.AA_TEXT, (255, 255, 255))

    menuTextAbout = fontMenuText.render(TEXT_SET[4], scene_config.AA_TEXT, (255, 255, 255))

    arrowImage = createImage("ASSETS/SPRITE/UI/BUTTON/ARROW/1.png")
    arrowImage = pygame.transform.scale(arrowImage, (32, 32))
    arrowRect = arrowImage.get_rect(center=(27, 245))

    TEXT_SET_POWERED: str = "Зробив: TitleChanQWERTY *2023*"

    match LANGUAGE_SET:
        case "EN":
            TEXT_SET_POWERED = "Powered By: TitleChanQWERTY *2023*"

    PoweredByText = TextOptionOther.render(TEXT_SET_POWERED, scene_config.AA_TEXT, (76, 255, 153))
    PoweredByRect = PoweredByText.get_rect(center=(WINDOWS_SIZE[0] // 2, 700))

    select: int = 0

    yRectSelect: int = 234
    ColorSelect = (0, 0, 200)

    EKeyImage = createImage("ASSETS/SPRITE/UI/KEY/E/2.png")
    EKeyImage = pygame.transform.scale(EKeyImage, (31, 31))
    EKeyImageRect = EKeyImage.get_rect(center=(1190, 680))

    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/white.png")

    FrameLogo: int = 0
    LogoGame = createImage("ASSETS/SPRITE/UI/MainMenu/Logo.png")
    LogoGame = pygame.transform.scale(LogoGame, (189, 135))
    size = [299, 255]

    character = ["ASSETS/SPRITE/UI/MainMenu/Character/Zero.png", "ASSETS/SPRITE/UI/MainMenu/Character/Danna.png",
                 "ASSETS/SPRITE/UI/MainMenu/Character/Ioann.png", "ASSETS/SPRITE/UI/MainMenu/Character/QWERTY.png"]

    CharacterShow = createImage(character[randint(0, len(character) - 1)])
    CharacterShow = pygame.transform.scale(CharacterShow, (278, 382))
    CharacterShow = pygame.transform.flip(CharacterShow, True, False)

    profileLang = "Профіль: "

    if LANGUAGE_SET == "EN":
        profileLang = "Profile: "

    profileTXT = FontKILL.render(profileLang+scene_select_profile.select_profile, scene_config.AA_TEXT, (255, 255, 255))

    reset()

    while 1:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                scene_config.switch_scene(None)
                return 0
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_DOWN:
                    SELECT_MENU.play()
                    match select:
                        case 0:
                            select = 1
                        case 1:
                            select = 2
                        case 2:
                            select = 3
                        case 3:
                            select = 4
                        case 4:
                            select = 0
                if e.key == pygame.K_UP:
                    SELECT_MENU.play()
                    match select:
                        case 0:
                            select = 4
                        case 1:
                            select = 0
                        case 2:
                            select = 1
                        case 3:
                            select = 2
                        case 4:
                            select = 3
                if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                    ENTER_SELECT.play()
                    match select:
                        case 0:
                            scene_config.switch_scene(scene_SelectMode.scene_select)
                        case 1:
                            scene_config.switch_scene(scene_score.scene)
                        case 2:
                            scene_config.switch_scene(scene_select_setings.scene)
                        case 3:
                            scene_config.switch_scene(about)
                        case 4:
                            scene_config.switch_scene(None)
                    return 0
                if e.key == pygame.K_F6:
                    take_screenshot.take()

        match select:
            case 0:
                ColorSelect = (0, 145, 0)
                yRectSelect = 234
                menuTextPlay.set_alpha(300)
                menuTextAbout.set_alpha(40)
                menuTextExit.set_alpha(40)
                menuTextOption.set_alpha(40)
                menuTextRecord.set_alpha(40)
                arrowRect = arrowImage.get_rect(center=(99, 245))
            case 1:
                ColorSelect = (0, 0, 200)
                yRectSelect = 274
                menuTextPlay.set_alpha(40)
                menuTextAbout.set_alpha(40)
                menuTextExit.set_alpha(40)
                menuTextOption.set_alpha(40)
                menuTextRecord.set_alpha(300)
                arrowRect = arrowImage.get_rect(center=(79, 285))
                if LANGUAGE_SET == "EN":
                    arrowRect = arrowImage.get_rect(center=(90, 285))
            case 2:
                yRectSelect = 320
                menuTextPlay.set_alpha(40)
                menuTextAbout.set_alpha(40)
                menuTextExit.set_alpha(40)
                menuTextOption.set_alpha(300)
                menuTextRecord.set_alpha(40)
                arrowRect = arrowImage.get_rect(center=(35, 330))
                if LANGUAGE_SET == "EN":
                    arrowRect = arrowImage.get_rect(center=(80, 330))
            case 3:
                ColorSelect = (0, 0, 200)
                yRectSelect = 363
                menuTextPlay.set_alpha(40)
                menuTextAbout.set_alpha(300)
                menuTextExit.set_alpha(40)
                menuTextOption.set_alpha(40)
                menuTextRecord.set_alpha(40)
                arrowRect = arrowImage.get_rect(center=(105, 374))
                if LANGUAGE_SET == "EN":
                    arrowRect = arrowImage.get_rect(center=(90, 374))
            case 4:
                ColorSelect = (200, 0, 0)
                yRectSelect = 404
                menuTextPlay.set_alpha(40)
                menuTextAbout.set_alpha(40)
                menuTextExit.set_alpha(300)
                menuTextOption.set_alpha(40)
                menuTextRecord.set_alpha(40)
                arrowRect = arrowImage.get_rect(center=(95, 415))
                if LANGUAGE_SET == "EN":
                    arrowRect = arrowImage.get_rect(center=(100, 415))
        menuTextPlayRect = menuTextPlay.get_rect(center=(161, 245))
        menuTextRecordRect = menuTextRecord.get_rect(center=(161, 285))
        menuTextOptionRect = menuTextOption.get_rect(center=(161, 330))
        menuTextExitRect = menuTextExit.get_rect(center=(161, 415))
        menuTextAboutRect = menuTextAbout.get_rect(center=(161, 374))

        sc.fill((5, 5, 5))
        pygame.draw.rect(sc, ColorSelect, (0, yRectSelect + 8, WINDOWS_SIZE[0], 8))
        sc.blit(menuTextPlay, menuTextPlayRect)
        sc.blit(menuTextRecord, menuTextRecordRect)
        sc.blit(menuTextOption, menuTextOptionRect)
        sc.blit(menuTextExit, menuTextExitRect)
        sc.blit(menuTextAbout, menuTextAboutRect)
        sc.blit(arrowImage, arrowRect)
        sc.blit(EKeyImage, EKeyImageRect)
        sc.blit(TextOK, TextOKRect)
        sc.blit(PoweredByText, PoweredByRect)
        sc.blit(profileTXT, profileTXT.get_rect(center=(WINDOWS_SIZE[0]//2+300, 25)))
        if FrameLogo < 6:
            LogoGame = createImage("ASSETS/SPRITE/UI/MainMenu/Logo.png")
            LogoGame = pygame.transform.scale(LogoGame, size)
            size[0] -= 25
            size[1] -= 26
            FrameLogo += 1
        sc.blit(LogoGame, LogoGame.get_rect(center=(155, 110)))
        sc.blit(CharacterShow, CharacterShow.get_rect(center=(WINDOWS_SIZE[0] // 2 + 350, WINDOWS_SIZE[1] // 2 + 196)))
        fill_group.draw(sc)
        notfication_group.draw(sc)
        for n in notfication_group:
            sc.blit(n.text, n.rectTxt)
        pygame.display.update()
        clock.tick(FPS)
        fill_group.update()
        notfication_group.update()


def about():
    running: bool = True
    FPS: int = 60

    TEXT_SET = ("", "", "")

    match LANGUAGE_SET:
        case "UA":
            TEXT_SET = ("Про Гру", "Керування", "Подяки")
        case "EN":
            TEXT_SET = ("About", "Control", "Special Thanks")

    textAboutGame = fontMenuText.render(TEXT_SET[0], scene_config.AA_TEXT, (255, 255, 255))
    textControl = fontMenuText.render(TEXT_SET[1], scene_config.AA_TEXT, (255, 255, 255))
    textThanks = fontMenuText.render(TEXT_SET[2], scene_config.AA_TEXT, (255, 255, 255))

    arrowImage = createImage("ASSETS/SPRITE/UI/BUTTON/ARROW/1.png")
    arrowImage = pygame.transform.scale(arrowImage, (32, 32))
    arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 50, WINDOWS_SIZE[1] // 2 - 60))

    select: int = 0

    yRectSelect: int = WINDOWS_SIZE[1] // 2 - 60

    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/white.png", 12)

    QKeyImage = createImage("ASSETS/SPRITE/UI/KEY/Q.png")
    QKeyImage = pygame.transform.scale(QKeyImage, (31, 31))
    QKeyImageRect = QKeyImage.get_rect(center=(1100, 680))

    TextQUIT = FontKILL.render(":QUIT", scene_config.AA_TEXT, (255, 255, 255))
    TextQUITRect = TextQUIT.get_rect(center=(1150, 680))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                scene_config.switch_scene(None)
                running = False
            if e.type == pygame.KEYDOWN:
                match e.key:
                    case pygame.K_DOWN:
                        SELECT_MENU.play()
                        match select:
                            case 0:
                                select = 1
                            case 1:
                                select = 2
                            case 2:
                                select = 0
                    case pygame.K_UP:
                        SELECT_MENU.play()
                        match select:
                            case 0:
                                select = 2
                            case 1:
                                select = 0
                            case 2:
                                select = 1
                    case pygame.K_e:
                        ENTER_SELECT.play()
                        match select:
                            case 0:
                                scene_config.switch_scene(aboutGame)
                                running = False
                            case 1:
                                scene_config.switch_scene(control)
                                running = False
                            case 2:
                                scene_config.switch_scene(thanks)
                                running = False
                    case pygame.K_RETURN:
                        ENTER_SELECT.play()
                        match select:
                            case 0:
                                scene_config.switch_scene(aboutGame)
                                running = False
                            case 1:
                                scene_config.switch_scene(control)
                                running = False
                            case 2:
                                scene_config.switch_scene(thanks)
                                running = False
                    case pygame.K_q:
                        ENTER_SELECT.play()
                        scene_config.switch_scene(scene_menu)
                        running = False

        match select:
            case 0:
                yRectSelect: int = WINDOWS_SIZE[1] // 2 - 70
                arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 85, WINDOWS_SIZE[1] // 2 - 60))
            case 1:
                yRectSelect: int = WINDOWS_SIZE[1] // 2 - 10
                arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 100, WINDOWS_SIZE[1] // 2))
            case 2:
                yRectSelect: int = WINDOWS_SIZE[1] // 2 + 50
                arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 95, WINDOWS_SIZE[1] // 2 + 60))

        sc.fill((0, 0, 0))
        pygame.draw.rect(sc, (0, 0, 200), (0, yRectSelect, WINDOWS_SIZE[0], 22))
        sc.blit(textAboutGame, textAboutGame.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 60)))
        sc.blit(textControl, textControl.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
        sc.blit(textThanks, textThanks.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 60)))
        sc.blit(arrowImage, arrowRect)
        sc.blit(QKeyImage, QKeyImageRect)
        sc.blit(TextQUIT, TextQUITRect)
        fill_group.draw(sc)
        pygame.display.update()
        clock.tick(FPS)
        fill_group.update()


def control():
    running: bool = True
    FPS: int = 60

    TEXT_SET = ("", "", "", "", "", "", "", "", "", "", "")

    match LANGUAGE_SET:
        case "UA":
            TEXT_SET = (
                "Рух: ", "Стріляти: ", "Стріляти в ліво: ", "Стріляти в право: ", "Стріляти в низ: ",
                "Зробити скріншот: F6", "УВАГА! КОНТРОЛЕР У ГРІ ПОГАНО ПІДТРИМУЄТЬСЯ!.", "Кинути Бобму: ", "Закрити рекламу і повернутисся: ", "Пауза:", "OK:")
        case "EN":
            TEXT_SET = (
                "MOVE: ", "SHOOT: ", "SHOOT LEFT: ", "SHOOT RIGHT: ", "SHOOT DOWN: ",
                "TAKE SCREENSHOT: ", "Warning! BAD GAMEPAD SUPPORT", "DROP BOMB: ", "CLOSE AD OR BACK:", "PAUSE:", "OK:")

    MoveText = fontMenuText.render(TEXT_SET[0] + "WASD", scene_config.AA_TEXT, (255, 255, 255))
    ShootText = fontMenuText.render(TEXT_SET[1] + "K", scene_config.AA_TEXT, (255, 255, 255))
    ShootLeftText = fontMenuText.render(TEXT_SET[2] + "J", scene_config.AA_TEXT, (255, 255, 255))
    ShootRightText = fontMenuText.render(TEXT_SET[3] + "L", scene_config.AA_TEXT, (255, 255, 255))
    ShootDownText = fontMenuText.render(TEXT_SET[4] + "SPACE", scene_config.AA_TEXT, (255, 255, 255))
    ScreenshootText = fontMenuText.render(TEXT_SET[5], scene_config.AA_TEXT, (255, 255, 255))
    BombTXT = fontMenuText.render(TEXT_SET[7] + "I", scene_config.AA_TEXT, (255, 255, 255))
    CloseADTXT = fontMenuText.render(TEXT_SET[8] + "Q", scene_config.AA_TEXT, (255, 255, 255))

    QKeyImage = createImage("ASSETS/SPRITE/UI/KEY/Q.png")
    QKeyImage = pygame.transform.scale(QKeyImage, (31, 31))
    QKeyImageRect = QKeyImage.get_rect(center=(1100, 680))

    TextQUIT = FontKILL.render(":QUIT", scene_config.AA_TEXT, (255, 255, 255))
    TextQUITRect = TextQUIT.get_rect(center=(1150, 680))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                scene_config.switch_scene(None)
                running = False
            if e.type == pygame.KEYDOWN and e.key == pygame.K_q:
                ENTER_SELECT.play()
                scene_config.switch_scene(about)
                running = False

        sc.fill((0, 0, 0))
        sc.blit(MoveText, MoveText.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 150)))
        sc.blit(ShootText, ShootText.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 100)))
        sc.blit(ShootLeftText, ShootLeftText.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 50)))
        sc.blit(ShootRightText, ShootRightText.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
        sc.blit(ShootDownText, ShootDownText.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 50)))
        sc.blit(ScreenshootText, ScreenshootText.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 100)))
        sc.blit(BombTXT, BombTXT.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 150)))
        sc.blit(CloseADTXT, CloseADTXT.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 200)))
        sc.blit(QKeyImage, QKeyImageRect)
        sc.blit(TextQUIT, TextQUITRect)
        pygame.display.update()
        clock.tick(FPS)


def thanks():
    running: bool = True
    FPS: int = 60

    TEXT_SET = "", "", ""

    match LANGUAGE_SET:
        case "UA":
            TEXT_SET = ("ПОДЯКИ:", "ЛЮДЯМ:", "І ТОБІ ПУПСІК!")
        case "EN":
            TEXT_SET = ("SPECIAL THANKS:", "PEOPLE:", "AND YOU!")

    TextThanks = FinishFont.render(TEXT_SET[0], scene_config.AA_TEXT, (255, 255, 255))

    GameVarLogo = createImage("ASSETS/SPRITE/UI/StartImage/SPECIAL_THANKS/GameVar.png")
    GameVarLogo = pygame.transform.scale(GameVarLogo, (185, 185))

    font1 = pygame.font.Font("ASSETS/FONT/Alice-Regular.ttf", 38)
    font2 = pygame.font.Font("ASSETS/FONT/Alice-Regular.ttf", 24)
    GameVarText = font1.render("Ігровари", scene_config.AA_TEXT, (255, 255, 0))

    PeopleThanks = font2.render(TEXT_SET[1] + " @Arficord, @BodaMat, "
                                              "@Kem, @Tracker_UA, @LoveJoy", scene_config.AA_TEXT,
                                (255, 255, 255))

    AndYouText = font1.render(TEXT_SET[2], scene_config.AA_TEXT, (255, 255, 255))

    QKeyImage = createImage("ASSETS/SPRITE/UI/KEY/Q.png")
    QKeyImage = pygame.transform.scale(QKeyImage, (31, 31))
    QKeyImageRect = QKeyImage.get_rect(center=(1100, 680))

    TextQUIT = FontKILL.render(":QUIT", scene_config.AA_TEXT, (255, 255, 255))
    TextQUITRect = TextQUIT.get_rect(center=(1150, 680))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                scene_config.switch_scene(None)
                running = False
            if e.type == pygame.KEYDOWN and e.key == pygame.K_q:
                ENTER_SELECT.play()
                scene_config.switch_scene(about)
                running = False

        sc.fill((0, 0, 0))
        sc.blit(TextThanks, TextThanks.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 200)))
        sc.blit(GameVarLogo, GameVarLogo.get_rect(center=(WINDOWS_SIZE[0] // 2 - 175, WINDOWS_SIZE[1] // 2 - 105)))
        sc.blit(GameVarText, GameVarText.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 75)))
        sc.blit(PeopleThanks, PeopleThanks.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 55)))
        sc.blit(AndYouText, AndYouText.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 175)))
        sc.blit(QKeyImage, QKeyImageRect)
        sc.blit(TextQUIT, TextQUITRect)
        pygame.display.update()
        clock.tick(FPS)


def aboutGame():
    running: bool = True
    FPS: int = 60

    fontInfo = pygame.font.Font("ASSETS/FONT/recharge bd.ttf", 21)

    fontName = pygame.font.Font("ASSETS/FONT/FFFFORWA.TTF", 25)

    TextNameGame = fontName.render("Adriana::System: Circulatory", scene_config.AA_TEXT, (255, 255, 255))

    textVersion = FontStat.render("ver: 1.1.0(1.04.23)", scene_config.AA_TEXT, (255, 255, 255))

    secondGameName = FontStat.render("Shoot 'em up - Bullet Hell - Arena Shooter:  Game", scene_config.AA_TEXT,
                                     (255, 255, 255))

    AutorRule = fontInfo.render("Copyright © Bogdan Praporshikov  \"TitleChanQWERTY\"", scene_config.AA_TEXT,
                                (255, 255, 255))

    useSoftware = FontKILL.render("Using Software And Library: "
                                  "Pygame, LMMS, Aseprite, GIMP, Arch Linux, PyCharm Community Edition, Audacity",
                                  scene_config.AA_TEXT, (255, 255, 255))

    License = fontInfo.render("LICENSE: GNU General Public License v3.0", scene_config.AA_TEXT, (255, 255, 255))

    QKeyImage = createImage("ASSETS/SPRITE/UI/KEY/Q.png")
    QKeyImage = pygame.transform.scale(QKeyImage, (31, 31))
    QKeyImageRect = QKeyImage.get_rect(center=(1100, 680))

    TextQUIT = FontKILL.render(":QUIT", scene_config.AA_TEXT, (255, 255, 255))
    TextQUITRect = TextQUIT.get_rect(center=(1150, 680))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                scene_config.switch_scene(None)
                running = False
            if e.type == pygame.KEYDOWN and e.key == pygame.K_q:
                ENTER_SELECT.play()
                scene_config.switch_scene(about)
                running = False

        sc.fill((0, 0, 0))
        sc.blit(TextNameGame, TextNameGame.get_rect(center=(WINDOWS_SIZE[0] // 2, 135)))
        sc.blit(textVersion, textVersion.get_rect(center=(WINDOWS_SIZE[0] // 2, 200)))
        sc.blit(secondGameName, secondGameName.get_rect(center=(WINDOWS_SIZE[0] // 2, 295)))
        sc.blit(AutorRule, AutorRule.get_rect(center=(WINDOWS_SIZE[0] // 2, 415)))
        sc.blit(License, License.get_rect(center=(WINDOWS_SIZE[0] // 2, 475)))
        sc.blit(useSoftware, useSoftware.get_rect(center=(WINDOWS_SIZE[0] // 2, 550)))
        sc.blit(QKeyImage, QKeyImageRect)
        sc.blit(TextQUIT, TextQUITRect)
        pygame.display.update()
        clock.tick(FPS)
