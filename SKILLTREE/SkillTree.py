from random import randint

import pygame.transform

import group_config
import take_screenshot
from config_script import *
from SCENE import scene_config, scene_game, scene_menu, scene_novel
from LOADING import loading
from font_config import skillFontNumber, skillFont, FontKILL, ScoreFont
import collision

slot_group = pygame.sprite.Group()

TEXT_SET = ("", "", "", "", "")

match LANGUAGE_SET:
    case "UA":
        TEXT_SET = (TEXT_SKILL_UA[0], TEXT_SKILL_UA[1], TEXT_SKILL_UA[2], TEXT_SKILL_UA[3], TEXT_SKILL_UA[4])
    case "EN":
        TEXT_SET = (TEXT_SKILL_EN[0], TEXT_SKILL_EN[1], TEXT_SKILL_EN[2], TEXT_SKILL_EN[3], TEXT_SKILL_EN[4])


class TreeSlot(pygame.sprite.Sprite):
    def __init__(self, x, y, text, color=(255, 255, 255), slot=0):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage("ASSETS/SPRITE/UI/SkillTree/1.png")
        self.image = pygame.transform.scale(self.image, (87, 82))
        self.rect = self.image.get_rect(center=(x, y))

        self.idSlot: int = slot
        self.isBuy: bool = False

        self.text = skillFontNumber.render(text, scene_config.AA_TEXT, color)
        self.textRect = self.text.get_rect(center=(x + 1, y + 7))

        self.add(slot_group)


def createSlot(x: int, y: int, text, color=(255, 255, 255), slot: int = 0):
    return TreeSlot(x, y, text, color, slot)


createSlot(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2, "B", (255, 0, 0))
createSlot(WINDOWS_SIZE[0] // 2 + 85, WINDOWS_SIZE[1] // 2, "M", (255, 255, 255), 1)
createSlot(WINDOWS_SIZE[0] // 2 - 85, WINDOWS_SIZE[1] // 2, "D", (200, 15, 0), 2)
createSlot(WINDOWS_SIZE[0] // 2 - 170, WINDOWS_SIZE[1] // 2, "A", (255, 255, 0), 3)
createSlot(WINDOWS_SIZE[0] // 2 + 170, WINDOWS_SIZE[1] // 2, "E", (255, 255, 255), 4)


def skill_tree():
    running: bool = True
    FPS: int = 0

    countSkill = (8500, 9600, 6200, 3300, 10100)

    BackFill = createImage("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png")
    BackFill = pygame.transform.scale(BackFill, WINDOWS_SIZE)
    BackFillRect = BackFill.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2))
    BackFill.set_alpha(210)

    Selector = createImage("ASSETS/SPRITE/UI/SkillTree/2.png")
    Selector = pygame.transform.scale(Selector, (97, 97))
    SelectorRect = Selector.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 3))

    QKeyImage = createImage("ASSETS/SPRITE/UI/KEY/Q.png")
    QKeyImage = pygame.transform.scale(QKeyImage, (31, 31))
    QKeyImageRect = QKeyImage.get_rect(center=(1082, 680))

    TextQUIT = FontKILL.render(":QUIT", scene_config.AA_TEXT, (255, 255, 255))
    TextQUITRect = TextQUIT.get_rect(center=(1130, 680))

    TextNameSkill = skillFont.render(TEXT_SET[0], scene_config.AA_TEXT, (255, 255, 255))

    selectSlot: int = 0

    timeEButton: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                scene_config.switch_scene(None)
                running = False
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    take_screenshot.take()
                if e.key == pygame.K_LEFT:
                    match selectSlot:
                        case 0:
                            selectSlot = -1
                        case 1:
                            selectSlot = 0
                        case 2:
                            selectSlot = 1
                        case -1:
                            selectSlot = -2
                        case -2:
                            selectSlot = 2
                if e.key == pygame.K_RIGHT:
                    match selectSlot:
                        case 0:
                            selectSlot = 1
                        case -1:
                            selectSlot = 0
                        case -2:
                            selectSlot = -1
                        case 1:
                            selectSlot = 2
                        case 2:
                            selectSlot = -2
                if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                    timeEButton = 3
                    match selectSlot:
                        case 0:
                            if scene_config.Score >= countSkill[0]:
                                for s in slot_group:
                                    if s.idSlot == 0 and not s.isBuy:
                                        s.isBuy = True
                                        collision.isKillBullet = False
                                        scene_config.Score -= countSkill[0]
                                        s.image = createImage("ASSETS/SPRITE/UI/SkillTree/3.png")
                                        s.image = pygame.transform.scale(s.image, (87, 82))
                        case -1:
                            if scene_config.Score >= countSkill[2]:
                                for s in slot_group:
                                    if s.idSlot == 2 and not s.isBuy:
                                        scene_config.Score -= countSkill[2]
                                        s.isBuy = True
                                        scene_config.p.ExtraDamageBullet += randint(5, 10)
                                        s.image = createImage("ASSETS/SPRITE/UI/SkillTree/3.png")
                                        s.image = pygame.transform.scale(s.image, (87, 82))
                        case -2:
                            if scene_config.Score >= countSkill[3]:
                                for s in slot_group:
                                    if s.idSlot == 3 and not s.isBuy:
                                        scene_config.Score -= countSkill[3]
                                        s.isBuy = True
                                        scene_config.p.Bomb += 10
                                        s.image = createImage("ASSETS/SPRITE/UI/SkillTree/3.png")
                                        s.image = pygame.transform.scale(s.image, (87, 82))
                        case 1:
                            if scene_config.Score >= countSkill[1]:
                                for s in slot_group:
                                    if s.idSlot == 1 and not s.isBuy:
                                        scene_config.p.isAutoRegen = True
                                        s.isBuy = True
                                        scene_config.Score -= countSkill[1]
                                        s.image = createImage("ASSETS/SPRITE/UI/SkillTree/3.png")
                                        s.image = pygame.transform.scale(s.image, (87, 82))
                        case 2:
                            if scene_config.Score >= countSkill[4]:
                                for s in slot_group:
                                    if s.idSlot == 4 and not s.isBuy:
                                        scene_config.Score -= countSkill[4]
                                        s.isBuy = True
                                        scene_config.p.autoBullet = True
                                        s.image = createImage("ASSETS/SPRITE/UI/SkillTree/3.png")
                                        s.image = pygame.transform.scale(s.image, (87, 82))
                if e.key == pygame.K_q:
                    match scene_game.LEVEL:
                        case 2:
                            running = False
                            scene_config.switch_scene(scene_novel.scene0)
                        case 5:
                            running = False
                            scene_config.switch_scene(scene_novel.scene1)
                        case 8:
                            running = False
                            scene_config.switch_scene(scene_novel.scene2)
                        case 14:
                            running = False
                            scene_config.switch_scene(scene_novel.scene3)
                        case 18:
                            running = False
                            scene_config.switch_scene(scene_novel.scene5)
                        case _:
                            for i in range(15):
                                for b in group_config.back_effect_group:
                                    scene_game.createEffect = 0
                                    b.kill()
                                running = False
                                scene_config.switch_scene(loading.loading)

        match selectSlot:
            case 0:
                TextNameSkill = skillFont.render(TEXT_SET[1], scene_config.AA_TEXT, (255, 255, 255))
                SelectorRect = Selector.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 3))
            case 1:
                TextNameSkill = skillFont.render(TEXT_SET[2], scene_config.AA_TEXT, (255, 255, 255))
                SelectorRect = Selector.get_rect(center=(WINDOWS_SIZE[0] // 2 + 85, WINDOWS_SIZE[1] // 2 + 3))
            case 2:
                TextNameSkill = skillFont.render(TEXT_SET[4], scene_config.AA_TEXT, (255, 255, 255))
                SelectorRect = Selector.get_rect(center=(WINDOWS_SIZE[0] // 2 + 170, WINDOWS_SIZE[1] // 2 + 3))
            case -1:
                TextNameSkill = skillFont.render(TEXT_SET[0], scene_config.AA_TEXT, (255, 255, 255))
                SelectorRect = Selector.get_rect(center=(WINDOWS_SIZE[0] // 2 - 85, WINDOWS_SIZE[1] // 2 + 3))
            case -2:
                TextNameSkill = skillFont.render(TEXT_SET[3], scene_config.AA_TEXT, (255, 255, 255))
                SelectorRect = Selector.get_rect(center=(WINDOWS_SIZE[0] // 2 - 170, WINDOWS_SIZE[1] // 2 + 3))
        TextAboutRect = TextNameSkill.get_rect(center=(WINDOWS_SIZE[0]//2, 595))

        if timeEButton > 0:
            timeEButton -= 1
            scene_menu.EKeyImage = createImage("ASSETS/SPRITE/UI/KEY/E/2.png")
            scene_menu.EKeyImage = pygame.transform.scale(scene_menu.EKeyImage, (31, 31))
        else:
            scene_menu.EKeyImage = createImage("ASSETS/SPRITE/UI/KEY/E/1.png")
            scene_menu.EKeyImage = pygame.transform.scale(scene_menu.EKeyImage, (31, 31))

        scene_game.ScoreText = ScoreFont.render(scene_game.TEXT_SET[3] + str(scene_config.Score), scene_config.AA_TEXT,
                                                (255, 255, 0))
        sc.fill((0, 0, 0))
        sc.blit(BackFill, BackFillRect)
        for t in slot_group:
            sc.blit(t.text, t.textRect)
        slot_group.draw(sc)
        sc.blit(Selector, SelectorRect)
        sc.blit(scene_menu.EKeyImage, scene_menu.EKeyImageRect)
        sc.blit(scene_menu.TextOK, scene_menu.TextOKRect)
        sc.blit(QKeyImage, QKeyImageRect)
        sc.blit(TextQUIT, TextQUITRect)
        sc.blit(scene_game.ScoreText, scene_game.ScoreTextRect)
        sc.blit(TextNameSkill, TextAboutRect)
        pygame.display.update()
        clock.tick(FPS)
        slot_group.update()
