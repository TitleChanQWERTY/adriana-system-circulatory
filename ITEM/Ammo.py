from random import randint

import pygame
from pygame import BLEND_RGB_ADD

from collision import collisionItem
from glow import createCircle
from group_config import item_group
from config_script import createImage, sc
from SCENE import scene_config


class ammo(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.size = 31
        self.image = createImage("ASSETS/SPRITE/ITEM/Ammo/1.png")
        self.image = pygame.transform.scale(self.image, (self.size, self.size))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(item_group)

        self.idItem: int = 2

        self.radius = self.size
        self.radiusForce: int = 10

        self.moveInt = randint(0, 4)
        self.speedMove: int = 1

        self.alpha: int = 250

        self.Frame: int = 0

        self.timeDestroy: int = 500

        self.timeSetMove: int = 35

        self.angle: int = 0
        self.x = x
        self.y = y

    def update(self):
        collisionItem()
        if scene_config.isLight:
            if self.radius < self.size * 2:
                self.radius += self.radiusForce
            else:
                self.radius = self.size-25
            sc.blit(createCircle(self.radius, (34, 26, 0)),
                    (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                    special_flags=BLEND_RGB_ADD)

        self.move()

        if self.timeSetMove <= 0:
            self.timeSetMove = 35
            self.moveInt = randint(0, 4)
        else:
            self.timeSetMove -= 1

        if self.timeDestroy <= 0:
            match self.Frame:
                case 38:
                    self.kill()
                case 34:
                    self.image = pygame.image.load("ASSETS/SPRITE/ITEM/Ammo/DIE/2.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (29, 29))
                case 24:
                    self.image = pygame.image.load("ASSETS/SPRITE/ITEM/Ammo/DIE/1.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (29, 29))
            self.Frame += 1
        else:
            self.timeDestroy -= 1
        if self.timeDestroy <= 65:
            if self.alpha >= 250:
                self.radiusForce = 25
                self.alpha -= 220
            else:
                self.alpha = 250
            self.image.set_alpha(self.alpha)

    def move(self):
        if self.timeDestroy > 0:
            match self.moveInt:
                case 0:
                    if self.rect.y > 25:
                        self.y -= self.speedMove
                    else:
                        self.moveInt = 4
                    if self.rect.x < 1205:
                        self.x += self.speedMove
                    else:
                        self.moveInt = 1
                case 1:
                    if self.rect.y < 640:
                        self.y += self.speedMove
                    else:
                        self.moveInt = 4
                    if self.rect.x > 25:
                        self.x -= self.speedMove
                    else:
                        self.moveInt = 0
                case 2:
                    if self.rect.y > 25:
                        self.y -= self.speedMove
                    else:
                        self.moveInt = 3
                    if self.rect.x > 25:
                        self.x -= self.speedMove
                    else:
                        self.moveInt = 0
                case 3:
                    if self.rect.y < 640:
                        self.y += self.speedMove
                    else:
                        self.moveInt = 4
                    if self.rect.x < 1205:
                        self.x += self.speedMove
                    else:
                        self.moveInt = 0
                case 4:
                    if self.rect.y < 640:
                        self.y += self.speedMove
                    else:
                        self.moveInt = 2
            if scene_config.isRotateObject:
                self.image = createImage("ASSETS/SPRITE/ITEM/Ammo/1.png")
                self.image = pygame.transform.scale(self.image, (self.size, self.size))
                self.angle -= 2
                self.image = pygame.transform.rotate(self.image, self.angle)
            self.rect = self.image.get_rect(center=(self.x, self.y))
