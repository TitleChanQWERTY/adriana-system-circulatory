import pygame

from collision import collisionItem
from config_script import createImage
from group_config import item_group


class tp_item(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/ITEM/teleporter.png")
        self.image = pygame.transform.scale(self.image, (33, 33))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(item_group)

        self.x = x
        self.y = y

        self.angel: int = 0

        self.idItem: int = 3

    def update(self):
        collisionItem()
        if self.rect.y < 690:
            self.y += 2
        else:
            self.kill()
        self.image = createImage("ASSETS/SPRITE/ITEM/teleporter.png")
        self.angel += 2
        self.image = pygame.transform.rotate(self.image, self.angel)
        self.image = pygame.transform.scale(self.image, (33, 33))
        self.rect = self.image.get_rect(center=(self.x, self.y))
