from ITEM import exp, Ammo, Health, crystal_task, TextCreate, teleporter_item, bomb


def create_exp(x, y, e):
    return exp.exp_item(x, y, e)


def create_ammo(x, y):
    return Ammo.ammo(x, y)


def createX_CUSE(x, y):
    return Health.x_cuse(x, y)


def create_crystal_task():
    return crystal_task.crystal_task()


def create_teleporter(x, y):
    return teleporter_item.tp_item(x, y)


def create_bomb(x, y):
    return bomb.bomb(x, y)


# This i create Text

def createTXT(x, y, txt, color=(255, 255, 255), pos=0):
    return TextCreate.text(x, y, txt, color, pos)
