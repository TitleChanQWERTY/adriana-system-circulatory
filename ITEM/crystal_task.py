from random import randint

from pygame import BLEND_RGB_ADD

from config_script import sc
from glow import createCircle
from group_config import item_group
from collision import createParticle, collisionItem
from SCENE import scene_config

import pygame


class crystal_task(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load_extended("ASSETS/SPRITE/ITEM/Soul_Crystal/Create/1.png").convert_alpha()
        self.image = pygame.transform.scale(self.image, (40, 40))
        self.rect = self.image.get_rect(center=(randint(55, 990), randint(65, 600)))
        self.add(item_group)

        self.idItem: int = 6

        self.timeUp: int = 0
        self.timeDown: bool = False

        self.Force: int = 2

        self.size = 15 * 2
        self.color = [0, 45, 0]
        self.radius = self.size

        self.firstCreate: bool = True
        self.timeDestroy: int = 0
        self.Frame_Create: int = 0

    def update(self):
        collisionItem()
        match self.firstCreate:
            case True:
                self.animation_create()
            case False:
                if self.timeDestroy <= 425:
                    if scene_config.isLight:
                        if self.radius < self.size * 2 - 5:
                            self.radius += self.Force
                        else:
                            self.radius = 4
                        sc.blit(createCircle(self.radius, self.color),
                                (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                                special_flags=BLEND_RGB_ADD)
                else:
                    if scene_config.isParticle:
                        for i in range(4):
                            createParticle(self.rect.x, self.rect.y, i, "ASSETS/SPRITE/EFFECT/8.png", False, 25)
                    self.kill()
                self.timeDestroy += 1
                self.animation_life()
                if self.timeDestroy >= 300:
                    self.color[0] = 25
                    self.color[1] = 15
                    self.color[2] = 0
                    self.Force = 20

    def animation_create(self):
        match self.Frame_Create:
            case 10:
                self.image = pygame.image.load("ASSETS/SPRITE/ITEM/Soul_Crystal/Create/1.png").convert_alpha()
                self.image = pygame.transform.scale(self.image, (40, 40))
            case 15:
                self.image = pygame.image.load("ASSETS/SPRITE/ITEM/Soul_Crystal/Create/2.png").convert_alpha()
                self.image = pygame.transform.scale(self.image, (40, 40))
            case 25:
                self.firstCreate = False
                self.image = pygame.image.load("ASSETS/SPRITE/ITEM/Soul_Crystal/Create/3.png").convert_alpha()
                self.image = pygame.transform.scale(self.image, (40, 40))
        self.Frame_Create += 1

    def animation_life(self):
        if self.timeUp < 15 and self.timeDown:
            self.timeUp += 1
            self.image = pygame.image.load("ASSETS/SPRITE/ITEM/Soul_Crystal/1.png").convert_alpha()
            self.image = pygame.transform.scale(self.image, (40, 40))
            self.rect.y -= 2
        else:
            self.timeDown = False
            if self.timeUp > 0:
                self.image = pygame.image.load("ASSETS/SPRITE/ITEM/Soul_Crystal/2.png").convert_alpha()
                self.image = pygame.transform.scale(self.image, (40, 40))
                self.rect.y += 2
                self.timeUp -= 1
            else:
                self.timeDown = True
