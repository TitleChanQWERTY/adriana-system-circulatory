from random import randint

import pygame
from pygame import BLEND_RGB_ADD

from collision import collisionItem
from group_config import item_group
from config_script import createImage, sc
from SCENE import scene_config

from glow import createCircle


class exp_item(pygame.sprite.Sprite):
    def __init__(self, x, y, id=0):
        super().__init__()
        self.size: int = 31
        self.idItem: int = id
        self.filename = ("ASSETS/SPRITE/ITEM/EXP/1.bmp", "ASSETS/SPRITE/ITEM/EXP/2.bmp")
        self.image = createImage(self.filename[self.idItem])
        self.image = pygame.transform.scale(self.image, (self.size, self.size))
        self.rect = self.image.get_rect(center=(x, y))

        self.radius = self.size-5

        self.add(item_group)

        self.radiusForce: int = 9

        self.moveInt: int = 1
        self.speedMove: int = 1

        self.alpha: int = 250

        self.Frame: int = 0

        self.timeDestroy: int = 450

        self.timeSetMove: int = 35

        self.x: int = x
        self.y: int = y
        self.angle: int = 0
        self.angleSelect = randint(0, 1)

        if self.idItem == 1:
            self.size = 32

    def update(self):
        collisionItem()
        self.rect = self.image.get_rect(center=(self.x, self.y))

        if scene_config.isLight:
            if self.radius < self.size * 2 + 2:
                self.radius += self.radiusForce
            else:
                self.radius = self.size-25
            sc.blit(createCircle(self.radius, (16, 26, 42)),
                    (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                    special_flags=BLEND_RGB_ADD)

        self.move()

        if self.timeSetMove <= 0:
            self.timeSetMove = 45
            self.moveInt = randint(0, 3)
        else:
            self.timeSetMove -= 1

        if self.timeDestroy <= 0:
            match self.Frame:
                case 28:
                    self.kill()
                case 24:
                    self.image = pygame.image.load("ASSETS/SPRITE/ITEM/EXP/DIE/3.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (self.size, self.size))
                case 18:
                    self.image = pygame.image.load("ASSETS/SPRITE/ITEM/EXP/DIE/2.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (self.size, self.size))
            self.Frame += 1
        else:
            self.timeDestroy -= 1
        if self.timeDestroy <= 55:
            if self.alpha >= 300:
                self.radiusForce = 25
                self.alpha -= 300
            else:
                self.alpha = 300
            self.image.set_alpha(self.alpha)

    def move(self):
        if self.timeDestroy > 0:
            match self.moveInt:
                case 0:
                    if self.rect.x < 1090 and self.rect.y > 35:
                        self.x += self.speedMove
                        self.y -= self.speedMove
                    else:
                        self.moveInt = 1
                case 1:
                    if self.rect.x > 35 and self.rect.y < 600:
                        self.x -= self.speedMove
                        self.y += self.speedMove
                    else:
                        self.moveInt = 0
                case 2:
                    if self.rect.x > 35 and self.rect.y > 35:
                        self.x -= self.speedMove
                        self.y -= self.speedMove
                    else:
                        self.moveInt = 3
                case 3:
                    if self.rect.x < 1090 and self.rect.y < 600:
                        self.x += self.speedMove
                        self.y += self.speedMove
                    else:
                        self.moveInt = 2
            if scene_config.isRotateObject:
                self.image = pygame.image.load(self.filename[self.idItem]).convert_alpha()
                match self.angleSelect:
                    case 0:
                        self.angle += 2
                    case 1:
                        self.angle -= 2
                self.image = pygame.transform.scale(self.image, (self.size, self.size))
            self.image = pygame.transform.rotate(self.image, self.angle)
