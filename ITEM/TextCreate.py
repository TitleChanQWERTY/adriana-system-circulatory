import pygame
from group_config import TextEffect_group
from SCENE import scene_config


class text(pygame.sprite.Sprite):
    def __init__(self, x, y, txt, color, posMove):
        super().__init__()
        self.size: int = 18
        self.txt = txt
        self.color = color
        self.itemFont = pygame.font.Font("ASSETS/FONT/FFFFORWA.TTF", self.size)
        self.text = self.itemFont.render(self.txt, scene_config.AA_TEXT, self.color)
        self.image = self.text
        self.rect = self.text.get_rect(center=(x, y))

        self.x, self.y = x, y

        self.add(TextEffect_group)

        self.timeKill: int = 0

        self.alpha: int = 300

        self.Frame: int = 0

        self.pos = posMove

        match self.pos:
            case 1:
                self.size = 3

    def update(self):
        match self.timeKill:
            case 25:
                self.kill()
            case _:
                self.timeKill += 1
                self.alpha -= 7
                match self.pos:
                    case 0:
                        self.y -= 2
                        if self.size >= 9:
                            match self.Frame:
                                case 4:
                                    self.size -= 1
                                case _:
                                    self.Frame += 1
                        self.itemFont = pygame.font.Font("ASSETS/FONT/FFFFORWA.TTF", self.size)
                        self.text = self.itemFont.render(self.txt, scene_config.AA_TEXT, self.color)
                        self.image = self.text
                        self.rect = self.text.get_rect(center=(self.x, self.y))
                        self.text.set_alpha(self.alpha)
                    case 1:
                        self.y += 2
                        if self.size <= 9:
                            match self.Frame:
                                case 4:
                                    self.size += 1
                                case _:
                                    self.Frame += 1
                        self.itemFont = pygame.font.Font("ASSETS/FONT/FFFFORWA.TTF", self.size)
                        self.text = self.itemFont.render(self.txt, scene_config.AA_TEXT, self.color)
                        self.image = self.text
                        self.rect = self.text.get_rect(center=(self.x, self.y))
                        self.text.set_alpha(self.alpha)
