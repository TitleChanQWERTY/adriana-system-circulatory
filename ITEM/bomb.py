from random import randint

import pygame

from collision import collisionItem
from config_script import createImage
from group_config import item_group
from SCENE import scene_config


class bomb(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/ITEM/bomb.png")
        self.image = pygame.transform.scale(self.image, (19, 19))
        self.rect = self.image.get_rect(center=(x, y))
        self.idItem: int = 5
        self.add(item_group)

        self.moveSelect: int = randint(0, 3)

        self.timeLive: int = 0

        self.angle: int = 0

    def update(self):
        collisionItem()
        if scene_config.isRotateObject:
            self.image = createImage("ASSETS/SPRITE/ITEM/bomb.png")
            self.image = pygame.transform.scale(self.image, (19, 19))
            self.angle += 5
            self.image = pygame.transform.rotate(self.image, self.angle)
        match self.moveSelect:
            case 0:
                self.rect.x += 2
                self.rect.y += 2
            case 1:
                self.rect.x -= 2
                self.rect.y -= 2
            case 2:
                self.rect.x += 2
                self.rect.y -= 2
            case 3:
                self.rect.x += 2
                self.rect.y -= 2

        if self.timeLive > randint(500, 1000):
            self.kill()
        self.timeLive += 1
